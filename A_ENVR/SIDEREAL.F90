! ROUTINE: SIDEREAL
! %Z%GTOSS %M% H.10 code v01.01
!              H.10 code v01.01  (baseline for vH10 delivery)
!****************************************************************
!****************************************************************
!
      SUBROUTINE SIDEREAL (LROTO,  OMPDUM)
!
!****************************************************************
!****************************************************************
! PURPOSE OF SUBROUTINE
!       GENERALIZED BUFFER ROUTINE TO PROVIDE A TOSS INTERFACE TO
!       PLANET ROTATION RATES
!
! INPUT ARGUMENTS ARE:
!       LROTO   -  SIMULATION FIDELITY FLAG
!                     0 =   EARTH STANDARD ROTATION RATE
!                     1 =   EARTH STANDARD ROTATION RATE
!                     2 =   EARTH STANDARD ROTATION RATE
!
!                     3 = LUNAR    ROTATION RATE
!                     4 = SUN      ROTATION RATE
!                     5 = MERCURY  ROTATION RATE
!                     6 = VENUS    ROTATION RATE
!                     7 = MARS     ROTATION RATE
!                     8 = JUPITER  ROTATION RATE
!                     9 = SATURN   ROTATION RATE
!                    10 = URANUS   ROTATION RATE
!                    11 = NEPTUNE  ROTATION RATE
!                    12 = PLUTO    ROTATION RATE
!

      include "../A_HDR/COM_ALL.i"


! SELECT PLANET SPHERE FIDELITY LEVEL
!------------------------------------
      IF(LROTO .EQ. 0) GO TO   10
      IF(LROTO .EQ. 1) GO TO   10
      IF(LROTO .EQ. 2) GO TO   10
      IF(LROTO .EQ. 3) GO TO  300
      IF(LROTO .EQ. 4) GO TO  400
      IF(LROTO .EQ. 5) GO TO  500
      IF(LROTO .EQ. 6) GO TO  600
      IF(LROTO .EQ. 7) GO TO  700
      IF(LROTO .EQ. 8) GO TO  800
      IF(LROTO .EQ. 9) GO TO  900
      IF(LROTO .EQ.10) GO TO 1000
      IF(LROTO .EQ.11) GO TO 1100
      IF(LROTO .EQ.12) GO TO 1200

      IF(LROTO .GT.12) STOP 'UNKNOWN PLANET ROTATION MODEL'


!************************************************
! LROTO = 0, 1, 2 IS STANDARD EARTH ROTATION RATE
!************************************************
10    CONTINUE

! SPECIFY STANDARD VALUE OF EARTH ROTATION RATE
      OMPDUM = 0.72921151464592D-4

      GO TO 5000


!*************************************************************
! LROTO = 3 IS A MINIMAL LUNAR MODEL (1 ROT PER 31 EARTH DAYS)
!*************************************************************
300   CONTINUE

      OMPDUM = 0.02352295D-4

      GO TO 5000


!*************************************************************
! LROTO = 4 IS A MINIMAL SUN MODEL (1 ROT PER 25.4 EARTH DAYS)
!*************************************************************
400   CONTINUE

      OMPDUM = 0.02870911D-4

      GO TO 5000


!*****************************************************************
! LROTO = 5 IS A MINIMAL MERCURY MODEL (1 ROT PER 58.6 EARTH DAYS)
!*****************************************************************
500   CONTINUE

      OMPDUM = 0.01244388D-4

      GO TO 5000


!**************************************************************
! LROTO = 6 IS A MINIMAL VENUS MODEL (1 ROT PER 243 EARTH DAYS)
!**************************************************************
600   CONTINUE

      OMPDUM = 0.00300087D-4

      GO TO 5000


!**************************************************************
! LROTO = 7 IS A MINIMAL MARS MODEL (1 ROT PER 1.03 EARTH DAYS)
!**************************************************************
700   CONTINUE

      OMPDUM = 0.70797234D-4

      GO TO 5000


!*****************************************************************
! LROTO = 8 IS A MINIMAL JUPITER MODEL (1 ROT PER 0.41 EARTH DAYS)
!*****************************************************************
800   CONTINUE

      OMPDUM = 1.77856467D-4

      GO TO 5000


!****************************************************************
! LROTO = 9 IS A MINIMAL SATURN MODEL (1 ROT PER 0.45 EARTH DAYS)
!****************************************************************
900   CONTINUE

      OMPDUM = 1.62047003D-4

      GO TO 5000


!*****************************************************************
! LROTO = 10 IS A MINIMAL URANUS MODEL (1 ROT PER 0.72 EARTH DAYS)
!*****************************************************************
1000  CONTINUE

      OMPDUM = 1.01279377D-4

      GO TO 5000


!******************************************************************
! LROTO = 11 IS A MINIMAL NEPTUNE MODEL (1 ROT PER 0.67 EARTH DAYS)
!******************************************************************
1100  CONTINUE

      OMPDUM = 1.0883754D-4

      GO TO 5000


!****************************************************************
! LROTO = 12 IS A MINIMAL PLUTO MODEL (1 ROT PER 6.39 EARTH DAYS)
!****************************************************************
1200  CONTINUE

      OMPDUM = 0.11411761D-4


! STANDARD RETURN
5000  CONTINUE
      RETURN
      END
