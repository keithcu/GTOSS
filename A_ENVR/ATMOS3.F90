! ROUTINE: ATMOS3
! %Z%GTOSS %M% H.10 code v01.10
!              H.10 code v01.10 Forced call to ATMOS3V to fix late start
!                               Changed 1.0 in SIGN call to a var for compiler
!-----------------------------------------------------------------------------
!              H.7 code v01.00  (baseline for vH.7 delivery)
!****************************************************************
!****************************************************************
!     
      SUBROUTINE ATMOS3(DJ,RM50,H,  T,SS,RHO)
!     
!****************************************************************
!****************************************************************
!     PURPOSE
!         COMPUTE ATMOSPHERIC DENSITY USING MODIFIED 1970
!          JACCHIA MODEL
!
!     REFERENCES
!          1. NEW STATIC MODELS OF THE THERMOSPHERE AND EXO-
!             SPHERE WITH EMPIRICAL TEMPERATURE MODELS.
!             SMITHSONIAN ASTROPHYSICAL OBSERVATORY
!             SPECIAL REPORT 313.  MAY 6, 1970.
!          2. RTCC AND MOPS REQUIREMENTS FOR:  MODIFIED
!             1970 JACCHIA ATMOSPHERE MODEL.  MSC INTERNAL
!             NOTE NO. 70-FM-202.  JAN 26, 1971.
!
!     NOTE
!          NUMBERS IN PARENTHESES AFTER VARIABLES REFER TO
!          EQUATIONS FOUND IN REFERENCE 2.
!
!     INPUTS
!          DJ   - JULIAN DATE (DAYS)
!          RM50 - ARIES MEAN OF 50 INERTIAL POSITION (FT)
!          H    - ALTITUDE (KM)
!
!     OUTPUTS
!          T    - ATMOSPHERIC TEMPERATURE (K)
!          SS   - SPEED OF SOUND (FT/S)
!          RHO  - DENSITY (LBM/FT**3)


      include "../A_HDR/COM_ALL.i"


      include "../A_HDR/CATMO3.i"
      include "../A_HDR/CM50EF.i"




      DIMENSION RM50(3), BNHO(6), GAMI(6)

! PERFORM INITIALIZATION OF JACCHIA MODEL
!----------------------------------------
! NOTE: INITIALIZATION OF THE ATMO3 ROUTINE IS DONE EVERY PASS TO
!       ALLOW THE JACHIA TO FUNCTION PROPERLY UNDER THE GTOSS LATE 
!       START FEATURE. THIS HAD TO BE DONE SINCE VARIABLE VALUES WERE
!       BEING RETAINED VIA A LABELED COMMON, WHICH WAS NOT UNDER THE
!       PURVIEW OF THE GTOSS LATE START SYSTEM. THIS HAS NEGLIGIBLE
!       COMPUTE OVERHEAD, AND IS POSSIBLE SINCE NO VARIABLES INVOLVED
!       ARE BEING INITIALIZED FOR THE PURPOSE OF ACCUMULATION PROCESSES.

!      IF(ICSTAG .EQ. 1) CALL ATMO3V

       CALL ATMO3V


! CONVERT M50 POSITION FROM FEET TO KM
      FT2KM = 0.3048
      CALL VECSCL(FT2KM,RM50,RM50)
      RMAG = VECMAG(RM50)

! CALCULATE DECLINATION (DELTA), RIGHT ASCENSION (ALPHA) OF SUN
      DTR0 = DTRALL
      CALL SOLEPH(DJ,DTR0,ALPHAA,DELTAA,RSE)

! RIGHT ASCENSION OF VEHICLE
      ALPHV = ATAN2(RM50(2),RM50(1))
      IF(ALPHV.LT.0.0) ALPHV = ALPHV + 2.0*PIIALL

! SUN HOUR ANGLE
      HA  = ALPHV - ALPHAA

! RP = FUNCTION OF THE GEOMAGNETIC INDEX (4-11)
      RP = 0.134 + 0.09*GKPB

! TPO = TC(4-1) NIGHTIME MINIMUM OF THE GLOBAL EXOSPHERIC
! TEMPERATURE WHEN K-SUB-P IS ZERO
      TPO = 383.0 + 3.32*F107B
      TPO = TPO + 1.8*(F107 - F107B)

! TAU (4,8)
      TAU = HA + BETAA + P*SIN(HA + GAM)
      IF(TAU.GT. PIIALL) TAU = TAU - 2.0*PIIALL
      IF(TAU.LT.-PIIALL) TAU = TAU + 2.0*PIIALL

! VEHICLE DECLINATION
      DELV = ASIN(RM50(3)/RMAG)

! SUN DECLINATION
      DELT = DELTAA

! THETA (4-5)
      THET = 0.5*ABS(DELV + DELT)

! ETA  (4-4)
      ETA  = 0.5*ABS(DELV - DELT)

      SNTM = SIN(THET)**FM
      CSEM = COS( ETA)**FM
      CSTN = COS(TAU*0.5)**FN

! TO = T-SUB-L(4-10)
      A  = 1.0 + RP*SNTM
      TO = TPO*A*(1.0 + RP*CSTN*(CSEM - SNTM)/A)

! DELTA (4-15)
      DTY = DAZE/365.25
      DLT = DTY + .1145*((0.5*(1.0 +&
     &             SIN(2.0*PIIALL*DTY + 5.974262)))**2.16 - 0.5)

! DELTA T-SUB S (4-14)
      DTS = F107B*(0.349 + 0.206*SIN(2.0*PIIALL*DLT + 3.9531708))&
     &          *SIN(12.56637*DLT + 4.3214352)
      DTS = TO + 2.41 + DTS

! TINF = T-SUB INFINITY(4-13)
      TINF = DTS + 28.0*GKP + 0.03*EXP(GKP)

! SCALE = EPSILON (4-27)
      SCALE = 0.0
      IF(H.LE.350.0)THEN
      DUM1 = 1.0
      A = 0.02*(H - 90.0)*SIGN(DUM1,DELV)*SIN(DELV)**2
      SCALE = A*SIN(2.0*PIIALL*(DAZE + 100.0)/365.25)&
     &         *EXP(-0.045*(H - 90.0))
      ENDIF

! GEOPOTENTIAL ALTITUDE
      GA = (REMDUM + HO)*(H - HO)/(H + REMDUM)

! X(4-23)
      A = TINF - 800.0
      X = A/(750.0 + 0.0001722*A*A)

! V(4-22)
      A = (2000.0 - TINF)/2000.0
      A2 = A*A
      V = 0.0263 - (0.0081 + (0.0148 - (0.0649 + 0.036*A)*A2)*A)*A

! S(4-21)
      S = V*EXP(-X*X*0.5)

! SIG(4-20)
      SIG = S + 1.0/(REMDUM + HO)

! TX(4-18) - TEMPERATURE AT BASE ALTITUDE
      TX = 444.3807 + 0.02385*TINF-392.8292*EXP(-0.0021357*TINF)

! ATMOSPHERIC TEMPERATURE AT ALTITDUE
      IF (H.LE.125) THEN
         T = TX
      ELSE
         A = 2.0*(TINF-TX)/PIIALL
         GX = 1.9*(TX-TZO)/(HO-ZO)
         T = TX + A*ATAN(GX/A*(H-HO)*(1.0 + 4.5E-6*(H-HO)**FM))
      ENDIF

! BNHO(SEE TABLE II) N(B) - SUBI CONCENTRATION AT BASE ALTITUDE
!          FOR THE ITH CONSTITUENT(I = N2,O2,O,HE,AR)
      A = (486.6 - TX)/486.6
      A2 = A*A
      A3 = A*A2
      DO 15 I = 1,5
      BNHO(I) = 10.0**(CON1(I) + CON2(I)*A + CON3(I)*A2 + CON4(I)*A3)
   15 CONTINUE

      K = 5
      IF(H.GE.500.0)THEN
      K = 6

! TBAR-SUB-INFINITY(4-25) ,90 DAY MEAN EXOSPHERIC TEMPERATURE
      B = (383.0 + 3.320*F107B)*(1.0 + RP*0.5) + 56.0

      A = LOG10(B)
      A = 79.13 - (39.40 - 5.50*A)*A
      BNHO(K) = 10.0**A
      A2 = TINF*TINF
      A3 = A2*TINF
      A =  -10.48947 + 2.8442911E-02*TINF - 3.6209582E-05*A2
      A = A + 2.3411931E-08*A3 - 7.5775092E-12*A2*A2
      AA(K) = A + 9.753963E-16*A2*A3
      ENDIF

! GAMI(4-19)
      A = GB/(SIG*BK*TINF)
      DO 25 I = 1,K
      GAMI(I) = BM(I)*A
   25 CONTINUE

! RHO(4-26) - DENSITY
      A = EXP(-SIG*GA)
      RHO = 0.0
      B = (TINF - TX)/TINF
      B = (1.0 - B)/(1.0 - B*A)

      DO 30 I = 1,K
      A2 = 1.0 + AA(I) + GAMI(I)
      A3 = EXP(-SIG*GAMI(I)*GA)
      A = B**A2
      A2 = BNHO(I)*A3*A
      RHO = RHO + BM(I)*A2
   30 CONTINUE

! RHO(4-28) DENSITY ADJUSTED FOR SEASONAL-LATITUDINAL VARIATION
! OF THE LOWER THERMOSPHERE(Z .LE. 350 KM)
      IF(H.LE.350.0)THEN
      RHO = RHO*10.0**SCALE
      ENDIF

! CONVERT TO LBM/FT**3
      RHO = 0.001940320722 * RHO

! COMPUTE SPEED OF SOUND
      SS = SQRT(CPCV*UNGC*T/WTM)

! CONVERT TO FT/SEC
      SS = SS/0.3048

      RETURN
      END
