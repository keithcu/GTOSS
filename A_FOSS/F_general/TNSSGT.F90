! ROUTINE: TNSSGT
! %Z%GTOSS %M% G.1 code v01.01 (baseline for vH1 inertial bead model)
!****************************************
!****************************************
!
      SUBROUTINE TNSXXX(JSEG, SEGMAG)
!
!****************************************
!****************************************
! THIS ROUTINE CALLED TNSSGT OFFICIALLY SEEMS NEVER TO HAVE
! BEEN USED BY ANYONE, BUT IS MAINTAINED HERE IN CASE IT MIGHT
! BE NEEDED. THE FALSE NAME, TNSXXX IS USED TO FLUSH OUT ANY
! USERS WHO MIGHT BE HIDING SOMEWHERE

! THIS ROUTINE RETURNS THE DISTANCE BETWEEN TWO
! BEADS DEFINING A SEGMENT

!------------------------------------------
! THIS ROUTINE ASSUMES BEAD STATE IS IN THE
! TETHER FRAME AND IS USED ONLY DURING SKIP
! ROPE INITIALIZATION
!------------------------------------------

! INPUT ARG: JSEG, THE SEGMENT NUMBER BETWEEN
!                  TWO BEADS (WITH JSEG BEING
!                  .GE. 1 AND .LE. NBEAD+1)

! THE ROUTINE ALSO USES THE UNIVERSAL BEAD STATE
! VECTOR IN COMMON AS INPUT,

! THE OUTPUT RETURNS AS THE FOLLOWING VALUE

!     SEGMAG = MAGNITUDE(VEC BETWEEN SEGMENTS BEADS)


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND 
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.i"





      DIMENSION SUTDUM(3)

! CALC POINTERS TO BEAD STATE AT X-END OF SEGMENT BETWEEN 2 BEADS
      JB1P = 3*(JSEG-2) + 1
      JB2P = JB1P+1
      JB3P = JB1P+2

! POINTERS TO BEAD STATE AT Y-END OF SEGMENT BETWEEN 2 BEADS
      JB1 = 3*(JSEG-1) + 1
      JB2 = JB1+1
      JB3 = JB1+2

!-------------------------------------------------------------
! FORM SEGMENT VECTOR BETWEEN ADJACENT BEADS (IN TETHER FRAME)
!-------------------------------------------------------------
! FIND OUT WHETHER THIS SEGMENT IS AN END SEGMENT
      IF(JSEG .EQ.       1) GO TO 31
      IF(JSEG .EQ. NBEAD+1) GO TO 33

! FOR INTERIOR SEGMENTS
         SUTDUM(1) = BUT(JB1) - BUT(JB1P) + ELSGX
         SUTDUM(2) = BUT(JB2) - BUT(JB2P)
         SUTDUM(3) = BUT(JB3) - BUT(JB3P)
         GO TO 30

! FOR X-END SEGMENT
31       SUTDUM(1) = BUT(JB1) + ELSGX
         SUTDUM(2) = BUT(JB2)
         SUTDUM(3) = BUT(JB3)
         GO TO 30

! FOR Y-END SEGMENT
33       SUTDUM(1) = - BUT(JB1P) + ELSGX
         SUTDUM(2) = - BUT(JB2P)
         SUTDUM(3) = - BUT(JB3P)

30    CONTINUE
!-----------------------------
! END OF SEGMENT POS VEC LOGIC
!-----------------------------

! CALC DIST BETWEEN BEADS ADJACENT TO THIS SEGMENT
!-------------------------------------------------
      SEGMAG = VECMAG(SUTDUM)

      RETURN
      END
