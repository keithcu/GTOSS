! ROUTINE: TNSELC
! %Z%GTOSS %M% H.9 code v03.01
!              H.9 code v03.01 No change in calcs...just added gimmick
!                              to fix an arg encroachment caused by
!                              invoking both the Jacchia Atmos and World
!                              Data Bank mag. model at Fidelity .GE. 5
!----------------------------------------------------------------------
!              H.7 code v03.00 Referenced elec loads to seg current
!------------------------------------------------------------------
!              H.5 code v02.02 Changed EARTH to PLANET in comments
!-----------------------------------------------------------------
!              H.1 code v02.01 (baseline for vH1 inertial bead model)
!**************************************
!**************************************
!
      SUBROUTINE TNSELC(JSEG)
!
!**************************************
!**************************************
! THIS ROUTINE EVALUATES THE ELECTRODYNAMIC FORCE
! DENSITY ON A SEGMENT BETWEEN 2 BEADS

! THIS ROUTINES INPUT PARAMETRS ARE TAKEN THRU COMMON
! AND ARE ASSUMED TO BE EVALUATED PRIOR TO EXECUTION


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND 
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.i"





      DIMENSION DUMV1(3), DUMV2(3), DUMV3(3)

! SET IN ELECTRICAL CONVERSION FACTOR
      GTWFAC = 6.854273E-6

! FIND PLANET MAGNETIC FIELD (IN GAUSS) AND ITS INERTIAL VELOCITY
!---------------------------------------------------------------
! FIND PLANET FRAME COMPONENTS OF POSITION VECTOR
            CALL EITEF (LJULOP,TISTIM,RURI,   RURE)

! FIND PLANET MAGNETIC FIELD DATA IN PLANET FRAME (IF HOST COMMANDS)
      IF(LEVMAG .EQ. 1) THEN
            CALL GAUSS (LMAGOP,TISTIM,RURE,   BFLDE,BVELE)

! SAVE VALUE FOR NEXT PASS (IN CASE WIND ROUTINE IS NOT EVALUATED)
            BFLSEG(JSEG,1) = BFLDE(1)
            BFLSEG(JSEG,2) = BFLDE(2)
            BFLSEG(JSEG,3) = BFLDE(3)

            BVLSEG(JSEG,1) = BVELE(1)
            BVLSEG(JSEG,2) = BVELE(2)
            BVLSEG(JSEG,3) = BVELE(3)
      ELSE

! RESTORE PREV TOTAL ATMOS WIND (IN CASE WINDS NOT EVALUATED THIS PASS)
            BFLDE(1) = BFLSEG(JSEG,1)
            BFLDE(2) = BFLSEG(JSEG,2)
            BFLDE(3) = BFLSEG(JSEG,3)

            BVELE(1) = BVLSEG(JSEG,1)
            BVELE(2) = BVLSEG(JSEG,2)
            BVELE(3) = BVLSEG(JSEG,3)
      END IF

! TRANSFORM PLANET FRAME COMPONENTS OF FIELD DATA TO INERTIAL FRAME
      CALL EFTEI (LJULOP,TISTIM,BFLDE,   BFLDI)
      CALL EFTEI (LJULOP,TISTIM,BVELE,   BVELI)


! FIND VEL OF THIS TETHER POINT RELATIVE TO PLANET MAG FIELD
! (FOR USE TO CALCULATE EMF IN TETHER - NOT USED FOR FORCE)
      CALL VECDIF (RDURI,BVELI,  BVRELI)


! CALCULATE ELECTROMAGNETIC FORCE DENSITY
!----------------------------------------
! EVALUATE ELECTRO FORCE DENSITY DIRECTION (INER COMPONENTS)
!*********************************************************************
! NOTE: THESE REDUNDANT VECTOR MOVES ARE TO FIX A CONFLICT ON THE MAC
!       OF UNKNOWN ORIGIN, EXCEPT THAT IT OCCURS BETWEEN THE JACCHIA
!       ATMOSPHERE AND WORLD DATA BANK MAGNETIC MODEL (FIDELITY .GE. 5)
!*********************************************************************
      CALL VECMOV(TUI, DUMV1)
      CALL VECMOV(BFLDI, DUMV2)
      CALL CROSS (DUMV1,DUMV2,      DUMV3)
      CALL VECMOV(DUMV3, FUEI)

! SCALE FORCE TO SEGMENT SPECIFIC AMPERE LEVEL
      DUMCUR = CURRT + CURRTS(JSEG)
      CALL VECSCL (DUMCUR,FUEI,    FUEI)

! MULTIPLY BY GAUSS-AMP(IE. N/M) TO LB/FT CONVERS FACTOR
      CALL VECSCL (GTWFAC,FUEI, FUEI)


! SUM EMF CONTRIBUTION FOR THIS SEGMENT (VOLTS)
!----------------------------------------------
      EMFTOT = EMFTOT + 9.29506E-6*ELBSG(JSEG)*TDOT(BVRELI,BFLDI,TUI)

      RETURN
      END
