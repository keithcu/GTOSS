! ROUTINE: TNSSEG
! %Z%GTOSS %M% H.10 code v02.02
!              H.10 code v02.02 Protected possible singular unit vec calc.
!                               Cleaned up over-indexed variables.
!                               Added code to detect max prop seg-rates.
!------------------------------------------------------------------------
!              H.1 code v02.01 (baseline for vH1 inertial bead model)
!******************************
!******************************
!
      SUBROUTINE TNSSEG
!
!******************************
!******************************
! FOR THE CURRENTLY LOADED FINITE SOLUTION WORKING AREA,
!
! THIS ROUTINE FORMS A POS VECTOR (AND ITS DERIVATIVE)
! BETWEEN TWO ADJACENT BEADS, THUS DEFINING THE SEGMENT
! BETWEEN THE BEADS. IN ADDITION VARIOUS OTHER SEGMENT
! KINEMATIC STATE VARIABLES ARE EVALUATED
!
! THE ROUTINE ALSO USES THE UNIVERSAL BEAD STATE
! VECTOR IN COMMON AS INPUT,
!
! THE OUTPUT RETURNS AS THE FOLLOWING VALUES IN COMMON:

!     ELBSG (JSEG)   = DISTANCE BETWEEN BEAD POINTS
!     ELBSGD(JSEG)   = TIME DERIVATIVE OF ELBSG
!
!     BBTUI (JSEG,J) = UNIT VECTOR ALONG JTH SEGMENT
!     BBTUDI(JSEG,J) = TIME DERIV OF UNIT VECTOR ALONG JTH SEGMENT
!     DSDU  (JSEG)   = DERIV OF ARC LENGTH WR/T MATERIAL VARIABLE MU
!     DSDUD (JSEG)   = TIME DERIV OF DSDU (ABOVE)



! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND 
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_TOSS.i"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.i"






! ARRAYS ASSOCIATED WITH VECTOR OPTIMIZATION OF CODE
      DIMENSION  DUMTU  (3)
      DIMENSION  DUM1   (3)
      DIMENSION  DUM1P  (3)


! SET ACCUMULATOR FOR TOTAL SPATIAL ARC LENGTH
      ARCTOT = 0.0

!************************************
!************************************
! DO BIG LOOP TO PROCESS ALL SEGMENTS
!************************************
!************************************
      DO 1000 JSEG = 1,NBEAD+1

! CALC POINTERS TO BEAD STATE AT X-END OF SEGMENT BETWEEN 2 BEADS
        JB1P = 3*(JSEG-2) + 1
        JB2P = JB1P+1
        JB3P = JB1P+2

! POINTERS TO BEAD STATE AT Y-END OF SEGMENT BETWEEN 2 BEADS
        JB1 = 3*(JSEG-1) + 1
        JB2 = JB1+1
        JB3 = JB1+2

!***********************************************************
! FORM SEGMENT VECTOR BETWEEN ADJACENT BEADS (IN INER FRAME)
!***********************************************************
! FIND OUT WHETHER THIS SEGMENT IS AN END SEGMENT
        IF(JSEG .EQ.       1) GO TO 31
        IF(JSEG .EQ. NBEAD+1) GO TO 33

! FOR INTERIOR SEGMENTS
           SUI(1) = BUI(JB1) - BUI(JB1P) + ELSGX*UITI(1)
           SUI(2) = BUI(JB2) - BUI(JB2P) + ELSGX*UITI(2)
           SUI(3) = BUI(JB3) - BUI(JB3P) + ELSGX*UITI(3)
           GO TO 30

! FOR X-END SEGMENT
31         SUI(1) = BUI(JB1) + ELSGX*UITI(1)
           SUI(2) = BUI(JB2) + ELSGX*UITI(2)
           SUI(3) = BUI(JB3) + ELSGX*UITI(3)
           GO TO 30

! FOR Y-END SEGMENT
33         SUI(1) = - BUI(JB1P) + ELSGX*UITI(1)
           SUI(2) = - BUI(JB2P) + ELSGX*UITI(2)
           SUI(3) = - BUI(JB3P) + ELSGX*UITI(3)

30      CONTINUE
! END OF SEGMENT POS VEC LOGIC


! CALC DIST BETWEEN BEADS ADJACENT TO THIS SEGMENT
!-------------------------------------------------
         ELBSG(JSEG) = VECMAG(SUI)
         DUMEL = ELBSG(JSEG)

! TALLY TOTAL SPATIAL ARC LENGTH
         ARCTOT = ARCTOT + DUMEL

!---------------------------------------------
! POPULATE THE JSEG ELEMENT OF THE BBTUI ARRAY
!---------------------------------------------
! FORM UNIT VECTOR FROM X-END TO Y-END OF BEAD SEGMENT
         CALL VECNRM(SUI,  TUI)
         BBTUI(JSEG,1) = TUI(1)
         BBTUI(JSEG,2) = TUI(2)
         BBTUI(JSEG,3) = TUI(3)

! FINALLY, ARC LENGTH WR/T UNDEFORMED TETHER
!--------------------------------------------
! (NOTE, NEGATIVE SIGN REFLECTS MU DEFINITION FROM FAR END)
         DSDU(JSEG)  = - DUMEL/ELB

1000  CONTINUE
!*****************************
! END OF BIG DISPLACEMENT LOOP
!*****************************



!*************************************************
!*************************************************
! DO BIG LOOP TO PROCESS VELOCITY FOR ALL SEGMENTS
!*************************************************
!*************************************************
      DO 2000 JSEG = 1,NBEAD+1

! CALC POINTERS TO BEAD STATE AT X-END OF SEGMENT BETWEEN 2 BEADS
         JB1P = 3*(JSEG-2) + 1
         JB2P = JB1P+1
         JB3P = JB1P+2

! POINTERS TO BEAD STATE AT Y-END OF SEGMENT BETWEEN 2 BEADS
         JB1 = 3*(JSEG-1) + 1
         JB2 = JB1+1
         JB3 = JB1+2

!**************************************************************
! FORM NON-CONVECTIVE DERIVATIVE OF SEGMENT VECTOR (INER FRAME)
!**************************************************************
! FIND OUT WHETHER THIS SEGMENT IS AN END SEGMENT
      IF(JSEG .EQ.       1) GO TO 41
      IF(JSEG .EQ. NBEAD+1) GO TO 43

! FOR INTERIOR SEGMENTS (GET COMPONENT DERIVS)
         SUDI(1) = BUDI(JB1) -BUDI(JB1P) +ELSGXD*UITI(1) +ELSGX*UITDI(1)
         SUDI(2) = BUDI(JB2) -BUDI(JB2P) +ELSGXD*UITI(2) +ELSGX*UITDI(2)
         SUDI(3) = BUDI(JB3) -BUDI(JB3P) +ELSGXD*UITI(3) +ELSGX*UITDI(3)
         GO TO 40

! FOR X-END SEGMENT (GET COMPONENT DERIVS)
41       SUDI(1) = BUDI(JB1) + ELSGXD*UITI(1) + ELSGX*UITDI(1)
         SUDI(2) = BUDI(JB2) + ELSGXD*UITI(2) + ELSGX*UITDI(2)
         SUDI(3) = BUDI(JB3) + ELSGXD*UITI(3) + ELSGX*UITDI(3)
         GO TO 40

! FOR Y-END SEGMENT (GET COMPONENT DERIVS)
43       SUDI(1) = - BUDI(JB1P) + ELSGXD*UITI(1) + ELSGX*UITDI(1)
         SUDI(2) = - BUDI(JB2P) + ELSGXD*UITI(2) + ELSGX*UITDI(2)
         SUDI(3) = - BUDI(JB3P) + ELSGXD*UITI(3) + ELSGX*UITDI(3)

40    CONTINUE
! END OF SEGMENT VELOCITY LOGIC

!--------------------------------------------------------------------
! CALCULATE CONTRIBUTION DUE TO CONVECTIVE DERIVATIVES IF APPROPRIATE
!--------------------------------------------------------------------
         IF(ELD .EQ. 0.0) GO TO 250

! CALC SOME COMMON TERMS
             DUMLOL = ELMG/EL

! FORM THE NORMALIZED MATERIAL VARIABLES
             DUMCJ  = REAL(NBEAD-JSEG+1)*OBNSEG
             DUMCJP = REAL(NBEAD-JSEG+2)*OBNSEG


! CALC AVER UNIT TANGENT TO SPACE CURVE AND DERIV OF MU (JSEG)
!-------------------------------------------------------------
             IF(JSEG .LT. NBEAD+1) THEN

                DUMTU(1) = BBTUI(JSEG,1) + BBTUI(JSEG+1,1)
                DUMTU(2) = BBTUI(JSEG,2) + BBTUI(JSEG+1,2)
                DUMTU(3) = BBTUI(JSEG,3) + BBTUI(JSEG+1,3)

! CALC VECTOR MAGNITUDE AND PROTECT POSSIBLE SINGULAR CALC
                DUMG0 = VECMAG(DUMTU)

                IF(DUMG0 .EQ. 0.0) THEN
                    DUMTU(1) = 0.0
                    DUMTU(2) = 0.0
                    DUMTU(3) = 0.0
                ELSE
                    DUMTU(1) = DUMTU(1)/DUMG0
                    DUMTU(2) = DUMTU(2)/DUMG0
                    DUMTU(3) = DUMTU(3)/DUMG0
                END IF


! CALC AVER PARTIAL OF DEFORMED ARC LENGTH WR/T UNDEFORMED MATERIAL
                DUMDS = 0.5*( DSDU(JSEG) + DSDU(JSEG+1) )

! CALC COEFFICENT
                DUM1(1) = DUMDS*DUMTU(1)
                DUM1(2) = DUMDS*DUMTU(2)
                DUM1(3) = DUMDS*DUMTU(3)

             END IF

! CALC AVER UNIT TANGENT TO SPACE CURVE AND DERIV OF MU (PREVIOUS JSEG)
!----------------------------------------------------------------------
             IF(JSEG .GT. 1) THEN

                DUMTU(1) = BBTUI(JSEG-1,1) + BBTUI(JSEG-1+1,1)
                DUMTU(2) = BBTUI(JSEG-1,2) + BBTUI(JSEG-1+1,2)
                DUMTU(3) = BBTUI(JSEG-1,3) + BBTUI(JSEG-1+1,3)

! CALC VECTOR MAGNITUDE AND PROTECT POSSIBLE SINGULAR CALC
                DUMG0 = VECMAG(DUMTU)

                IF(DUMG0 .EQ. 0.0) THEN
                     DUMTU(1) = 0.0
                     DUMTU(2) = 0.0
                     DUMTU(3) = 0.0
                ELSE
                     DUMTU(1) = DUMTU(1)/DUMG0
                     DUMTU(2) = DUMTU(2)/DUMG0
                     DUMTU(3) = DUMTU(3)/DUMG0
                END IF

! CALC AVER PARTIAL OF DEFORMED ARC LENGTH WR/T UNDEFORMED MATERIAL
                DUMDS = 0.5*( DSDU(JSEG-1) + DSDU(JSEG-1+1)  )

! CALC COEFFICENT
                DUM1P(1) = DUMDS*DUMTU(1)
                DUM1P(2) = DUMDS*DUMTU(2)
                DUM1P(3) = DUMDS*DUMTU(3)

             END IF

! FIND OUT WHETHER THIS SEGMENT IS AN END SEGMENT
!------------------------------------------------
          IF(JSEG .EQ.       1) GO TO 61
          IF(JSEG .EQ. NBEAD+1) GO TO 63

! FOR INTERIOR SEGMENTS
          SUDI(1) = SUDI(1)   &
     &              + ELD*DUMCJP*(DUM1P(1) + DUMLOL*UITI(1))   &
     &              - ELD*DUMCJ *(DUM1 (1) + DUMLOL*UITI(1))

          SUDI(2) = SUDI(2)   &
     &              + ELD*DUMCJP*(DUM1P(2) + DUMLOL*UITI(2))   &
     &              - ELD*DUMCJ *(DUM1 (2) + DUMLOL*UITI(2))

          SUDI(3) = SUDI(3)   &
     &              + ELD*DUMCJP*(DUM1P(3) + DUMLOL*UITI(3))   &
     &              - ELD*DUMCJ *(DUM1 (3) + DUMLOL*UITI(3))
          GO TO 60

! FOR CLOSE-END SEGMENT
61        SUDI(1) = SUDI(1)   &
     &              - ELD*DUMCJ *(DUM1 (1) + DUMLOL*UITI(1))
          SUDI(2) = SUDI(2)   &
     &              - ELD*DUMCJ *(DUM1 (2) + DUMLOL*UITI(2))
          SUDI(3) = SUDI(3)   &
     &              - ELD*DUMCJ *(DUM1 (3) + DUMLOL*UITI(3))
          GO TO 60

! FOR FAR-END SEGMENT
63        SUDI(1) = SUDI(1)   &
     &              + ELD*DUMCJP*(DUM1P(1) + DUMLOL*UITI(1))
          SUDI(2) = SUDI(2)   &
     &              + ELD*DUMCJP*(DUM1P(2) + DUMLOL*UITI(2))
          SUDI(3) = SUDI(3)   &
     &              + ELD*DUMCJP*(DUM1P(3) + DUMLOL*UITI(3))

60    CONTINUE

! END OF SEGMENT LDOT CONTRIBUTION LOGIC
250   CONTINUE



! FETCH SOME TERMS NEEDED FOR FINAL CALC
!---------------------------------------
      DUMEL  = ELBSG(JSEG)

      TUI(1) = BBTUI(JSEG,1)
      TUI(2) = BBTUI(JSEG,2)
      TUI(3) = BBTUI(JSEG,3)

      CALL VECSCL(DUMEL,TUI,   SUI)


! CALC DERIV OF DIST BETWEEN BEADS ADJACENT TO SEGMENT
!-----------------------------------------------------
      ELBSGD(JSEG) = DOT(TUI,SUDI)
      DUMELD = ELBSGD(JSEG)


!--------------------------------------------------------
! POPULATE THE SEGMENT DATA ARRAY'S FOR THIS JSEG ELEMENT
! (FOR AVERAGING AND NUMERICAL DIFF IN DEPLOY FLOW CALCS)
!--------------------------------------------------------
      CALL VECSCL(       1.0/DUMEL, SUDI,   TOSVX1)
      CALL VECSCL(-DUMELD/DUMEL**2, SUI,    TOSVX2)
      CALL VECSUM (TOSVX1,  TOSVX2,         TOSVX3)

      BBTUDI(JSEG,1) = TOSVX3(1)
      BBTUDI(JSEG,2) = TOSVX3(2)
      BBTUDI(JSEG,3) = TOSVX3(3)

! FINALLY, ARC LENGTH DERIV WR/T UNDEFORMED TETHER, AND ITS TIME DERIV
!---------------------------------------------------------------------
! (NOTE, NEGATIVE SIGN REFLECTS MU DEFINITION FROM FAR END)
      DSDUD(JSEG) = - ( DUMELD - DUMEL*ELD/EL )/ELB

2000  CONTINUE
!***********************************
! END OF BIG SEGMENT DERIVATIVE LOOP
!***********************************

      RETURN
      END
