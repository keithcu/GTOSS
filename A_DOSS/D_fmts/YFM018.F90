! ROUTINE: YFM018
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM018 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! THIS FORMAT DISPLAYS BOOM-TIP FRAME ATTITUDE STATES


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_DOSS.i"
      include "../../A_HDR/COM_BOOM.i"




!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! WRITE HEADING TO PRINTOUT PAGE
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE DATA FOR PRINTOUT PAGE
      IF(JFUNC .EQ. 2) GO TO 2000

! PUT OUT A LINE OF DATA TO PRINTOUT PAGE
      IF(JFUNC .EQ. 3) GO TO 3000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM018-0'


!******************************************
!******************************************
!    WRITE PAGE HEAD TO PRINTOUT FILE
!******************************************
!******************************************
1000  CONTINUE

      CALL XFEED(IUOUT)

      WRITE(IUOUT,9) NAPBOM
    9 FORMAT(3X,   &
     &'DISPLAY FMT 18:   <FLEX BOOM> TIP FRAME ATTITUDE STATE',   &
     &  9X,'FOR ATT PT #',I2,'  (ON TOSS OBJECT #1)' /)


! ACTIVATE FORMATS OF UNITS CHOICE
      IF(NUNIT .EQ. 1) ASSIGN 51 TO JF1DUM
      IF(NUNIT .EQ. 1) ASSIGN 52 TO JF2DUM

      IF(NUNIT .EQ. 2) ASSIGN 53 TO JF1DUM
      IF(NUNIT .EQ. 2) ASSIGN 54 TO JF2DUM

      WRITE(IUOUT,JF1DUM)
      WRITE(IUOUT,JF2DUM)

! ENGLISH UNITS FORMAT
!---------------------
   51 FORMAT(3X,' FX-TIME     THTFP     PSIFP     PHIFP     OMP(1)',   &
     &          '     OMP(2)     OMP(3)     OMPD(1)     OMPD(2)',   &
     &          '     OMPD(3)' )

   52 FORMAT(3X,'   (SEC)     (DEG)     (DEG)     (DEG)    (DEG/S)',   &
     &          '    (DEG/S)    (DEG/S)    (DEG/SS)    (DEG/SS)',   &
     &          '    (DEG/SS)' /)


! METRIC  UNITS FORMAT
!---------------------
   53 FORMAT(3X,' FX-TIME     THTFP     PSIFP     PHIFP     OMP(1)',   &
     &          '     OMP(2)     OMP(3)     OMPD(1)     OMPD(2)',   &
     &          '     OMPD(3)' )

   54 FORMAT(3X,'   (SEC)     (DEG)     (DEG)     (DEG)    (DEG/S)',   &
     &          '    (DEG/S)    (DEG/S)    (DEG/SS)    (DEG/SS)',   &
     &          '    (DEG/SS)' /)

      RETURN



!******************************************
!******************************************
!    CALCULATE DATA FOR PRINTOUT PAGE
!******************************************
!******************************************
2000  CONTINUE

! IF ATTACH POINT NUMBER HAS NOT BEEN DEFINED, GET OUT
      IF(NAPBOM .EQ. 0) GO TO 100

! DISPLAY TIME
      OUTP( 1) = TIMBOM

! BOOM-TO-TIP FRAME EULER ANGLES
      CALL MATEUL (1,GPF,  PHIDUM,THTDUM,PSIDUM)

      OUTP( 2) = XRDXDG * THTDUM
      OUTP( 3) = XRDXDG * PSIDUM
      OUTP( 4) = XRDXDG * PHIDUM

! SET IN ATTACH PT ANGULAR VELOCITY DATA
      OUTP( 5) = XRDXDG * OMP(1)
      OUTP( 6) = XRDXDG * OMP(2)
      OUTP( 7) = XRDXDG * OMP(3)

      OUTP( 8) = XRDXDG * OMPD(1)
      OUTP( 9) = XRDXDG * OMPD(2)
      OUTP(10) = XRDXDG * OMPD(3)


! STATE # OF OUTPUT ITEMS IN ABOVE ARRAY FOR USE IN XPLASO
      OUTP(25) = 10.0

! STANDARD RETURN
100   CONTINUE

      RETURN


!******************************************
!******************************************
!   WRITE LINE OF DATA TO PRINTOUT PAGE
!******************************************
!******************************************
3000  CONTINUE

      IF(NUNIT .EQ. 1) ASSIGN 55 TO JF1DUM
      IF(NUNIT .EQ. 2) ASSIGN 56 TO JF1DUM

      WRITE(IUOUT,JF1DUM) (OUTP(J),J=1,LASITM)

! ENGLISH UNITS FORMAT
!---------------------
55    FORMAT(F10.2,  3(F10.4),  3(F11.4),  3(F12.4))

! METRIC  UNITS FORMAT
!---------------------
56    FORMAT(F10.2,  3(F10.4),  3(F11.4),  3(F12.4))

      RETURN
      END
