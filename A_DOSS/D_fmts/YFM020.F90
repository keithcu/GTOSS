! ROUTINE: YFM020
! %Z%GTOSS %M% H.10 code v01.01 (baseline for vH10 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM020 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! DISPLAYS VARIABLES ASSOCIATED WITH THE RP PLANET-FIXED
! BASE MOTION PERTURBATION STATE


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_DOSS.i"
      include "../../A_HDR/COM_HOST.i"
      include "../../A_HDR/EQU_HOST.i"
      include "../../A_HDR/COM_RPS.i"




!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! WRITE HEADING TO PRINTOUT PAGE
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE DATA FOR PRINTOUT PAGE
      IF(JFUNC .EQ. 2) GO TO 2000

! PUT OUT A LINE OF DATA TO PRINTOUT PAGE
      IF(JFUNC .EQ. 3) GO TO 3000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM020-0'


!******************************************
!******************************************
!    WRITE PAGE HEAD TO PRINTOUT FILE
!******************************************
!******************************************
1000  CONTINUE

      CALL XFEED(IUOUT)

      WRITE(IUOUT,10)
   10 FORMAT(3X,   &
     &  'DISPLAY FMT 20:   REF PT PLANET-FIXED BASE MOTION SUMMARY', /)

! ACTIVATE FORMATS OF UNITS CHOICE
      IF(NUNIT .EQ. 1) ASSIGN 11 TO JF1DUM
      IF(NUNIT .EQ. 1) ASSIGN 12 TO JF2DUM

      IF(NUNIT .EQ. 2) ASSIGN 13 TO JF1DUM
      IF(NUNIT .EQ. 2) ASSIGN 14 TO JF2DUM

      WRITE(IUOUT,JF1DUM)
      WRITE(IUOUT,JF2DUM)

! ENGLISH UNITS FORMAT
!---------------------
   11 FORMAT(3X,' G-TIME   X-BASE +N   Y-BASE +E    Z-ELEV +UP  ',   &
     &               'XD-BASE   YD-BASE   ZD-BASE     XDD-BASE   ',   &
     &                                        'YDD-BASE   ZDD-BASE' )

   12 FORMAT(3X,'  (SEC)       (FT)        (FT)         (FT)    ',   &
     &               '  (F/S)     (F/S)     (F/S)       (F/SS)   ',   &
     &                                        '  (F/SS)     (F/SS)'/)


! METRIC UNITS FORMAT
!--------------------
   13 FORMAT(3X,' G-TIME   X-BASE +N   Y-BASE +E    Z-ELEV +UP  ',   &
     &                 'XD-BASE   YD-BASE   ZD-BASE     XDD-BASE  ',   &
     &                                        ' YDD-BASE   ZDD-BASE' )

   14 FORMAT(3X,'  (SEC)        (M)         (M)          (M)    ',   &
     &                 '  (M/S)     (M/S)     (M/S)       (M/SS)  ',   &
     &                                       '   (M/SS)     (M/SS)' /)

      RETURN



!******************************************
!******************************************
!    CALCULATE DATA FOR PRINTOUT PAGE
!******************************************
!******************************************
2000  CONTINUE

! DISPLAY FOR BASIC REFERENCE POINT STATE AND FORCE SUMMARY
      OUTP( 1) = T

      OUTP( 2) =  XXLNS * BSPER(1)
      OUTP( 3) =  XXLNS * BSPER(2)
      OUTP( 4) = -XXLNS * BSPER(3)

      OUTP( 5) =  XXLNS * BSPERD(1)
      OUTP( 6) =  XXLNS * BSPERD(2)
      OUTP( 7) = -XXLNS * BSPERD(3)

      OUTP( 8) =  XXLNS * BSPERDD(1)
      OUTP( 9) =  XXLNS * BSPERDD(2)
      OUTP(10) = -XXLNS * BSPERDD(3)

! STATE # OF OUTPUT ITEMS IN ABOVE ARRAY FOR USE IN XPLASO
      OUTP(25) = 10.0

      RETURN


!******************************************
!******************************************
!   WRITE LINE OF DATA TO PRINTOUT PAGE
!******************************************
!******************************************
3000  CONTINUE

      IF(NUNIT .EQ. 1) ASSIGN 16 TO JF1DUM
      IF(NUNIT .EQ. 2) ASSIGN 17 TO JF1DUM

      WRITE(IUOUT,JF1DUM) (OUTP(J),J=1,LASITM)

! ENGLISH UNITS FORMAT
!---------------------
16    FORMAT(F10.2, 2(F12.0), F12.2, F11.3, 2(F10.3), F13.6, 2(F11.6))


! METRIC UNITS FORMAT
!--------------------
17    FORMAT(F10.2, 2(F12.0), F12.2, F11.3, 2(F10.3), F13.6, 2(F11.6))

      RETURN
      END
