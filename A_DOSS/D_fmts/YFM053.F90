! ROUTINE: YFM053
! %Z%GTOSS %M% H.7 code v01.02
!              H.7 code v01.02  Changed header for current display
!-----------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM053 (JFUNC)
!**************************************************
!**************************************************
!**************************************************


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_DOSS.i"




!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! WRITE HEADING TO PRINTOUT PAGE
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE DATA FOR PRINTOUT PAGE
      IF(JFUNC .EQ. 2) GO TO 2000

! PUT OUT A LINE OF DATA TO PRINTOUT PAGE
      IF(JFUNC .EQ. 3) GO TO 3000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM053-0'


!******************************************
!******************************************
!    WRITE PAGE HEAD TO PRINTOUT FILE
!******************************************
!******************************************
1000  CONTINUE

      CALL XFEED(IUOUT)

      WRITE(IUOUT, 9)  NOBJZ, NTTSHO, NTPLIB
    9 FORMAT(3X,   &
     &  'DISPLAY FMT 53:  <FOR ALL OBJECTS/TETHERS>  ORB ELEMENTS',   &
     &  ' OF OBJ #',I3,'  W/ ELEC+AERO RESUME OF TOSS',   &
     &  ' TETH #',I3,' (TYPE',I2,' LIBRATION ANGLES)'/)

      WRITE(IUOUT,10)
   10 FORMAT(3X,'RP-TIME   ALT-GEO    GREENWICH    ORBIT   ORBIT  ',   &
     &  'ELEC+AERO FORCE  SAT LIBRATON   DEL TETH ANG END/END L_AVG',   &
     &            '  PERIG  APOGE  RIGHT')

! ACTIVATE FORMATS OF UNITS CHOICE
      IF(NUNIT .EQ. 1) ASSIGN 11 TO JF1DUM
      IF(NUNIT .EQ. 1) ASSIGN 12 TO JF2DUM

      IF(NUNIT .EQ. 2) ASSIGN 13 TO JF1DUM
      IF(NUNIT .EQ. 2) ASSIGN 14 TO JF2DUM

      WRITE(IUOUT,JF1DUM)
      WRITE(IUOUT,JF2DUM)

! ENGLISH UNITS FORMAT
!---------------------
   11 FORMAT(3X,'                    LAT    LONG   PLANE   ECCEN    ',   &
     &  'IN-PLANE-OUT   IN-PLANE-OUT   IN-PLANE-OUT   EMF   CURR   ',   &
     &  'S-ALT  S-ALT  ASCEN')

   12 FORMAT(3X,'  (SEC)    (NM)    (DEG)   (DEG)  (DEG)            ',   &
     &  '(LB)....(LB)  (DEG)....(DEG)  (DEG)..(DEG)  VOLTS  (AMP)   ',   &
     &  '(NM)   (NM)  (DEG)'/)

! METRIC UNITS FORMAT
!--------------------
   13 FORMAT(3X,'                    LAT    LONG   PLANE   ECCEN    ',   &
     &  'IN-PLANE-OUT   IN-PLANE-OUT   IN-PLANE-OUT   EMF   CURR   ',   &
     &  'S-ALT  S-ALT  ASCEN')

   14 FORMAT(3X,'  (SEC)    (KM)    (DEG)   (DEG)  (DEG)            ',   &
     &  '(N)......(N)  (DEG)....(DEG)  (DEG)..(DEG)  VOLTS  (AMP)   ',   &
     &  '(KM)   (KM)  (DEG)'/)

      RETURN


!******************************************
!******************************************
!    CALCULATE DATA FOR PRINTOUT PAGE
!******************************************
!******************************************
2000  CONTINUE

! INVOKE ELECTRO-ORBITAL ELEMENT DISPLAY
      CALL YPDFEL

      RETURN


!******************************************
!******************************************
!   WRITE LINE OF DATA TO PRINTOUT PAGE
!******************************************
!******************************************
3000  CONTINUE

      IF(NUNIT .EQ. 1) ASSIGN 15 TO JF1DUM
      IF(NUNIT .EQ. 2) ASSIGN 16 TO JF1DUM

      WRITE(IUOUT,JF1DUM) (OUTP(J),J=1,LASITM)

! ENGLISH UNITS FORMAT
!---------------------
15    FORMAT(F10.2, F8.1,  F7.1,F8.1,  F9.2, F8.6,  F8.2,F7.2,   &
     &              F9.1,F7.1,  F7.1,F7.1,  F8.0,F6.2,  F8.1,F7.1,   &
     &              F7.2)

! METRIC UNITS FORMAT
!--------------------
16    FORMAT(F10.2, F8.1,  F7.1,F8.1,  F9.2, F8.6,  F8.2,F7.2,   &
     &              F9.1,F7.1,  F7.1,F7.1,  F8.0,F6.2,  F8.1,F7.1,   &
     &              F7.2)

      RETURN
      END
