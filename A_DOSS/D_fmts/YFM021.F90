! ROUTINE: YFM021
! %Z%GTOSS %M% H.10 code v01.01 (baseline for vH10 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM021 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! DISPLAYS ATTACH POINT DISTANCES OF SPECIFIED TOSS TETHERS


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_DOSS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"





!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! WRITE HEADING TO PRINTOUT PAGE
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE DATA FOR PRINTOUT PAGE
      IF(JFUNC .EQ. 2) GO TO 2000

! PUT OUT A LINE OF DATA TO PRINTOUT PAGE
      IF(JFUNC .EQ. 3) GO TO 3000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM021-0'


!******************************************
!******************************************
!    WRITE PAGE HEAD TO PRINTOUT FILE
!******************************************
!******************************************
1000  CONTINUE

      CALL XFEED(IUOUT)

      WRITE(IUOUT,50)
   50 FORMAT(3X,   &
     &'DISPLAY FMT 22:   ATTACH POINT DISTANCE FOR TOSS TETHER ',   &
     &'NUMBERS SHOWN)'/)

! ACTIVATE FORMATS OF UNITS CHOICE
      IF(NUNIT .EQ. 1) ASSIGN 51 TO JF1DUM
      IF(NUNIT .EQ. 1) ASSIGN 52 TO JF2DUM

      IF(NUNIT .EQ. 2) ASSIGN 53 TO JF1DUM
      IF(NUNIT .EQ. 2) ASSIGN 54 TO JF2DUM

      WRITE(IUOUT,JF1DUM) ( NINT( PTSHOW(J) ),J=1,15 )
      WRITE(IUOUT,JF2DUM)

! ENGLISH UNITS FORMAT
!---------------------
   51 FORMAT(3X,' Q-TIME', 15( 4X, 'TETH #',I2) )

   52 FORMAT(3X,'  (SEC)', 15( 5X, 'AP (FT)')  /)

! METRIC  UNITS FORMAT
!---------------------
   53 FORMAT(3X,' Q-TIME', 15( 4X, 'TETH #',I2) )

   54 FORMAT(3X,'  (SEC)', 15( 5X, 'AP (M) ')  /)

      RETURN


!******************************************
!******************************************
!    CALCULATE DATA FOR PRINTOUT PAGE
!******************************************
!******************************************
2000  CONTINUE

! DISPLAY FOR TETHER FORCE PARAMETERS

      OUTP( 1) = QTIME

! DISPLAY TENSION AT X-END OF TETHER
      DO 10 J=1,15
         NTSHO = NINT(PTSHOW(J))
         IF(NTSHO .EQ. 0) GO TO 10
            OUTP(J+1) = XXLNS * TDIST(NTSHO)
10    CONTINUE

! STATE # OF OUTPUT ITEMS IN ABOVE ARRAY FOR USE IN XPLASO
      OUTP(25) = 16.0

      RETURN


!******************************************
!******************************************
!   WRITE LINE OF DATA TO PRINTOUT PAGE
!******************************************
!******************************************
3000  CONTINUE

      IF(NUNIT .EQ. 1) ASSIGN 55 TO JF1DUM
      IF(NUNIT .EQ. 2) ASSIGN 56 TO JF1DUM

      WRITE(IUOUT,JF1DUM) (OUTP(J),J=1,LASITM)

! ENGLISH UNITS FORMAT
!---------------------
   55  FORMAT(F10.2, 15F12.3 )

! METRIC  UNITS FORMAT
!---------------------
   56  FORMAT(F10.2, 15F12.3 )

      RETURN
      END
