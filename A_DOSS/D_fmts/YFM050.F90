! ROUTINE: YFM050
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM050 (JFUNC)
!**************************************************
!**************************************************
!**************************************************


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_DOSS.i"




!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! WRITE HEADING TO PRINTOUT PAGE
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE DATA FOR PRINTOUT PAGE
      IF(JFUNC .EQ. 2) GO TO 2000

! PUT OUT A LINE OF DATA TO PRINTOUT PAGE
      IF(JFUNC .EQ. 3) GO TO 3000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM050-0'


!******************************************
!******************************************
!    WRITE PAGE HEAD TO PRINTOUT FILE
!******************************************
!******************************************
1000  CONTINUE

      CALL XFEED(IUOUT)

      WRITE(IUOUT,10) NFTSHO
   10 FORMAT(3X,   &
     & 'DISPLAY FMT 50:  <BEAD MODEL TETHER>  BEAD POSITION IN REF ',   &
     & 'END ORB FRAME  (Z-COMP AT SPECIFIED BEADS):  SOLN #', I3 /)

! ACTIVATE FORMATS OF UNITS CHOICE
      IF(NUNIT .EQ. 1) ASSIGN 11 TO JF1DUM
      IF(NUNIT .EQ. 1) ASSIGN 12 TO JF2DUM

      IF(NUNIT .EQ. 2) ASSIGN 13 TO JF1DUM
      IF(NUNIT .EQ. 2) ASSIGN 14 TO JF2DUM

      WRITE(IUOUT,JF1DUM) (NINT(PNBSHO(J)),J=1,11)
      WRITE(IUOUT,JF2DUM)

! ENGLISH UNITS FORMAT
!---------------------
   11 FORMAT(3X,' F-TIME', 11('   ZORB-B',I3) )

   12 FORMAT(3X,'  (SEC)', 11('      (FT)  ') /)

! METRIC  UNITS FORMAT
!---------------------
   13 FORMAT(3X,' F-TIME', 11('   ZORB-B',I3) )

   14 FORMAT(3X,'  (SEC)', 11('      (M)   ') /)

      RETURN


!******************************************
!******************************************
!    CALCULATE DATA FOR PRINTOUT PAGE
!******************************************
!******************************************
2000  CONTINUE

! MAKE CALL TO GET ORB FRAME POS COMP OF SELECTED BEADS
      CALL YPDFBB(3)

      RETURN


!******************************************
!******************************************
!   WRITE LINE OF DATA TO PRINTOUT PAGE
!******************************************
!******************************************
3000  CONTINUE

      IF(NUNIT .EQ. 1) ASSIGN 15 TO JF1DUM
      IF(NUNIT .EQ. 2) ASSIGN 16 TO JF1DUM

      WRITE(IUOUT,JF1DUM) (OUTP(J),J=1,LASITM)

! ENGLISH UNITS FORMAT
!---------------------
15    FORMAT(F10.2, 11F12.1 )

! METRIC  UNITS FORMAT
!---------------------
16    FORMAT(F10.2, 11F12.1 )

      RETURN
      END
