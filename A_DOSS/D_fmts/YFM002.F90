! ROUTINE: YFM002
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM002 (JFUNC)
!**************************************************
!**************************************************
!**************************************************


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_DOSS.i"
      include "../../A_HDR/COM_HOST.i"
      include "../../A_HDR/EQU_HOST.i"
      include "../../A_HDR/COM_RPS.i"




!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! WRITE HEADING TO PRINTOUT PAGE
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE DATA FOR PRINTOUT PAGE
      IF(JFUNC .EQ. 2) GO TO 2000

! PUT OUT A LINE OF DATA TO PRINTOUT PAGE
      IF(JFUNC .EQ. 3) GO TO 3000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM002-0'


!******************************************
!******************************************
!    WRITE PAGE HEAD TO PRINTOUT FILE
!******************************************
!******************************************
1000  CONTINUE

      CALL XFEED(IUOUT)

      WRITE(IUOUT,20)  NTPEUL
   20 FORMAT(3X,   &
     &    'DISPLAY FMT 2:   REF PT ATTITUDE (TYPE',I2,' EULER',   &
     &                      ' ANGLES), AND ORBITAL STATE',/)

! ACTIVATE FORMATS OF UNITS CHOICE
      IF(NUNIT .EQ. 1) ASSIGN 21 TO JF1DUM
      IF(NUNIT .EQ. 1) ASSIGN 22 TO JF2DUM

      IF(NUNIT .EQ. 2) ASSIGN 23 TO JF1DUM
      IF(NUNIT .EQ. 2) ASSIGN 24 TO JF2DUM

      WRITE(IUOUT,JF1DUM)
      WRITE(IUOUT,JF2DUM)

! ENGLISH UNITS FORMAT
!---------------------
   21 FORMAT(3X,' G-TIME   OMX    OMY    OMZ  OMXD  OMYD  OMZD ',   &
     &          '   THTI   PHII   PSII   THTO   PHIO   PSIO',   &
     &          '   MU-I  LA-I  ALTGEO   VEL-I      HD')

   22 FORMAT(3X,'  (SEC)  (D/S)  (D/S)  (D/S) (DSS) (DSS) (DSS)',   &
     &          '  (DEG)  (DEG)  (DEG)  (DEG)  (DEG)  (DEG)',   &
     &          '  (DEG)  (DEG)   (FT)   (F/S)    (F/S)' /)

! METRIC UNITS FORMAT
!--------------------
   23 FORMAT(3X,' G-TIME   OMX    OMY    OMZ  OMXD  OMYD  OMZD ',   &
     &          '   THTI   PHII   PSII   THTO   PHIO   PSIO',   &
     &          '   MU-I  LA-I  ALTGEO   VEL-I      HD')

   24 FORMAT(3X,'  (SEC)  (D/S)  (D/S)  (D/S) (DSS) (DSS) (DSS)',   &
     &          '  (DEG)  (DEG)  (DEG)  (DEG)  (DEG)  (DEG)',   &
     &          '  (DEG)  (DEG)    (M)   (M/S)    (M/S)' /)

      RETURN


!******************************************
!******************************************
!    CALCULATE DATA FOR PRINTOUT PAGE
!******************************************
!******************************************
2000  CONTINUE

! USE EULER ANGLE TYPE SPECIFIED BY POST PROCESSOR REQUEST
      CALL MATEUL (NTPEUL,RPGRI,  RPPHII,RPTHTI,RPPSII)

! USE EULER ANGLE TYPE SPECIFIED BY POST PROCESSOR REQUEST
      CALL MATMUL (2, RPGRI,RPGOI,   R3X3)
      CALL MATEUL (NTPEUL,R3X3,   RPPHIO,RPTHTO,RPPSIO)

! DISPLAY FOR RP ATTITUDE AND ORBITAL STATE
      OUTP( 1) = T

      OUTP( 2) = XRDXDG * RPOM(1)
      OUTP( 3) = XRDXDG * RPOM(2)
      OUTP( 4) = XRDXDG * RPOM(3)

      OUTP( 5) = XRDXDG * RPOMD(1)
      OUTP( 6) = XRDXDG * RPOMD(2)
      OUTP( 7) = XRDXDG * RPOMD(3)

      OUTP( 8) = XRDXDG * RPTHTI
      OUTP( 9) = XRDXDG * RPPHII
      OUTP(10) = XRDXDG * RPPSII

      OUTP(11) = XRDXDG * RPTHTO
      OUTP(12) = XRDXDG * RPPHIO
      OUTP(13) = XRDXDG * RPPSIO

      OUTP(14) = DEGMUI
      OUTP(15) = DEGLAI

      OUTP(16) = XXLNS * RFPALT
      OUTP(17) = XXLNS * RPVELI
      OUTP(18) = XXLNS * RPHD

! STATE # OF OUTPUT ITEMS IN ABOVE ARRAY FOR USE IN XPLASO
      OUTP(25) = 18.0

      RETURN


!******************************************
!******************************************
!   WRITE LINE OF DATA TO PRINTOUT PAGE
!******************************************
!******************************************
3000  CONTINUE

      IF(NUNIT .EQ. 1) ASSIGN 25 TO JF1DUM
      IF(NUNIT .EQ. 2) ASSIGN 26 TO JF1DUM

      WRITE(IUOUT,JF1DUM) (OUTP(J),J=1,LASITM)

! ENGLISH UNITS FORMAT
!---------------------
25    FORMAT(F10.2, 3F7.1, 3F6.1, 7F7.1, F6.1, F9.0,   &
     &          F8.0, F9.2 )

! METRIC UNITS FORMAT
!--------------------
26    FORMAT(F10.2, 3F7.1, 3F6.1, 7F7.1, F6.1, F9.0,   &
     &          F8.0, F9.2 )

      RETURN
      END
