! ROUTINE: POPBST
! %Z%GTOSS %M% H.10 code v01.1
!              H.10 code v01.1 Archives some aero related data.
!                              Archives total thruts impulse and mass loss.
!                              Archives object bouyancy data.
!-------------------------------------------------------------------------
!              H.2 code v01.02 Archived PSD link loads to RDB.
!-------------------------------------------------------------
!                  code v01.01 (baseline for vG.1 delivery)
!*************************************
!*************************************
!
      SUBROUTINE POPBST(NPOP,JBODY)
!
!*************************************
!*************************************
! THIS SUBROUTINE POPULATES THE TOSS OBJECT BASIC
! RESULTS DATA BASE FILE


! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND 
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_ROSS.i"
      include "../../A_HDR/COM_TOSS.i"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.i"





! NOTE: THE ARG JBODY PASSED TO THIS ROUTINE IS NOT
!       CURRENTLY USED BY IT, BUT IS MADE AVAILABLE
!       IN CASE IN THE FUTURE FUNCTIONS REQUIRING IT
!       MAY ARISE (SUCH AS LOADING THE WORKING ARRAYS,
!       OR CALLING THE AUX DATA ROUTINES - WHICH IT
!       CURRENTLY DOES NOT DO)

!-----------------------------------------------------
! IT IS  THE RESPONSIBILITY  OF THIS ROUTINE  TO STATE
! ITS CONTRIBUTION TO ITS ASSOCIATED RDB FILES RECORD
! SIZE IN SUCH A WAY THAT  IF NPOP=0, THEN THE SIZE IS
! STATED, AND NO  UNINITIALIZED DATA  WILL BE HANDLED.
! IF NPOP=1, DATA ARRAYS ARE POPULATED AS USUAL
!-----------------------------------------------------

! STATE RECORD SIZE REQUIRED TO CONTAIN BASIC TOSS OBJECT DATA
!-------------------------------------------------------------
      NBAST = 166 + LNCHOR
      IF(NPOP .EQ. 0) RETURN

!**********************************
! SET IN THE BASIC TOSS OBJECT DATA
!**********************************
      RDBOUT( 1) = TOSTIM

      RDBOUT( 2) = RBI(1)
      RDBOUT( 3) = RBI(2)
      RDBOUT( 4) = RBI(3)

      RDBOUT( 5) = RBID(1)
      RDBOUT( 6) = RBID(2)
      RDBOUT( 7) = RBID(3)

      RDBOUT( 8) = RBIDD(1)
      RDBOUT( 9) = RBIDD(2)
      RDBOUT(10) = RBIDD(3)

      RDBOUT(11) = RI(1)
      RDBOUT(12) = RI(2)
      RDBOUT(13) = RI(3)

      RDBOUT(14) = RID(1)
      RDBOUT(15) = RID(2)
      RDBOUT(16) = RID(3)

      RDBOUT(17) = RBOR(1)
      RDBOUT(18) = RBOR(2)
      RDBOUT(19) = RBOR(3)

      RDBOUT(20) = RBORD(1)
      RDBOUT(21) = RBORD(2)
      RDBOUT(22) = RBORD(3)

      RDBOUT(23) = RBAR(1)
      RDBOUT(24) = RBAR(2)
      RDBOUT(25) = RBAR(3)

      RDBOUT(26) = VBAR(1)
      RDBOUT(27) = VBAR(2)
      RDBOUT(28) = VBAR(3)

      RDBOUT(29) = AGI(1)
      RDBOUT(30) = AGI(2)
      RDBOUT(31) = AGI(3)

      RDBOUT(32) = FI(1)
      RDBOUT(33) = FI(2)
      RDBOUT(34) = FI(3)

      RDBOUT(35) = FB(1)
      RDBOUT(36) = FB(2)
      RDBOUT(37) = FB(3)

      RDBOUT(38) = FCB(1)
      RDBOUT(39) = FCB(2)
      RDBOUT(40) = FCB(3)

      RDBOUT(41) = FDB(1)
      RDBOUT(42) = FDB(2)
      RDBOUT(43) = FDB(3)

      RDBOUT(44) = FTB(1)
      RDBOUT(45) = FTB(2)
      RDBOUT(46) = FTB(3)

      RDBOUT(47) = FEEB(1)
      RDBOUT(48) = FEEB(2)
      RDBOUT(49) = FEEB(3)

      RDBOUT(50) = OMB(1)
      RDBOUT(51) = OMB(2)
      RDBOUT(52) = OMB(3)

      RDBOUT(53) = OMBD(1)
      RDBOUT(54) = OMBD(2)
      RDBOUT(55) = OMBD(3)

      RDBOUT(56) = GB(1)
      RDBOUT(57) = GB(2)
      RDBOUT(58) = GB(3)

      RDBOUT(59) = GCB(1)
      RDBOUT(60) = GCB(2)
      RDBOUT(61) = GCB(3)

      RDBOUT(62) = GDB(1)
      RDBOUT(63) = GDB(2)
      RDBOUT(64) = GDB(3)

      RDBOUT(65) = GTB(1)
      RDBOUT(66) = GTB(2)
      RDBOUT(67) = GTB(3)

      RDBOUT(68) = CEEB(1)
      RDBOUT(69) = CEEB(2)
      RDBOUT(70) = CEEB(3)

      RDBOUT(71) = GGB(1)
      RDBOUT(72) = GGB(2)
      RDBOUT(73) = GGB(3)

      RDBOUT(74) = NATPAT

      RDBOUT(75) = VRWB(1)
      RDBOUT(76) = VRWB(2)
      RDBOUT(77) = VRWB(3)

      RDBOUT(78) = VWI(1)
      RDBOUT(79) = VWI(2)
      RDBOUT(80) = VWI(3)

      RDBOUT(81) = DMASS

      RDBOUT(82) = RIP(1)
      RDBOUT(83) = RIP(2)
      RDBOUT(84) = RIP(3)

      RDBOUT(85) = RANGE
      RDBOUT(86) = RANGED

      RDBOUT(87) = DENS
      RDBOUT(88) = VR
      RDBOUT(89) = QBAR

      RDBOUT(90) = 0.0
      RDBOUT(91) = 0.0

      RDBOUT(92) = DMUE
      RDBOUT(93) = DLAE
      RDBOUT(94) = ALTOBL

      RDBOUT( 95) = GBI(1,1)
      RDBOUT( 96) = GBI(2,1)
      RDBOUT( 97) = GBI(3,1)

      RDBOUT( 98) = GBI(1,2)
      RDBOUT( 99) = GBI(2,2)
      RDBOUT(100) = GBI(3,2)

      RDBOUT(101) = GBI(1,3)
      RDBOUT(102) = GBI(2,3)
      RDBOUT(103) = GBI(3,3)

      DO 20 J=1,8
         RDBOUT(J+103) = XTB(J)
         RDBOUT(J+111) = YTB(J)
         RDBOUT(J+119) = ZTB(J)
20    CONTINUE

      RDBOUT(128) = HMOMI(1)
      RDBOUT(129) = HMOMI(2)
      RDBOUT(130) = HMOMI(3)

      RDBOUT(131) = DIXX
      RDBOUT(132) = DIYY
      RDBOUT(133) = DIZZ
      RDBOUT(134) = DIXY
      RDBOUT(135) = DIXZ
      RDBOUT(136) = DIYZ
      RDBOUT(137) = PCGB(1)
      RDBOUT(138) = PCGB(2)
      RDBOUT(139) = PCGB(3)

      RDBOUT(140) = SOUND
      RDBOUT(141) = AIRTMP

! SKIP DAMPER INFORMATION
      RDBOUT(142) = DLFRIC
      RDBOUT(143) = FRICNM

      IF(LNCHOR .GT. 0) THEN
          DO 22 J = 1,LNCHOR
               RDBOUT(143+J) = ARMLOD(J)
22        CONTINUE
      END IF

! ARCHIVE SOME AERO-SPECIFIC DATA FOR OBJECTS
      RDBOUT(144 + LNCHOR) = DMACH
      RDBOUT(145 + LNCHOR) = CA_OBJ
      RDBOUT(146 + LNCHOR) = CN_OBJ
      RDBOUT(147 + LNCHOR) = CM_OBJ
      RDBOUT(148 + LNCHOR) = FAMAG_OBJ
      RDBOUT(149 + LNCHOR) = FNMAG_OBJ
      RDBOUT(150 + LNCHOR) = ACMAG_OBJ
      RDBOUT(151 + LNCHOR) = ALP_OBJ
      RDBOUT(152 + LNCHOR) = DMASINC
      RDBOUT(153 + LNCHOR) = DMASDOT

! ARCHIVE SOME GENERAL OBJECT AERO TERMS
      RDBOUT(154 + LNCHOR) = CDB(1)
      RDBOUT(155 + LNCHOR) = CDB(2)
      RDBOUT(156 + LNCHOR) = CDB(3)

! ARCHIVE ATMOS WIND PERTURBATION AT OBJECT
      RDBOUT(157 + LNCHOR) = VWPP(1)
      RDBOUT(158 + LNCHOR) = VWPP(2)
      RDBOUT(159 + LNCHOR) = VWPP(3)

! ARCHIVE TOTAL IMPULSE DUE TO THRUSTING
      RDBOUT(160 + LNCHOR) = TOTIPULS

! ARCHIVE OBJECT BUOYANCY DATA
      RDBOUT(161 + LNCHOR) = FBYB(1)
      RDBOUT(162 + LNCHOR) = FBYB(2)
      RDBOUT(163 + LNCHOR) = FBYB(3)
      RDBOUT(164 + LNCHOR) = GBYB(1)
      RDBOUT(165 + LNCHOR) = GBYB(2)
      RDBOUT(166 + LNCHOR) = GBYB(3)


!*****************************************************************
! THIS ARRAY SIZE MUST BE EXPLICITLY STATED AT TOP OF THIS ROUTINE
!*****************************************************************

      RETURN
      END
