! ROUTINE: POPAXA
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************
!*************************************
!
      SUBROUTINE POPAXA(NPOP)
!
!*************************************
!*************************************
! THIS SUBROUTINE ALLOWS FOR A USER DEFINED ADDITION
! TO BE MADE TO THE ALREADY WRITTEN BASIC RESULTS
! DATA BASE FILE ASSOCIATED WITH VARIABLE SIZE ARRAYS


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_ROSS.i"




! NOTE: THIS ROUTINE EXISTS ONLY TO PRESERVE SYMMETRY
!       OF THE RDB SYSTEM STRUCTURE.  ITS ONLY FUNCTION
!       IS TO RETURN A VALUE FOR NINCXA.  ITS LOGICAL
!       FUNCTION SEEMS BEST HANDLED AT PRESENT BY
!       ROUTINE POPDB (AND IS NOT CALLED BY POPRDB).
!       ONLY ROUTINE OPNRDB CALLS THIS ROUTINE.


!-----------------------------------------------------
! IT IS  THE RESPONSIBILITY  OF THIS ROUTINE  TO STATE
! ITS CONTRIBUTION TO ITS ASSOCIATED RDB FILES RECORD
! SIZE IN SUCH A WAY THAT  IF NPOP=0, THEN THE SIZE IS
! STATED, AND NO  UNINITIALIZED DATA  WILL BE HANDLED.
! IF NPOP=1, DATA ARRAYS ARE POPULATED AS USUAL
!-----------------------------------------------------

! STATE REC WORD SIZE INCREMENT FOR AUX VARIABLE ARRAY DATA
!----------------------------------------------------------
!     NINCXA = NA      SEE BELOW FOR DEFN OF NA
      NINCXA = 0
      IF(NPOP .EQ. 0) RETURN

!*******************************
! SET IN AUX VARIABLE ARRAY DATA
!*******************************
! NO FUNCTION AT PRESENT

!     RDBOUT(NBASA+1) = ??????
!
!     RDBOUT(NBASA+NA) = ??????


      RETURN
      END
