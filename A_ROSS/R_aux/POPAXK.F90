! ROUTINE: POPAXK
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************
!*************************************
!
      SUBROUTINE POPAXK(NPOP)
!
!*************************************
!*************************************
! THIS SUBROUTINE ALLOWS FOR A USER DEFINED ADDITION
! TO BE MADE TO THE ALREADY WRITTEN RDB K-FILE


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_ROSS.i"




!-----------------------------------------------------
! IT IS  THE RESPONSIBILITY  OF THIS ROUTINE  TO STATE
! ITS CONTRIBUTION TO ITS ASSOCIATED RDB FILES RECORD
! SIZE IN SUCH A WAY THAT  IF NPOP=0, THEN THE SIZE IS
! STATED, AND NO  UNINITIALIZED DATA  WILL BE HANDLED.
! IF NPOP=1, DATA ARRAYS ARE POPULATED AS USUAL
!-----------------------------------------------------

!******************************************
! RESPOND TO RTOSS REQUEST FOR RECORD COUNT
!******************************************
      IF(NPOP .EQ. 0) THEN

! STATE RECORD WORD COUNT REQUIRED TO CONTAIN YOUR DATA
!        NINCXK = NXK      SEE BELOW FOR DEFN OF NXK
         NINCXK = 0

         RETURN

      END IF

!***********************
! SET IN AUX K-FILE DATA
!***********************
! NO FUNCTION AT PRESENT

!     RDBOUT(NBASK +1   ) = ??????
!
!     RDBOUT(NBASK+ NXK) = ??????

      RETURN
      END
