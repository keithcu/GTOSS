! ROUTINE: PULOPN
! %Z%GTOSS %M% H.10 code v01.10
!              H.10 code v01.20 Added CHAIN array  data to T-file hdr.
!                               Added Grav, Aero, Elec activation flags.
!                               Fixed bug in Skip Damper retrieve code
!                               Added Thermal activation flags.
!                               Added 3 DOF/6 DOF flag for each TOSS Obj
!--------------------------------------------------------------------
!              H.9 code v01.04 Added TOSS Ref date/time to archive
!-----------------------------------------------------------------
!              H.5 code v01.03 Deassigned storage for RTD, DTR
!                              Eliminated OMEGAE from RDB archive
!----------------------------------------------------------------
!              H.2 code v01.02 Pulls LNCHOR from record 1
!----------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!*****************************
!*****************************
!
      SUBROUTINE PULOPN
!
!*****************************
!*****************************
! THIS ROUTINE OPENS THE DIRECT ACCESS FILES
! ASSOCIATED WITH THE RESULTS DATA BASE


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND 
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS

! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND 
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_ROSS.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.i"


!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.i"





! CHAR STORAGE FOR READING DATE LINE OF ASCII RDB I-FILE
      CHARACTER *1 CDUM(131)

!****************************************************************
!****************************************************************
! FIRST, OPEN RDB FILE TO DETERMINE OPEN SIZES OF OTHER RDB FILES
!****************************************************************
!****************************************************************

! MAKE A FACILTY-DEPENDENT PATH TO THIS RDB FILE
!-----------------------------------------------
           CALL GETPTH (DBROOT, RDBFNO,   DBPATH)

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! CYBER PERMANENT FILE ACCESS
!@NOS      CALL PF('ATTACH', RDBFNO, RDBFNO)

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! (THIS FILE HAS TO HAVE A REC SIZE KNOWN APRIORI TO DISPLAY PROGS)
!@NOS      OPEN(IOUXXO, FILE=RDBFNO, STATUS='OLD', FORM='UNFORMATTED',
!@NOS     1             ACCESS='DIRECT', RECL=NRBYTE*NOPEN)

      OPEN(IOUXXO, FILE = DBPATH, STATUS='OLD',   &
     &             FORM='UNFORMATTED', ACCESS='DIRECT',   &
     &             RECL=NRBYTE*NOPEN)

!@VAX      OPEN(IOUXXO, FILE = DBPATH, STATUS='OLD',
!@VAX     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@VAX     2             RECL=NRBYTE*NOPEN)

!@DOS      OPEN(IOUXXO, FILE = DBPATH, STATUS='OLD',
!@DOS     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@DOS     2             RECL=NRBYTE*NOPEN)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
!@MAC      OPEN(IOUXXO, FILE = DBPATH, STATUS='OLD',
!@MAC     1        FORM='UNFORMATTED',ACCESS='DIRECT', RECL=NRBYTE*NOPEN)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@


!------------------------------------------------------------------
! READ THE RDB FILE SET OPEN-SIZES (PLUS OTHER INFO), THEN CLOSE IT
!------------------------------------------------------------------
! (THERES ONLY 1 RECORD IN THIS FILE, WHICH CONTAINS ALL THE INFO)
      READ(IOUXXO,REC=1) (RDBOUT(J),J=1,NOPEN)
      CLOSE(IOUXXO,STATUS='KEEP')

! EXTRACT INTEGER VALUE OF OPEN RECORD SIZES (WORDS)
! NOTE: THE F-FILE DATA BELOW IS UNCHANGED IN POSITION EVEN ACROSS
!       THE MODAL SYNTHESIS PURGE WHEN THE F-FILE BECAME A WILD CARD
!       FILE. THIS WAS DONE TO MAINTAIN COMPATIBILITY ACROSS RDBS.

      NOPSZA = NINT(RDBOUT( 1))
      NOPSZT = NINT(RDBOUT( 2))
      NOPSZF = NINT(RDBOUT( 3))
      NOPSZB = NINT(RDBOUT( 4))

      NOPSZG = NINT(RDBOUT( 5))
      NOPSZJ = NINT(RDBOUT( 6))
      NOPSZK = NINT(RDBOUT( 7))

! EXTRACT INTEGER NUMBER OF LAST RECORD WRITTEN TO EACH FILE
      LASRCA = NINT(RDBOUT( 8))
      LASRCT = NINT(RDBOUT( 9))
      LASRCF = NINT(RDBOUT(10))
      LASRCB = NINT(RDBOUT(11))

      LASRCG = NINT(RDBOUT(12))
      LASRCJ = NINT(RDBOUT(13))
      LASRCK = NINT(RDBOUT(14))

! EXTRACT AUX-DATA-AVAILABLE FLAGS
      NLAXA  = NINT(RDBOUT(15))
      NLAXT  = NINT(RDBOUT(16))
      NLAXF  = NINT(RDBOUT(17))
      NLAXB  = NINT(RDBOUT(18))

      NLAXG  = NINT(RDBOUT(19))
      NLAXJ  = NINT(RDBOUT(20))
      NLAXK  = NINT(RDBOUT(21))

! EXTRACT DATA-SNAP-SHOT-INHIBITED FLAGS
      MISSA  = NINT(RDBOUT(22))
      MISST  = NINT(RDBOUT(23))
      MISSF  = NINT(RDBOUT(24))
      MISSB  = NINT(RDBOUT(25))

      MISSG  = NINT(RDBOUT(26))
      MISSJ  = NINT(RDBOUT(27))
      MISSK  = NINT(RDBOUT(28))



!**************************************************
!**************************************************
! NEXT, FETCH THE DATE FROM THE RDB ASCII DATA FILE
!**************************************************
!**************************************************

! MAKE A FACILTY-DEPENDENT PATH TO THIS RDB FILE
!-----------------------------------------------
           CALL GETPTH (DBROOT, RDBFNI,   DBPATH)

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! CYBER PERMANENT FILE ACCESS
!@NOS      CALL PF('ATTACH', RDBFNI, RDBFNI)

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! OPEN THE ASCII RDB RUN GEOMETRY FILE
!@NOS      OPEN(IOUXXI, FILE = RDBFNI, STATUS='OLD')
      OPEN(IOUXXI, FILE = DBPATH, STATUS='OLD')
!@VAX      OPEN(IOUXXI, FILE = DBPATH, STATUS='OLD')
!@DOS      OPEN(IOUXXI, FILE = DBPATH, STATUS='OLD')
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
!@MAC      OPEN(IOUXXI, FILE = DBPATH, STATUS='OLD')
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@

      REWIND(IOUXXI)


! READ LINE 1 OF RDB I-FILE (THROW DATA AWAY)
!--------------------------------------------
      READ(IOUXXI,300) (CDUM(J),J=1,131)

! READ LINE 2 OF RDB I-FILE (CONTAINS DATE AND TIME)
!---------------------------------------------------
      READ(IOUXXI,300) (CDUM(J),J=1,131)

300   FORMAT(131A1)

! NOW CLOSE THE ASCII RDB RUN GEOMETRY FILE
      CLOSE(IOUXXI, STATUS='KEEP')

! THEN RE-CONSTITUTE THE RTOSS STANDARD DATE AND TIME ARRAYS
!-----------------------------------------------------------
      CDATEC =  CDUM( 6) // CDUM( 7) // CDUM( 8) // CDUM( 9) //   &
     &          CDUM(10) // CDUM(11) // CDUM(12) // CDUM(13) //   &
     &          CDUM(14) // CDUM(15)

      CTIMEC =  CDUM(20) // CDUM(21) // CDUM(22) // CDUM(23) //   &
     &          CDUM(24) // CDUM(25) // CDUM(26) // CDUM(27) //   &
     &          CDUM(28) // CDUM(29)




!*******************************************
!*******************************************
! NOW OPEN REMAINING RESULTS DATA BASE FILES
!*******************************************
!*******************************************

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! CYBER PERMANENT FILE ACCESS
!@NOS      CALL PF('ATTACH', RDBFNA, RDBFNA)
!@NOS      CALL PF('ATTACH', RDBFNT, RDBFNT)
!@NOS      IF(NOPSZF.GT.0) CALL PF('ATTACH', RDBFNF, RDBFNF)
!@NOS      IF(NOPSZB.GT.0) CALL PF('ATTACH', RDBFNB, RDBFNB)

!@NOS      IF(NOPSZG.GT.0) CALL PF('ATTACH', RDBFNG, RDBFNG)
!@NOS      IF(NOPSZJ.GT.0) CALL PF('ATTACH', RDBFNJ, RDBFNJ)
!@NOS      IF(NOPSZK.GT.0) CALL PF('ATTACH', RDBFNK, RDBFNK)

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! OPEN RDB FILE FOR BINARY OUTPUT OF VARIABLE SIZE ARRAYS (A-FILE)

! MAKE A FACILTY-DEPENDENT PATH TO THIS RDB FILE
!-----------------------------------------------
           CALL GETPTH (DBROOT, RDBFNA,   DBPATH)

!@NOS      OPEN(IOUXXA, FILE=RDBFNA, STATUS='OLD', FORM='UNFORMATTED',
!@NOS     1             ACCESS='DIRECT', RECL=NRBYTE*NOPSZA)

      OPEN(IOUXXA, FILE = DBPATH, STATUS='OLD',   &
     &             FORM='UNFORMATTED', ACCESS='DIRECT',   &
     &             RECL=NRBYTE*NOPSZA)

!@VAX      OPEN(IOUXXA, FILE = DBPATH, STATUS='OLD',
!@VAX     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@VAX     2             RECL=NRBYTE*NOPSZA)

!@DOS      OPEN(IOUXXA, FILE = DBPATH, STATUS='OLD',
!@DOS     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@DOS     2             RECL=NRBYTE*NOPSZA)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
!@MAC       OPEN(IOUXXA, FILE = DBPATH, STATUS='OLD',
!@MAC     1        FORM='UNFORMATTED',ACCESS='DIRECT', RECL=NRBYTE*NOPSZA)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@



!------------------------------------------------------
! OPEN RDB FILE FOR BINARY OUTPUT OF TOSS DATA (T-FILE)
!------------------------------------------------------

! MAKE A FACILTY-DEPENDENT PATH TO THIS RDB FILE
!-----------------------------------------------
           CALL GETPTH (DBROOT, RDBFNT,   DBPATH)

!@NOS      OPEN(IOUXXT, FILE=RDBFNT, STATUS='OLD', FORM='UNFORMATTED',
!@NOS     1             ACCESS='DIRECT', RECL=NRBYTE*NOPSZT)

      OPEN(IOUXXT, FILE = DBPATH, STATUS='OLD',   &
     &             FORM='UNFORMATTED', ACCESS='DIRECT',   &
     &             RECL=NRBYTE*NOPSZT)

!@VAX      OPEN(IOUXXT, FILE = DBPATH, STATUS='OLD',
!@VAX     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@VAX     2             RECL=NRBYTE*NOPSZT)

!@DOS      OPEN(IOUXXT, FILE = DBPATH, STATUS='OLD',
!@DOS     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@DOS     2             RECL=NRBYTE*NOPSZT)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
!@MAC      OPEN(IOUXXT, FILE = DBPATH, STATUS='OLD',
!@MAC     1        FORM='UNFORMATTED',ACCESS='DIRECT', RECL=NRBYTE*NOPSZT)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@



!------------------------------------------------------------------
! IF EXISTANT, OPEN RDB FILE FOR OUTPUT OF BEAD MODEL DATA (B-FILE)
!------------------------------------------------------------------
      IF(NOPSZB .GT. 0) THEN

! MAKE A FACILTY-DEPENDENT PATH TO THIS RDB FILE
!-----------------------------------------------
           CALL GETPTH (DBROOT, RDBFNB,   DBPATH)

!@NOS      OPEN(IOUXXB, FILE=RDBFNB, STATUS='OLD', FORM='UNFORMATTED',
!@NOS     1             ACCESS='DIRECT', RECL=NRBYTE*NOPSZB)

      OPEN(IOUXXB, FILE = DBPATH, STATUS='OLD',   &
     &             FORM='UNFORMATTED', ACCESS='DIRECT',   &
     &             RECL=NRBYTE*NOPSZB)

!@VAX      OPEN(IOUXXB, FILE = DBPATH, STATUS='OLD',
!@VAX     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@VAX     2             RECL=NRBYTE*NOPSZB)

!@DOS      OPEN(IOUXXB, FILE = DBPATH, STATUS='OLD',
!@DOS     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@DOS     2             RECL=NRBYTE*NOPSZB)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
!@MAC           OPEN(IOUXXB, FILE = DBPATH, STATUS='OLD',
!@MAC     1        FORM='UNFORMATTED',ACCESS='DIRECT', RECL=NRBYTE*NOPSZB)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
      END IF




!------------------------------------------------------------
! IF EXISTANT, OPEN RDB FILE FOR BINARY OUTPUT OF G-FILE DATA
!------------------------------------------------------------
      IF(NOPSZG .GT. 0) THEN

! MAKE A FACILTY-DEPENDENT PATH TO THIS RDB FILE
!-----------------------------------------------
           CALL GETPTH (DBROOT, RDBFNG,   DBPATH)

!@NOS      OPEN(IOUXXG, FILE=RDBFNG, STATUS='OLD', FORM='UNFORMATTED',
!@NOS     1             ACCESS='DIRECT', RECL=NRBYTE*NOPSZG)

!@VAX      OPEN(IOUXXG, FILE = DBPATH, STATUS='OLD',
!@VAX     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@VAX     2             RECL=NRBYTE*NOPSZG)

!@DOS      OPEN(IOUXXG, FILE = DBPATH, STATUS='OLD',
!@DOS     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@DOS     2             RECL=NRBYTE*NOPSZG)

      OPEN(IOUXXG, FILE = DBPATH, STATUS='OLD',   &
     &             FORM='UNFORMATTED', ACCESS='DIRECT',   &
     &             RECL=NRBYTE*NOPSZG)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
!@MAC           OPEN(IOUXXG, FILE = DBPATH, STATUS='OLD',
!@MAC     1        FORM='UNFORMATTED',ACCESS='DIRECT', RECL=NRBYTE*NOPSZG)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
      END IF


!------------------------------------------------------------
! IF EXISTANT, OPEN RDB FILE FOR BINARY OUTPUT OF J-FILE DATA
!------------------------------------------------------------
      IF(NOPSZJ .GT. 0) THEN

! MAKE A FACILTY-DEPENDENT PATH TO THIS RDB FILE
!-----------------------------------------------
           CALL GETPTH (DBROOT, RDBFNJ,   DBPATH)

!@NOS      OPEN(IOUXXJ, FILE=RDBFNJ, STATUS='OLD', FORM='UNFORMATTED',
!@NOS     1             ACCESS='DIRECT', RECL=NRBYTE*NOPSZJ)

      OPEN(IOUXXJ, FILE = DBPATH, STATUS='OLD',   &
     &             FORM='UNFORMATTED', ACCESS='DIRECT',   &
     &             RECL=NRBYTE*NOPSZJ)

!@VAX      OPEN(IOUXXJ, FILE = DBPATH, STATUS='OLD',
!@VAX     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@VAX     2             RECL=NRBYTE*NOPSZJ)

!@DOS      OPEN(IOUXXJ, FILE = DBPATH, STATUS='OLD',
!@DOS     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@DOS     2             RECL=NRBYTE*NOPSZJ)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
!@MAC           OPEN(IOUXXJ, FILE = DBPATH, STATUS='OLD',
!@MAC     1        FORM='UNFORMATTED',ACCESS='DIRECT', RECL=NRBYTE*NOPSZJ)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
      END IF


!------------------------------------------------------------
! IF EXISTANT, OPEN RDB FILE FOR BINARY OUTPUT OF K-FILE DATA
!------------------------------------------------------------
      IF(NOPSZK .GT. 0) THEN

! MAKE A FACILTY-DEPENDENT PATH TO THIS RDB FILE
!-----------------------------------------------
           CALL GETPTH (DBROOT, RDBFNK,   DBPATH)

!@NOS      OPEN(IOUXXK, FILE=RDBFNK, STATUS='OLD', FORM='UNFORMATTED',
!@NOS     1             ACCESS='DIRECT', RECL=NRBYTE*NOPSZK)

      OPEN(IOUXXK, FILE = DBPATH, STATUS='OLD',   &
     &             FORM='UNFORMATTED', ACCESS='DIRECT',   &
     &             RECL=NRBYTE*NOPSZK)

!@VAX      OPEN(IOUXXK, FILE = DBPATH, STATUS='OLD',
!@VAX     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@VAX     2             RECL=NRBYTE*NOPSZK)

!@DOS      OPEN(IOUXXK, FILE = DBPATH, STATUS='OLD',
!@DOS     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@DOS     2             RECL=NRBYTE*NOPSZK)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
!@MAC           OPEN(IOUXXK, FILE = DBPATH, STATUS='OLD',
!@MAC     1        FORM='UNFORMATTED',ACCESS='DIRECT', RECL=NRBYTE*NOPSZK)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
      END IF


!------------------------------------------------------------
! IF EXISTANT, OPEN RDB FILE FOR BINARY OUTPUT OF F-FILE DATA
!------------------------------------------------------------
      IF(NOPSZF .GT. 0) THEN

! MAKE A FACILTY-DEPENDENT PATH TO THIS RDB FILE
!-----------------------------------------------
           CALL GETPTH (DBROOT, RDBFNK,   DBPATH)

!@NOS      OPEN(IOUXXF, FILE=RDBFNF, STATUS='OLD', FORM='UNFORMATTED',
!@NOS     1             ACCESS='DIRECT', RECL=NRBYTE*NOPSZF)

      OPEN(IOUXXF, FILE = DBPATH, STATUS='OLD',   &
     &             FORM='UNFORMATTED', ACCESS='DIRECT',   &
     &             RECL=NRBYTE*NOPSZF)

!@VAX      OPEN(IOUXXF, FILE = DBPATH, STATUS='OLD',
!@VAX     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@VAX     2             RECL=NRBYTE*NOPSZF)

!@DOS      OPEN(IOUXXF, FILE = DBPATH, STATUS='OLD',
!@DOS     1             FORM='UNFORMATTED', ACCESS='DIRECT',
!@DOS     2             RECL=NRBYTE*NOPSZF)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
!@MAC           OPEN(IOUXXF, FILE = DBPATH, STATUS='OLD',
!@MAC     1        FORM='UNFORMATTED',ACCESS='DIRECT', RECL=NRBYTE*NOPSZF)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@
      END IF




!------------------------------------------------------
! INITIALIZE NEXT-RECORD-POINTERS FOR RESULTS DATA BASE
!------------------------------------------------------
      NEXTA = 1
      NEXTT = 1
      NEXTB = 1

      NEXTG = 1
      NEXTJ = 1
      NEXTK = 1
      NEXTF = 1

!--------------------------------------------------------------
! EXTRACT RECORD-1 HEADER FOR VARIABLE SIZE ARRAY FILE (A-FILE)
!--------------------------------------------------------------
      READ(IOUXXA,REC=NEXTA) (RDBOUT(J),J=1,NOPSZA)
      NEXTA =NEXTA + 1

      NDUM   = NINT(RDBOUT(1))
      NBASA  = NINT(RDBOUT(2))
      NINCXA = NINT(RDBOUT(3))
      NRECA  = NINT(RDBOUT(4))

      NRPSXX = NINT(RDBOUT(5))
      NAPSXX = NINT(RDBOUT(6))
      NATTXX = NINT(RDBOUT(7))
      NOBJXX = NINT(RDBOUT(8))

      LASOBJ = NINT(RDBOUT(9))
      NAPRFP = NINT(RDBOUT(10))
      NTETH  = NINT(RDBOUT(11))
      NFINIT = NINT(RDBOUT(12))

      LGRVOP = NINT(RDBOUT(13))
      LATMOP = NINT(RDBOUT(14))
      LMAGOP = NINT(RDBOUT(15))
      LGLBOP = NINT(RDBOUT(16))
      LWNDOP = NINT(RDBOUT(17))
      LJULOP = NINT(RDBOUT(18))
      LEULOP = NINT(RDBOUT(19))

      LRYEAR = NINT(RDBOUT(20))
      LRMON  = NINT(RDBOUT(21))
      LRDAY  = NINT(RDBOUT(22))
      LRHOUR = NINT(RDBOUT(23))
      LRMIN  = NINT(RDBOUT(24))
      LRSEC  = NINT(RDBOUT(25))

!     RESERVED FOR EXPANSION = NINT(RDBOUT(26))
!     ................................
!     RESERVED FOR EXPANSION = NINT(RDBOUT(NAPS))

! INITIALIZE REF PT TIME FOR USE IN OPTIMIZED TOSLDW/TOSSTW
! (OBSOLETE COMMENT WITH ADVENT OF POINTER-REF TO OBJECTS)
      RPTIME = 0.0

!-------------------------------------------------
! EXTRACT RECORD-1 HEADER FOR TOSS OBJECT (T-FILE)
!-------------------------------------------------
      READ(IOUXXT,REC=NEXTT) (RDBOUT(J),J=1,NOPSZT)
      NEXTT =NEXTT + 1

      NDUM   = NINT(RDBOUT(1))
      NBAST  = NINT(RDBOUT(2))
      NINCXT = NINT(RDBOUT(3))
      NRECT  = NINT(RDBOUT(4))
      LASOBJ = NINT(RDBOUT(5))

! FOR WORKING ARRAY
!     NLW = NINT(RDBOUT(6))  NOT EXECUTED, AS IT IS A PARAMETER
!     NJW = NINT(RDBOUT(7))  NOT EXECUTED, AS IT IS A PARAMETER
!     NFW = NINT(RDBOUT(8))  NOT EXECUTED, AS IT IS A PARAMETER
!     NDW = NINT(RDBOUT(9))  NOT EXECUTED, AS IT IS A PARAMETER

! FOR EACH OF THE TOSS OBJECTS
!     NF2 = NINT(RDBOUT(10))  NOT EXECUTED, AS IT IS A PARAMETER
      NLZZ2 = NINT(RDBOUT(11))

!     NF3 = NINT(RDBOUT(12))  NOT EXECUTED, AS IT IS A PARAMETER
      NLZZ3 = NINT(RDBOUT(13))

!     NF4 = NINT(RDBOUT(14))  NOT EXECUTED, AS IT IS A PARAMETER
      NLZZ4 = NINT(RDBOUT(15))

!     NF5 = NINT(RDBOUT(16))  NOT EXECUTED, AS IT IS A PARAMETER
      NLZZ5 = NINT(RDBOUT(17))

!     NF6 = NINT(RDBOUT(18))  NOT EXECUTED, AS IT IS A PARAMETER
      NLZZ6 = NINT(RDBOUT(19))

!     NF7 = NINT(RDBOUT(20))  NOT EXECUTED, AS IT IS A PARAMETER
      NLZZ7 = NINT(RDBOUT(21))

!     NF8 = NINT(RDBOUT(22))  NOT EXECUTED, AS IT IS A PARAMETER
      NLZZ8 = NINT(RDBOUT(23))

!     NF9 = NINT(RDBOUT(24))  NOT EXECUTED, AS IT IS A PARAMETER
      NLZZ9 = NINT(RDBOUT(25))

!     RTD = RDBOUT(26)       no longer used
!     DTR = RDBOUT(27)       no longer used

      OMEGAE = RDBOUT(28)
      RE = RDBOUT(29)
      GE = RDBOUT(30)

! STATE LAST ELEMENT USED TO THIS POINT IN HEADER
      LASELT = 30

! EXTRACT SOME SKIP DAMPER PARAMETERS FOR EACH OBJECT
      JCNT = 0

      DO 20 J = 2,NOBJ

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
         CALL TOSLDW(J)

         JDUM = 5*(J-2) + 1 + LASELT

         LOBJSR = NINT(RDBOUT(JDUM  ))
         LAPPSR = NINT(RDBOUT(JDUM+1))
         LTHBAS = NINT(RDBOUT(JDUM+2))
         LTHDMP = NINT(RDBOUT(JDUM+3))
         LNCHOR = NINT(RDBOUT(JDUM+4))

         JCNT = JCNT + 5

20    CONTINUE

! STATE LAST ELEMENT USED TO THIS POINT IN HEADER
      LASELT = LASELT + JCNT




! EXTRACT PART OF TETHER CHAIN CONNECTIVITY ARRAY FOR POST PROCESSING
         DO 22 JCH = 1,4
            DO 22 JSG = 1,3
               DO 22 JEL = 1,3
                  LASELT = LASELT + 1
                  CHAIN(JCH,JSG,JEL) = RDBOUT(LASELT)
22    CONTINUE


! STORE TOSS-WIDE TETH GRAV, AERO, AND ELEC LOADS ACTIVATION FLAG
      LOPG = RDBOUT(LASELT  )
      LOPA = RDBOUT(LASELT+1)
      LOPE = RDBOUT(LASELT+2)

! STATE LAST ELEMENT USED TO THIS POINT IN HEADER
      LASELT = LASELT + 3


! EXTRACT 3DOF/6DOF FLAG FOR EACH OBJECT
      DO 21 J = 2,NOBJ

         LASELT = LASELT + 1

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
         CALL TOSLDW(J)

         JDUM = (J-2) + LASELT

         JNOMOM = NINT(RDBOUT(JDUM  ))

21    CONTINUE



!<><><><><><><><><><><><><><><><><><><><><><><><>
!
!   FOR MORE TOSS OBJECTS, ADD SIMILAR CODE HERE
!
!<><><><><><><><><><><><><><><><><><><><><><><><>


!-----------------------------------------------------
! EXTRACT RECORD-1 HEADER FOR BEAD MODEL SOLN (B-FILE)
!-----------------------------------------------------
      IF(NOPSZB .GT. 0) THEN
        READ(IOUXXB,REC=NEXTB) (RDBOUT(J),J=1,NOPSZB)
        NEXTB =NEXTB + 1

        NDUM   = NINT(RDBOUT(1))
        NBASB  = NINT(RDBOUT(2))
        NINCXB = NINT(RDBOUT(3))
        NRECB  = NINT(RDBOUT(4))
        NFINIT = NINT(RDBOUT(5))

! EXTRACT THERMAL ACTIVATION FLAGS APPLICABLE TO ALL FINITE SOLUTIONS
       LQSOLR = NINT(RDBOUT( 6))
       LQALB  = NINT(RDBOUT( 7))
       LQEBB  = NINT(RDBOUT( 8))
       LQAERO = NINT(RDBOUT( 9))
       LQOHMS = NINT(RDBOUT(10))
       LQTRAD = NINT(RDBOUT(11))
       LQCOND = NINT(RDBOUT(12))
       LEXPND = NINT(RDBOUT(13))




! FOR EACH BEAD MODEL SOLN ............
        DO 40 J=1,NFSOLN

        JDUM = 7*(J-1) + 1 + 13

            NBEADS(J) = NINT(RDBOUT(JDUM  ))
            NSBRK1(J) = NINT(RDBOUT(JDUM+1))
            NSBRK2(J) = NINT(RDBOUT(JDUM+2))
            TSBRK1(J) = RDBOUT(JDUM+3)
            TSBRK2(J) = RDBOUT(JDUM+4)
            JASIGN(J) = RDBOUT(JDUM+5)
            NFTYPE(J) = RDBOUT(JDUM+6)

40      CONTINUE

      END IF

!<><><><><><><><><><><><><><><><><><><><><><><><>
!
!   FOR MORE BEAD SOLNS, UP THE INDEX PAST 9
!
!<><><><><><><><><><><><><><><><><><><><><><><><>



!------------------------------------------------------------
! IF EXISTANT, EXTRACT RECORD-1 HEADER FOR WILD CARD (G-FILE)
!------------------------------------------------------------
      IF(NOPSZG .GT. 0) THEN

         READ(IOUXXG,REC=NEXTG) (RDBOUT(J),J=1,NOPSZG)
         NEXTG =NEXTG + 1

! CALL ROUTINE TO EXTRACT REC 1 DATA ASSOCIATED G-FILE
         CALL PULG1

      END IF


!------------------------------------------------------------
! IF EXISTANT, EXTRACT RECORD-1 HEADER FOR WILD CARD (J-FILE)
!------------------------------------------------------------
      IF(NOPSZJ .GT. 0) THEN

         READ(IOUXXJ,REC=NEXTJ) (RDBOUT(J),J=1,NOPSZJ)
         NEXTJ =NEXTJ + 1

! CALL ROUTINE TO EXTRACT REC 1 DATA ASSOCIATED J-FILE
         CALL PULJ1

      END IF


!------------------------------------------------------------
! IF EXISTANT, EXTRACT RECORD-1 HEADER FOR WILD CARD (K-FILE)
!------------------------------------------------------------
      IF(NOPSZK .GT. 0) THEN

         READ(IOUXXK,REC=NEXTK) (RDBOUT(J),J=1,NOPSZK)
         NEXTK =NEXTK + 1

! CALL ROUTINE TO EXTRACT REC 1 DATA ASSOCIATED K-FILE
         CALL PULK1

      END IF


!------------------------------------------------------------
! IF EXISTANT, EXTRACT RECORD-1 HEADER FOR WILD CARD (F-FILE)
!------------------------------------------------------------
      IF(NOPSZF .GT. 0) THEN

         READ(IOUXXF,REC=NEXTF) (RDBOUT(J),J=1,NOPSZF)
         NEXTF =NEXTF + 1

! CALL ROUTINE TO EXTRACT REC 1 DATA ASSOCIATED F-FILE
         CALL PULF1

      END IF


!**************************************************************
! PERFORM TOSS-RDB VERSION/POST-PROCESSOR COMPATIBILITY TESTING
!**************************************************************
! TEST FOR POSSIBLE SERIOUS RDB COMPATIBILITY THAT COULD BOMB YOU
      IF(NRPSXX .GT. NRPS) GO TO 550
      IF(NAPSXX .GT. NAPS) GO TO 550
      IF(NATTXX .GT. NATT) GO TO 550
      IF(NOBJXX .GT. NOBJ) GO TO 550

! TEST FOR LESS SERIOUS RDB COMPATIBILITY PROBLEM (PROBABLY OK)
      IF(NRPSXX .LT. NRPS) GO TO 560
      IF(NAPSXX .LT. NAPS) GO TO 560
      IF(NATTXX .LT. NATT) GO TO 560
      IF(NOBJXX .LT. NOBJ) GO TO 560

      GO TO 1000


! GIVE SERIOUS RDB COMPATIBILITY WARNING
!---------------------------------------
550   WRITE(IRBERR,551)
551   FORMAT(//'IN PULOPN: SERIOUS RDB INCOMPATIBILITY IS POSSIBLE.',/,   &
     &  'NRPS, OR NAPS, OR NATT, OR NOBJ EXCEED CURRENT TOSS VALUES')

      GO TO 1000

! GIVE MILD RDB COMPATIBILITY WARNING
!---------------------------------------
560   WRITE(IRBERR,561)
561   FORMAT(//'IN PULOPN: MILD RDB INCOMPATIBILITY IS POSSIBLE. ',/,   &
     & 'NRPS, OR NAPS, OR NATT, OR NOBJ ARE .LT. CURRENT TOSS VALUES')


! STANDARD RETURN
1000  CONTINUE

      RETURN
      END
