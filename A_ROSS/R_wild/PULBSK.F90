! ROUTINE: PULBSK
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************
!*************************************
!
      SUBROUTINE PULBSK
!
!*************************************
!*************************************
! THIS SUBROUTINE EXTRACTS BASIC RDB K-FILE DATA

! THIS ROUTINE IS AN EXACT FUNCTIONAL RECIPROCAL
! OF ROUTINE "POPBSK"


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_ROSS.i"
      include "../../A_HDR/COM_BOOM.i"




!**********************************************************
! EXTRACT BASIC DATA FROM READ ARRAY TO YOUR WORKING ARRAYS
!**********************************************************
! INITIALIZE NEXT ARRAY ELEMENT POINTER TO BE EXTRACTED
      ISKDUM = 1

      TIMBOM = RDBOUT(ISKDUM)
      ISKDUM = ISKDUM + 1


! EXTRACT Y AXIS DATA
!--------------------
      DO 10 J=1,NBOMY
         JDUM = ISKDUM +J - 1
         QBY(J)   = RDBOUT(JDUM+0*NBOMY)
         QBYD(J)  = RDBOUT(JDUM+1*NBOMY)
         QBYDD(J) = RDBOUT(JDUM+2*NBOMY)
10    CONTINUE

! TALLY THIS BLOCK UTILIZATION
      ISKDUM = ISKDUM + 3*NBOMY



! EXTRACT Z AXIS DATA
!--------------------
      DO 20 J=1,NBOMZ
         JDUM = ISKDUM +J - 1
         QBZ(J)   = RDBOUT(JDUM+0*NBOMZ)
         QBZD(J)  = RDBOUT(JDUM+1*NBOMZ)
         QBZDD(J) = RDBOUT(JDUM+2*NBOMZ)
20    CONTINUE

! TALLY THIS BLOCK UTILIZATION
      ISKDUM = ISKDUM + 3*NBOMZ



! EXTRACT A BUNCH OF X3 STUFF
!----------------------------
      DO 30 J=1,3
         JDUM = ISKDUM + J - 1
         TENSF(J)  = RDBOUT(JDUM   )
         FORBF(J)  = RDBOUT(JDUM+ 3)
         CUPBF(J)  = RDBOUT(JDUM+ 6)
         TIPF(J)   = RDBOUT(JDUM+ 9)
         TIPDF(J)  = RDBOUT(JDUM+12)
         TIPDDF(J) = RDBOUT(JDUM+15)
         AGBSF(J)  = RDBOUT(JDUM+18)
30    CONTINUE

! TALLY THIS BLOCK UTILIZATION
      ISKDUM = ISKDUM + 7*3


! EXTRACT SOME MISCELLANEOUS DATA ASSOCIATED WITH TIP FRAME
         OMP(1)  = RDBOUT(ISKDUM     )
         OMP(2)  = RDBOUT(ISKDUM +  1)
         OMP(3)  = RDBOUT(ISKDUM +  2)

         OMPD(1) = RDBOUT(ISKDUM +  3)
         OMPD(2) = RDBOUT(ISKDUM +  4)
         OMPD(3) = RDBOUT(ISKDUM +  5)

         PSIP    = RDBOUT(ISKDUM +  6)
         PSIPD   = RDBOUT(ISKDUM +  7)
         PSIPDD  = RDBOUT(ISKDUM +  8)

         THTP    = RDBOUT(ISKDUM +  9)
         THTPD   = RDBOUT(ISKDUM + 10)
         THTPDD  = RDBOUT(ISKDUM + 11)

         GPF(1,1) = RDBOUT(ISKDUM + 12)
         GPF(1,2) = RDBOUT(ISKDUM + 13)
         GPF(1,3) = RDBOUT(ISKDUM + 14)

         GPF(2,1) = RDBOUT(ISKDUM + 15)
         GPF(2,2) = RDBOUT(ISKDUM + 16)
         GPF(2,3) = RDBOUT(ISKDUM + 17)

         GPF(3,1) = RDBOUT(ISKDUM + 18)
         GPF(3,2) = RDBOUT(ISKDUM + 19)
         GPF(3,3) = RDBOUT(ISKDUM + 20)

         COUPF(1) = RDBOUT(ISKDUM + 21)
         COUPF(2) = RDBOUT(ISKDUM + 22)
         COUPF(3) = RDBOUT(ISKDUM + 23)

         PUFCY(1) = RDBOUT(ISKDUM + 24)
         PUFCY(2) = RDBOUT(ISKDUM + 25)
         PUFCY(3) = RDBOUT(ISKDUM + 26)
         PUFCY(4) = RDBOUT(ISKDUM + 27)
         PUFCY(5) = RDBOUT(ISKDUM + 28)

         PUFCZ(1) = RDBOUT(ISKDUM + 29)
         PUFCZ(2) = RDBOUT(ISKDUM + 30)
         PUFCZ(3) = RDBOUT(ISKDUM + 31)
         PUFCZ(4) = RDBOUT(ISKDUM + 32)
         PUFCZ(5) = RDBOUT(ISKDUM + 33)

! TALLY THIS BLOCK UTILIZATION
      ISKDUM = ISKDUM + 34

      RETURN
      END
