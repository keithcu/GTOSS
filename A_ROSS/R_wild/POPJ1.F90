! ROUTINE: POPJ1
! %Z%GTOSS %M% H.5 code v02.01
!              H.5 code v02.01 Activates code to archive mmds deployer
!---------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!********************************
!********************************
!
      SUBROUTINE POPJ1(NUMITM)
!
!*******************************
!*******************************
! THIS SUBROUTINE POPULATES RECORD 1 OF THE WILD-CARD
! RDB J-FILE.  IT IS USED TO SAVE NON-TIME VARYING
! INFO ASSOCIATED WITH THE ENTITY USING THIS FILE.

! IN THIS CASE (GTOSS), IT IS USED TO ARCHIVE DATA
! PERTAINING TO THE TSS TYPE DEPLOYER SCENARIO

! THE USER MUST STATE THE NUMBER OF ITEMS BEING PUT
! INTO HEADER RECORD AS A RETURNED VALUE: NUMITM


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_ROSS.i"





! AS A COURTESY, STASH AWAY CERTAIN J-FILE ATTRIBUTES
!----------------------------------------------------
      RDBOUT(1) = 0.0
      RDBOUT(2) = NBASJ
      RDBOUT(3) = NINCXJ
      RDBOUT(4) = NRECJ

! NO INTERESTING NON-TIME VARYING DATA TO REPORT IN THIS ARCHIVE

! RETURN NUMER OF ITEMS TO WRITTEN IN HEADER
      NUMITM = 4

      RETURN
      END
