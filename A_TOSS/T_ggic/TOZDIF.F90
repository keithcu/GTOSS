! ROUTINE: TOZDIF
! %Z%GTOSS %M% H.10 code v01.03
!              H.10 code v01.03 Fixed bug in GOI insertion
!-----------------------------------------------------------------
!              H.5 code v01.02 Changed EARTH to PLANET in comments
!-----------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************
!*************************************
!
      SUBROUTINE TOZDIF
!
!*************************************
!*************************************
! THIS SUBROUTINE CALCULATES THE DIFFERENTIAL GRAVITY
! AND RELATIVE POSITIONS BETWEEN END END OBJECTS IN TETHER
! CHAINS, AND SETS IN OBJECT MASSES AS NEEDED


! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND 
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.i"






! SOME MORE VECTOR SCRATCH STORAGE
      DIMENSION G1DUM(3),G2DUM(3), ZDUM(3), V1DUM(3),V2DUM(3)
      DIMENSION GSSDUM(3),  FMSDUM(3)

! WE NEED A ZERO VECTOR LATER ON
      ZDUM(1) = 0.0
      ZDUM(2) = 0.0
      ZDUM(3) = 0.0

! FOR EACH SEGMENT WITHIN EACH CONNECTIVITY CHAIN,
! FIND THE STATE OF THE END OBJECTS, THE GRAVITATIONAL
! ACCEL INCREMENT, AND THE ANGULAR RATE OF THE ORBITAL
! FRAME AT THE ORIGIN OBJECT FOR THE FRAME

!******************************
!******************************
! START BIG LOOP FOR EACH CHAIN
!******************************
!******************************
      DO 800 JCH = 1,4

! CHAIN IS CONSIDERED EMPTY IF SEGMENT 1 HAS ZERO TETHER NUMBER
         IF(CHAIN(JCH,1,1) .EQ. 0.0) GO TO 800


! FIND INER TO ORB FRAME XFORMATION FOR OBJECT AT ORIGIN OF CHAIN
! (SAVE AS SCRATCH MATRIX TOSMX1 FOR ALL SEGMENTS IN THIS CHAIN)
!---------------------------------------------------------------
         JORGIN = NINT( ABS(CHAIN(JCH,1,2)) )

! FIRST, ASSUME ORIGIN OBJECT IS THE REF PT (SET TOSMX1 = GOI)
         CALL MATMOV (0,RPGOI,  TOSMX1)

! IF ORIGIN OBJ IS A TOSS OBJECT INSTEAD
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
         IF(JORGIN .GT. 1) CALL TOSLDW (JORGIN)
! THEN CALL ROUTINE TO DEFINE ORB FRAME ASSOCIATED W/OBJECTS STATE
! NOTE: AT THIS POINT, RI (=RPI+RBI) IS NOT CALCULATED YET IN TOSS
         IF(JORGIN .GT. 1) CALL VECSUM (RPI,RBI,     RI)
         IF(JORGIN .GT. 1) CALL VECSUM (RPID,RBID,  RID)
         IF(JORGIN .GT. 1) CALL ORBFRM (RI,RID,  TOSMX1)

! STASH THE INERTIAL-TO-ORBITAL FRAME XFORMATION MATRIX
! ASSOCIATED WITH THE CHAIN ORIGINATION OBJECT, FOR OTHERS USE
!-------------------------------------------------------------
         CHAIN(JCH,1,13) = TOSMX1(1,1)
         CHAIN(JCH,1,14) = TOSMX1(1,2)
         CHAIN(JCH,1,15) = TOSMX1(1,3)

         CHAIN(JCH,2,13) = TOSMX1(2,1)
         CHAIN(JCH,2,14) = TOSMX1(2,2)
         CHAIN(JCH,2,15) = TOSMX1(2,3)

         CHAIN(JCH,3,13) = TOSMX1(3,1)
         CHAIN(JCH,3,14) = TOSMX1(3,2)
         CHAIN(JCH,3,15) = TOSMX1(3,3)


! CALC ANGULAR VEL OF ORB FRAME AT CHAIN ORIGINATION OBJECT
!----------------------------------------------------------
         IF(JORGIN .EQ. 1) CALL CROSS(RPI,RPID, TOSVX3)
         IF(JORGIN .GT. 1) CALL CROSS(RI,RID,   TOSVX3)

         CNDUM = VECMAG(TOSVX3)

         IF(JORGIN .EQ. 1) CDDUM = DOT(RPI,RPI)
         IF(JORGIN .GT. 1) CDDUM = DOT(RI, RI )

         OMODUM = CNDUM/CDDUM



!***********************************
! FOR EACH SEGMENT, DO THE FOLLOWING
!***********************************
         DO 700 JSG = 1,3

! SEGMENT IS CONSIDERED EMPTY IF SEGMENT HAS ZERO TETHER NUMBER
         JTETH = NINT( CHAIN(JCH,JSG,1) )
         IF(JTETH .EQ. 0) GO TO 700


! EXTRACT OBJECT NUMBERS AT EACH END OF THIS SEGMENT
            JORGIN = NINT( ABS(CHAIN(JCH,JSG,2)) )
            JOTHER = NINT( ABS(CHAIN(JCH,JSG,3)) )

! IF A TOSS OBJECT IS LOCATED AT ORIGIN END
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
!-------------------------------------------------------------
            IF(JORGIN .GT. 1) CALL TOSLDW(JORGIN)

! FIND MASS OF OBJECT AT ORIGIN END OF THIS SEGMENT
!--------------------------------------------------
            IF(JORGIN .EQ. 1) CMIDUM = RPMAST
            IF(JORGIN .GT. 1) CMIDUM = DMASS


! FIND POSITION STATES AT ORIGIN END OF THIS SEG (SAVE IN TOSVX1,V1DUM)
!----------------------------------------------------------------------
! POSITION WR/T INERTIAL POINT (IN INERTIAL FRAME)
! NOTE: AT THIS POINT, RI (=RPI+RBI) IS NOT CALCULATED YET IN TOSS
            IF(JORGIN .EQ. 1) CALL VECMOV(RPI,     TOSVX1)
            IF(JORGIN .GT. 1) CALL VECSUM(RPI,RBI, TOSVX1)

! POSITION WR/T TOSS REF POINT (IN INERTIAL FRAME)
            IF(JORGIN .EQ. 1) CALL VECMOV(ZDUM,  V1DUM)
            IF(JORGIN .GT. 1) CALL VECMOV(RBI,   V1DUM)


! IF A TOSS OBJECT IS LOCATED AT THE OTHER END
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
!----------------------------------------------------------------
            IF(JOTHER .GT. 1) CALL TOSLDW(JOTHER)

! FIND MASS OF OBJECT AT OTHER END OF THIS SEGMENT
!-------------------------------------------------
            IF(JOTHER .EQ. 1) CMJDUM = RPMAST
            IF(JOTHER .GT. 1) CMJDUM = DMASS


! FIND POSITION STATES AT OTHER END OF THIS SEG (SAVE IN TOSVX2,V2DUM)
!--------------------------------------------------------------------
! POSITION WR/T INERTIAL POINT (IN INERTIAL FRAME)
! NOTE: RI (=RPI+RBI) IS NOT CALCULATED YET AT THIS POINT
            IF(JOTHER .EQ. 1) CALL VECMOV(RPI,     TOSVX2)
            IF(JOTHER .GT. 1) CALL VECSUM(RPI,RBI, TOSVX2)

! POSITION WR/T TOSS REF POINT (IN INERTIAL FRAME)
            IF(JOTHER .EQ. 1) CALL VECMOV(ZDUM,  V2DUM)
            IF(JOTHER .GT. 1) CALL VECMOV(RBI,   V2DUM)


!-----------------------------------------------
! CALCULATE INCREMENT IN GRAV ACCEL OVER SEGMENT
!-----------------------------------------------

! FIND ACCEL DUE TO GRAV AT ORIGIN END
!-------------------------------------
! FIND PLANET FRAME COMPONENTS OF POSITION VECTOR
           CALL EITEF (LJULOP,TOSTIM,TOSVX1,   TOSVX4)
! GET PLANET FRAME COMPONENTS OF ACCELERATION OF GRAVITY
           CALL GRAV (LGRVOP,TOSTIM,TOSVX4,    TOSVX5)
! TRANSFORM PLANET FRAME COMPONENTS OF ACCELERATION TO INER FRAME
           CALL EFTEI (LJULOP,TOSTIM,TOSVX5,    G1DUM)

! SAVE GRAV ACCEL OF SEG 1 ORIGIN FOR LATER USE
            IF(JSG .EQ. 1) CALL VECMOV(G1DUM,  GSSDUM)

! FIND ACCEL DUE TO GRAV AT OTHER END
!------------------------------------
! FIND PLANET FRAME COMPONENTS OF POSITION VECTOR
           CALL EITEF (LJULOP,TOSTIM,TOSVX2,   TOSVX4)
! GET PLANET FRAME COMPONENTS OF ACCELERATION OF GRAVITY
           CALL GRAV (LGRVOP,TOSTIM,TOSVX4,    TOSVX5)
! TRANSFORM PLANET FRAME COMPONENTS OF ACCELERATION TO INER FRAME
           CALL EFTEI (LJULOP,TOSTIM,TOSVX5,    G2DUM)

! FIND GRAV ACCEL DIFFERENCE VECTOR (INER FRAME)
            CALL VECDIF(G1DUM,G2DUM,  TOSVX3)

! TRANSFORM TO ORBITAL FRAME
            CALL MATVEC(0,TOSMX1,TOSVX3,  TOSVX3)

! SAVE K UNIT VECTOR COMPONENT
            AIJDUM = TOSVX3(3)


! CALCULATE THE RANGE COMPONENT OF SEGMENT
!-----------------------------------------
            CALL VECDIF(V2DUM,V1DUM,  TOSVX3)

! TRANSFORM TO ORBITAL FRAME
            CALL MATVEC(0,TOSMX1,TOSVX3,  TOSVX3)

! SAVE 'K' UNIT VECTOR COMPONENT
            DIJDUM = TOSVX3(3)

! COLLECT DATA AND INSERT FOR THIS SEGMENT
!-----------------------------------------
            CHAIN(JCH,JSG, 4) = CMIDUM
            CHAIN(JCH,JSG, 5) = AIJDUM
            CHAIN(JCH,JSG, 6) = DIJDUM
            CHAIN(JCH,JSG,10) = CMJDUM


! SOME SPECIAL COLLECTION (ORB FRAME ANG VEL OF ORIGIN OBJECT-SEG 1)
            IF(JSG .EQ. 1) CHAIN(JCH,JSG,11) = OMODUM**2


!*************************
! END OF LOOP FOR SEGMENTS
!*************************
700   CONTINUE


!-------------------------------------------------------------
!-------------------------------------------------------------
! FOR EACH CHAIN, CALC POS AND ACCEL OF OVERALL CENTER-OF-MASS
!-------------------------------------------------------------
!-------------------------------------------------------------

! FIRST, CALCULATE TOTAL GRAVITY FORCE ON EACH OBJECT
!----------------------------------------------------
! TRANSFORM ORIGIN OBJECT ACCEL OF GRAV TO ORBITAL FRAME
       CALL MATVEC(0,TOSMX1,GSSDUM,  TOSVX3)

! ORIGIN OBJECTS ACCEL OF GRAV = K UNIT VECTOR COMPONENT
       GA1DUM = TOSVX3(3)
! ORIGIN TETHER-SEGMENTS MID POINT ACCEL OF GRAVITY
       GS1DUM = GA1DUM - 0.5*CHAIN(JCH,1,5)

! NEXT OBJECTS ACCEL OF GRAV = PREVIOUS ACCEL - ACCEL DIFFERENCE
       GA2DUM = GA1DUM - CHAIN(JCH,1,5)
! NEXT TETHER SEGMENTS MID POINT ACCEL OF GRAVITY
       GS2DUM = GA2DUM - 0.5*CHAIN(JCH,2,5)

! NEXT OBJECTS ACCEL OF GRAV
       GA3DUM = GA2DUM - CHAIN(JCH,2,5)
! NEXT TETHER SEGMENTS MID POINT ACCEL OF GRAVITY
       GS3DUM = GA3DUM - 0.5*CHAIN(JCH,3,5)

! LAST OBJECTS ACCEL OF GRAV
       GA4DUM = GA3DUM - CHAIN(JCH,3,5)


!---------------------------
! DO CHAIN MASS CALCULATIONS
!---------------------------

! EXTRACT OBJECT MASSES FROM THIS CHAIN ARRAY
!--------------------------------------------
       CM1DUM = CHAIN(JCH,1, 4)
       CM2DUM = CHAIN(JCH,1,10)
       CM3DUM = CHAIN(JCH,2,10)
       CM4DUM = CHAIN(JCH,3,10)

! FIND SEGMENT FINITE TETHER MASSES FOR THIS CHAIN ARRAY
!-------------------------------------------------------
       DO 300 JSEG = 1,3

          FMSDUM(JSEG) = 0.0

! SEE IF A TOSS TETHER IS ASSOCIATED WITH SEGMENT
          JTETH = NINT( CHAIN(JCH,JSEG,1) )
          IF(JTETH .GT. 0) FMSDUM(JSEG) = TTMASS(JTETH)

! END OF SEGMENT LOOP FOR GETTING FINITE TETHER MASS
!---------------------------------------------------
300    CONTINUE


! CALC TOTAL MASS OF CHAIN (OBJECTS + FINITE TETHERS)
!---------------------------------------------------
       CMTDUM = CM1DUM + CM2DUM + CM3DUM + CM4DUM   &
     &          + FMSDUM(1) + FMSDUM(2) + FMSDUM(3)


!------------------------------------------------------------
! DO OBJECT AND TETHER DISTANCE CALC WR/T ORIGIN END OF CHAIN
!------------------------------------------------------------

! EXTRACT DISTANCES BETWEEN OBJECTS FROM THIS CHAIN ARRAY
!--------------------------------------------------------
       D12DUM = CHAIN(JCH,1,6)
       D23DUM = CHAIN(JCH,2,6)
       D34DUM = CHAIN(JCH,3,6)

! CALC DISTANCES FROM ORIGIN END TO EACH OBJECT
       D12DUM = D12DUM
       D13DUM = D12DUM + D23DUM
       D14DUM = D12DUM + D23DUM + D34DUM

! CALC DISTANCES FROM ORIGIN END TO MID POINT OF EACH SEGMENT
       S11DUM = 0.5*D12DUM
       S12DUM = D12DUM + 0.5*D23DUM
       S13DUM = D12DUM + D23DUM + 0.5*D34DUM

! CALCULATE TOTAL GRAV FORCE ON OBJECTS IN SEGMENT
!-------------------------------------------------
       FGDUM = CM1DUM*GA1DUM + FMSDUM(1)*GS1DUM   &
     &       + CM2DUM*GA2DUM + FMSDUM(2)*GS2DUM   &
     &       + CM3DUM*GA3DUM + FMSDUM(3)*GS3DUM   &
     &       + CM4DUM*GA4DUM

! CHAIN CENTER-OF-MASS ACCEL = TOTAL CHAIN GRAV FORCE/TOTAL CHAIN MASS
!---------------------------
       ACGDUM = FGDUM/CMTDUM


!---------------------------------------------------
! FIND POS WR/T CHAIN ORIGIN OF CHAIN CENTER-OF-MASS
!---------------------------------------------------
       CMODUM = CM2DUM*D12DUM + FMSDUM(1)*S11DUM   &
     &        + CM3DUM*D13DUM + FMSDUM(2)*S12DUM   &
     &        + CM4DUM*D14DUM + FMSDUM(3)*S13DUM

! CHAIN CENTER-OF-MASS POSITION
!------------------------------
       DCMDUM= CMODUM/CMTDUM


!-------------------------------------------------------------
! FIND POS OF THE X-END OF EACH SEG TETHER WR/T CENTER-OF-MASS
!-------------------------------------------------------------
! FOR SEGMENT 1
!--------------
       JOBORG = CHAIN(JCH,1,2)
! ASSUME ORIG-END IS X-END OF TETHER (THEN CHECK SIGN OF OBJ NUMBER)
       DC1DUM =DCMDUM
       IF(JOBORG .LT. 0) DC1DUM = DCMDUM - D12DUM


! FOR SEGMENT 2
!--------------
       JOBORG = CHAIN(JCH,2,2)
! ASSUME ORIG-END IS X-END OF TETHER (THEN CHECK SIGN OF OBJ NUMBER)
       DC2DUM = DCMDUM - D12DUM
       IF(JOBORG .LT. 0) DC2DUM = DCMDUM - D13DUM


! FOR SEGMENT 3
!--------------
       JOBORG = CHAIN(JCH,3,2)
! ASSUME ORIG-END IS X-END OF TETHER (THEN CHECK SIGN OF OBJ NUMBER)
       DC3DUM = DCMDUM - D13DUM
       IF(JOBORG .LT. 0) DC3DUM = DCMDUM - D14DUM


!------------------------------------------------------
! CALCULATE TOTAL ACCEL OF THE X-END OF EACH SEG TETHER
!------------------------------------------------------
       OMSDUM = CHAIN(JCH,1,11)

! STASH IN CHAIN ARRAY FOR USE BY OTHERS
       CHAIN(JCH,1,12) = ACGDUM + DC1DUM*OMSDUM
       CHAIN(JCH,2,12) = ACGDUM + DC2DUM*OMSDUM
       CHAIN(JCH,3,12) = ACGDUM + DC3DUM*OMSDUM


!***************************
! END OF LOOP FOR EACH CHAIN
!***************************
800   CONTINUE

      RETURN
      END
