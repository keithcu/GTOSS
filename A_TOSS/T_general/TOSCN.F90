! ROUTINE: TOSCN
! %Z%GTOSS %M% H.12 code v02.3
!              H.12 code v02.3 Modified control logic, added new options
!---------------------------------------------------------------------------
!              H.11 code v02.2 Added exec call to new routineS TOSCNSK1 & 2
!---------------------------------------------------------------------------
!              H.10 code v02.1 Added exec call to new routine TOSCNO
!                              Added exec call to new routine TOSCNAP
!                              Checked obj no. against LASOBJ instead of NOBJ
!--------------------------------------------------------------------
!              H.9 code v02.02 Added exec call for TOSCNA (opt 5)
!                              Now zeros cntrl mom/forces before calls
!----------------------------------------------------------------------
!              H.3 code v02.01 Added exec calls for 3 new TOSCN options
!----------------------------------------------------------------------
!              H.2 code v01.02 now stores individual link arm loads
!------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!********************************
!********************************
!
      SUBROUTINE TOSCN(JBODY)
!
!********************************
!********************************
! THIS ROUTINE PRESIDES OVER TOSS CONTROL SYSTEM SIMULATIONS
! ONE RESULT OF WHICH ARE CONTROL FORCES BEING APPLIED TO BODIES

! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND 
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS

      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.i"



!*************************************************************
! ZERO CONTROL FORCES/MOMENTS TO ACCUMULATE ACROSS ALL OPTIONS
!*************************************************************
      FCB(1) = 0.0
      FCB(2) = 0.0
      FCB(3) = 0.0

      GCB(1) = 0.0
      GCB(2) = 0.0
      GCB(3) = 0.0


!--------------------------------------------
! DO CONTROL CALCULATIONS ONLY IF APPROPRIATE
!--------------------------------------------
      IF(LCONT .EQ. 0) GO TO 10000


!**********************************************
! SEE IF CONTROL FORCE CALCULATIONS ARE DESIRED
!**********************************************
! CHECK FOR GENERAL CONTROL ACTIVATION
      IF(LCONT .EQ. 1) GO TO 4000

! CHECK FOR OBJECT-TEST FORCES AND MOMENTS
      IF(LCONT .EQ. 2) CALL TOSCNF(JBODY)

! CHECK FOR PASSIVE SKIPROPE DAMPER FORCES
      IF(LCONT .EQ. 3) CALL TOSCND(JBODY)

! CHECK FOR LIBRATION CONTROL FORCES
      IF(LCONT .EQ. 4) CALL TOSCNL(JBODY)

! CHECK FOR GENERAL TOSS OBJ ATTITUDE CONTROL OPTIONS
      IF(LCONT .EQ. 5) CALL TOSCNA(JBODY)

! CHECK FOR GENERAL TOSS OBJ ON-ORBIT MANEUVER THRUSTING OPTION
      IF(LCONT .EQ. 6) CALL TOSCNO(JBODY)

! CHECK FOR GENERAL TOSS OBJ AOA/ATTITUDE-PROFILE CONTROL OPTION
      IF(LCONT .EQ. 7) CALL TOSCNAP(JBODY)

! CHECK FOR GENERAL TOSS OBJ NOMINAL ROCKET THRUST CONTROL OPTION
      IF(LCONT .EQ. 8) CALL TOSCNTH(JBODY)

! CHECK FOR TOSS OBJ CONTROL VIA DIFFERENTIAL TETH TENSIONS VERS 1
      IF(LCONT .EQ.  9) CALL TOSCN_SK_LABW_MAIN(JBODY)

! CHECK FOR TOSS OBJ CONTROL VIA DIFFERENTIAL TETH TENSIONS VERS 2
      IF(LCONT .EQ. 10) CALL TOSCN_SK_LABW_K(JBODY)

! CHECK FOR TOSS OBJ CONTROL VIA SKY-CRANE DIFFERENTIAL TETH DEPLOY
      IF(LCONT .EQ. 11) CALL TOSCN_CRANE(JBODY)

! USERS CAN ADD MORE OBJECT CONTROL OPTIONS HERE....
!      IF(LCONT .EQ. 12) CALL TOSCN_xxxxx(JBODY)


! INFORM  USER OF UNKNOWN CONTROL OPTION
!---------------------------------------
4000  IF(LCONT .GT. 11) THEN

          WRITE (LUE,403) LCONT
403       FORMAT('TOSS CONTROL OPTION NUMBER',I3, ' NOT VALID')
          STOP 'IN TOSCN-1'

      END IF


! STANDARD RETURN
10000 CONTINUE

      RETURN
      END



