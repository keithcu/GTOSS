! ROUTINE: TOSCNF
! %Z%GTOSS %M% H.10 code v01.2
!              H.10 code v01.2 Added Isp calc of mass loss due to thrust.
!-----------------------------------------------------------------------
!              H.9 code v01.10 Removed mom/force zeros & now accumulates.
!                              Modified logic for accum compatibility.
!-----------------------------------------------------------------------
!              H.5 code v01.02 Changed 6.28 to 2.0*PIIALL from COM_ALL.i
!----------------------------------------------------------------------
!              H.3 code v01.01 (New routine for vH.3)
!********************************
!********************************
!
      SUBROUTINE TOSCNF (JBODY)
!
!********************************
!********************************
! THIS ROUTINE PROVIDES SPECIAL CONTROL FORCES TOSS
! BODIES FOR CHECKOUT, TESTING, ETC


! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND 
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.i"





! STATE SLUG-TO-LBM CONVERSION FACTOR
      SLUGTOP = 32.1740487

!******************************************************************
! APPLY USED-PROVIDED CONSTANT MOMENT UNDER GUISE OF CONTROL FORCES
!******************************************************************
      GCB(1) = GCB(1) + GXCO
      GCB(2) = GCB(2) + GYCO
      GCB(3) = GCB(3) + GZCO

!**********************************************************
! TIME SEQUENCE PSUEDO CONTROL FORCES ONLY IF CFT1 .NE. 0.0
!**********************************************************
      IF(CFT1 .EQ. 0.0) GO TO 300

! A TIMED SEQUENCE OF VARIOUS CONSTANT CONTROL FORCES
!----------------------------------------------------
      IF((TOSTIM .GE. CFT1) .AND. (TOSTIM .LT. CFT2)) THEN
         FCB(1) = FCB(1) + CFORX1
         FCB(2) = FCB(2) + CFORY1
         FCB(3) = FCB(3) + CFORZ1
      END IF

      IF((TOSTIM .GE. CFT2) .AND. (TOSTIM .LT. CFT3)) THEN
         FCB(1) = FCB(1) + CFORX2
         FCB(2) = FCB(2) + CFORY2
         FCB(3) = FCB(3) + CFORZ2
      END IF

      IF((TOSTIM .GE. CFT3) .AND. (TOSTIM .LT. CFT4)) THEN
         FCB(1) = FCB(1) + CFORX3
         FCB(2) = FCB(2) + CFORY3
         FCB(3) = FCB(3) + CFORZ3
      END IF

      IF((TOSTIM .GE. CFT4) .AND. (TOSTIM .LT. CFT5)) THEN
         FCB(1) = FCB(1) + CFORX4
         FCB(2) = FCB(2) + CFORY4
         FCB(3) = FCB(3) + CFORZ4
      END IF

      IF((TOSTIM .GE. CFT5) .AND. (TOSTIM .LT. CFT6)) THEN
         FCB(1) = FCB(1) + CFORX5
         FCB(2) = FCB(2) + CFORY5
         FCB(3) = FCB(3) + CFORZ5
      END IF

      IF((TOSTIM .GE. CFT6) .AND. (TOSTIM .LT. CFT7)) THEN
         FCB(1) = FCB(1) + CFORX6
         FCB(2) = FCB(2) + CFORY6
         FCB(3) = FCB(3) + CFORZ6
      END IF

      IF((TOSTIM .GE. CFT7) .AND. (TOSTIM .LE. CFT8)) THEN
         FCB(1) = FCB(1) + CFORX7
         FCB(2) = FCB(2) + CFORY7
         FCB(3) = FCB(3) + CFORZ7
      END IF

! DO SIMPLE MASS LOSS CALC DUE TO THRUSTING
!------------------------------------------
! CALC TOTAL THRUST FORCE
       TOTFDUM = VECMAG(FCB)

! IF INTEGRATORS ARE RELEASED ACCUM TOTAL THRUSTING IMPULSE
       IF(JINTEG .NE. -1)  THEN
            TOTIPULS = TOTIPULS + TOTFDUM*DELTOS
       END IF

! DUE MASS LOSS CALC ONLY IF ISP IS NOT ZERO
        IF(FCNISP .NE. 0.0) THEN

           DMASDOT = TOTFDUM/(FCNISP * SLUGTOP)

! IF INTEGRATORS ARE RELEASED, ACCUMULATE MASS LOSS, AND TOTAL IMPLUSE
           IF(JINTEG .NE. -1)  THEN
               DMASINC = DMASINC + DMASDOT*DELTOS
           END IF

        END IF

! SINUSOIDAL FORCE CREATING SPECIFIED OBJ DISPLACEMEMT AT SPECIFIED FREQ
!-----------------------------------------------------------------------
! START THESE PSUEDO CONTROL FORCES AT COSTRT (STOP AT COSTOP)
300   IF(TOSTIM .LT. COSTRT) GO TO 5000
      IF(TOSTIM .GT. COSTOP) GO TO 5000

      COMEG = 2.0*PIIALL*CFREQX
      FCB(1) = FCB(1) + (CDELX*DMASS*COMEG*COMEG)* COS(COMEG*TOSTIM)

      COMEG = 2.0*PIIALL*CFREQY
      FCB(2) = FCB(2) + (CDELY*DMASS*COMEG*COMEG)* COS(COMEG*TOSTIM)

      COMEG = 2.0*PIIALL*CFREQZ
      FCB(3) = FCB(3) + (CDELZ*DMASS*COMEG*COMEG)* COS(COMEG*TOSTIM)


! STANDARD RETURN
5000  CONTINUE

      RETURN
      END
