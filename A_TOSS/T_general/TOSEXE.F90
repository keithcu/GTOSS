! ROUTINE: TOSEXE
! %Z%GTOSS %M% H.3 code v03.00
!              H.3 code v03.00 Removed call to TOSSTG (new deltat scheme)
!------------------------------------------------------------------------
!              H.1 code v02.01 (baseline for vH1 inertial bead model)
!**********************************
!**********************************
!
      SUBROUTINE TOSEXE(JSNAP)
!
!**********************************
!**********************************
! THIS ROUTINE PERFORMS HIGH LEVEL INVOKEMENT
! OF TOSS SOLUTION MODULES

! IF THE ARGUMENT JSNAP = 1, THEN TOSS WILL CREATE
! A SNAP SHOT FILE TO SUPPORT A LATE START IC


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"





! CREATE A LATE START DATA SNAP SHOT IF HOST SIM REQUESTS
!--------------------------------------------------------
      IF(JSNAP .EQ. 1) CALL XSNAP

! START SYSTEM-WIDE DYNAMICS VERIFICATION CALCULATIONS
! (INITIALIZE, FIND CG AND TOTAL MASS, ADD RP CONTRIBUTION)
!----------------------------------------------------------
      CALL TOSRR1


! SNAP-SHOT FINITE SOLN TETHER FRAMES BEFORE TOSS OBJ ADVANCES
!-------------------------------------------------------------
      CALL TOSSFM


! ZERO ACCUMULATORS ASSOCIATED WITH OBJECT SKIP DAMPER EFFECTS
!-------------------------------------------------------------
! JAPSRD IS THE LARGEST ATT PT NUMBER AFFECTED BY A DAMPER ON OBJ KOB
      DO 11 KOB = 1,LASOBJ

! SEE IF THIS OBJECT HAS ANY AFFECTED ATTACH POINTS
         IF(JSRON(KOB) .EQ. 1) THEN
            DO 33 KAP = 1,JAPSRD(KOB)
               DO 22 J = 1,3
                  APS(36+J,KAP,KOB) = 0.0
                  APS(39+J,KAP,KOB) = 0.0
22             CONTINUE
33          CONTINUE
         END IF

11    CONTINUE


! ADVANCE TOSS OBJ DYNAMICS FOR EACH OBJ BEING CONSIDERED
!--------------------------------------------------------
      DO 100 KOBJ = 2,LASOBJ
             CALL TOSSA(KOBJ)
100   CONTINUE


! MAKE TOSFQ CALL IN BEHALF OF OBJECT 1 (REF PT HOST SIM)
!--------------------------------------------------------
      CALL TOSSFQ(1)


! INVOKE TETHER DYNAMICS/FORCE CALCULATIONS
!------------------------------------------
      CALL TOSSET

      RETURN
      END
