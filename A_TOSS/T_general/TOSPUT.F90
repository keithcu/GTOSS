! ROUTINE: TOSPUT
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!************************************
!************************************
!
      SUBROUTINE TOSPUT (JBODY)
!
!************************************
!************************************
! THIS ROUTINE PUTS THE ATTACH POINT STATE INTO THE APS
! ARRAY FOR OBJECT "JBODY"


! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND 
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.i"





! BEGIN: LOOP TO SET AP ATTITUDE STATES INTO APS ARRAY
      DO 10 JAP = 1,NATPAT

! THEN SET IN OBJECT TIME FOR THIS AP
            APS(31,JAP,JBODY) = TOSTIM

! SET IN ATTACH FRAME ANGULAR  VEL AND ACC (ASSUME RIGID BODY)
            IF(JNOMOM .NE. 0) GO TO 10

            APS(25,JAP,JBODY) = OMB(1)
            APS(26,JAP,JBODY) = OMB(2)
            APS(27,JAP,JBODY) = OMB(3)

            APS(28,JAP,JBODY) = OMBD(1)
            APS(29,JAP,JBODY) = OMBD(2)
            APS(30,JAP,JBODY) = OMBD(3)


! SET IN ATTACH FRAME ATTITUDE (ASSUME RIGID BODY ATT)
            APS(16,JAP,JBODY) = GBI(1,1)
            APS(17,JAP,JBODY) = GBI(2,1)
            APS(18,JAP,JBODY) = GBI(3,1)

            APS(19,JAP,JBODY) = GBI(1,2)
            APS(20,JAP,JBODY) = GBI(2,2)
            APS(21,JAP,JBODY) = GBI(3,2)

            APS(22,JAP,JBODY) = GBI(1,3)
            APS(23,JAP,JBODY) = GBI(2,3)
            APS(24,JAP,JBODY) = GBI(3,3)


10    CONTINUE

! END: LOOP TO SET IN ATTITUDE STATES



!**********************************************
! UPDATE ATTACH PT POSITION IN /TOSS/ APS ARRAY
!**********************************************


! BEGIN: LOOP TO SET IN AP POSITION
      DO 20 JAP = 1,NATPAT

! TRANSFORM AP POSITION WR/T BODY TO INERTIAL FRAME
            TOSVX1(1) = XTB(JAP)
            TOSVX1(2) = YTB(JAP)
            TOSVX1(3) = ZTB(JAP)
            CALL MATVEC (1, GBI, TOSVX1,    TOSVX2)
            XTI(JAP) = TOSVX2(1)
            YTI(JAP) = TOSVX2(2)
            ZTI(JAP) = TOSVX2(3)

! CALCULATE AP POSITION WR/T RP IN INERTIAL FRAME
            APS(7,JAP,JBODY) = RBI(1) + XTI(JAP)
            APS(8,JAP,JBODY) = RBI(2) + YTI(JAP)
            APS(9,JAP,JBODY) = RBI(3) + ZTI(JAP)

20    CONTINUE

! END: LOOP TP SET IN AP POSITION



!**********************************************
! UPDATE ATTACH PT VELOCITY IN /TOSS/ APS ARRAY
!**********************************************

! BEGIN: LOOP TO SET IN AP VELOCITY
      DO 30 JAP = 1,NATPAT

! CALCULATE UNDEFORMED AP VEL COMP WR/T CG
            IF(JNOMOM .NE. 0) GO TO 31

            TOSVX1(1) = XTUB(JAP)
            TOSVX1(2) = YTUB(JAP)
            TOSVX1(3) = ZTUB(JAP)
            CALL CROSS (OMB, TOSVX1,   TOSVX2)
            VXTUB(JAP) = TOSVX2(1)
            VYTUB(JAP) = TOSVX2(2)
            VZTUB(JAP) = TOSVX2(3)

   31       CONTINUE

! CALCULATE TOTAL AP VEL COMP WR/T CG
            VXTB(JAP) = VXTUB(JAP) + VXTEB(JAP)
            VYTB(JAP) = VYTUB(JAP) + VYTEB(JAP)
            VZTB(JAP) = VZTUB(JAP) + VZTEB(JAP)

! TRANSFORM THIS AP VEL COMP INTO INERTIAL FRAME
            TOSVX1(1) = VXTB(JAP)
            TOSVX1(2) = VYTB(JAP)
            TOSVX1(3) = VZTB(JAP)
            CALL MATVEC (1, GBI, TOSVX1,   TOSVX2)
            VXTI(JAP) = TOSVX2(1)
            VYTI(JAP) = TOSVX2(2)
            VZTI(JAP) = TOSVX2(3)

! CALCULATE AP VELOCITY WR/T RP IN INERTIAL FRAME
            APS(10,JAP,JBODY) = RBID(1) + VXTI(JAP)
            APS(11,JAP,JBODY) = RBID(2) + VYTI(JAP)
            APS(12,JAP,JBODY) = RBID(3) + VZTI(JAP)

30    CONTINUE

! END: LOOP TO SET IN AP VELOCITY



!**************************************************
! UPDATE ATTACH PT ACCELERATION IN /TOSS/ APS ARRAY
!**************************************************

! BEGIN: LOOP TO SET IN AP ACCELERATION
      DO 40 JAP = 1,NATPAT

! CALCULATE UNDEFORMED ACCEL WR/T CG IN BODY FRAME
            IF(JNOMOM .NE. 0) GO TO 35

            TOSVX1(1) = XTUB(JAP)
            TOSVX1(2) = YTUB(JAP)
            TOSVX1(3) = ZTUB(JAP)
            CALL CROSS (OMBD, TOSVX1,  TOSVX2)

            TOSVX1(1) = VXTUB(JAP)
            TOSVX1(2) = VYTUB(JAP)
            TOSVX1(3) = VZTUB(JAP)
            CALL CROSS (OMB, TOSVX1,   TOSVX3)

            AXTUB(JAP) = TOSVX2(1) + TOSVX3(1)
            AYTUB(JAP) = TOSVX2(2) + TOSVX3(2)
            AZTUB(JAP) = TOSVX2(3) + TOSVX3(3)

   35       CONTINUE


! CALCULATE TOTAL ACCEL WR/T CG IN BODY FRAME
            AXTB(JAP) = AXTUB(JAP) + AXTEB(JAP)
            AYTB(JAP) = AYTUB(JAP) + AYTEB(JAP)
            AZTB(JAP) = AZTUB(JAP) + AZTEB(JAP)

! TRANSFORM TOTAL ACCEL WR/T CG INTO INERTIAL FRAME
            TOSVX1(1) = AXTB(JAP)
            TOSVX1(2) = AYTB(JAP)
            TOSVX1(3) = AZTB(JAP)
            CALL MATVEC (1, GBI, TOSVX1,   TOSVX2)
            AXTI(JAP) = TOSVX2(1)
            AYTI(JAP) = TOSVX2(2)
            AZTI(JAP) = TOSVX2(3)

! CALCULATE TOTAL ACCEL WR/T RP IN INERTIAL FRAME
            APS(13,JAP,JBODY) = RBIDD(1) + AXTI(JAP)
            APS(14,JAP,JBODY) = RBIDD(2) + AYTI(JAP)
            APS(15,JAP,JBODY) = RBIDD(3) + AZTI(JAP)

40    CONTINUE

! END: LOOP TO SET IN ACCEL

      RETURN
      END
