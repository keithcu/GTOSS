! ROUTINE: TOSRR4
! %Z%GTOSS %M% H.7 code v02.02
!              H.7 code v02.02 Eliminated unused variables DUMCOF, DUMDS
!                              DUMG0, DUMTU
!-----------------------------------------------------------------------
!              H.1 code v02.01 (baseline for vH1 inertial bead model)
!*****************************
!*****************************
!
      SUBROUTINE TOSRR4(JT)
!
!*****************************
!*****************************
! THIS ROUTINE EVALUATES THE DYNAMIC EQUILIBRIUM OF THE BEAD MODEL
! FINITE SOLUTION CURRENTLY IN THE WORKING ARRAY.
!
! IT PERFORMS THE FOLLOWING FUNCTIONS,
!------------------------------------
!
!  1. DETERMINES BEAD MODEL CENTER-OF-MASS STATE (WR/T X-END ATTACH PT)
!
!  2. CALCULATES TOTAL INERTIAL LINEAR MOMENTUM OF BEAD MODEL
!
!  3. CALCULATES TOTAL ANGULAR MOMENTUM WR/T BEAD MODEL CENTER OF MASS
!
!  4. DETERMINES NUMERICAL DERIVATIVES OF LINEAR AND ANGULAR MOMENTUM
!
!  5. CALCULATES THE TOTAL EXTERNALLY APPLIED FORCE AND MOMENT ON THE
!     BEAD MODEL (INCLUDES CONTRIBUTION FROM TETHER END-SEG TENSIONS).
!
!  6. CALCULATES THE FRACTIONAL ERROR BETWEEN THE NUMERICAL DERIVATIVES
!     OF THE MOMENTA AND THE APPLIED FORCES AND MOMENTS TO PROVIDE AN
!     ASSESSMENT OF THE DYNAMIC EQUILBRIUM OF THE FINITE SOLUTION
!
! THE INPUT ARGUMENT JT IS THE TOSS TETHER NUMBER


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND 
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.i"





! NON-VECTOR-OPTIMIZED ARRAYS
      DIMENSION DUMRI(3),   DUMBI(3)
      DIMENSION DUMRID(3),  DUMBID(3)
      DIMENSION DUMAID(3),  DUMFI(3)

      DIMENSION DUMBL1(3), DUMBL2(3), DUMBL3(3), DUMBL4(3)


! GET OUT IF DYNAMICS VERIFICATION IS NOT ACTIVATED
!--------------------------------------------------
      IF(LVERFY .EQ. 0) GO TO 5000


!**************************************************************
!**************************************************************
! FIND BEAD MODEL CENTER-OF-MASS STATE WR/T TETHER X-END ATT PT
!**************************************************************
!**************************************************************
! INITIALIZE BEAD MODEL CENTER OF MASS STATE
      BBCGI(1) = 0.0
      BBCGI(2) = 0.0
      BBCGI(3) = 0.0

      BBCGID(1) = 0.0
      BBCGID(2) = 0.0
      BBCGID(3) = 0.0

!-------------------------------------------------------------
!-------------------------------------------------------------
! START LOOP TO ACCUMULATE EACH BEADS MASS MOMENT CONTRIBUTION
!-------------------------------------------------------------
!-------------------------------------------------------------
      DO 100 JBEAD = 1,NBEAD

! FORM INDEX INTO THIS BEADS POSITION COMPONENTS OF BEAD STATE VECTOR
             JB1 = 3*(JBEAD - 1) + 1
             JB2 = JB1 + 1
             JB3 = JB1 + 2

! FETCH POSITION AND VELOCITY OF BEAD WR/T X-END ATT PT (INER FRAME)
!-------------------------------------------------------------------
             DUMRI(1) = RUBI(JB1)
             DUMRI(2) = RUBI(JB2)
             DUMRI(3) = RUBI(JB3)

             DUMRID(1) = VUBI(JB1)
             DUMRID(2) = VUBI(JB2)
             DUMRID(3) = VUBI(JB3)

! ADD THIS BEADS CONTRIBUTION TO POSITION MASS MOMENT
!----------------------------------------------------
             CALL VECSCL(BMS(JBEAD),DUMRI,    TOSVX6)
             CALL VECSUM(BBCGI,TOSVX6,         BBCGI)

! ADD THIS BEADS CONTRIBUTION TO VELOCITY MASS MOMENT
!----------------------------------------------------
             CALL VECSCL(BMS(JBEAD),DUMRID,   TOSVX6)
             CALL VECSUM(BBCGID,TOSVX6,       BBCGID)

!---------------------------
! END OF BEAD SUMMATION LOOP
!---------------------------
100   CONTINUE


!*******************************************************************
! CALC VECTOR (AND DERIV) FROM X-END ATT PT TO MODEL CG (INER FRAME)
!*******************************************************************
      CALL VECSCL(1.0/BMSTOT, BBCGI,    BBCGI )
      CALL VECSCL(1.0/BMSTOT, BBCGID,   BBCGID)




!*********************************************************************
!*********************************************************************
! CALC LINEAR, ANGULAR MOMENTUM, AND TOTAL FORCE, MOMENT ON BEAD MODEL
!*********************************************************************
!*********************************************************************
! INITIALIZE VARIOUS ACCUMULATED QUANTITIES
      DPBAI(1) = 0.0
      DPBAI(2) = 0.0
      DPBAI(3) = 0.0

      HBBI(1) = 0.0
      HBBI(2) = 0.0
      HBBI(3) = 0.0

      FEBBI(1) = 0.0
      FEBBI(2) = 0.0
      FEBBI(3) = 0.0

      GEBBI(1) = 0.0
      GEBBI(2) = 0.0
      GEBBI(3) = 0.0

!---------------------------------------------------------------
!---------------------------------------------------------------
! START LOOP TO PERFORM RELATED FUNCTIONS FOR EACH BEAD IN MODEL
!---------------------------------------------------------------
!---------------------------------------------------------------
      DO 200 JBEAD = 1,NBEAD

! FORM INDEX INTO THIS BEADS POSITION COMPONENTS OF BEAD STATE VECTOR
             JB1 = 3*(JBEAD - 1) + 1
             JB2 = JB1 + 1
             JB3 = JB1 + 2

! FETCH POSITION AND VELOCITY OF BEAD WR/T X-END ATT PT (INER FRAME)
!-------------------------------------------------------------------
             DUMRI(1) = RUBI(JB1)
             DUMRI(2) = RUBI(JB2)
             DUMRI(3) = RUBI(JB3)

             DUMRID(1) = VUBI(JB1)
             DUMRID(2) = VUBI(JB2)
             DUMRID(3) = VUBI(JB3)


! CALC POSITION OF BEAD WR/T CENTER OF MASS (INER FRAME)
             CALL VECDIF(DUMRI, BBCGI,       DUMBI)

! CALC DERIV OF BEAD POSITION WR/T CENTER OF MASS (INER FRAME)
             CALL VECDIF(DUMRID, BBCGID,    DUMBID)


!*****************************************************
! ACCUMULATE LINEAR MOMENTUM OF BEAD WR/T X-END ATT PT
!*****************************************************
             CALL VECSCL(BMS(JBEAD),DUMRID,    DUMBL1)
             CALL VECSUM(DPBAI,DUMBL1,         DPBAI )


!**************************************************************
! ACCUMULATE ANGULAR MOMENTUM OF BEAD WR/T MODEL CENTER OF MASS
!**************************************************************
             CALL CROSS (DUMBI, DUMBID,         DUMBL1 )
             CALL VECSCL(BMS(JBEAD),DUMBL1,     DUMBL2 )
             CALL VECSUM(HBBI,     DUMBL2,      HBBI   )


!************************************************
! ACCUMULATE TOTAL FORCE AND MOMENT ON BEAD MODEL
!************************************************
! FIND THE TOTAL FORCE ON THIS BEAD
             DUMFI(1) = BFEXI(JB1) + BFSI(JB1) + BMS(JBEAD)*BAGI(JB1)
             DUMFI(2) = BFEXI(JB2) + BFSI(JB2) + BMS(JBEAD)*BAGI(JB2)
             DUMFI(3) = BFEXI(JB3) + BFSI(JB3) + BMS(JBEAD)*BAGI(JB3)

! ACCUMULATE TOTAL FORCE ON BEAD MODEL (INER FRAME)
!--------------------------------------------------
             CALL VECSUM(FEBBI, DUMFI,   FEBBI)

! ACCUMULATE TOTAL MOMENT ON THIS BEAD WR/T MODEL CENTER OF MASS
!---------------------------------------------------------------
             CALL CROSS(DUMBI, DUMFI,   TOSVX4)
             CALL VECSUM(GEBBI,TOSVX4,   GEBBI)


!---------------------------
! END OF BEAD SUMMATION LOOP
!---------------------------
200   CONTINUE



!***************************************************************
! NOW, CALC LINEAR MOMENTUM CONTRIBUTION DUE TO ATTACH PT MOTION
!***************************************************************
! FIND THE ATT PT NUMBER AND OBJECT AT X END OF THIS TETHER
      NXADUM = LATTX(JT)
      NXODUM = NOBJX(JT)

! FIND INERTIAL VELOCITY OF X-END ATT PT (RP VEL + AP VEL WR/T RP)
      DUMAID(1) = RPID(1) + APS(10,NXADUM,NXODUM)
      DUMAID(2) = RPID(2) + APS(11,NXADUM,NXODUM)
      DUMAID(3) = RPID(3) + APS(12,NXADUM,NXODUM)

! THEN GET LINEAR MOMENTUM CONTRIBUTION DUE TO ATT PT VEL
      CALL VECSCL(BMSTOT, DUMAID,  DPAAI)


!-----------------------------------------------
! THEN, TOTAL LINEAR MOMENTUM AND ITS DIFFERENCE
!-----------------------------------------------
! CALCULATE TOTAL LINEAR MOMENTUM FOR DISPLAY
      CALL VECSCL(BMSTOT, DUMAID,  DPAAI)
      CALL VECSUM(DPBAI, DPAAI,     PBBI)

! FOR MAX PRECISION, FORM SEPARATE DIFFERENCES OF LINEAR MOMENTA
      CALL VECDIF(DPBAI,DPBAIL,     DUMBL1)
      CALL VECDIF(DPAAI,DPAAIL,     DUMBL2)



!--------------------------------------------------------------
! FINALLY, CALC DERIV OF LINEAR MOMENTA AND SAVE CURRENT VALUES
!--------------------------------------------------------------
! CALCULATE NUMERICAL DERIVATIVE OF LINEAR MOMENTA COMPONENT
      CALL VECSCL(1./DELTIS,DUMBL1,    DUMBL3)
      CALL VECSCL(1./DELTIS,DUMBL2,    DUMBL4)

! CALC THE DIFFERENCE FOR TOTAL LINEAR MOMENTUM
      CALL VECSUM(DUMBL3,DUMBL4,  PBBID)

! SAVE CURRENT VALUES
      CALL VECMOV(DPBAI,  DPBAIL)
      CALL VECMOV(DPAAI,  DPAAIL)



!*******************************************************
! CALC DERIV OF ANGULAR MOMENTUM AND SAVE CURRENT VALUES
!*******************************************************
! CALCULATE NUMERICAL DERIVATIVE OF ANGULAR MOMENTUM
      CALL VECDIF(HBBI, DHBBIL,        DUMBL4)
      CALL VECSCL(1./DELTIS,DUMBL4,     HBBID)

! SAVE CURRENT VALUES
      CALL VECMOV(HBBI, DHBBIL)




!***************************************
!***************************************
! DETERMINE ERROR IN DYNAMIC EQUILIBRIUM
!***************************************
!***************************************
! (JUST USE A SIMPLE DIFFERENCE FOR NOW, LATER A
!  CONVERGENCE CRITERIA WILL BE DEVELOPED HERE)
      ERRPBB(1) = PBBID(1) - FEBBI(1)
      ERRPBB(2) = PBBID(2) - FEBBI(2)
      ERRPBB(3) = PBBID(3) - FEBBI(3)

      ERRHBB(1) = HBBID(1) - GEBBI(1)
      ERRHBB(2) = HBBID(2) - GEBBI(2)
      ERRHBB(3) = HBBID(3) - GEBBI(3)


! STANDARD RETURN
5000  CONTINUE

      RETURN
      END
