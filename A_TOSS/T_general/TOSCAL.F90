! ROUTINE: TOSCAL
! %Z%GTOSS %M% G.1.1 code v01.02 (fix RMAG, re:GAMTOP migration to UTIL)
!*******************************
!*******************************
!
      SUBROUTINE TOSCAL
!
!*******************************
!*******************************
! THIS ROUTINE PERFORMS CALCULATION OF INTERPRETATIVE
! VARIABLES FOR DISPLAY OR OTHERWISE

! TOSCAL USES THE WORKING AREA DATA HENCE, DERIVED VALUES
! ARE DERIVED-FROM AND PERTAIN-TO THE OBJECT WHOSE
! DATA CURRENTLY RESIDES IN THE WORKING AREA


! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND 
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.i"




! FORM OBJECTS ORBITAL TO INERTIAL FRAME TRANSFORMATION
!------------------------------------------------------
      CALL ORBFRM (RI,RID,  GOI)



!-------------------------------------------------
! FIND REF PT "ORBITAL FRAME" COMP OF RANGE VECTOR
!-------------------------------------------------
      CALL MATVEC (0, RPGOI,RBI,  RBOR)



!-------------------------------------------------------
! FIND DERIV OF REF PT "ORBITAL FRAME" COMP OF RANGE VEC
!-------------------------------------------------------
      CALL MATVEC (0, RPGOI,RPID,  TOSVX3)

      RPMAG = VECMAG(RPI)

      TOSVX2(1) = 0.0
      TOSVX2(2) = - TOSVX3(1)/RPMAG
      TOSVX2(3) = 0.0

      CALL MATVEC (0, RPGOI,RBID,  RBORD)

      CALL CROSS (TOSVX2,RBOR,  TOSVX3)

      CALL VECDIF (RBORD,TOSVX3,  RBORD)




!---------------------------------------------------
! FIND REF PT "ARBITRARY FRAME" COMP OF RANGE VECTOR
!---------------------------------------------------
      CALL MATVEC (0, RPGRI,RBI,  RBAR)


!------------------------------------------------------
! FIND REF PT "ARB FRAME" COMP OF DERIV OF RANGE VECTOR
!------------------------------------------------------
      CALL MATVEC (0, RPGRI,RBID, VBAR)


!--------------------------------------------
! CALCULATE RANGE FROM REF POINT TO OBJECT CG
!--------------------------------------------
      RANGE = VECMAG (RBI)



!------------------------------------------------
! FIND RANGE AND RANGE RATE OF OBJECT WR/T REF PT
!------------------------------------------------
! SET DEFAULT VALUE OF ZERO
      RANGED = 0.0

! IF RANGE = 0, RANGE RATE IS UNDEFINED
      IF(RANGE .NE. 0.0)  THEN

          CALL VECNRM (RBI, TOSVX1)
          RANGED = DOT (TOSVX1,RBID)

      END IF


! DETERMINE OBJECTS PLANET LONGITUDE AND LATITUDE
!------------------------------------------------
      RMAG = VECMAG (RIP)
      SINDUM = RIP(3)/RMAG
! DONT EVALUATE MATRIX AT LONG/LAT SINGULARITY
      IF(ABS(SINDUM) .LE. 1.0)  GO TO 1
! NOTE THAT ARG TO ASIN IS IMPOSSIBLE
            WRITE(LUE,2)  SINDUM
2           FORMAT('TOSCAL: WARNING, SIN(LAT-E)=',E16.10)
            GO TO 10

! DETERMINE PLANET LONGITUDE AND LATITUDE
1     DLAE = ASIN (SINDUM)
      DMUE = 0.0
      IF( (RIP(1)+RIP(2)) .NE. 0.0) DMUE = ATAN2 (RIP(2),RIP(1))

10    CONTINUE

      RETURN
      END
