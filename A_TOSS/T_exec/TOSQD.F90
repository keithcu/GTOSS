! ROUTINE: TOSQD
! %Z%GTOSS %M% H.3 code v02.00
!              H.3 code v02.00  removed stepsize determination code
!------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!****************************
!****************************
!
      SUBROUTINE TOSQD(JLATE)
!
!****************************
!****************************
! THIS ROUTINE ZEROS THE "D" ARRAY FOR GENERAL COMMON,
! THEN INTERCEPTS THE INPUT FILE DATA STREAM
! TO ALLOW USER INPUT TO THE DTOSQ ARRAY

! ALSO, SOME SPECIAL D ARRAY INITIALIZATION ASSOCIATED
! WITH TOSS MASTER INTEGRATION TIME IS DONE HERE


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"




!************************
! FIRST, ZERO DTOSQ ARRAY
!************************
! DONT DO IT IF ITS A LATE START
      IF(JLATE .NE. 1) THEN

          DO 2 J = 1,NDQ
               DTOSQ(J) = 0.0
2         CONTINUE

      END IF



! SPECIFY THAT ONE LINE OF TEXT IS TO BE READ (AND DISPLAYED)
      LTEXT = 2

!******************************************
! START A LOOP TO READ IN MANY REAL NUMBERS
!******************************************

! CALL ROUTINE THAT HANDLES REAL DATA READ-IN
1     CALL TOSRIN (LUI,LUO,LUE,LUC, NDQMIN,NDQMAX, LTEXT,ISHOIN,   &
     &             LINDEX,RVALUE)

! SEE IF READ-IN WENT AWRIE
      IF(LINDEX .LT. 0) GO TO 300

! SEE IF ALL REALS ARE IN
      IF(LINDEX .EQ. 0) GO TO 500

! A SUCCESSFUL READ-IN ,SO STORE IT
      DTOSQ(LINDEX) = RVALUE

! GO DO ANOTHER REAL READ-IN
      GO TO 1


!------------------------------------------
! DO SPECIAL PURPOSE D ARRAY INITIALIZATION
!------------------------------------------
500   CONTINUE


!-----------------------------------------------
! SET IN TOSS MASTER STEPSIZE AS THE RP STEPSIZE
!-----------------------------------------------
      QDELT = RPDELT

      RETURN


!************************
! ERROR REPORTING SECTION
!************************

! IDENTIFY SOURCE OF ABOVE ERROR COMMENTS FROM TOSRIN
300   WRITE (LUE,301)
301   FORMAT('=> => => => ABOVE APPLIES TO ARRAY: DTOSQ  ')

! JUST STOP THE SHOW UNTIL DATA IS STRAIGHTENED OUT
      STOP 'IN TOSQD'

      END
