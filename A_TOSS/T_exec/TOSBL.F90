! ROUTINE: TOSBL
! %Z%GTOSS %M% H.11 code v02.0 
!              H.11 code v02.0  Rework for compatiblity w/New TOSS Obj scheme
!----------------------------------------------------------------------------
!              H.10 code v01.02 Increased No. of Objects to 15.
!--------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!*******************************
!*******************************
!
      SUBROUTINE TOSBL(JBODY)
!
!*******************************
!*******************************
! THIS ROUTINE INTERCEPTS THE INPUT FILE DATA STREAM
! TO ALLOW USER INPUT TO THE LTOS2, 3, .... ARRAYS

! THE ARGUMENT IS THE BODY NUMBER "JBODY"

      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_OBJI.i"


! PERFORM (AND SETUP) OBJECT NUMBER VALIDITY CHECKS
      IF( (JBODY .LT. 2) .OR. (JBODY .GT. NOBJ) ) GO TO 400
      
! SET IN ARRAY LIMITS BASED ON ARRAYS DEFINED IN FILE "EQU_OBJI.i"
! (THIS IS MORE OR LESS AN ARTIFACT OF THE ORIGINAL TOSS INPUT SCHEME)
      LMIN = 17
      LMAX = 100

! CHECK ON POTENTIAL "NOBJ" EXPANSION CODE OMISSION
      IF( (LMIN+LMAX) .EQ. 0) GO TO 500



!**************************************
! START A LOOP TO READ IN MANY INTEGERS
!**************************************
! SPECIFY THAT A LINE OF TEXT IS TO BE READ (AND DISPLAYED)
      LTEXT = 2

! CALL ROUTINE THAT HANDLES INTEGER READ-IN
1     CALL TOSLIN (LUI,LUO,LUE,LUC, LMIN,LMAX, LTEXT,ISHOIN,   &
     &             LINDEX,LVALUE)

! SEE IF READ-IN WENT AWRY
      IF(LINDEX .LT. 0) GO TO 300

! SEE IF ALL INTEGERS ARE IN
      IF(LINDEX .EQ. 0) GO TO 1000

! SUCCESSFUL READ-IN, STORE INTO OBJECT READ-IN ARRAY FOR INTEGERS
      L_TOBJ_IN(LINDEX) = LVALUE

! GO DO ANOTHER INTEGER READ-IN
      GO TO 1

! STANDARD RETURN
1000  RETURN



!************************
! ERROR REPORTING SECTION
!************************

! IDENTIFY SOURCE OF INPUT FILE ERROR COMMENTS (FROM TOSLIN)
300   WRITE (LUE,301) JBODY
301   FORMAT('=> => => => ABOVE APPLIES TO DATA INPUT FOR JBODY',I2)
! JUST STOP THE SHOW UNTIL DATA IS STRAIGHTENED OUT
      STOP 'IN TOSBL-1'

! APPARENTLY AN UNKNOWN BODY IS SPECIFIED, SO WARN AND STOP
400   WRITE (LUE,401) JBODY
401   FORMAT('BODY NUMBER UNKNOWN TO SUBR TOSBL, #=',I3 )
      STOP 'IN TOSBL-2'

! APPARENTLY AN UNKNOWN BODY IS SPECIFIED, SO WARN AND STOP
500   WRITE (LUE,501) JBODY
501   FORMAT('POSSIBLE "NOBJ" EXPANSION ERROR IN TOSBL, #=',I3 )
      STOP 'IN TOSBL-3'

      END
