! ROUTINE: MMD
! %Z%GTOSS %M% H.5 code v02.00
!              H.5 code v02.00 (baseline for vH.5, simple TSS deployer)
!----------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!***********************************************************************
!***********************************************************************
      SUBROUTINE MMD
!***********************************************************************
!***********************************************************************
!
!        LATEST VERSION ON: 11/21/90
!                           09:11:18
!
!**** PURPOSE OF SUBROUTINE
!       DEFAULT VARIABLE EXECUTIVE FOR THE
!       MARTIN MARIETTA SATELLITE DEPLOYER SYSTEM IN THE
!       TETHERED SATELLITE SYSTEM (TSS)
!
!**** NOTES
!     REF. TETHERED SATELLITE SYSTEM
!          MMC TSS DYNAMICS AND CONTROL DATA
!          FEBRUARY 1988
!
!**** EXTERNALS
!     MMGSED

! COMMON INCLUDE STATEMENTS - none
!---------------------------------
! DEFAULT VARIABLE INITIALIZATION FOR GUIDANCE SYSTEM
!----------------------------------------------------
      CALL MMGSED

      RETURN
      END
