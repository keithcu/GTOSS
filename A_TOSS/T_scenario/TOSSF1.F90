! ROUTINE: TOSSF1
! %Z%GTOSS %M% H.5 code v01.02
!              H.5 code v01.02 Consolidated program-specific permanence
!                              variables: F1LEN, TSLF1, FSLF1,
!                              into universal permanence variables
!-----------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************
!*************************************
!
      SUBROUTINE TOSSF1(JS,JTETH)
!
!*************************************
!*************************************
! THIS ROUTINE INTERPRETS A SCENARIO TO BE A
! LINEAR RAMPED LENGTH-RATE CONTROL DEPLOYMENT


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"




! INITIALIZE LENGTH BASE PRIOR TO INTEGRATION
      IF(QTIME .LE. TDSCON(JS))  FPLEN(JTETH) = TLENT(JTETH)

! INITIALIZE REDUCED-TIME PARAMETERS (TRIGGERED ON QTIME)
!--------------------------------------------------------
      IF(QTIME .EQ. 0.0) FSLFP(JTETH) = 0.0
      IF(QTIME .EQ. 0.0) TSLFP(JTETH) = DDINV1(JS) + DDINV2(JS)   &
     &                     + DDINV3(JS) + DDINV4(JS) + DDINV5(JS)

! DO ABSOLUTE-TIME START/STOP DETERMINATION
!------------------------------------------
! GO TO NEXT TETHER IF ITS NOT TIME TO START THIS SCENARIO
      IF(QTIME .LT. TDSCON(JS)) GO TO 1500

      TLENTD(JTETH) = 0.0
      TLENDD(JTETH) = 0.0

! GO TO NEXT TETHER IF TIME TO STOP THIS SCENARIO
! (ZERO STOP TIME DEFAULTS TO ENDLESS REPETITION)
      IF( (TDSCOF(JS) .NE. 0.0)  .AND.   &
     &               (QTIME .GT. TDSCOF(JS))  ) GO TO 1500

!-----------------------------------------------------
! START REDUCED TIME AND SCENARIO PERIOD DETERMINATION
!-----------------------------------------------------
      RTDUM = QTIME - TDSCON(JS) - FSLFP(JTETH)*TSLFP(JTETH)
      IF(RTDUM .GE. TSLFP(JTETH)) FSLFP(JTETH) = FSLFP(JTETH) + 1.0

! CAPTURE SCENARIO DATA FOR APPROPRIATE PERIOD
!---------------------------------------------
! ASSUME PERIOD 1
      BGDUM = BGVAL1(JS)
      ENDUM = ENVAL1(JS)
      DLDUM = DDINV1(JS)
      RLDUM = RTDUM
      IF(RLDUM .LE. DLDUM) GO TO 1100

! ASSUME PERIOD 2
      BGDUM = BGVAL2(JS)
      ENDUM = ENVAL2(JS)
      DLDUM = DDINV2(JS)
      RLDUM = RLDUM - DDINV1(JS)
      IF(RLDUM .LE. DLDUM) GO TO 1100

! ASSUME PERIOD 3
      BGDUM = BGVAL3(JS)
      ENDUM = ENVAL3(JS)
      DLDUM = DDINV3(JS)
      RLDUM = RLDUM - DDINV2(JS)
      IF(RLDUM .LE. DLDUM) GO TO 1100

! ASSUME PERIOD 4
      BGDUM = BGVAL4(JS)
      ENDUM = ENVAL4(JS)
      DLDUM = DDINV4(JS)
      RLDUM = RLDUM - DDINV3(JS)
      IF(RLDUM .LE. DLDUM) GO TO 1100

! ASSUME PERIOD 5
      BGDUM = BGVAL5(JS)
      ENDUM = ENVAL5(JS)
      DLDUM = DDINV5(JS)
      RLDUM = RLDUM - DDINV4(JS)

! END OF PERIOD DETERMINATION
1100  CONTINUE


! DO RATE DETERMINATION FOR THIS PERIOD
!--------------------------------------
! DETERMINE RATE COMMANDED AT THIS POINT IN TIME (VIA LINEAR RAMP)
      IF(DLDUM.NE.0.0) RATDUM = BGDUM + RLDUM*(ENDUM-BGDUM)/DLDUM

! APPLY TOSS TETHER MULTIPICATIVE FACTOR (0.0 DEFAULTS TO 1.0)
      IF(FACDEP(JTETH) .NE. 0.0) RATDUM = FACDEP(JTETH)*RATDUM


! ACCUM CHANGE TO INSTANTANEOUS UNDEFORMED LENGTH
!------------------------------------------------
      DUMINC = RATDUM*QDELT

! DONT DO TETHER LENGTH INTEGRATION UNTIL DYNAMICS ARE INTEGRATING
      IF(JINTEG .EQ. 1) FPLEN(JTETH) = FPLEN(JTETH) + DUMINC

! SET IN INSTANTANEOUS DEPLOY LENGTH AND DERIVATIVES
!---------------------------------------------------
! (DEPLOY LENGTH ACCEL=0 FOR LINEAR RAMPS, ALONG WITH
!  IGNORING TRANSIENTS DUE TO SCENARIO-PERIOD TRANSITIONS)
      TLENT(JTETH)  = FPLEN(JTETH)
      TLENTD(JTETH) = RATDUM
      TLENDD(JTETH) = 0.0

! SET IN COMMANDED PARAMETER VALUE FOR THIS TETHER
      TTNCOM(JTETH) = RATDUM

! STANDARD RETURN FOR SCENARIO
1500  CONTINUE

      RETURN
      END
