! ROUTINE: TOSSH3
! %Z%GTOSS %M% H.5 code v01.02
!              H.5 code v01.02 Replaced RTD with RTDALL
!------------------------------------------------------
!              H.3 code v01.01  new routine, baseline for vH.3 delivery
!*************************************
!*************************************
!
      SUBROUTINE TOSSH3(JS,JTETH)
!
!*************************************
!*************************************
! THIS ROUTINE INTERPRETS A POWER GENERATION
! SCENARIO TO GENERATE PHASED SKIP ROPE PUMPING



! STANDARD SCENARIO DATA WILL BE INTERPRETED AS FOLLOWS:
!
!    STANDARD INTEPRETATION        SKIP-PHASE SCENARIO INTERPRETATION
!    ----------------------        ----------------------------------

!    POWER GENERATION START TIME   MEANS THE SAME

!    PERIOD DURATION  - PERIOD 1   AMOUNT OF PHASE ANG (DEG) W/AMPS ON
!    BEGIN PWR LEVEL  - PERIOD 1   PHASE ANG (DEG) AT WHICH AMPS START
!    ENDING PWR LEVEL - PERIOD 1   AMPERAGE LEVEL (+/-)
!    LIMIT PWR LEVEL  - PERIOD 1   -NA-

!    PERIOD DURATION  - PERIOD 2,3,4,5,6  -NA-
!    BEGIN PWR LEVEL  - PERIOD 2,3,4,5,6  -NA-
!    ENDING PWR LEVEL - PERIOD 2,3,4,5,6  -NA-
!    LIMIT PWR LEVEL  - PERIOD 2,3,4,5,6  -NA-

!    POWER GENERATION STOP TIME    MEANS THE SAME



! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND 
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.i"






! EXTRACT TOSS-TETHER-SPECIFIC ELECTRO ATTRIBUTES (MINIMIZE INDEXING)
      PWFDUM = FACPWR(JTETH)


! INITIALIZE REDUCED-TIME PARAMETERS (TRIGGERED ON QTIME)
!--------------------------------------------------------
      IF(QTIME .EQ. 0.0) FSGH1(JTETH) = 0.0
      IF(QTIME .EQ. 0.0) TSGH1(JTETH) = DPINV1(JS) + DPINV2(JS)   &
     &            + DPINV3(JS) + DPINV4(JS) + DPINV5(JS) + DPINV6(JS)


! DO ABSOLUTE-TIME START/STOP DETERMINATION
!------------------------------------------
! GO TO NEXT TETHER IF ITS NOT TIME TO START THIS SCENARIO
      IF(QTIME .LT. TGENON(JS)) GO TO 2500

! TURN OFF CURRENT, GO TO NEXT TETHER IF STOP TIME FOR THIS SCENARIO
! NOTE: STOP-TIME=0.0  DEFAULTS TO AN ENDLESS REPETITION
      CURDUM = 0.0
      IF(   (TGENOF(JS).GT.0.0)   &
     &             .AND.   &
     &    (QTIME.GT.TGENOF(JS))  ) GO TO 2200


! INTERPRET SCENARIO DEFINITION DATA FROM PERIOD 1
!-------------------------------------------------
! DURATION OF PHASE ANGLE THRU WHICH CURRENT IS TO BE ON
      DURDUM = DPINV1(JS)

! STARTING PHASE ANGLE AT WHICH TO TURN CURRENT ON
      PONDUM = BGPWR1(JS)

! CURRENT CONSTANT AMPERAGE LEVEL
      AMPDUM = ENPWR1(JS)


! CALCULATE THE STOPPING PHASE ANGLE
!-----------------------------------
      POFDUM = PONDUM + DURDUM


!*******************************************
! CALCULATE THE PHASE ANGLE OF THE SKIP ROPE
!*******************************************
! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
      JFTETH = LASIGN(JTETH)
      CALL TISLDW(JFTETH)

! USE THE MID POINT BEAD FOR PHASING CALCS
      JMDBB = NINT( REAL(NBEAD)/2.0 )

! CALCULATE POINTERS INTO BEAD STATE VECTOR FOR MID POINT
      JB1 = 3*(JMDBB-1) + 1
      JB2 = JB1+1
      JB3 = JB1+2

! EXTRACT BEAD STATES
      YBBMID = BUT(JB2)
      ZBBMID = BUT(JB3)

! CALC A MEASURE OF SKIP ROPE PHASE ANGLE (CONVERT TO DEG)
      PHSDUM = RTDALL*ATAN2(YBBMID,ZBBMID)

! CONVERT SKIP ROPE PHASE ANGLE TO A VALUE BETWEEN O AND 360 DEGREES
      IF(PHSDUM .LT. 0.0) PHSDUM = PHSDUM + 360.


!********************************************************
! DETERMINE IF SKIP ROPE PHASE ANGLE IS THE AMP-ON REGION
!********************************************************
! ASSUME IT IS NOT, THEN TEST THIS HYPOTHESIS
      CURDUM = 0.0
      IF((PHSDUM.GT.PONDUM) .AND. (PHSDUM.LT.POFDUM)) CURDUM = AMPDUM


! DO TOSS-TETHER-SPECIFIC POWER FACTOR RATIOING
! (IF ZERO IS ENTERED, DEFAULT TO 1.0)
      IF( (PWFDUM .NE. 0.0) ) CURDUM = PWFDUM*CURDUM


! SET IN APPROPRIATE AMPERAGE
! (ALSO ZERO AMPS EXIT PATH)
!----------------------------
2200  CURRF(JTETH) = CURDUM


! STANDARD RETURN FOR SCENARIO
2500  CONTINUE

      RETURN
      END
