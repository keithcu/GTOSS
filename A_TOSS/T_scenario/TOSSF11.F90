! ROUTINE: TOSSF11
! %Z%GTOSS %M% H.10 code v01.00
!              H.10 code v01.00 Space Elevator GEO deployment.
!-------------------------------------------------------------
!              H.10 code v01.01 (baseline for vH.10 delivery)
!*************************************
!*************************************
!
      SUBROUTINE TOSSF11(JS,JTETH)
!
!*************************************
!*************************************
! THIS ROUTINE DEPLOYS TETHER IN A MANNER CONSISTENT WITH POTENTIAL
! DEPLOYMENT OF A SPACE ELEVATOR TETHER STARTING FROM GEO.
!
! THE CONTROL MODE OF THIS SCENARIO PERFORMS THE FOLLOWING:
!
!  1. CREATES A DEPLOYMENT RATE CONSISTING OF THE SUM OF:
!
!     A BASELINE RATE DEFINED BY DEPLOY-RATE -VS- DEPLOYMENT-LENGTH
!
!     + A RATE EQUAL TO ALT RATE OF THE TETHER'S X-END ATTACH POINT



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"




      DIMENSION  DUMRX(3),  DUMRDX(3), DUMRDDX(3)
      DIMENSION  ARIDUM(3), AVIDUM(3), AAIDUM(3), UDUMR(3)


! SCRATCH DEPLOYMENT/RETRIEVAL INDICATOR
      LOGICAL DEPDUM


! EXTRACT (FREQUENTLY-USED) CURRENTLY DEPLOYED TETHER LENGTH
!-----------------------------------------------------------
      TLDUM = TLENT(JTETH)

! INITIALIZE LENGTH BASE PRIOR TO INTEGRATION
      IF(QTIME .LE. TDSCON(JS))  FPLEN(JTETH) = TLDUM

! REMEMBER TETHER LENGTH AT SCENARIO INITIALIZATION
      IF(QTIME .LE. TDSCON(JS)) BGLFP(JTETH) = TLDUM



! DO ABSOLUTE-TIME START/STOP DETERMINATION
!------------------------------------------
! GO TO NEXT TETHER IF ITS NOT TIME TO START THIS SCENARIO
      IF(QTIME .LT. TDSCON(JS)) GO TO 1500

      TLENTD(JTETH) = 0.0
      TLENDD(JTETH) = 0.0

! GO TO NEXT TETHER IF TIME TO STOP THIS SCENARIO
      IF( (TDSCOF(JS) .NE. 0.0)  .AND.   &
     &               (QTIME .GT. TDSCOF(JS))  ) GO TO 1500



!***************************************************************
!***************************************************************
! DETERMINE THE DEPLOY LENGTH DEPENDENT COMPONENT OF DEPLOY RATE
!***************************************************************
!***************************************************************
! USE SCENARIO DATA FROM APPLICABLE PERIOD

! SEE IF IT IS A DEPLOYMENT OR RETRIEVAL PROCESS
! (BY VIRTUE OF SIGN OF THE 1ST PERIODS DURATION)
!-----------------------------------------------
      DEPDUM = .TRUE.
      IF(DDINV1(JS) .LT. 0.0) DEPDUM = .FALSE.

! ASSUME PERIOD 1
      BGDUM = BGVAL1(JS)
      ENDUM = ENVAL1(JS)
      VXDUM = VALIM1(JS)
      DLDUM = DDINV1(JS)
      RLDUM = TLDUM - BGLFP(JTETH)
      IF(      DEPDUM  .AND. (RLDUM.LE.DLDUM)) GO TO 1310
      IF((.NOT.DEPDUM) .AND. (RLDUM.GE.DLDUM)) GO TO 1310

! ASSUME PERIOD 2
      BGDUM = BGVAL2(JS)
      ENDUM = ENVAL2(JS)
      VXDUM = VALIM2(JS)
      DLDUM = DDINV2(JS)
      RLDUM = RLDUM - DDINV1(JS)
      IF(      DEPDUM  .AND. (RLDUM.LE.DLDUM)) GO TO 1310
      IF((.NOT.DEPDUM) .AND. (RLDUM.GE.DLDUM)) GO TO 1310

! ASSUME PERIOD 3
      BGDUM = BGVAL3(JS)
      ENDUM = ENVAL3(JS)
      VXDUM = VALIM3(JS)
      DLDUM = DDINV3(JS)
      RLDUM = RLDUM - DDINV2(JS)
      IF(      DEPDUM  .AND. (RLDUM.LE.DLDUM)) GO TO 1310
      IF((.NOT.DEPDUM) .AND. (RLDUM.GE.DLDUM)) GO TO 1310

! ASSUME PERIOD 4
      BGDUM = BGVAL4(JS)
      ENDUM = ENVAL4(JS)
      VXDUM = VALIM4(JS)
      DLDUM = DDINV4(JS)
      RLDUM = RLDUM - DDINV3(JS)
      IF(      DEPDUM  .AND. (RLDUM.LE.DLDUM)) GO TO 1310
      IF((.NOT.DEPDUM) .AND. (RLDUM.GE.DLDUM)) GO TO 1310

! ASSUME PERIOD 5
      BGDUM = BGVAL5(JS)
      ENDUM = ENVAL5(JS)
      VXDUM = VALIM5(JS)
      DLDUM = DDINV5(JS)
      RLDUM = RLDUM - DDINV4(JS)

! END OF PERIOD DETERMINATION
1310  CONTINUE


!------------------------------------------------
! DO BASELINE LDOT DETERMINATION FOR THIS PERIOD
!------------------------------------------------
! DETERMINE BASELINE COMMANDED LDOT AT THIS POINT (VIA LINEAR RAMP)
      IF(DLDUM.NE.0.0) BS_RT_DUM = BGDUM + RLDUM*(ENDUM-BGDUM)/DLDUM



!************************************************************
!************************************************************
! ADD IN ATTACH POINT RATE DEPENDENT COMPONENT OF DEPLOY RATE
!************************************************************
!************************************************************
! GET THIS TOSS TETHER'S X-END ATTACH POINT AND RELATED OBJECT NUMBER
      JAX = LATTX(JTETH)
      JOX = NOBJX(JTETH)

! EXTRACT TETHER REF END (X) STATE WR/T TOSS REF PT
       DO 200 J=1,3
          DUMRX(J)   = APS(J+6 , JAX, JOX)
          DUMRDX(J)  = APS(J+9 , JAX, JOX)
          DUMRDDX(J) = APS(J+12, JAX, JOX)
200    CONTINUE

! FIND POS,VEL,ACCEL OF TETHER REF END (X) WR/T INER PT (INER FRAME)
!-------------------------------------------------------------------
       CALL VECSUM (RPI,   DUMRX,       ARIDUM)
       CALL VECSUM (RPID,  DUMRDX,      AVIDUM)
       CALL VECSUM (RPIDD, DUMRDDX,     AAIDUM)

! CALC ALTITUDE RATE & ACCEL OF X-END OF TETHER
!----------------------------------------------
      CALL VECNRM(ARIDUM,UDUMR)
      DUMHD  = DOT(UDUMR,AVIDUM)
      DUMHDD = DOT(UDUMR,AAIDUM)


!--------------------------------------------------------
! PROCESS FINAL COMPOSITE DEPLOY RATE AND STATE VARIABLES
!--------------------------------------------------------
! ADD THE ATT PT ALTITUDE RATE TO BASELINE RATE
      RATDUM = BS_RT_DUM + DUMHD

! APPLY TOSS TETHER MULTIPICATIVE FACTOR (0.0 DEFAULTS TO 1.0)
      IF(FACDEP(JTETH) .NE. 0.0) RATDUM = FACDEP(JTETH)*RATDUM

! LIMIT BASELINE DEPLOY RATE IF PERIOD HAS NON-TRIVIAL LIMIT VALUE
!-----------------------------------------------------------------
      IF((VXDUM .GT. 0.0) .AND. (RATDUM .GT. VXDUM)) RATDUM = VXDUM
      IF((VXDUM .LT. 0.0) .AND. (RATDUM .LT. VXDUM)) RATDUM = VXDUM

! ACCUM CHANGE TO INSTANTANEOUS UNDEFORMED LENGTH
!------------------------------------------------
      DUMINC = RATDUM*QDELT

! DONT DO TETHER LENGTH INTEGRATION UNTIL INTEGRATORS ARE RELEASED
      IF(JINTEG .EQ. 1) FPLEN(JTETH) = FPLEN(JTETH) + DUMINC

! SET IN INSTANTANEOUS DEPLOY LENGTH AND DERIVATIVES
!---------------------------------------------------
! (TO CALC DEPLOY LENGTH ACCEL, AND IGNORE TRANSIENTS
!  DUE TO RATE LIMITING AND SCENARIO-PERIOD TRANSITIONS)
      TLENT(JTETH)  = FPLEN(JTETH)
      TLENTD(JTETH) = RATDUM
      TLENDD(JTETH) = RATDUM*(ENDUM-BGDUM)/DLDUM + DUMHDD

! SET IN TETHER'S COMMANDED PARAMETER VALUE FOR THIS TETHER
      TTNCOM(JTETH) = BS_RT_DUM

! STANDARD RETURN FOR SCENARIO
1500  CONTINUE

      RETURN
      END
