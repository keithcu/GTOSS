! ROUTINE: CMODE2
! %Z%GTOSS %M% H.9 code v01.02
!              H.9 code v01.02  This is the latest vers from MFSC (Ken)
!-----------------------------------------------------------------------
!              H.7 code v01.01  New routine, baseline for vH.7 delivery
!****************************************
!****************************************
!
      SUBROUTINE CMODE2(EMIDUM, BNIDUM)
!
!****************************************
!****************************************
! THIS ROUTINE CALCULATES THE CURRENT DISTRIBUTION IN A FINITE TETHER
! ARISING DUE TO BARE WIRE ELECTRODYNAMIC OPERATIONS AND IN RESPONSE
! TO A PARTICULAR ELECTRICAL CONFIGURATION OF THE PLASMA-CONTACTING
! END OF THE TETHER.
!
! THIS ROUTINE DEFINES CURRENT DISTRIBUTION OF MODE 2, OR "SHUNT MODE"
!
! INPUTS
! ------
!         USES VARIOUS FINITE SOLN PARAMETERS VIA COMMON
!
! OUTPUTS
! -------
!         EMIDUM   CURRENT AT THE EMITTER END OF TETHER
!         BNIDUM   NET CURRENT TO A BATTERY
!
!         THE OFFICIAL TETHER CURRENT DISTRIBUTION CURRTS(NSEG),
!         RETURNED VIA COMMON



! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND 
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.i"





      DIMENSION CTBDUM(2*MAXSEG+5)

      DIMENSION DMODE2(36,14)


!** Following Data is from Bob Estes for Al tether
! DEFINE TABLE OF POLYNOMIAL COEFFICIENTS FOR FOURTH ORDER POLY
! IN EDENS, EMF, AND RESISTANCE THAT
! PORTRAYS MEASURED CURRENT AT EMITTER FOR MODE 4
!-----------------------------------------------------------------------

      data ((DMODE2(i,j),j=1,5),i=1,36) /   &
!      >1.e12,     >1.e12,     >1.e12,     >1.e12,     >1.e12
!      250-300,    250-300,    300-400,    400-600,    600-1600
!      140-200,    200-300,    140-300,    140-300,    140-200
!         1            2           3           4           5   &
     & 4.8096E-05, 4.5759E-05, 4.1683E-05, 3.4897E-04, 1.7521E-04,   &
     & 3.2392E-05, 6.2710E-05, 3.5094E-05, 5.2712E-04, 2.4479E-04,   &
     & 3.9049E-04, 3.7601E-04, 3.7504E-04, 2.5892E-03, 1.6701E-03,   &
     & 1.3230E-04, 1.1423E-04, 1.2266E-04, 1.1554E-03, 8.7497E-04,   &
     &-2.7033E-05, 5.5523E-05, 2.9318E-05, 7.6129E-04, 3.1994E-04,   &
     & 2.2088E-03, 2.1569E-03, 2.3818E-03, 1.2760E-02, 1.1248E-02,   &
     & 3.6337E-04, 2.7853E-04, 3.4715E-04, 3.3995E-03, 3.2352E-03,   &
     & 2.2871E-04, 2.9347E-04, 1.0867E-04, 2.4988E-03, 2.0472E-03,   &
     & 9.0580E-05, 1.5760E-04, 6.0975E-05, 1.5125E-03, 1.1331E-03,   &
     & 9.2452E-04, 8.3407E-04, 9.9528E-04, 6.0728E-03, 5.6576E-03,   &
     &-1.8541E-04,-5.1470E-05, 1.2470E-04, 9.9536E-04, 3.6654E-04,   &
     &-4.0689E-04,-2.9728E-04,-4.6676E-04,-1.0817E-04,-9.8722E-04,   &
     & 9.9654E-04, 6.5753E-04, 9.2126E-04, 7.4794E-03, 3.2773E-03,   &
     &-2.7411E-04,-1.6860E-04,-1.2340E-04, 1.4799E-03, 1.9130E-03,   &
     &-6.9376E-05, 1.5011E-04,-7.0327E-05, 1.5864E-03, 1.2506E-03,   &
     & 1.1741E-03,-1.1688E-03,-2.1565E-03,-5.6583E-03, 9.7543E-03,   &
     & 3.0555E-03, 3.3429E-03, 4.8607E-03,-3.2314E-03,-8.6506E-04,   &
     & 2.5090E-04, 3.8143E-04,-1.3057E-05, 3.1547E-03, 3.4163E-03,   &
     & 2.0821E-03, 1.6257E-03, 2.2096E-03, 9.2186E-03, 1.8065E-03,   &
     & 4.0434E-04, 6.5190E-04,-6.9141E-05, 3.1545E-03, 5.6365E-03,   &
     &-5.5706E-04,-4.5852E-04, 8.1021E-04, 9.9188E-04, 2.9119E-04,   &
     & 1.3536E-05, 9.9001E-06, 1.2835E-05, 5.4008E-06, 1.2347E-04,   &
     & 6.8867E-04, 8.7488E-04,-8.8208E-04,-1.1833E-03,-1.0721E-03,   &
     &-1.4869E-03,-9.5631E-04, 1.7064E-03, 8.4033E-05, 1.5328E-04,   &
     &-4.9732E-04,-6.1497E-05,-6.0484E-05, 4.2592E-04, 8.2755E-04,   &
     & 2.1242E-04, 4.2219E-04,-4.8115E-05, 6.7957E-04,-5.9086E-03,   &
     &-9.6644E-04,-5.6224E-04, 4.9557E-04, 6.7759E-04,-2.4310E-05,   &
     &-1.8347E-04, 3.7319E-04,-8.8527E-04,-4.5966E-04, 1.4114E-03,   &
     & 3.9495E-04, 1.0375E-03,-2.3473E-03,-1.7367E-03,-7.0886E-04,   &
     &-1.0929E-03,-5.6668E-06,-1.0151E-03,-1.8841E-03, 6.1861E-04,   &
     &-4.0927E-07,-3.6544E-07, 1.8910E-06, 4.5685E-07, 7.4976E-06,   &
     & 4.3174E-03, 2.3014E-03, 2.6690E-03,-4.7751E-04, 3.0372E-05,   &
     & 1.3536E-05, 9.9001E-06, 1.2835E-05, 5.4008E-06, 1.2347E-04,   &
     & 1.6215E-04, 1.1453E-04, 9.6984E-05, 2.2057E-05, 2.1649E-05,   &
     &-1.7602E-03,-1.4156E-03,-1.5990E-03, 4.9511E-05,-3.1256E-05,   &
     & 2.7291E-03, 1.4796E-03, 2.1625E-03,-2.4752E-04,-1.1116E-04/




      data ((DMODE2(i,j),j=6,10),i=1,36) /   &
!      >1.e12,     <1.e11,     <1.e11,     <1.e11,     <1.e11
!      600-1600,   250-600,    250-600,    600-1600,   600-1600
!      200-300,    140-200,    200-300,    140-200,    200-300
!         6           7            8           9           10   &
     & 4.3029E-04,-7.5855E-03,-1.6741E-03, 1.6939E-03, 4.5979E-04,   &
     & 6.5463E-04,-9.0036E-03,-4.5615E-03, 2.2100E-03, 8.6999E-04,   &
     & 3.6476E-03, 3.4106E-02, 3.3184E-02, 1.3649E-03, 5.2777E-04,   &
     & 2.2819E-03, 3.3265E-03, 1.3203E-03, 4.1518E-03, 1.3239E-03,   &
     & 8.6123E-04,-8.1310E-03,-5.6607E-03, 2.4883E-03, 1.4561E-03,   &
     & 2.2115E-02, 3.2031E-03, 2.1201E-03, 6.6333E-04, 2.8562E-04,   &
     & 8.5966E-03, 1.2466E-03, 3.7954E-03, 2.6766E-05, 1.2777E-03,   &
     & 3.2762E-03, 1.1503E-02, 2.1843E-02, 2.2611E-03, 1.0299E-03,   &
     & 2.9275E-03, 8.1011E-03, 6.5248E-05, 4.6479E-03, 1.9517E-03,   &
     & 1.2871E-02, 5.4262E-02, 4.9116E-02, 5.5393E-03, 3.0746E-03,   &
     & 8.5380E-04,-3.5209E-03, 6.4109E-04, 1.7935E-03, 1.7253E-03,   &
     &-9.4012E-04,-2.4634E-03,-2.8454E-03, 2.8179E-04, 1.5148E-04,   &
     & 4.5601E-03, 1.2888E-04,-5.4784E-04, 6.9045E-06,-8.2989E-05,   &
     & 5.0678E-04,-1.3917E-02,-1.3085E-02, 3.7718E-03, 1.8876E-03,   &
     & 2.7393E-03, 1.3023E-02, 6.2879E-03, 3.3765E-03, 2.0899E-03,   &
     &-9.8589E-03,-9.4083E-03,-1.1422E-02, 1.1246E-03, 4.9736E-04,   &
     & 2.0333E-03,-8.2695E-03,-1.1565E-02, 2.0850E-03, 1.2830E-03,   &
     & 6.5854E-03,-7.4038E-03,-3.8321E-03,-2.1281E-03,-1.6716E-03,   &
     &-5.6565E-03, 2.0116E-02, 2.1454E-02, 1.3893E-02, 1.4582E-02,   &
     & 3.1479E-03, 9.9539E-05,-2.2113E-04, 8.0032E-03, 4.4902E-03,   &
     & 4.1177E-04, 3.5926E-03, 3.0238E-04,-1.6815E-03,-9.4720E-04,   &
     & 3.8770E-05,-1.1444E-03, 1.0593E-03, 4.7751E-04, 2.0957E-04,   &
     &-1.6640E-03, 2.2576E-04, 1.7879E-04, 9.1106E-06, 3.0930E-05,   &
     &-1.1755E-03,-6.2725E-03,-1.4536E-04, 6.3319E-03, 3.0412E-03,   &
     & 9.8683E-04,-5.8715E-03,-1.4705E-03,-2.9790E-03,-6.4806E-04,   &
     & 2.0237E-03,-4.1268E-03,-9.7904E-04, 1.9308E-03, 7.2377E-04,   &
     &-2.4203E-04,-5.9434E-04,-2.7464E-03, 2.4499E-03, 6.6184E-04,   &
     & 1.4635E-03, 1.7248E-03, 4.5150E-04, 8.5800E-04, 2.8730E-04,   &
     & 1.3371E-03,-6.1474E-03,-2.9576E-03,-4.7671E-03,-2.6303E-03,   &
     &-4.1163E-03, 1.4032E-02, 4.6750E-03, 1.1451E-02, 3.6125E-03,   &
     & 1.1419E-05, 1.1779E-03, 1.4903E-03, 8.2676E-05, 8.3051E-05,   &
     & 1.2292E-04,-1.3426E-03,-1.6973E-03,-2.9106E-04,-3.7375E-04,   &
     & 3.8770E-05,-1.1444E-03, 1.0593E-03, 4.7751E-04, 2.0957E-04,   &
     &-1.0759E-05, 2.3674E-03, 1.6315E-03,-1.2712E-06, 2.8820E-04,   &
     &-3.5947E-05, 7.9353E-05, 8.8777E-04,-1.0316E-03,-9.8788E-04,   &
     &-8.9578E-05,-2.3642E-06, 3.7841E-05, 1.1165E-06, 2.4802E-06/




      data ((DMODE2(i,j),j=11,14),i=1,36) /   &
!      (e11,e12),  (e11,e12),  (e11,e12),  (e11,e12)
!      250-600,    250-600,    600-1600,   600-1600
!      140-200,    200-300,    140-200,    200-300
!         11          12          13          14   &
     &-1.7906E-03,-2.9807E-02,-9.3236E-04, 2.3172E-03,   &
     &-6.3961E-03,-3.5531E-02,-2.0201E-03, 2.5882E-03,   &
     & 2.7007E-03,-1.2376E-02, 9.8878E-03, 2.2030E-02,   &
     & 6.9764E-03,-2.5408E-03, 8.5188E-04, 6.3050E-03,   &
     &-1.1726E-02,-2.2064E-02,-3.1095E-03, 1.4326E-03,   &
     & 1.1807E-02, 1.3492E-02, 2.3324E-02, 3.6705E-02,   &
     & 1.8589E-02, 6.5317E-02, 2.1257E-02, 2.4717E-03,   &
     &-9.1384E-03,-9.7645E-03, 1.0547E-02, 1.6109E-02,   &
     &-4.2208E-04,-4.4321E-03,-4.4607E-03, 4.5252E-03,   &
     & 6.3127E-02, 6.6208E-02, 3.7232E-02, 8.0991E-02,   &
     &-1.3150E-02, 2.3250E-02,-2.9168E-03,-1.8711E-03,   &
     & 1.7292E-03, 2.4620E-03,-6.2485E-03, 1.2638E-03,   &
     &-5.4937E-03,-1.1725E-02,-1.6281E-03,-1.1072E-04,   &
     &-1.7335E-02, 9.2182E-03, 9.3610E-03,-4.9939E-03,   &
     &-5.4236E-03, 2.4165E-03,-7.7894E-03,-1.6846E-03,   &
     &-2.5108E-02,-1.7781E-02, 1.3658E-02,-4.9202E-02,   &
     &-1.0372E-02,-1.3739E-02,-3.7173E-03,-5.4187E-03,   &
     &-2.0526E-03,-2.1429E-02,-9.7090E-03,-2.4993E-03,   &
     & 7.7519E-03, 1.1842E-02, 2.9753E-03,-1.1028E-03,   &
     & 4.8487E-02, 1.1887E-02, 2.5887E-02, 1.5004E-02,   &
     & 3.6168E-03,-4.4391E-03, 2.2719E-03,-4.9092E-04,   &
     & 2.3751E-04, 6.9311E-05, 1.5463E-03, 7.4139E-04,   &
     & 1.6163E-03, 1.5337E-03, 2.7201E-04, 4.8838E-05,   &
     & 6.3676E-03,-2.0260E-03, 4.5692E-03, 1.0617E-03,   &
     & 1.0149E-02, 3.8579E-04, 3.0937E-03, 9.9193E-04,   &
     & 6.4378E-03, 3.0986E-03,-1.7329E-02, 6.9400E-03,   &
     &-4.8751E-04, 7.7946E-04,-1.0325E-03,-6.5225E-04,   &
     &-4.2019E-03, 2.4160E-03, 1.7031E-03, 2.5205E-04,   &
     &-1.7009E-03,-1.8293E-03,-6.2767E-04, 8.9992E-05,   &
     &-1.6014E-02,-3.7403E-03,-5.1629E-03,-4.2115E-03,   &
     &-1.1401E-04,-1.5212E-04, 1.1336E-04,-1.4296E-04,   &
     &-4.9402E-04,-5.0272E-04,-1.1698E-04,-1.7059E-05,   &
     & 2.3751E-04, 6.9311E-05, 1.5463E-03, 7.4139E-04,   &
     & 3.2618E-04, 4.9266E-04, 3.3811E-05, 8.3661E-05,   &
     & 1.8649E-04,-1.6573E-05, 1.4257E-04, 1.4116E-04,   &
     & 2.3644E-04, 5.9687E-04, 3.8717E-05, 2.0435E-06/




! IDENTIFY MODE OF CURRENT OPS FOR OTHERS
!----------------------------------------
      CURMOD = 2.


!*********************************************************************
! STATE VALUE OF BATTERY VOLTAGE AND IMPEDANCE ASSOCIATED W/ THIS MODE
!*********************************************************************
      VCDUM = 90.0
      ZCDUM = -4.0

!*********************************************************************
! DETERMINE TETHER CURRENT AT EMITTER VIA POLYNOMIAL APPROXIMATION
!*********************************************************************
!
! FIRST, DETERMINE IF THERMAL CALCULATIONS ARE INVOKED IN TOSS
!   IF SO, DETERMINE RESISTANCE AT AVERAGE TETHER TEMP
!   IF NOT, USE ROOM TEMP RESISTANCE

      TETHTMP = 293.0
      IF (JQTHRM .EQ. 1) THEN
        DUMSUM = 0.
        DO J=1,NBEAD+1
          DUMSUM = DUMSUM + STEMPK(J)
        ENDDO
        TETHTMP = DUMSUM/REAL(NBEAD+1)
      ENDIF

      RESIST = 265.*(1. + 0.0045*(TETHTMP-293.))

!**
! determine if there is enough voltage to overcome vcdum

      if (EMFTOT .lt. VCDUM) then
         EMIDUM = 0.
         goto 99
      endif


!
! NEXT, Compute Scaled EMF (SCEMF), EDENS (SCED),
! AND Resistance (SCRES) for polynomial use
!

      SCED = EDENS(1)/1.0E+11
      SCEMF = EMFTOT/100.
      SCRES = RESIST/100.

!
! NEXT, DETERMINE POLY REGION (JCOL OF DMODE2) FOR PRESENT CONDITIONS
!
      IF (SCED .GT. 10.) THEN
        IF (SCEMF .LT. 3.) THEN
          JCOL = 2
          IF (SCRES .LT. 2.) JCOL = 1
        ELSE IF (SCEMF .LT. 4.) THEN
          JCOL = 3
        ELSE IF (SCEMF .LT. 6.) THEN
          JCOL = 4
        ELSE
          JCOL = 6
          IF (SCRES .LT. 2.) JCOL = 5
        ENDIF
      ELSE IF (SCED .LT. 1.) THEN
        IF (SCEMF .LT. 6.) THEN
          JCOL = 8
          IF (SCRES .LT. 2.) JCOL = 7
        ELSE
          JCOL = 10
          IF (SCRES .LT. 2.) JCOL = 9
        ENDIF
      ELSE
        IF (SCEMF .LT. 6.) THEN
          IF (SCRES .LT. 3.) JCOL = 12
          IF (SCRES .LT. 2.) JCOL = 11
        ELSE
          JCOL = 14
          IF (SCRES .LT. 2.) JCOL = 13
        ENDIF
      ENDIF

!
! NEXT, COMPUTE EMMITTER CURRENT USING JCOL COLUMN OF COEFFICIENTS
!   FROM DMODE2 MATRIX
!
      EMIDUM = DMODE2(1,JCOL) + DMODE2(2,JCOL)*SCRES   &
     &       + DMODE2(3,JCOL)*SCED + DMODE2(4,JCOL)*SCEMF   &
     & + DMODE2(5,JCOL)*SCRES**2 + DMODE2(6,JCOL)*SCED**2   &
     & + DMODE2(7,JCOL)*SCEMF**2 + DMODE2(8,JCOL)*SCRES*SCED   &
     & + DMODE2(9,JCOL)*SCRES*SCEMF + DMODE2(10,JCOL)*SCED*SCEMF   &
     & + DMODE2(11,JCOL)*SCRES**3 + DMODE2(12,JCOL)*SCED**3   &
     & + DMODE2(13,JCOL)*SCEMF**3 + DMODE2(14,JCOL)*SCED*SCRES**2   &
     & + DMODE2(15,JCOL)*SCEMF*SCRES**2 + DMODE2(16,JCOL)*SCRES*SCED**2   &
     & + DMODE2(17,JCOL)*SCEMF*SCED**2 + DMODE2(18,JCOL)*SCRES*SCEMF**2   &
     & + DMODE2(19,JCOL)*SCED*SCEMF**2+DMODE2(20,JCOL)*SCRES*SCED*SCEMF   &
     & + DMODE2(21,JCOL)*SCRES**4 + DMODE2(22,JCOL)*SCRES*SCED**3   &
     & + DMODE2(23,JCOL)*SCRES*SCEMF**3 + DMODE2(24,JCOL)*SCED*SCRES**3   &
     & + DMODE2(25,JCOL)*SCEMF*SCRES**3   &
     & + DMODE2(26,JCOL)*(SCRES**2)*(SCED**2)   &
     & + DMODE2(27,JCOL)*SCRES*SCEMF*SCED**2   &
     & + DMODE2(28,JCOL)*(SCRES**2)*(SCEMF**2)   &
     & + DMODE2(29,JCOL)*SCRES*SCED*SCEMF**2   &
     & + DMODE2(30,JCOL)*(SCRES**2)*SCED*SCEMF   &
     & + DMODE2(31,JCOL)*SCED**4 + DMODE2(32,JCOL)*SCED*SCEMF**3   &
     & + DMODE2(33,JCOL)*SCRES*SCED**3   &
     & + DMODE2(34,JCOL)*SCEMF*SCED**3   &
     & + DMODE2(35,JCOL)*(SCED**2)*(SCEMF**2)   &
     & + DMODE2(36,JCOL)*SCEMF**4


 99   continue

! VISIT PROGRAM TO EVALUATE THE ANALYTICAL CURRENT DISTRIBUTION
!--------------------------------------------------------------
      CALL CURCAL(VCDUM, ZCDUM, RESIST, EMIDUM,   CTBDUM)


! DETERMINE NET CURRENT TO THE BATTERY
!-------------------------------------
      BNIDUM = 0.0


!**************************************************************
! PUT SEGMENT-SPECIFIC NON-UNIFORM CURRENT VALUES INTO OFFICIAL
! FINITE SOLN ARRAY BY EXTRACTING CURRENT AT SEGMENT MID POINTS
!**************************************************************
      DO 111 JSEG = 1, NBEAD+1
             DISDUM = ARCTOT*REAL(2*JSEG-1)/REAL(2*(NBEAD+1))
             CURRTS(JSEG) = SLINT(DISDUM, CTBDUM)
111   CONTINUE

      RETURN


! TABLE VALUE ERROR NOTIFICATION
!-------------------------------
1000  CONTINUE

!C NOTIFY USER ABOUT OUT OF RANGE ARGUMENTS
!      WRITE (LUE,301) ILFDUM
!301   FORMAT (' MODE2 2-D TABLE ARGS OUT OF BOUNDS, DLINT ERR =',I2)
!      write(lue,302) emftot,edens(1)
!302   format('EMF = ',f7.2,' Electron Density = ',e9.3)
!      STOP 'IN CMODE2'

      END
