! ROUTINE: XSNAPL
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!***************************************************
!***************************************************
!
      SUBROUTINE XSNAPL(LRRAY, NSIZE, NEXREC)
!
!***************************************************
!***************************************************
! THIS ROUTINE WRITES THE INDICATED ARRAY (OF THE
! INDICATED SIZE) TO THE LATE START SNAP-SHOT FILE
!
! INPUT ARGS
!       LRRAY = LOGICAL ARRAY
!               -------
!       NSIZE = NUMBER OF ITEMS IN ARRAY
!
! OUTPUT ARGS
!       NEXREC = NEXT RECORD NUMBER AVAIL IN SNAP SHOT FILE
!
! NOTE THAT THIS ROUTINE IS PRIVY TO THE OFFICIAL RECORD SIZE
!      AND I/O UNIT DEVOTED TO WRITING THE SNAP SHOT (AS WELL
!      AS THE RDBOUT ARRAY), ALL VIA COM_ROSS.


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_ROSS.i"




      LOGICAL LRRAY(NSIZE)


      NITML = NSIZE
      JITM1 = 0
      JITM2 = 0

100   IF(NITML .LE. 0)  RETURN

          JITM1 = JITM2 + 1

          IF(NITML .GT. NRECS)  THEN
                 JITM2 = JITM2 + NRECS
          ELSE
                 JITM2 = NSIZE
          END IF

          JR = 1
          DO 110 J = JITM1,JITM2
             LOGOUT(JR) = LRRAY(J)
             JR = JR + 1
110       CONTINUE

! WRITE THIS RECORD TO FILE
          WRITE(IOUXXO, REC=NEXREC) (LOGOUT(J),J=1,NRECS)
          NEXREC = NEXREC + 1
          NITML  = NITML - NRECS

          GO TO 100

      END
