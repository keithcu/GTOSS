! ROUTINE: XSNAP
! %Z%GTOSS %M% H.12 code v02.0
!              H.12 code v02.0 Upgrades to include new Fin Soln & Objs
!---------------------------------------------------------------------
!              H.10 code v01.1 Added changes to use DBLOUT for output.
!                              Increased no. of TOSS Objects to 15.
!---------------------------------------------------------------------
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************
!*************************************
!
      SUBROUTINE XSNAP
!
!*************************************
!*************************************
! THIS SUBROUTINE WILL CREATE A NEW DIRECT ACCESS
! BINARY FILE CONTAINING ALL OF TOSS ESSENTIAL COMMON
! REQUIRED TO START A NEW TOSS RUN AT THIS POINT IN TIME

! NOTE: THE I/O UNIT NUMBER USUALLY ASSOCIATED WITH THE
!       RDB O-FILE IS USED TO WRITE THE LATE START. THIS IS
!       POSSIBLE BECAUSE THE O-FILE IS ONLY OPEN AT RDB
!       WRITE TIME, THUS THE I/O CHANNEL CAN BE BORROWED
!       OCCASIONALLY, AS PER THIS ROUTINE

! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND 
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS

! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND 
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS

      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
      include "../../A_HDR/COM_ROSS.i"

      CHARACTER *2 DUMNUM



! INITIALIZE SEQUENCE NUMBER FOR SNAP-EVENT FILE NAMING   
!------------------------------------------------------
      JSNAP = JSNAP + 1
      
! MAKE A SNAP SEQUENCE NUMBER CHAR STRING FOR ALL TO USE
      WRITE (DUMNUM,11)  JSNAP
11    FORMAT(I2.2)



!*************************************************
!*************************************************
!*************************************************
!   CREATE THE GENERAL TOSS-DATA SNAP SHOT FILE
!*************************************************
!*************************************************
!*************************************************
! INITIALIZE RECORD POINTER
      NEXDUM = 1

! MAKE THE NAME
      SNAPNM = 'TSNAP'//DUMNUM

! MAKE A FACILTY-DEPENDENT PATH TO THE SNAP SHOT DATA FILE
      CALL GETPTH (DBROOT, SNAPNM,   DBPATH)

! CREATE THE LATE START SNAP-SHOT DATA FILE
! (THIS FILE HAS AN APRIORI, AGREED-UPON, RECORD SIZE)
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      OPEN(IOUXXO, FILE = DBPATH, &
     &             FORM='UNFORMATTED',  STATUS='UNKNOWN', &
     &             ACCESS='DIRECT',     RECL=2*NRBYTE*NRECS)


!************************************
! WRITE SNAP-SHOTS OF THE TOSS ARRAYS
!************************************
! GENERAL TOSS COMMON
      CALL XSNAPI(LTOSQ, NLQ, NEXDUM)
      CALL XSNAPI(JTOSQ, NJQ, NEXDUM)
      CALL XSNAPS(FTOSQ, NFQ, NEXDUM)
      CALL XSNAPS(DTOSQ, NDQ, NEXDUM)

! TOSS OBJECT WORKING AREA
! NOTE: THIS AREA IS NOT USED AS SUCH W/ THE ADVENT OF OBJ DATA TYPES,
!       RATHER IT IS MAINTAINED FOR INPUT PROCESSING ROLE, AND INCLUDED
!       HERE IN CASE IT IS NECESSARY FOR LATE-START RECONSTRUCTION
!
! NOTE: THE SIZE PARAMETERS MAY BE SUSPECT, SINCE THE OBJ DATA TYPES
!       ARE NOT SELF-MAINTAINING WR/T THESE ARRAYS
!-------------------------------------------------
      CALL XSNAPI(LTOSW, NLW, NEXDUM)
      CALL XSNAPI(JTOSW, NJW, NEXDUM)
      CALL XSNAPS(FTOSW, NFW, NEXDUM)
      CALL XSNAPS(DTOSW, NDW, NEXDUM)

! CLOSE THIS SNAP-SHOT FILE
!--------------------------
      CLOSE(IOUXXO,STATUS='KEEP')




!*****************************************************
!*****************************************************
!*****************************************************
!   CREATE THE TOSS OBJECT DATA-TYPE SNAP SHOT FILE
!*****************************************************
!*****************************************************
!*****************************************************
! MAKE THE NAME
      SNAPNM = 'OSNAP'//DUMNUM

! MAKE A FACILTY-DEPENDENT PATH TO THE SNAP SHOT DATA FILE
      CALL GETPTH (DBROOT, SNAPNM,   DBPATH)

! DETERMINE THE REQUIRED RECORD SIZE
      INQUIRE(IOLENGTH=JOBNREC) TOBJ_2

      print*,' '
      print*,' '
      print*,'Object data file RECL',JOBNREC
      print*,' '
      
! CREATE THE LATE START DATA FILE
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      OPEN(IOUXXO, FILE = DBPATH, &
     &             FORM='UNFORMATTED', STATUS='UNKNOWN', &
     &             ACCESS='DIRECT',    RECL=JOBNREC)


!***********************************************
! WRITE SNAP-SHOTS OF THE TOSS OBJECT DATA-TYPES
!***********************************************
! WRITE TOSS OBJECT DATA-TYPE RECORDS TO THIS FILE
      WRITE(IOUXXO, REC= 1) TOBJ_2
      WRITE(IOUXXO, REC= 2) TOBJ_3
      WRITE(IOUXXO, REC= 3) TOBJ_4
      WRITE(IOUXXO, REC= 4) TOBJ_5
      WRITE(IOUXXO, REC= 5) TOBJ_6
      WRITE(IOUXXO, REC= 6) TOBJ_7
      WRITE(IOUXXO, REC= 7) TOBJ_8
      WRITE(IOUXXO, REC= 8) TOBJ_9
      WRITE(IOUXXO, REC= 9) TOBJ_10
      
!      WRITE(IOUXXO, REC=10) TOBJ_11
!      WRITE(IOUXXO, REC=11) TOBJ_12
!      WRITE(IOUXXO, REC=12) TOBJ_13
!      WRITE(IOUXXO, REC=13) TOBJ_14
!      WRITE(IOUXXO, REC=14) TOBJ_15

! CLOSE THIS SNAP-SHOT FILE
!--------------------------
      CLOSE(IOUXXO,STATUS='KEEP')




!*****************************************************
!*****************************************************
!*****************************************************
!   CREATE THE FINITE-SOLN DATA-TYPE SNAP SHOT FILE
!*****************************************************
!*****************************************************
!*****************************************************
! MAKE THE NAME
      SNAPNM = 'FSNAP'//DUMNUM

! MAKE A FACILTY-DEPENDENT PATH TO THE SNAP SHOT DATA FILE
      CALL GETPTH (DBROOT, SNAPNM,   DBPATH)

! DETERMINE THE REQUIRED RECORD SIZE
      INQUIRE(IOLENGTH=JFSNREC) FSN_1

      print*,'finite data file RECL',JFSNREC
      print*,' '
      print*,' '
      

! CREATE THE LATE START DATA FILE
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      OPEN(IOUXXO, FILE = DBPATH, &
     &             FORM='UNFORMATTED', STATUS='UNKNOWN', &
     &             ACCESS='DIRECT',    RECL=JFSNREC)


!***********************************************
! WRITE SNAP-SHOTS OF THE FINITE SOLN DATA-TYPES
!***********************************************
! WRITE FINITE SOLN DATA-TYPE RECORDS TO THIS FILE
      WRITE(IOUXXO, REC= 1) FSN_1
      WRITE(IOUXXO, REC= 2) FSN_2
      WRITE(IOUXXO, REC= 3) FSN_3
      WRITE(IOUXXO, REC= 4) FSN_4
      WRITE(IOUXXO, REC= 5) FSN_5
      WRITE(IOUXXO, REC= 6) FSN_6
      WRITE(IOUXXO, REC= 7) FSN_7
      WRITE(IOUXXO, REC= 8) FSN_8
      WRITE(IOUXXO, REC= 9) FSN_9
      WRITE(IOUXXO, REC=10) FSN_10
      WRITE(IOUXXO, REC=11) FSN_11
      WRITE(IOUXXO, REC=12) FSN_12
      WRITE(IOUXXO, REC=13) FSN_13
      WRITE(IOUXXO, REC=14) FSN_14
      WRITE(IOUXXO, REC=15) FSN_15
     
!      WRITE(IOUXXO, REC=16) FSN_16
!      WRITE(IOUXXO, REC=17) FSN_17
!      WRITE(IOUXXO, REC=18) FSN_18
!      WRITE(IOUXXO, REC=19) FSN_19
!      WRITE(IOUXXO, REC=20) FSN_20
!      WRITE(IOUXXO, REC=21) FSN_21
!      WRITE(IOUXXO, REC=22) FSN_22
!      WRITE(IOUXXO, REC=23) FSN_23
!      WRITE(IOUXXO, REC=24) FSN_24
!      WRITE(IOUXXO, REC=25) FSN_25
!      WRITE(IOUXXO, REC=26) FSN_26
!      WRITE(IOUXXO, REC=27) FSN_27
!      WRITE(IOUXXO, REC=28) FSN_28
!      WRITE(IOUXXO, REC=29) FSN_29
!      WRITE(IOUXXO, REC=30) FSN_30
!      WRITE(IOUXXO, REC=31) FSN_31

! CLOSE THIS SNAP-SHOT FILE
!--------------------------
      CLOSE(IOUXXO,STATUS='KEEP')


      RETURN
      END
