! ROUTINE: TOSS_OBJ_IN_REST
! %Z%GTOSS %M% H.12 code v01.00
!              H.12 code v01.00 New routine to allow TOSS OBJ Late start input
!-----------------------------------------------------------------------------
!              H.11 code v01.00 baseline code for F90 conversion
!************************************
!************************************
!
      SUBROUTINE TOSS_OBJ_IN_REST
!
!************************************
!************************************
! THIS ROUTINE IS ONLY FOR SUPPORT OF LATE-START DATA SNAP SHOT
! RUN REENACTMENT. THIS ROUTINE IS THE EXACT INVERSE OF THE ROUTINE
! TOSS_OBJ_IN USED FOR NORMAL USER INPUT OF TOSS OBJECT DATA.
!
! THIS ROUTINE MOVES USER ENTERED DATA FROM THE OFFICIAL OBJ 
! DATA-TYPE DATA AREA FOR THE SPECIFIED OBJECT INTO THE INPUT ARRAY
! USED TO ALLOW USER INPUT. THUS AFTER EXECUTION, IT IS AS THOUGH
! THE USER HAD JUST ENTERED ALL THE ORIGINAL TOSS OBJ DATA THAT 
! HAD BEEN USED TO CREATE THE LATE START FROM THE BEGINNING.
!
! MORE INFO CAN BE OBTAINED BY EXAMINING THE ROUTINE: TOSS_OBJ_IN


! GAIN ACCESS TO THE TOSS DATA STRUCTURE AND 
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS

      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"

      include "../../A_HDR/EQU_OBJI.i"

! SPECIAL NOTE: THE "NAME RE-DEFINE" INCLUDE FILE THAT ACTS ON TRADITIONAL
!               TOSS OBJECT VARIABLE NAMES CANNOT BE USED IN THIS PROGRAM
!               SINCE THE EXPLICIT STRINGS THAT IT THEREBY RE-DEFINES ARE
!               HAVE ALSO BEEN USED (FOR CLARITY) AS TOKENS  WITHIN THE 
!               NAMES ASSOCIATED WITH CORRESPONDING INPUT_ARRAY -TO- 
!               TOSS_OBJECT VARIABLE NAME MAPPINGS DEFINED IN THE INCLUDE
!               FILE "EQU_OBJI.i"



!************************************************************************
!************************************************************************
!************************************************************************
! BELOW IN OBJECT DATA STRUCTURE IS USER-INPUT CONSTANTS DEFINING OBJECT
!************************************************************************
!************************************************************************
!************************************************************************
! IMPORTANT NOTE: THE DATA ITEMS BELOW MUST BE KEPT SYNCHRONIZED AND
!                 CONSISTENT WITH THE OBJI.i OBJECT INPUT ITEM-MAPPING
!                 EQUIVALENCE STATEMENTS. SO IF YOU ADD A NEW OBJECT
!                 INPUT DATA CONSTANT IT MUST BE RECOGNIZED IN TWO PLACES.

! INTEGER INPUT DATA DEFINING A TOSS OBJECT
        NATPAT_in  =  pTO%iNATPAT
        LAERO_in   =  pTO%iLAERO 
        LCONT_in   =  pTO%iLCONT 
        LICTRN_in  =  pTO%iLICTRN
        LICROT_in  =  pTO%iLICROT
        LICSPL_in  =  pTO%iLICSPL
        LNOMOM_in  =  pTO%iLNOMOM
        LEULER_in  =  pTO%iLEULER
        LGRVON_in  =  pTO%iLGRVON
        LGGTON_in  =  pTO%iLGGTON
        LIBTYP_in  =  pTO%iLIBTYP
        LICOMT_in  =  pTO%iLICOMT
        LINERT_in  =  pTO%iLINERT

! THESE ITEMS ARE ASSOCIATED WITH THE SKIP ROPE DAMPER FEATURE
!-------------------------------------------------------------
! ATTACH PT AND TOSS OBJECT NUMBER TO WHICH DAMPER BASE IS INSTALLED
        LOBJSR_in = pTO%iLOBJSR
        LAPPSR_in = pTO%iLAPPSR
! NUMBER OF ANCHOR POINTS FOR DAMPER LINK ARMS
        LNCHOR_in = pTO%iLNCHOR
! TOSS TETHER CONNECTING DAMPER CENTER-PIECE TO DAMPER BASE ATTACH PT
        LTHBAS_in = pTO%iLTHBAS
! TOSS TETHER WHOSE SKIP ROPE MOTION IS TO BE DAMPED
        LTHDMP_in = pTO%iLTHDMP
! TYPE OF INTEGRATOR TO BE USED FOR TOSS OBJECT CENTERPIECE (1=EULER)
        LSRDIN_in = pTO%iLSRDIN

! OBJ AND AP WR/T WHICH CONTROL OPT 4 PROVIDES INLINE-THRUST/LIB CONTROL
!-----------------------------------------------------------------------
        LREFOB_in = pTO%iLREFOB
        LREFAT_in = pTO%iLREFAT
        LAPLIB_in = pTO%iLAPLIB
! TOSS TETHER NUMBER WHICH CONTROL OPT 4 USES TO ACTIVATE INLINE-THRUST
        LTTNNL_in = pTO%iLTTNNL


! ATTITUDE CONTROL FRAME-REF SPECIFIER FOR CONTROL OPT 5
!-------------------------------------------------------
        L05TYP_in = pTO%iL05TYP

! FLAG TO ACTIVE THIS OBJECTS MASS AS A GRAVITY BODY TO OTHERS
!-------------------------------------------------------------
        LGRAVBOD_in = pTO%iLGRAVBOD

! ATTITUDE CONTROL FRAME-REF SPECIFIER FOR CONTROL OPT 7
!-------------------------------------------------------
        L07TYP_in = pTO%iL07TYP


!---------------------------------------
! MASS PROPERTIES, KINEMATICS, STATE ICS
!---------------------------------------
! MASS PROPERTIES AND GEOMETRY
        FMASSO_in = pTO%rFMASSO 
        FIXXO_in  = pTO%rFIXXO  
        FIYYO_in  = pTO%rFIYYO  
        FIZZO_in  = pTO%rFIZZO  
        FIXYO_in  = pTO%rFIXYO  
        FIXZO_in  = pTO%rFIXZO  
        FIYZO_in  = pTO%rFIYZO  

! COORDINATES OF CG WR/T BODY REF POINT
        DO J = 1,3
            PCGBO_in(J) = pTO%rPCGBO(J)
        END DO

! ATTACH POINT POSITIONS WR/T BODY GEOMETRY REF PT
!-------------------------------------------------
! (NOTE: THESE ARE FOR ATT PTS 1-8)
        DO J = 1,8
            PXTB_in(J) = pTO%rPXTB(J)
            PYTB_in(J) = pTO%rPYTB(J)
            PZTB_in(J) = pTO%rPZTB(J)
        END DO

! ATTACH POINT POSITIONS WR/T BODY GEOMETRY REF PT
! (NOTE: THESE ARE FOR ATT PTS 9-16)
       DO J = 1,8
            PXTB_in_(J) = pTO%rPXTB(J+8)
            PYTB_in_(J) = pTO%rPYTB(J+8)
            PZTB_in_(J) = pTO%rPZTB(J+8)
        END DO




        DO J = 1,3
        
! AERO REFERENCE POINT
            PDPB_in(J) = pTO%rPDPB(J)

! INITIAL CONDITIONS ON TRANSLATION POSITION STATE
            POSO_in(J) = pTO%rPOSO(J)

! INITIAL CONDITIONS ON TRANSLATION RATE STATE
            RATEO_in(J) = pTO%rRATEO(J)

! INITIAL CONDITONS ON BODY ANGULAR VELOCITY STATE
            OMBO_in(J) = pTO%rOMBO(J)
        END DO


! INITIAL EULER ANGLES (FOR ATTITUDE INITIALIZATION)
        PITCHO_in = pTO%rPITCHO
        ROLLO_in  = pTO%rROLLO
        YAWO_in   = pTO%rYAWO

! INITIAL CONDITIONS ON DIRECTION COSINE STATE
        DO J = 1,3
           DO K = 1,3
              GBIO_in(J,K) = pTO%rGBIO(J,K)
           END DO
        END DO

!----------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 2
!----------------------------------------------
! ISP VALUE FOR DETERMINING MASS LOSS DUE TO THRUST
        FCNISP_in = pTO%rFCNISP

! A CONSTANT COUPLE
        GXCO_in = pTO%rGXCO
        GYCO_in = pTO%rGYCO
        GZCO_in = pTO%rGZCO

! A SEQUENCE OF TIMED, CONSTANT FORCE PERIODS
         CFT1_in   = pTO%rCFT1  
        
         CFORX1_in = pTO%rCFORX1
         CFORY1_in = pTO%rCFORY1
         CFORZ1_in = pTO%rCFORZ1
        
         CFT2_in   = pTO%rCFT2  
         CFORX2_in = pTO%rCFORX2
         CFORY2_in = pTO%rCFORY2
         CFORZ2_in = pTO%rCFORZ2
        
         CFT3_in   = pTO%rCFT3  
         CFORX3_in = pTO%rCFORX3
         CFORY3_in = pTO%rCFORY3
         CFORZ3_in = pTO%rCFORZ3
        
         CFT4_in   = pTO%rCFT4  
         CFORX4_in = pTO%rCFORX4
         CFORY4_in = pTO%rCFORY4
         CFORZ4_in = pTO%rCFORZ4
        
         CFT5_in   = pTO%rCFT5  
         CFORX5_in = pTO%rCFORX5
         CFORY5_in = pTO%rCFORY5
         CFORZ5_in = pTO%rCFORZ5
        
         CFT6_in   = pTO%rCFT6  
         CFORX6_in = pTO%rCFORX6
         CFORY6_in = pTO%rCFORY6
         CFORZ6_in = pTO%rCFORZ6
        
         CFT7_in   = pTO%rCFT7  
         CFORX7_in = pTO%rCFORX7
         CFORY7_in = pTO%rCFORY7
         CFORZ7_in = pTO%rCFORZ7
        
         CFT8_in   = pTO%rCFT8  

! A SINUSOIDAL PSUEDO-CONTROL FORCE
! (SPECIFIED AS DISPLACEMENT HALF-AMPL AND FREQ-CPS)
        COSTRT_in = pTO%rCOSTRT  
        
        CDELX_in  = pTO%rCDELX   
        CFREQX_in = pTO%rCFREQX 
        
        CDELY_in  = pTO%rCDELY   
        CFREQY_in = pTO%rCFREQY  
        
        CDELZ_in  = pTO%rCDELZ   
        CFREQZ_in = pTO%rCFREQZ  
        
        COSTOP_in = pTO%rCOSTOP  



! CONSTANT DRAG COEFFICIENT DEFAULT AERO OPTION DATA
!---------------------------------------------------
        AAERO_in = pTO%rAAERO 
        CDRGO_in = pTO%rCDRGO 


! SECRET FORCE WHICH CAN BE GENERATED ONLY BETWEEN ATT POINTS 1 ON
! REF PT AND TOSS OBJECT 2 (ACTIVATED WITH NON-ZERO VALUE 'SECRET')
        SECRET_in = pTO%rSECRET



!------------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 3
! (PROVIDES BOOM TIP SKIP ROPE DAMPER CAPABILITY)
!------------------------------------------------
        DO J = 1,6
        
! DAMPER ARM ANCHOR POINT COORDS WR/T ATTACH PT (IN AP FRAME)
            XASRA_in(J) = pTO%rXASRA(J)
            YASRA_in(J) = pTO%rYASRA(J)
            ZASRA_in(J) = pTO%rZASRA(J)

! REFERENCE LENGTH OF DAMPER ARM (FOR SPRING CONSTANT CALCS)
            REFSRL_in(J) = pTO%rREFSRL(J)

! PRELOAD (ASSOCIATED WITH POSITIVE LINK EXTENSION RATE)
            SRPL_in(J) = pTO%rSRPL(J)

! SPRING CONSTANT (ASSOCIATED WITH POSITIVE LINK EXTENSION RATE)
            SRK_in(J) = pTO%rSRK(J)

! DAMPING CONSTANT (ASSOCIATED WITH POSITIVE LINK EXTENSION RATE)
            SRKD_in(J) = pTO%rSRKD(J)

! PRELOAD HYSTERESIS FACTOR (ASSOCIATED WITH NEG LINK EXTENSION RATE)
            HYSRPL_in(J) = pTO%rHYSRPL(J)

! SPRING HYSTERESIS FACTOR (ASSOCIATED WITH NEG LINK EXTENSION RATE)
            HYSRK_in(J) = pTO%rHYSRK(J)

! DAMPING HYSTERESIS FACTOR (ASSOCIATED WITH LINK ARM EXTENSION RATE)
            HYSRKD_in(J) = pTO%rHYSRKD(J)

        END DO
        
        
! COEFF OF FRICTION BETWEEN TETHER AND DAMPER CENTER-PIECE
        COEFMU_in = pTO%rCOEFMU 

! EXTENSION RATE AT WHICH FULL HYSTERESIS ONSET WILL HAVE OCCURED
        HYSONS_in = pTO%rHYSONS 

! CENTERPIECE/TETHER REL RATE AT WHICH FULL FRIC ONSET WILL HAVE OCCURED
        FRCONS_in = pTO%rFRCONS 


!-------------------------------------------------------------
! INITIAL VALUES FOR TRANSL IC OPT VIA LIBRATION SPECIFICATION
!-------------------------------------------------------------
! INITIAL LIBRATION IN-PLANE AND OUT-OF-PLANE ANGLES AND RATES
        AIPLO_in  = pTO%rAIPLO 
        AOPLO_in  = pTO%rAOPLO 
        AIPLDO_in = pTO%rAIPLDO 
        AOPLDO_in = pTO%rAOPLDO 

! INITIAL RANGE AND RANGE RATE
        ARNGO_in  = pTO%rARNGO 
        ARNGDO_in = pTO%rARNGDO 


!----------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 4
!----------------------------------------------
! OPERATIONAL LIBRATION ANGLE AND DEADBAND SPECIFICATION
        DBLIBD_in = pTO%rDBLIBD
        OPLIBD_in = pTO%rOPLIBD

! DATA INPUT, INLINE THRUST LEVEL AND ON-OFF TIME SPECIFICATION
        FINLIN_in = pTO%rFINLIN
        FINON_in  = pTO%rFINON 
        FINOFF_in = pTO%rFINOFF

! DATA INPUT, LENGTH OF TETHER, INSIDE OF WHICH, IN-LINE THRUST WILL BE TURNED ON
        FLONOF_in = pTO%rFLONOF



! ITEM OUT OF ORDER, THERMAL CONDUCTION AT OBJECT
!------------------------------------------------
! INITIAL BODY TEMPERATURE <---- OUT OF NUMERICAL ORDER
! (NOTE, LINAKAGE BETWEEN OBJECT TEMP AND TETHER THERMAL IS NOT ACTIVE)
        BTEMPO_in = pTO%rBTEMPO 



! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 4 (CONTINUED)
!----------------------------------------------------------
! DATA INPUT, START/STOP TIMES FOR LIBRATION CONTROL
        TLIBON_in = pTO%rTLIBON
        TLIBOF_in = pTO%rTLIBOF


!-------------------------------------------------
! DATA FOR TOSS OBJECT ATTITUDE CONTROL OPTION = 5
!-------------------------------------------------
! DATA INPUT, ATT ERROR-RATE GAINS
        GOREX_in = pTO%rGOREX
        GOREY_in = pTO%rGOREY
        GOREZ_in = pTO%rGOREZ
! DATA INPUT, ATT ERROR GAINS
        GOAEX_in = pTO%rGOAEX
        GOAEY_in = pTO%rGOAEY
        GOAEZ_in = pTO%rGOAEZ


!-------------------------------------------------
! DATA FOR TOSS OBJECT (TOSCNO)  CONTROL OPTION = 6
!-------------------------------------------------
! DATA INPUT, GENERAL DATA FOR ALL OPT 6 OPERATIONS
        ORBTISP_in = pTO%rORBTISP

! DATA INPUT, DATA RELATED TO ORBITAL VERTICAL-THRUST APPLICATION
        OVT_ENABL_in = pTO%rOVT_ENABL

        OVT_T_ON_in  = pTO%rOVT_T_ON
        OVT_T_OFF_in = pTO%rOVT_T_OFF
        OVT_H_OFF_in = pTO%rOVT_H_OFF

        OVT_DB_in = pTO%rOVT_DB   

        OVT_CONTVA_in = pTO%rOVT_CONTVA

        OVT_OBJ_in = pTO%rOVT_OBJ
        OVT_FAC_in = pTO%rOVT_FAC

        OVT_TZERO_in = pTO%rOVT_TZERO
        OVT_HZERO_in = pTO%rOVT_HZERO
        OVT_THRUS_in = pTO%rOVT_THRUS

! DATA INPUT, DATA RELATED TO ORBITAL HORIZONTAL-THRUST APPLICATION
        OHT_ENABL_in = pTO%rOHT_ENABL

        OHT_T_ON_in = pTO%rOHT_T_ON 
        OHT_T_OFF_in = pTO%rOHT_T_OFF

        OHT_CONTVA_in = pTO%rOHT_CONTVA
        OHT_CONAZ_in  = pTO%rOHT_CONAZ 
        OHT_CONLAT_in = pTO%rOHT_CONLAT
        OHT_CONLON_in = pTO%rOHT_CONLON

        OHT_LL_DB_in  = pTO%rOHT_LL_DB 
        OHT_VEL_DB_in = pTO%rOHT_VEL_DB

        OHT_THRUS_in  = pTO%rOHT_THRUS 

        OHT_T_LLDZ_in = pTO%rOHT_T_LLDZ
        OHT_H_LLDZ_in = pTO%rOHT_H_LLDZ

! DATA INPUT, DATA RELATED TO ORBITAL TENSION-ALIGNED THRUST APPLICATION
        OTT_ENABL_in = pTO%rOTT_ENABL

        OTT_T_ON_in  = pTO%rOTT_T_ON 
        OTT_T_OFF_in = pTO%rOTT_T_OFF

        OTT_FAC_in = pTO%rOTT_FAC

! DATA INPUT, TABLE FOR VERTICAL PROFILE (EITHER VELOCITY OR THRUST)
        DO J = 1,27
            OVTVATAB_in(J) = pTO%rOVTVATAB(J)
        END DO
        
        DO J = 1,21
            OHTVTAB_in(J) = pTO%rOHTVTAB(J)
            OHAZTAB_in(J) = pTO%rOHAZTAB(J)
        END DO
        
        
! DATA INPUT, GENERAL ERROR AND RATE GAINS ASSOCIATED WITH VERT CONTROL
        GAIN_VERR_in  = pTO%rGAIN_VERR 
        GAIN_VERRD_in = pTO%rGAIN_VERRD

! DATA INPUT, GENERAL ERROR AND RATE GAINS ASSOCIATED WITH HORIZ CONTROL
        GAIN_HERR_in  = pTO%rGAIN_HERR 
        GAIN_HERRD_in = pTO%rGAIN_HERRD

! DATA INPUT, TIME INTERVAL TO ASSESS TENSION FOR VERTICAL CONTROL
        ALT_T_INTVL_in = pTO%rALT_T_INTVL
        TENF_TTSPAN_in = pTO%rTENF_TTSPAN
        TENF_NSAMPL_in = pTO%rTENF_NSAMPL
        ALT_ALT_FLG_in = pTO%rALT_ALT_FLG
        TEN_FILTER_in  = pTO%rTEN_FILTER 
        ALT_FILTER_in  = pTO%rALT_FILTER 



!------------------------------------------------
! DATA INPUT, TO DEFINE BUOYANCY FOR TOSS OBEJCTS
!------------------------------------------------
! NOTE: THIS IS A WORK IN PROGRESS, SO CURRENTLY, A FIXED CONTAINER SIZE
!             IS ASSUMED, EG. ENVELOP EXPANSION EFFECTS ARE NOT INCLUDED
!             (IT IS ALSO ASSUMED THAT THE USER HAS INCLUDED IN THE TOSS OBJECT
!              MASS ACCOUNTING, THE MASS OF ANY CHOSEN GASEOUS "FILL")

        OBVOL_MAX = pTO%rOBVOL_MAX

! DATA INPUT, CENTER OF BUOYANCY REFERENCE POINT
        DO J = 1,3
            PBOYB_in(J) = pTO%rPBOYB(J)
        END DO
        

!-------------------------------------------------------------
! DATA INPUT, DATA FOR TOSS OBJECT ATTITUDE CONTROL OPTION = 7
!-------------------------------------------------------------
! ATT CONTROL ERROR-RATE GAINS
        GOREX7_in = pTO%rGOREX7
        GOREY7_in = pTO%rGOREY7
        GOREZ7_in = pTO%rGOREZ7
! ATT CONTROL ERROR GAINS
        GOAEX7_in = pTO%rGOAEX7
        GOAEY7_in = pTO%rGOAEY7
        GOAEZ7_in = pTO%rGOAEZ7

! DATA INPUT, AOA VALUE, GREATER THAN WHICH, AOA CONTROL STARTS
        AOA_MX7_in = pTO%rAOA_MX7

! DATA INPUT, TIME TO START CONTROL OPTION
        TSTR_CO7_in = pTO%rTSTR_CO7

! DATA INPUT, AOA CONTROL ERROR AND RATE GAINS
        AOAERR7_in = pTO%rAOAERR7
        AOARAT7_in = pTO%rAOARAT7

! DATA INPUT, DELTA-TIME TO TRANSITION TO FINAL COMMANDED AOA OF 180 DEG
        DELT7_AOA_in = pTO%rDELT7_AOA

! DATA INPUT, BODY RATE .LT. WHICH AOA TRANSITION IS ALLOWED TO PROCEED
        TST_OMD7_in = pTO%rTST_OMD7

! DATA INPUT, DELTA-TIME TO DRIVE OMB TO ZERO
        DELT7_OMB_in = pTO%rDELT7_OMB


!------------------------------------------
! DATA INPUT FOR TOSCNTH CONTROL OPTION = 8
!------------------------------------------
! DATA INPUT, HERE IS SOME SIMPLE THRUST DATA
        CISP3_in    = pTO%rCISP3   
        TTIMON3_in  = pTO%rTTIMON3 
        TTIMOFF3_in = pTO%rTTIMOFF3
        VTHRUS3_in  = pTO%rVTHRUS3 
        P_AREA3_in  = pTO%rP_AREA3 


!-------------------------------------------------
! DATA FOR TOSS OBJECT ATTITUDE CONTROL OPTION = 9
!-------------------------------------------------
! DEFINE NUMBER OF TETHERS INVOKED TO CONTROL THIS OBJECT
        C9_TTCNT_in = pTO%rC9_TTCNT
      
! DEFINE WHETHER SHEAVE RATES ARE CONSIDERED IN CONTROL CALCS
        C9_SHVONOF_in = pTO%rC9_SHVONOF
           
! DEFINE RATE-HOLD CONTROL FEEDBACK GAINS
        C9_RTH_KERR_in = pTO%rC9_RTH_KERR
        C9_RTH_KRAT_in = pTO%rC9_RTH_KRAT
      
! DEFINE POSITION-HOLD CONTROL FEEDBACK GAINS
        C9_PSH_KERR_in = pTO%rC9_PSH_KERR
        C9_PSH_KRAT_in = pTO%rC9_PSH_KRAT
        
! DEFINE LIBRATION CONTROL FEEDBACK GAINS, UTIL FACTOR, DEADBANDS
        C9_PSH_KLRAT_in = pTO%rC9_PSH_KLRAT
        C9_PSH_KLACC_in = pTO%rC9_PSH_KLACC
        C9_PSH_KFACT_in = pTO%rC9_PSH_KFACT
        C9_LHPOS_DB_in = pTO%rC9_LHPOS_DB
        C9_LHVEL_DB_in = pTO%rC9_LHVEL_DB

! DEFINE USER-SPECIFIED TENSION ERROR GAINS FOR THE TENSION CONTROLLER
        C9_TERR_GAIN_in  = pTO%rC9_TERR_GAIN 
        C9_TERRR_GAIN_in = pTO%rC9_TERRR_GAIN
        C9_TERRI_GAIN_in = pTO%rC9_TERRI_GAIN
        C9_LBW_FLG_in = pTO%rC9_LBW_FLG

! DEFINE USER-SPECIFIED DEBUG FLAG
        C9_DEBUG_FLG_in = pTO%rC9_DEBUG_FLG
        C9_DEBUG_TTN1_in = pTO%rC9_DEBUG_TTN1
        C9_DEBUG_TTN2_in = pTO%rC9_DEBUG_TTN2
        C9_DEBUG_DT_in = pTO%rC9_DEBUG_DT
        
! DEFINE TIME TO START/STOP CONTROLLER
        C9_TSTART_in = pTO%rC9_TSTART
        C9_TSTOP_in = pTO%rC9_TSTOP

        DO J = 1,10
! DEFINE TOSS TETHER NUMBERS DELEGATED TO CONTROLLING OBJECT
            C9_TT_in(J) = pTO%rC9_TT(J)
      
! DEFINE MAXIMUM ALLOWED TENSION FOR TETHER
            C9_TMAX_in(J) = pTO%rC9_TMAX(J)
      
! DEFINE MINIMUM ALLOWED TENSION FOR TETHER
            C9_TMIN_in(J) = pTO%rC9_TMIN(J)
            
! DEFINE MAXIMUM ALLOWED DEPLOY RATE FOR TETHER
            C9_LDMAX_in(J) = pTO%rC9_LDMAX(J)
      
! DEFINE MAXIMUM ALLOWED DEPLOY ACCELERATION FOR TETHER
            C9_LDDMAX_in(J) = pTO%rC9_LDDMAX(J)
        END DO

! DEFINE XD (NORTH), YD (EAST), AND ALT-RATE (UP) COMMAND TABLES VS TIME
        DO J = 1,30
           C9_XD_TAB_in(J)  = pTO%rC9_XD_TAB(J)
           C9_YD_TAB_in(J)  = pTO%rC9_YD_TAB(J)
           C9_HD_TAB_in(J)  = pTO%rC9_HD_TAB(J)
        END DO

! DEFINE MINIMUM TENSION TABLE -VS- TETHER LENGTH
        DO J = 1,20
            C9_TSAG_TAB_in(J) = pTO%rC9_TSAG_TAB(J)
        END DO

! DEFINE USER-PREEMPT OF RATE COMMANDS VIA CONSTANT VALUES
        C9_XDCON_CMD_in = pTO%rC9_XDCON_CMD
        C9_YDCON_CMD_in = pTO%rC9_YDCON_CMD
        C9_HDCON_CMD_in = pTO%rC9_HDCON_CMD

! DEFINE USER-SPECIFIED GO-TO COMMANDS VIA CONSTANT VALUES
        C9_XCON_CMD_in = pTO%rC9_XCON_CMD
        C9_YCON_CMD_in = pTO%rC9_YCON_CMD
        C9_HCON_CMD_in = pTO%rC9_HCON_CMD

! DEFINE USER-SPECIFIED MULTIPLIER-FACTORS ON TABLE LOOKUP COMMANDS
        C9_XDFAC_in = pTO%rC9_XDFAC
        C9_YDFAC_in = pTO%rC9_YDFAC
        C9_HDFAC_in = pTO%rC9_HDFAC
        C9_TSAGFAC_in = pTO%rC9_TSAGFAC

! DEFINE MASS-LOADING EVENT 1
        C9_TIME_MSEV_1_in = pTO%rC9_TIME_MSEV_1
        C9_DELT_MSEV_1_in = pTO%rC9_DELT_MSEV_1
        C9_MASS_MSEV_1_in = pTO%rC9_MASS_MSEV_1
      
! DEFINE MASS-LOADING EVENT 2
        C9_TIME_MSEV_2_in = pTO%rC9_TIME_MSEV_2
        C9_DELT_MSEV_2_in = pTO%rC9_DELT_MSEV_2
        C9_MASS_MSEV_2_in = pTO%rC9_MASS_MSEV_2
      
! DEFINE MASS-LOADING EVENT 3
        C9_TIME_MSEV_3_in = pTO%rC9_TIME_MSEV_3
        C9_DELT_MSEV_3_in = pTO%rC9_DELT_MSEV_3
        C9_MASS_MSEV_3_in = pTO%rC9_MASS_MSEV_3

! DEFINE OBJECT TO WHICH MASS-LOADING IS APPLIED
        C9_MSEV_OBJ_in = pTO%rC9_MSEV_OBJ

! DEFINE DATA RELATED TO TENSION AVERAGING FILTER
        C9_TENF_TTSPAN_in = pTO%rC9_TENF_TTSPAN
        C9_TENF_NSAMPL_in = pTO%rC9_TENF_NSAMPL

! DEFINE DATA RELATED TO SAG CONTROL LIMITS AND DEADBANDS
        C9_SAG_LIMIT_FRAC_in = pTO%rC9_SAG_LIMIT_FRAC
        C9_SAG_LIMIT_MIN_in  = pTO%rC9_SAG_LIMIT_MIN 
        C9_SAG_DB_FRAC_in    = pTO%rC9_SAG_DB_FRAC   
        C9_SAG_DB_VALUE_in   = pTO%rC9_SAG_DB_VALUE  

      END  
      
      
