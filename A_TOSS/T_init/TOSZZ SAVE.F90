! ROUTINE: TOSZZ
! %Z%GTOSS %M% H.11 code v03.0 
!              H.11 code v03.0  Rework for compatiblity w/New TOSS Obj scheme
!----------------------------------------------------------------------------
!              H.10 code v02.0 Removed planet geo-states from argument list.
!                              No longer sets in values for RE, GE, OMEGAE.
!                              Increased no. of TOSS Object to 15.
!-----------------------------------------------------------------------------
!              H.5 code v01.02 Eliminated evaluation of RTD and DTR
!------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!************************************************************
!************************************************************
!
      SUBROUTINE TOSZZ (IUASCI,IUASCO,IUERRL,IUCRTI,IUCRTO)
!
!************************************************************
!************************************************************
! THIS SUBROUTINE ZEROS ALL TOSS COMMON, AND SETS ANY ONCE-ONLY
! INITIALIZATION.  ARGUMENTS DEFINE THE LOGICAL I/O UNITS AND
! CERTAIN ENVIRONMENT DATUM SPECIFIED BY THE REF PT ENTITY

! ARGUMENTS ARE:
!      IUASCI - I/O CHANNEL FOR READING ASCII INPUT DATA
!      IUASCO - I/O CHANNEL FOR WRITING ASCII INPUT DATA
!      IUERRL - I/O CHANNEL FOR LOGGING ERROR MESSAGES
!      IUCRTI - I/O CHANNEL FOR READING INTERACTIVE CRT INPUT
!      IUCRTO - I/O CHANNEL FOR WRITING INTERACTIVE CRT OUTPUT

!      RPRREF - REF PT ENTITYS REFERENCE EARTH RADIUS
!      RPGREF - REF PT ENTITYS REFERENCE GRAVITATIONAL ACCEL
!      RPOREF - REF PT ENTITYS REFERENCE EARTH ROTATION RATE


! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND 
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"

!      include "../../A_HDR/EQU_OBJ.i"   (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.i"




!********************************************************
!  SPECIFY LOGICAL INPUT/OUTPUT UNITS FOR USE BY  TOSS
!  (THESE ASSIGNMENTS ARE DERIVED FROM THE TOSS INTERFACE
!   ARRAY AND ARE DEFINED BY THE NON-TOSS ENTITY)
!********************************************************

! UNIT FOR ASCII INPUT DATA FILE
      LUI = IUASCI

! UNIT FOR ASCII OUTPUT DATA FILE
      LUO = IUASCO

! UNIT FOR ERROR MESSAGE LOGGING
      LUE = IUERRL

! UNIT FOR INTERACTIVE CRT OUTPUT (CURRENT TOSS HAS NO CRT INPUT)
      LUC = IUCRTO


!****************************************
! VISUALLY INSPECT COM_TOSS ARRAYS THEN SET
! SIZE INDICES BELOW FOR ZEROING COMMON
!****************************************

! STATE UNIVERSAL STARTING POINT TO ZERO OBJECT "L" ARRAYS
!---------------------------------------------------------
      NLZZ = 18


! STATE "GENERAL" TOSS/FORCE GENERATION ARRAY CONTROL LIMITS
!-----------------------------------------------------------
! STATE STARTING POINT AT WHICH GENERAL "L" ARRAY IS ZEROED
      NLZZQ = 22

! THEN OFFSETS TO "READ-IN-INDEX"
      NLQOFF = 0
      NJQOFF = 0
      NFQOFF = 0
      NDQOFF = 0

! AND THEN ITS "READ-IN-INDEX" LIMITS
      NLQMIN = NLZZQ
      NLQMAX = NLQ

      NJQMIN = 1
      NJQMAX = NJQ

      NFQMIN = 10
      NFQMAX = NFQ

      NDQMIN = 1
      NDQMAX = NDQ


!**********************************
! ZERO THE VARIOUS TOSS DATA ARRAYS
!**********************************

!------------------------------------
! ZERO THE HOST/TOSS INTERFACE ARRAYS
!------------------------------------

! ZERO THE COM_ALL INTERGER ARRAY
      DO 1  K = 1,NJTOSS
         JTOSS(K) = 0
1     CONTINUE

! ZERO THE REFERENCE POINT STATE ARRAY
      DO 2  K = 1,NRPS
         RPS(K) = 0.0
2     CONTINUE


! ZERO THE ATTACH POINT STATE ARRAY
      DO 10 JOB = 1,NOBJ
      DO 10 JAP = 1,NATT
         DO 10  K = 1,NAPS
            APS(K,JAP,JOB) = 0.0
10    CONTINUE


!------------------------------------
! ZERO THE GENERAL TOSS COMMON ARRAYS
!------------------------------------
      DO 11 J = NLZZQ,NLQ
11       LTOSQ(J) = 0

      DO 12 J = 1,NJQ
12       JTOSQ(J) = 0

      DO 13 J = 1,NFQ
13       FTOSQ(J) = 0.0

      DO 14 J = 1,NDQ
14       DTOSQ(J) = 0.0



!----------------------------------------
! ZERO THE TOSS COMMON ARRAYS FOR OBJECTS
!----------------------------------------
      DO J = 2,NOBJ
      
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
         CALL TOSLDW(J)

! ZERO THE OBJECT'S CONSTANT VARIABLES
         CALL ZERO_OBJ_IN_DATA(pTO)

! ZERO THE OBJECT'S DYNAMIC VARIABLES
         CALL ZERO_OBJ_DYN_DATA(pTO)

      END DO
      

!**********************************************
!
! DO DEFAULT INITIALIZATION ON INTERFACE ARRAYS
!
!**********************************************

! SET UP A UNITY/NULL SCRATCH MATRIX AND VECTOR
      DO 97 I = 1,3
            TOSVX1(I) = 1.0
            TOSVX2(I) = 0.0

      DO 97 J = 1,3
            TOSMX1(I,J) = 0.0
            IF(I .EQ. J) TOSMX1(I,J) = 1.0

            TOSMX2(I,J) = 0.0
97    CONTINUE


!**********************************************
! SET-IN THE COM_ALL UNITY AND NULL VECTORS, ETC
!**********************************************
      CALL MATMOV (0, TOSMX1, TMXUNT)
      CALL MATMOV (0, TOSMX2, TMXNUL)
      CALL VECMOV (TOSVX1, TVXUNT)
      CALL VECMOV (TOSVX2, TVXNUL)


! SET RPS ARRAY DIR COS MATRICES TO UNITY
      CALL MATMOV (0, TOSMX1, RPGOI)
      CALL MATMOV (0, TOSMX1, RPGRI)
      CALL MATMOV (0, TOSMX1, RPGIL)


! SET APS DIR COS MATRIX TO UNITY (USE COLUMN EQUIV OF TOSMX1)
      DO 99 JOB = 1,NOBJ
      DO 99 JAP = 1,NATT
         DO 98  K = 1,9
            APS(K+15,JAP,JOB) = TOSCX1(K)
   98    CONTINUE
99    CONTINUE



!*************************************************
!
! DO DEFAULT INITIALIZATION FOR GENERAL TOSS DATA
!
!*************************************************
! INHIBIT AERO AND ELECTRO FORCES AS DEFAULT
      LOPA = -1
      LOPE = -1



!*********************************************
!
! DO INITIALIZATION FOR FINITE TETHER SOLUTONS
!
!*********************************************
      CALL TISZZ


      RETURN
      END
