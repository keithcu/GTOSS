! COMDECK COM_VOSS
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!**********************************************
!**********************************************
!
!  GENERAL COMMON FOR VTOSS RESULTS DATA BASE
!        POST-PROCESSING DISPLAY SYSTEM
!
!**********************************************
!**********************************************

! THIS IS AN 'INCLUDE' FILE CONTAINING COMMON FOR
! DISPLAY POST-PROCESSING

! THIS COMMON IS BEST DESCRIBED AND UNDERSTOOD IN
! VIEW OF THE FOLLOWING OBSERVATIONS:

!   1. IT BUILDS UPON ALL THE OTHER OFFICIAL GTOSS
!      AND TOSS COMMON INCLUDE FILES AS SUPPLEMENTS

!   2. BY INCORPORATING THESE OTHER INCLUDE FILES,
!      STORAGE ALLOCATION REQUIRED FOR GTOSS/TOSS
!      DATA RECONSTRUCTION, ARE AUTOMATICALLY AND
!      CONSISTENTLY MAINTAINED.

!   3. BY THIS MECHANISM, THE POST PROCESSING
!      DISPLAY ROUTINES CAN INVOKE THE ACTUAL WORKING
!      ARRAY NAME EQUIVALENCE INCLUDE FILES FOR USE
!      IN STORING RESULTS OF THE RDB DATA EXTRACTIONS.

!   4. FUNCTIONING OF THE DISPLAY ROUTINES DRAW
!      HEAVILY UPON VARIOUS INTEGER COMMON ALLOCATIONS
!      EXISTING IN GTOSS FOR SPECIFICATION OF RESULTS
!      DATA BASE FILE MANIPULATION VARIABLES. THIS SHOULD
!      BE SELF MAINTAINING AND CONSISTENT SINCE BOTH
!      PROGRAMS REFER TO THE SAME DATA BASE FILES.


! COMMON FOR GENERAL DATA VARIABLES
!----------------------------------
! STATE SIZE OF THIS COMMON ARRAY
      PARAMETER ( NPR = 250 )
      COMMON /PTOSR/ PR(NPR)


! COMMON FOR INTEGER VARIABLES
!-----------------------------
! STATE SIZE OF THIS COMMON ARRAY
      PARAMETER ( NING = 150 )
      COMMON /PTOSI/ ING(NING)
      INTEGER ING


! COMMON FOR LOGICAL VARIABLES
!-----------------------------
! STATE SIZE OF THIS COMMON ARRAY
      PARAMETER ( NLOGI = 15 )
      COMMON /PTOSL/ LOGI(NLOGI)
      LOGICAL LOGI


! DATA BUFFER ARRAYS FOR ALL TO USE
!----------------------------------
! STATE SIZE OF THE BUFFER ARRAYS BELOW
      PARAMETER ( NOUTP = 4700 )
      REAL *4 DATRL1,  DATMX1,  DATMN1
      REAL *4 DATRL2,  DATMX2,  DATMN2
      COMMON /PTOSO/ DATRL1(NOUTP), DATMX1(NOUTP), DATMN1(NOUTP)
      COMMON /PTOSO/ DATRL2(NOUTP), DATMX2(NOUTP), DATMN2(NOUTP)

      COMMON /PTEXT/ DSYMB1(NOUTP),  DSYMB2(NOUTP)
      CHARACTER *16  DSYMB1,         DSYMB2


!---------------------------
! COMMON FOR VTOSS TEXT DATA
!---------------------------
      COMMON  /PTEXT/ DTEXT(72,3), FF
      CHARACTER DTEXT, FF

! CURRENT IMPORT FILE NAME COMPONENTS AS ENTERED IN VTOSS INPUT STREAM
      COMMON  /PTEXT/  IFNAMS(2)
      CHARACTER *7 IFNAMS

! COMMON FOR WINDOW DEFINITION (HDR START, COL DEFINITOR, ROW DEFINITOR)
      COMMON  /PTEXT/  AST, HT, CR
      CHARACTER AST, HT, CR

! COMMON FOR VTOSS IDENTIFICATION STRING
      COMMON  /PTEXT/ VTSID
      CHARACTER *35 VTSID

!-----------------------------------------
! LOGICAL EQUIVALENCES OF A GENERAL NATURE
!-----------------------------------------
      EQUIVALENCE (LOGI(1),PROCE ), (LOGI(2),NOEN ), (LOGI(3),STAR ),   &
     &            (LOGI(4),RUNFL )

      LOGICAL PROCE, NOEN, STAR, RUNFL


!-----------------------------------------
! INTEGER EQUIVALENCES OF A GENERAL NATURE
!-----------------------------------------
      EQUIVALENCE (ING(1), NSKI   )
      EQUIVALENCE (ING(2), NLIN   )
      EQUIVALENCE (ING(3), NCOUN  )
      EQUIVALENCE (ING(4), NCR    )
      EQUIVALENCE (ING(5), LASITM )

! DEFINE GENERAL I/O UNIT-SPECIFIER VARIABLES FOR VTOSS
      EQUIVALENCE (ING( 6), IUIN   )
      EQUIVALENCE (ING( 7), IURPT  )
      EQUIVALENCE (ING( 8), IUCRT  )
      EQUIVALENCE (ING( 9), IUERR  )
      EQUIVALENCE (ING(10), INFIL1 )
      EQUIVALENCE (ING(11), INFIL2 )

!     EQUIVALENCE (ING(12),             UNASSIGNED )
!            "
!     EQUIVALENCE (ING(18),             UNASSIGNED )


! BATCH MODE/INTERACTIVE MODE SYSTEM DEFINITOR
      EQUIVALENCE (ING(19), LBATCC )

! CURRENT DATA BLOCK BEING PROCESSED
      EQUIVALENCE (ING(20), NBCURR )

! CURRENT DATA BLOCK BEING PROCESSED
      EQUIVALENCE (ING(21), NBDISC )

!     EQUIVALENCE (ING(22),              UNASSIGNED )
!          "
!     EQUIVALENCE (ING(99),              UNASSIGNED )

! FLAGS INDICATING MAX/MIN DATA NOT INCLUDED IN A DATA BVLOCK
      EQUIVALENCE (ING(100), NOMIN1 )
      EQUIVALENCE (ING(101), NOMIN2 )

! NUMBER OF DISCREPANCIES ASSOCIATED WITH A DATA BVLOCK
      EQUIVALENCE (ING(102), NUMDIS )

!     EQUIVALENCE (ING(103),             UNASSIGNED )
!          "
!     EQUIVALENCE (ING(150),             UNASSIGNED )


!-----------------------------------------------
! REAL VARIABLE EQUIVALENCES OF A GENERAL NATURE
!-----------------------------------------------
! VTOSS STANDARD TIME (FOR THOSE WHO WANT TO ASSIGN VALUE TO IT)
      EQUIVALENCE (PR(1), DTIME )

! REAL EQUIVALENCES ASSOCIATED W/BASIC CONTROL INPUT FOR VTOSS
      EQUIVALENCE (PR( 2), TMX   )

! SKIP/START/DURATION FOR ALL DISPLAYS - 1ST PERIOD
      EQUIVALENCE (PR( 3), TSKI1 )
      EQUIVALENCE (PR( 4), TSAR1 )
      EQUIVALENCE (PR( 5), TDU1  )
! SKIP/START/DURATION FOR ALL DISPLAYS - 2ND PERIOD
      EQUIVALENCE (PR( 6), TSKI2 )
      EQUIVALENCE (PR( 7), TSAR2 )
      EQUIVALENCE (PR( 8), TDU2  )
! SKIP/START/DURATION FOR ALL DISPLAYS - 3RD PERIOD
      EQUIVALENCE (PR( 9), TSKI3 )
      EQUIVALENCE (PR(10), TSAR3 )
      EQUIVALENCE (PR(11), TDU3  )
! SKIP/START/DURATION FOR ALL DISPLAYS - 4TH PERIOD
      EQUIVALENCE (PR(12), TSKI4 )
      EQUIVALENCE (PR(13), TSAR4 )
      EQUIVALENCE (PR(14), TDU4  )

!=====INPUT=====> PERCENT ERROR CRITERIA FOR DISCREPANCY REPORTING
      EQUIVALENCE (PR(15), DISLIM )

!=====INPUT=====> USER FORMAT SPECIFIER
      EQUIVALENCE (PR(16), PCHUZ  )

!=====INPUT=====> INHIBIT LABEL NAME EQUALITY CHECK/REPORT (WITH = -1.0)
      EQUIVALENCE (PR(17), CKLABL )

!=====INPUT=====> FLAG TO CONTROL BATCH MODE AND CRT OUTPUT OF PROGRESS
      EQUIVALENCE (PR(18), BATCON )

!     EQUIVALENCE (PR(19),            UNASSIGNED )

!=====INPUT=====> FILTER 200 PERCENT REPORT: EITHER VAL=0 (1=ACTIVATE)
      EQUIVALENCE (PR(20), P2ZER  )

!=====INPUT=====> FILTER 200 PERCENT REPORT: DIFF VAL SIGN (1=ACTIVATE)
      EQUIVALENCE (PR(21), P2DIF  )

!=====INPUT=====> FILTER 200 PERCENT REPORT: EXP DIFFERENCE (1=ACTIVATE)
      EQUIVALENCE (PR(22), P2MAG  )
!=====INPUT=====> EXP. DIFFERENCE LIMIT (FOR 200-PERCENT-ERROR FILTER)
      EQUIVALENCE (PR(23), P2MAGV )

!=====INPUT=====> FILTER ALL ERRORS: WITH SMALL EXPONENTS (1=ACTIVATE)
      EQUIVALENCE (PR(24), ALLEXP  )
!=====INPUT=====> EXP. LIMIT (FOR ALL-ERROR FILTER)
      EQUIVALENCE (PR(25), VALEXP )


!     EQUIVALENCE (PR( 26),            UNASSIGNED )
!           "
!     EQUIVALENCE (PR(189),            UNASSIGNED )


!---------------------------------------------------------
! PARAMETERS 190 TO 200 ARE RESERVED AS OUTSIDE USER INPUT
! AND WILL NOT BE EMPLOYED BY ANY STANDARD CTOSS FUNCTIONS
!---------------------------------------------------------
!     EQUIVALENCE (PR(190),  RESERVED FOR OUTSIDERS USE  )
!         "            "              "
!     EQUIVALENCE (PR(200),  RESERVED FOR OUTSIDERS USE  )
!---------------------------------------------------------


!     EQUIVALENCE (PR(201),             UNASSIGNED )
!          "
!     EQUIVALENCE (PR(250),             UNASSIGNED )

! END OF LABELLED COMMON BLOCK
