! COMDECK COM_HOST
! %Z%GTOSS %M% H.10 code v02.10
!              H.10 code v02.10 Increase P array (to support RP-fix opt).
!                               Added new I/O unit for animation file.
!                               Added permanent item for animation rec count.
!                               Added variable stepsize frequency counter.
!----------------------------------------------------------------------------
!              H.6 code v02.02 Upped P array size for att control sys
!---------------------------------------------------------------------
!              H.6 code v02.01 Removed PROCED flag (put into COM_ALL.i)
!---------------------------------------------------------------------
!              H.3 code v02.00 Changed the name of DELTAT to DLHOST
!                              Changed DELTAT to a variable status
!                              Removed unused variable TDUR1
!                              Added stepsize staging input data
!------------------------------------------------------------------
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!**********************************************
!**********************************************
!
!    GENERAL COMMON FOR HOST SIMULATION
!
!**********************************************
!**********************************************

!==============================================
!==============================================
! THIS COMMON  IS NOT AN EXPLICIT PART OF TOSS
! (BUT RATHER PART OF THE "STANDALONE" FUNCTION
!   OF REF POINT GENERATION AND RESULT DISPLAY)
!==============================================
!==============================================
! AN 'INCLUDE' FILE ALLOCATING REF PT COMMON

! COMMON FOR GENERAL DATA VARIABLES
!----------------------------------
! STATE SIZE OF THIS COMMON ARRAY
      PARAMETER (      NPAR = 1500 )
      COMMON /GTOSSR/ P(NPAR)

! COMMON FOR INTEGRATED VARIABLES
!--------------------------------
! STATE SIZE OF THIS COMMON ARRAY
      PARAMETER (      NDEQ = 20 )
      COMMON /GTOSSR/ Y(NDEQ), DYDX(NDEQ), DYDXP(NDEQ)

! COMMON FOR INTEGER VARIABLES
!--------------------------------
! STATE SIZE OF THIS COMMON ARRAY
      PARAMETER (        NGINT = 75 )
      COMMON /GTOSSI/ INTG(NGINT)

! COMMON FOR LOGICAL VARIABLES
!--------------------------------
! STATE SIZE OF THIS COMMON ARRAY
      PARAMETER (        NLOG = 15 )
      COMMON /GTOSSL/ LOGG(NLOG)
      LOGICAL LOGG

! TEXT COMMON USED BY GTOSS
      COMMON  /GTOSSC/  TEXT (72,3)
      CHARACTER TEXT

! COMMON FOR GTOSS IDENTIFICATION STRING
      COMMON  /GTOSSC/ GTSID
      CHARACTER *35 GTSID

! CURRENT RDB NAME COMPONENTS AS ENTERED IN GTOSS INPUT STREAM
      COMMON  /GTOSSC/  GDBNAM, GDBNUM
      CHARACTER *4 GDBNAM
      CHARACTER *2 GDBNUM

! THIS VARIABLE MAINTAINS THE FLAG STATE DEFINING A
! REQUESTED LATE START INITIALIZATION FROM BEING
! CORRUPTED BY GLOBAL READ-IN OF REF PT COMMON DATA
!--------------------------------------------------
      COMMON /GOSS/ LATEST

! THIS VARIABLE MAINTAINS THE FLAG STATE DEFINING NEW
! RDB NAMES BEING READ IN VIA INPUT STREAM FROM BEING
! CORRUPTED BY GLOBAL READ-IN OF REF PT COMMON DATA
!--------------------------------------------------
      COMMON /GOSS/ NGMFLG


! REAL EQUIVALENCES OF GENERAL NATURE
!------------------------------------
! INTEGRATED TIME (FOR THOSE WHO MIGHT WANT IT)
      EQUIVALENCE (Y(1),  DPTIME   )


!=====INPUT=====> INITIALLY EMPLOYED INTEGRATION STEPSIZE FOR THE HOST
      EQUIVALENCE (P( 1),  DLHOST   )

!=====INPUT=====> LENGTH OF REF PT SIMULATION RUN
      EQUIVALENCE (P( 2),  TMAX     )
      EQUIVALENCE (P( 3),  TSKIP1   )
      EQUIVALENCE (P( 4),  TSTAR1   )

!=====INPUT=====> INTEGRATION STEPSIZE STAGING DATA FOR HOST
      DIMENSION TMSTG(4),  DLSTG(4)
      EQUIVALENCE (P( 5),  TMSTG(1) )
      EQUIVALENCE (P( 9),  DLSTG(1) )


! CURRENT VALUE OF HOST INTEGRATION STEPSIZE
      EQUIVALENCE (P(13),  DELTAT   )


! OFFICIAL TIME VIA INTEGRATION COUNT INCRMEMENT
      EQUIVALENCE (P(14),  T        )

!=====INPUT=====> SECRET INPUT CONTROLS IC ITERATIONS (SEE INITIA)
      EQUIVALENCE (P(15),  PICITS   )

!=====INPUT=====> MISCELLANEOUS CONTROL FLAGS
      EQUIVALENCE (P(16),  RUNUM    )

!=====INPUT=====> FLAG CONTROLS BATCH/CRT OUTPUT OF GTOSS PROGRESS
      EQUIVALENCE (P(17),  BATGON   )


! INTEGER EQUIVALENCES OF A GENERAL NATURE
!-----------------------------------------
      EQUIVALENCE (INTG(1), NSKIP   )
      EQUIVALENCE (INTG(2), NLINE   )
      EQUIVALENCE (INTG(3), NCOUNT  )
      EQUIVALENCE (INTG(4), NCRT    )
      EQUIVALENCE (INTG(5), ITIME   )
      EQUIVALENCE (INTG(6), NPDOT   )
      EQUIVALENCE (INTG(7), NBCRT   )

! INTEGER I/O UNIT-SPECIFIER VARIABLES
!-------------------------------------
      EQUIVALENCE (INTG( 8), IOUIN  )
      EQUIVALENCE (INTG( 9), IOUQLK )
      EQUIVALENCE (INTG(10), IOUCRI )
      EQUIVALENCE (INTG(11), IOUCRO )
      EQUIVALENCE (INTG(12), IOUERR )
      EQUIVALENCE (INTG(13), IOUSCR )
      EQUIVALENCE (INTG(14), IOUANI )

! NUMBER OF LINES ON A PRINTED OUTPUT
      EQUIVALENCE (INTG(15), NLINX  )

! FACILITY FORM-FEED TYPE DESIGNATOR
      EQUIVALENCE (INTG(16), LFFAC   )

! BATCH MODE/INTERACTIVE MODE SYSTEM DEFINITOR
      EQUIVALENCE (INTG(17), LBATCH  )

! LATE START DATA SNAPSHOT TRIGGER FLAG
      EQUIVALENCE (INTG(18), JSNAPT  )

! LATE START DATA FILE SEQUENCE NUMBER
      EQUIVALENCE (INTG(19), JRSNAP  )


! INTEGER EQUIVALENCES OF AN APPLICATIONS ORIENTED NATURE
!--------------------------------------------------------

! LAST TOSS OBJECT BEING INVOKED BY REF PT SIMULATION
      EQUIVALENCE (INTG(25), LASOB    )
! NUMBER OF ATTACH S BEING INVOKED BY REF PT SIMULATION
      EQUIVALENCE (INTG(26), NAP      )
! EULER ANGLE TYPE BEING INVOKED FOR REF PT SIMULATION
      EQUIVALENCE (INTG(27), JEUL     )

! QUICK LOOK OUTPUT FORMAT SPECIFIERS
      DIMENSION  NOBJQ(6), NTSHQ(12), NBSHQ(3)
      EQUIVALENCE (INTG(28), NOBJQ(1) )
      EQUIVALENCE (INTG(34), NTSHQ(1) )
      EQUIVALENCE (INTG(46), NQSEL    )
      EQUIVALENCE (INTG(47), NFTSHQ   )
      EQUIVALENCE (INTG(48), NBSHQ(1) )

! LATE START SNAP-TIME DE-ACTIVATION FLAG
      DIMENSION LSNAP(10)
      EQUIVALENCE (INTG(51), LSNAP(1) )


! PERMANENT LOCATION NEEDED BY ANIMATION CODE GANIMO.F
      EQUIVALENCE (INTG(61), NANIMREC )

! VARIABLE STEPSIZE FREQUENCY COUNTER
      EQUIVALENCE (INTG(62), NCNT_GOV_STEP )


!     EQUIVALENCE (INTG(63),                   OPEN )
!          "
!     EQUIVALENCE (INTG(75),                   OPEN )


! LOGICAL EQUIVALENCES OF A GENERAL NATURE
!-----------------------------------------
!     EQUIVALENCE (LOGG(1),                    OPEN )
      EQUIVALENCE (LOGG(2), NOEND   )
      EQUIVALENCE (LOGG(3), START   )
      EQUIVALENCE (LOGG(4), RUNFLG  )


      LOGICAL NOEND, START, RUNFLG
