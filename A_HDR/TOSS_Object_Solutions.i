! COMDECK TOSS_OJECT_SOLUTIONS
! %Z%GTOSS %M% H.11 code v06.00
!              H.11 code v06.00 baseline code for F90 conversion
!*********************************************************
!*********************************************************
!*********************************************************
!
! THIS MODULE, WHEN "USED" IN ANY GTOSS SUBPROGRAM THAT
! REFERENCES VARIABLES WITHIN A T5OSS OBJECT SOLUTION,
! WILL PROVIDE ACCESS TO THE ACTUAL INSTANTIATED TOSS OBJECT
! SOLUTION DATA
!
! ALSO INCLUDED ARE ROUTINES THAT WILL PROVIDE A POINTER 
! WHEN CALLED WITH A TOSS OBJECT SOLUTION NUMBER.
!
! NOTE THAT THE TOSS OBJECT SOLN POINTER "pTO" DEFINED IN THIS
! FILE IS A GLOBAL ARGUMENT AVAILABLE TO ALL OTHER PROGRAMS
! THAT INCLUDE THIS FILE, AND IS SO USED BY ALL!
!
!*********************************************************
!*********************************************************
!*********************************************************
      MODULE TOSS_OBJECT_SOLUTIONS

! GAIN ACCESS TO THE TOSS OBJECT SOLUTION DATA STRUCTURE
         USE TOSS_OBJECT_DATA_STRUCTURE


! INSTANTIATE THE TOSS OBJECT SOLUTIONS
         TYPE (OBJECT_SOLN_STATE), TARGET ::  &
     &      TOBJ_2,  TOBJ_3,  TOBJ_4,  TOBJ_5,   TOBJ_6,  &
     &      TOBJ_7,  TOBJ_8,  TOBJ_9,  TOBJ_10,  TOBJ_11, &
     &      TOBJ_12, TOBJ_13, TOBJ_14, TOBJ_15
          
          
! DEFINE GLOBAL POINTER USED TO ACCESS ELEMENTS OF A TOSS OBJECT SOLUTION
         TYPE (OBJECT_SOLN_STATE), POINTER ::  pTO




!---------------------------------------------------------------------
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! CONTAINED BELOW ARE SUBROUTINES CONVENIENTLY RELATED TO TOSS OBJECT
! DATA STRUCTURE, AND THE TOSS POINTER ARRANGEMENT FOR REFERENCING IT.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!---------------------------------------------------------------------

      CONTAINS



!@NOS*DECK TOSLDW
! %Z%GTOSS %M% H.12 code v02.10
!              H.12 code v02.10 Fixed error in diagnostic output
!---------------------------------------------------------------------
!                   code v02.00 Converted to use F90 finite soln data
!                               Objects and pointers
!-------------------------------------------------------------------
!              H.7 code v01.03 Added comments to more clearly describe
!                              the data connection between TOSS/FOSS
!-------------------------------------------------------------------
!              H.2 code v01.02 Added time-varying ether factor
!---------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG delivery)
!************************************
!************************************
!
      SUBROUTINE TOSLDW(JOBJ)
!
!************************************
!************************************
! THIS ROUTINE ASSIGNS A POINTER TO THE DATA STRUCTURE FOR THE
! TOSS OBJECT INSTANTIATION OF THE ASSOCIATED TOSS OBJECT SOLN NUMBER
!
! THIS ROUTINE RESIDES IN THE TOSS OBJECT DATA STRUCTURE MODULE
! TO PROVIDE AN EXPLICIT INTERFACE DEFINITION TO THE CALLING
! REFERENCES.


! GAIN ACCESS TO THE TOSS DATA STRUCTURE AND 
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE

!@NOS*CALL COM_ALL
!@NOS*CALL COM_RPS
!@NOS*CALL COM_TOSS
!@NOS*CALL EQU_TOSS

      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"

!@VAX      INCLUDE 'COM_ALL.FOR'
!@VAX      INCLUDE 'COM_RPS.FOR'
!@VAX      INCLUDE 'COM_TOSS.FOR'
!@VAX      INCLUDE 'EQU_TOSS.FOR'

!@DOS      INCLUDE 'COM_ALL.INC'
!@DOS      INCLUDE 'COM_RPS.INC'
!@DOS      INCLUDE 'COM_TOSS.INC'
!@DOS      INCLUDE 'EQU_TOSS.INC'

!@MAC      INCLUDE '{GTOSS}A_TOSS:COM_ALL.i'
!@MAC      INCLUDE '{GTOSS}A_TOSS:COM_RPS.i'
!@MAC      INCLUDE '{GTOSS}A_TOSS:COM_TOSS.i'
!@MAC      INCLUDE '{GTOSS}A_TOSS:EQU_TOSS.i'


! DETERMINE WHICH TOSS OBJECT SOLUTION IS TO BE LOADED
!------------------------------------------------
       IF( (JOBJ .LT. 2) .OR. (JOBJ .GT. NOBJ) ) GO TO 300

       IF(JOBJ .EQ.  2) pTO => TOBJ_2
       IF(JOBJ .EQ.  3) pTO => TOBJ_3
       IF(JOBJ .EQ.  4) pTO => TOBJ_4
       IF(JOBJ .EQ.  5) pTO => TOBJ_5
       IF(JOBJ .EQ.  6) pTO => TOBJ_6
       IF(JOBJ .EQ.  7) pTO => TOBJ_7
       IF(JOBJ .EQ.  8) pTO => TOBJ_8
       IF(JOBJ .EQ.  9) pTO => TOBJ_9
       IF(JOBJ .EQ. 10) pTO => TOBJ_10
       IF(JOBJ .EQ. 11) pTO => TOBJ_11
       IF(JOBJ .EQ. 12) pTO => TOBJ_12
       IF(JOBJ .EQ. 13) pTO => TOBJ_13
       IF(JOBJ .EQ. 14) pTO => TOBJ_14
       IF(JOBJ .EQ. 15) pTO => TOBJ_15

       RETURN

!****************************************************
! EMIT ERROR MESSAGE ABOUT UNIDENTIFIED FINITE TETHER
!****************************************************
300    WRITE(LUE,301) JOBJ
301    FORMAT('TOSS OBJECT # UNKNOWN TO TOSLDW, #=', I3)
       STOP 'IN TOSLDW'

       END SUBROUTINE TOSLDW





! ROUTINE: ZERO_OBJ_DYN_DATA
! %Z%GTOSS %M% H.12 code v02.10 
!              H.12 code v02.10 Increased Att Pts to 16 total
!-------------------------------------------------------------
!              H.11 code v02.00 (baseline for v11 delivery)
!*****************************************************
!*****************************************************
!
         SUBROUTINE ZERO_OBJ_DYN_DATA
!        
!*****************************************************
!*****************************************************
! THIS ROUTINE ZERO'S OUT ALL DYNAMIC DATA RELATED TO A
! TOSS OBJECT SOLUTION DATA STRUCTURE.
!
! THIS ROUTINE RESIDES IN THE OBJECT DATA STRUCTURE MODULE
! TO PROVIDE AN EXPLICIT INTERFACE DEFINITION TO THE CALLING
! REFERENCES.

! FETCH ACCESS TO THE INSTANTIATED TOSS OBJECT SOLUTIONS
         USE TOSS_OBJECT_DATA_STRUCTURE

!         TYPE(OBJECT_SOLN_STATE), POINTER :: ptr_arg

         INTEGER J,K

!----------------------------------------
! INTEGER VARIABLES SPECIFIC TO EACH BODY
!----------------------------------------
            pTO%iNTIME  = 0
            pTO%iJNOMOM = 0
            pTO%iJEULER = 0
            pTO%iJGRVON = 0
            pTO%iJGGTON = 0
            pTO%iJLIBAG = 0


!-----------------------------------------
! TIME DEPENDENT MASS & GEOMETRY VARIABLES
!-----------------------------------------
            pTO%rDMASS = 0.0
            pTO%rDMASSI = 0.0

            pTO%rDIXX = 0.0
            pTO%rDIYY = 0.0
            pTO%rDIZZ = 0.0
            pTO%rDIXY = 0.0
            pTO%rDIXZ = 0.0
            pTO%rDIYZ = 0.0

! COORDINATES OF CG WR/T BODY REF POINT
            DO J = 1,3
               pTO%rPCGB(J) = 0.0
            END DO

! MASS MATRIX AND ITS INVERSE
            DO J = 1,3
               DO K = 1,3
                  pTO%rDIMS(J,K)  = 0.0
                  pTO%rDIMSI(J,K) = 0.0
               END DO
            END DO

! AERO REFERENCE POINT POSITION
            DO J = 1,3
               pTO%rRDPB(J) = 0.0
            END DO

! OBJECT TOTAL MASS LOSS DUE TO THRUSTING MASS FLOW RATE
            pTO%rDMASINC = 0.0
            pTO%rDMASDOT = 0.0

! TOTAL THRUSTING IMPULSE FROM ALL CONTRIBUTIONS
            pTO%rTOTIPULS = 0.0

! CENTER OF BUOYANCY POINT POSITION
            DO J = 1,3
               pTO%rRBYPB(J) = 0.0
            END DO


!------------------------
! ATTACH POINT VARIABLES
!------------------------
            DO J = 1,16

! UN-DEFORMED ATTACH POINT POS, VEL, ACC WR/T CG, BODY FRAME
               pTO%rXTUB(J) = 0.0
               pTO%rYTUB(J) = 0.0
               pTO%rZTUB(J) = 0.0
               pTO%rVXTUB(J) = 0.0
               pTO%rVYTUB(J) = 0.0
               pTO%rVZTUB(J) = 0.0
               pTO%rAXTUB(J) = 0.0
               pTO%rAYTUB(J) = 0.0
               pTO%rAZTUB(J) = 0.0

! ACTUAL ATTACH POINT POS, VEL, ACC  WR/T CG, BODY FRAME
               pTO%rXTB(J) = 0.0
               pTO%rYTB(J) = 0.0
               pTO%rZTB(J) = 0.0
               pTO%rVXTB(J) = 0.0
               pTO%rVYTB(J) = 0.0
               pTO%rVZTB(J) = 0.0
               pTO%rAXTB(J) = 0.0
               pTO%rAYTB(J) = 0.0
               pTO%rAZTB(J) = 0.0

! ACTUAL ATTACH POINT POS, VEL, ACC  WR/T CG, INER FRAME
               pTO%rXTI(J) = 0.0
               pTO%rYTI(J) = 0.0
               pTO%rZTI(J) = 0.0
               pTO%rVXTI(J) = 0.0
               pTO%rVYTI(J) = 0.0
               pTO%rVZTI(J) = 0.0
               pTO%rAXTI(J) = 0.0
               pTO%rAYTI(J) = 0.0
               pTO%rAZTI(J) = 0.0

! ELASTIC CONTRIBUTION TO ATTACH POINT POS, VEL, ACC WR/T CG
               pTO%rXTEB(J) = 0.0
               pTO%rYTEB(J) = 0.0
               pTO%rZTEB(J) = 0.0
               pTO%rVXTEB(J) = 0.0
               pTO%rVYTEB(J) = 0.0
               pTO%rVZTEB(J) = 0.0
               pTO%rAXTEB(J) = 0.0
               pTO%rAYTEB(J) = 0.0
               pTO%rAZTEB(J) = 0.0

! ATTACH POINT LOADS IN BODY FRAME
               pTO%rFXATB(J) = 0.0
               pTO%rFYATB(J) = 0.0
               pTO%rFZATB(J) = 0.0

            END DO


!----------------
! STATE VARIABLES
!----------------
! TIME VARIABLES FOR THIS OBJECT
            pTO%rTOSTIM = 0.0
            pTO%rDELTOS = 0.0

            DO J = 1,3
            
! TOSS OBJECT POS, VEL, ACC  WR/T TOSS REF PT, IN INER FRAME
               pTO%rRBI(J)   = 0.0
               pTO%rRBID(J)  = 0.0
               pTO%rRBIDD(J) = 0.0

! TOSS OBJECT POS, VEL WR/T INERTIAL POINT, IN INER FRAME
               pTO%rRI(J)  = 0.0
               pTO%rRID(J) = 0.0

! BODY AXIS ANGULAR VEL, ACC IN BODY FRAME
               pTO%rOMB(J)  = 0.0
               pTO%rOMBD(J) = 0.0

            END DO


            DO J = 1,3
               DO K = 1,3
               
! DIRECTION COSINES AND DERIVS BETWEEN BODY AND INER FRAME
                  pTO%rGBI(J,K)  = 0.0
                  pTO%rGBID(J,K) = 0.0
            
! TRANSF BETWEEN TOPOCENTRIC FRAME AT OBJECT AND INER FRAME
                  pTO%rGIL(J,K) = 0.0
            
               END DO
            END DO

! VARIABLES ASSOCIATED WITH HOST DRIVEN STEPSIZE CAPABILITY
            pTO%rTBIASO = 0.0
            pTO%rDELTPO = 0.0


! ATMOSPHERIC THERMO PROPERTIES
            pTO%rDENS = 0.0
            pTO%rSOUND = 0.0
            pTO%rAIRTMP = 0.0
            pTO%rVR = 0.0
            pTO%rVRSQ = 0.0
            pTO%rQBAR = 0.0

            DO J = 1,3

! TOTAL RELATIVE WIND WR/T TOSS OBJECT
               pTO%rVRWI(J) = 0.0
               pTO%rVRWB(J) = 0.0

! ATMOS PARTICLE: TOTAL, EARTH ROTATING, AND PERTURB WIND (INER FRAME)
               pTO%rVWI(J)  = 0.0
               pTO%rVWEI(J) = 0.0
               pTO%rVWPI(J) = 0.0

! ATMOS PARTICLE: TOTAL, EARTH ROTATING, AND PERTURB WIND (PLANET FRAME)
               pTO%rVWEP(J) = 0.0
               pTO%rVWPP(J) = 0.0
            
            END DO

! OBJECT RADIUS VECTOR MAG AND SPHERICAL ALT
            pTO%rRMAG = 0.0
            pTO%rALT = 0.0

! MACH NUMBER OF TOSS OBJECT
            pTO%rDMACH = 0.0

! OBJECT PLANET LONGITUDE, AND LATITUDE (RAD)
            pTO%rDMUE = 0.0
            pTO%rDLAE = 0.0

! OBJECT OBLATE ALT, LAT(DEG), AND RADIUS VECTOR MAG
            pTO%rALTOBL = 0.0
            pTO%rGEOLAT = 0.0
            pTO%rRGEOD  = 0.0

! PREVIOUS VALUES OF INTEGRATED STATE VARIABLE DERIVATIVES
            DO J = 1,3
               pTO%rOMBDP(J)  = 0.0
               pTO%rRBIDP(J)  = 0.0
               pTO%rRBIDDP(J) = 0.0
            END DO

            DO J = 1,3
               DO K = 1,3

! PREVIOUS VALUE OF DIRCOS MATRIX            
                  pTO%rGBIDP(J,K) = 0.0

! REFERENCE DIRCOS MATRIX FOR DATA RETENTION BY ROUTINE TOSCNA.F AND TOSCNAP.F
                   pTO%rGBOHOL(J,K) = 0.0
             
               END DO
            END DO


! GENERAL AERO TYPE DATA FOR GENERIC TOSS OBJECT AERODYNAMICS
! NOTE: WHETHER THESE ARE MEANINGFUL DEPENDS ON THE ACTUAL MODELS
            pTO%rCA_OBJ = 0.0
            pTO%rCN_OBJ = 0.0
            pTO%rCM_OBJ = 0.0
            pTO%rFAMAG_OBJ = 0.0
            pTO%rFNMAG_OBJ = 0.0
            pTO%rACMAG_OBJ = 0.0
            pTO%rALP_OBJ = 0.0
            pTO%rCNQ_OBJ = 0.0
            pTO%rCMQ_OBJ = 0.0
            pTO%rALP_DYN = 0.0
            pTO%rFNQMAG_OBJ = 0.0
            pTO%rACQMAG_OBJ = 0.0
            pTO%rAERO_RATE  = 0.0

! FOR ORB MANEUVER CONTROL PROGS: TOSCNO, TOSCNVT, TOSCNHT, TOSCNTT
!------------------------------------------------------------------
! FOR TOSCNO
            pTO%rOVT_PREV_H = 0.0
            pTO%rOVT_FLG_ALT = 0.0

! FOR TOSCNVT
            pTO%rALCRVP  = 0.0
            pTO%rACRVFLG = 0.0
            pTO%rALT_TIM_CNTR = 0.0
            pTO%rALT_TAR  = 0.0
            pTO%rALT_EQUI = 0.0
            pTO%rALT_EQUI_P = 0.0
            pTO%rALT_SOLN_P = 0.0
            pTO%rALT_AT_TRAP = 0.0
            pTO%rALT_LATCH = 0.0
            pTO%rTEN_TIM_CNTR = 0.0
            pTO%rPREV_AVG_TEN = 0.0
            pTO%rTENS_T_GRAD = 0.0

            DO J = 1,50
                  pTO%rTEN_FIL6(J) = 0.0
            END DO

! FOR TOSCNVZ
            pTO%rHDLATCH = 0.0

! FOR TOSCNHT
            pTO%rOPT10FAC = 0.0


! BOUYANCY FORCE AND MOMENT EFFECTS ON OBJECT
!--------------------------------------------
! TOTAL BUOYANT FORCE AND MOMENT ON OBJECT
            DO J = 1,3
               pTO%rFBYB(J) = 0.0
               pTO%rGBYB(J) = 0.0
            END DO


! STORAGE FOR TOSCNAP (OBJECT CNTRL OPT 7)
!-----------------------------------------
! AOA MODE SWITCH LATCHING
            pTO%rAOA_LATCH = 0.0
            pTO%rTO7_LATCH = 0.0
            pTO%rAOA_TRANS = 0.0

! OMB RATE DAMPING MODE SWITCH LATCHING
            pTO%rOMB_LATCH   = 0.0
            pTO%rTOMB7_LATCH = 0.0
            pTO%rOMB1_TRANS  = 0.0
            pTO%rOMB2_TRANS  = 0.0
            pTO%rOMB3_TRANS  = 0.0

!-------------------
! FORCES AND MOMENTS
!-------------------
            DO J = 1,3

! ACCEL DUE TO GRAVITY AT OBJECT
               pTO%rAGI(J) = 0.0

! TOTAL OF ALL FORCES ON OBJECT, INER AND BODY FRAME
               pTO%rFI(J) = 0.0
               pTO%rFB(J) = 0.0

! TOTAL AERO FORCE ON OBJECT
               pTO%rFDB(J) = 0.0

! TOTAL CONTROL FORCE ON OBJECT
               pTO%rFCB(J) = 0.0

! TOTAL FORCE ON OBJECT DUE TO ALL ATTACH POINTS
               pTO%rFTB(J) = 0.0

! TOTAL MOMENT ON OBJECT WR/T CG
               pTO%rGB(J) = 0.0

! TOTAL MOMENT WR/T CG DUE TO AERODYNAMICS
               pTO%rGDB(J) = 0.0

! TOTAL MOMENT WR/T CG DUE TO CONTROL SYSTEM
               pTO%rGCB(J) = 0.0

! TOTAL MOMENT WR/T CG DUE TO ALL ATTACH POINTS
               pTO%rGTB(J) = 0.0

! COUPLE DUE TO GRAVITY GRADIENT ON 6 DOF OBJECT
               pTO%rGGB(J) = 0.0

! SUM OF ATTACH POINT COUPLES IN BODY FRAME
               pTO%rSCTB(J) = 0.0

! AERODYNAMIC COUPLE (ASSOCIATED W/ AERO FORCE @ AERO REF PT)
               pTO%rCDB(J) = 0.0

! COUPLE DUE TO EXTERNAL EFFECTS ON TOSS OBJECT
               pTO%rCEEB(J) = 0.0

! FORCE DUE TO EXTERNAL EFFECTS ON TOSS OBJECT
               pTO%rFEEB(J) = 0.0

            END DO

!-----------------------------------
! VARIABLES OF INTERPRETATION + MISC
!-----------------------------------
            DO J = 1,3

! OBJECT POS, VEL, ACC WR/T RP: EXPRESSED IN RP ARBITRARY FRAME
               pTO%rRBAR(J) = 0.0
               pTO%rVBAR(J) = 0.0
               pTO%rABAR(J) = 0.0

! OBJECT POS, VEL, ACC WR/T RP: EXPRESSED IN RP ORBITAL FRAME
               pTO%rRBOR(J)   = 0.0
               pTO%rRBORD(J)  = 0.0
               pTO%rRBORDD(J) = 0.0
            
            END DO

! RANGE, AZIMUTH, ELEV (AND RATES), ETC
            pTO%rRANGE  = 0.0
            pTO%rRANGED = 0.0

! TRANSFORMATION FROM OBJECT ORB FRAME TO INERTIAL
            DO J = 1,3
               DO K = 1,3
                  pTO%rGOI(J,K) = 0.0
               END DO
            END DO


            DO J = 1,3
            
! OBJECT POS WR/T INER PT: EXPRESSED IN PLANET FIXED FRAME
               pTO%rRIP(J) = 0.0

! OBJECT RIGID BODY ANGULAR MOMENTUM IN INERTIAL FRAME
               pTO%rHMOMI(J) = 0.0
            END DO


!--------------------------------------------------------------
! VARIABLES ASSOCIATED WITH THE SKIP ROPE DAMPER IMPLEMENTATION
!--------------------------------------------------------------
! DIRECTION COSINES BETWEEN ASSOCIATED ATT PT FRAME AND INER FRAME
            DO J = 1,3
               DO K = 1,3
                  pTO%rGSDAI(J,K) = 0.0
               END DO
            END DO

! FRICTION GRADIENT AND NORMAL FORCE ON CENTER-PIECE
            pTO%rDLFRIC = 0.0
            pTO%rFRICNM = 0.0

! TOTAL LOAD IN EACH PSD DAMPER LINK
            DO J = 1,6
               pTO%rARMLOD(J) = 0.0
            END DO
            
! DIST FROM DEPLOY ORIFICE TO CP DURING DAMPER QUIESCENT STATE OPS
            pTO%rACPLEN = 0.0

! BODY TEMPERATURE
!-----------------
! (NOTE, LINKAGE BETWEEN OBJECT TEMP AND TETHER THERMAL IS NOT ACTIVE)
            pTO%rBTEMPT = 0.0


!-----------------------------------------
! STORAGE FOR TOSCNSK1 (OBJECT CNTRL OPT 9)
!-----------------------------------------
            pTO%rC9_XY_TOGGLE = 0.0
            pTO%rC9_Z_TOGGLE = 0.0
            pTO%rC9_X_HOLD_VAL = 0.0
            pTO%rC9_Y_HOLD_VAL = 0.0
            pTO%rC9_Z_HOLD_VAL = 0.0
            pTO%rC9_CMD_RATE_P = 0.0

         RETURN

         END SUBROUTINE ZERO_OBJ_DYN_DATA










! ROUTINE: ZERO_OBJ_IN_DATA
! %Z%GTOSS %M% H.12 code v02.10 
!              H.12 code v02.10 Increased Att Pts to 16 total
!-------------------------------------------------------------
!              H.11 code v02.00 (baseline for v11 delivery)
!*****************************************************
!*****************************************************
!
         SUBROUTINE ZERO_OBJ_IN_DATA
!
!*****************************************************
!*****************************************************
! THIS ROUTINE ZERO'S OUT THE DATA AREA RELATED TO THAT PART
! OF A TOSS OBJECT DATA STRUCTURE THAT HAS BEEN ENTERED BY USER.
!
! THIS ROUTINE RESIDES IN THE OBJECT DATA STRUCTURE MODULE
! TO PROVIDE AN EXPLICIT INTERFACE DEFINITION TO THE CALLING
! REFERENCES.

! FETCH ACCESS TO THE INSTANTIATED TOSS OBJECT SOLUTIONS
         USE TOSS_OBJECT_DATA_STRUCTURE

!         TYPE(OBJECT_SOLN_STATE), POINTER :: ptr_arg

         INTEGER J,K

!************************************************************************
! BELOW IN OBJECT DATA STRUCTURE IS USER-INPUT CONSTANTS DEFINING OBJECT
!************************************************************************
! IMPORTANT NOTE: THE DATA ITEMS BELOW MUST BE KEPT SYNCHRONIZED AND
!                 CONSISTENT WITH THE OBJI.i OBJECT INPUT ITEM-MAPPING
!                 EQUIVALENCE STATEMENTS. SO IF YOU ADD A NEW OBJECT
!                 INPUT DATA CONSTANT IT MUST BE RECOGNIZED IN TWO PLACES.

! INTEGER INPUT DATA DEFINING A TOSS OBJECT
            pTO%iNATPAT = 0
            pTO%iLAERO  = 0
            pTO%iLCONT  = 0
            pTO%iLICTRN = 0
            pTO%iLICROT = 0
            pTO%iLICSPL = 0
            pTO%iLNOMOM = 0
            pTO%iLEULER = 0
            pTO%iLGRVON = 0
            pTO%iLGGTON = 0
            pTO%iLIBTYP = 0
            pTO%iLICOMT = 0
            pTO%iLINERT = 0

! THESE ITEMS ARE ASSOCIATED WITH THE SKIP ROPE DAMPER FEATURE
!-------------------------------------------------------------
! ATTACH PT AND TOSS OBJECT NUMBER TO WHICH DAMPER BASE IS INSTALLED
            pTO%iLOBJSR = 0
            pTO%iLAPPSR = 0
! NUMBER OF ANCHOR POINTS FOR DAMPER LINK ARMS
            pTO%iLNCHOR = 0
! TOSS TETHER CONNECTING DAMPER CENTER-PIECE TO DAMPER BASE ATTACH PT
            pTO%iLTHBAS = 0
! TOSS TETHER WHOSE SKIP ROPE MOTION IS TO BE DAMPED
            pTO%iLTHDMP = 0
! TYPE OF INTEGRATOR TO BE USED FOR TOSS OBJECT CENTERPIECE (1=EULER)
            pTO%iLSRDIN = 0

! OBJ AND AP WR/T WHICH CONTROL OPT 4 PROVIDES INLINE-THRUST/LIB CONTROL
!-----------------------------------------------------------------------
            pTO%iLREFOB = 0
            pTO%iLREFAT = 0
            pTO%iLAPLIB = 0
! TOSS TETHER NUMBER WHICH CONTROL OPT 4 USES TO ACTIVATE INLINE-THRUST
            pTO%iLTTNNL = 0


! ATTITUDE CONTROL FRAME-REF SPECIFIER FOR CONTROL OPT 5
!-------------------------------------------------------
            pTO%iL05TYP = 0

! FLAG TO ACTIVE THIS OBJECTS MASS AS A GRAVITY BODY TO OTHERS
!-------------------------------------------------------------
            pTO%iLGRAVBOD = 0

! ATTITUDE CONTROL FRAME-REF SPECIFIER FOR CONTROL OPT 7
!-------------------------------------------------------
            pTO%iL07TYP = 0


!---------------------------------------
! MASS PROPERTIES, KINEMATICS, STATE ICS
!---------------------------------------
! MASS PROPERTIES AND GEOMETRY
            pTO%rFMASSO = 0.0
            pTO%rFIXXO  = 0.0
            pTO%rFIYYO  = 0.0
            pTO%rFIZZO  = 0.0
            pTO%rFIXYO  = 0.0
            pTO%rFIXZO  = 0.0
            pTO%rFIYZO  = 0.0

! COORDINATES OF CG WR/T BODY REF POINT
           DO J = 1,3
                pTO%rPCGBO(J) = 0.0
           END DO

! ATTACH POINT POSITIONS WR/T BODY GEOMETRY REF PT
           DO J = 1,16
                pTO%rPXTB(J) = 0.0
                pTO%rPYTB(J) = 0.0
                pTO%rPZTB(J) = 0.0
           END DO


           DO J = 1,3
        
! AERO REFERENCE POINT
                pTO%rPDPB(J) = 0.0

! INITIAL CONDITIONS ON TRANSLATION POSITION STATE
                pTO%rPOSO(J) = 0.0

! INITIAL CONDITIONS ON TRANSLATION RATE STATE
                pTO%rRATEO(J) = 0.0

! INITIAL CONDITONS ON BODY ANGULAR VELOCITY STATE
                pTO%rOMBO(J) = 0.0
           END DO


! INITIAL EULER ANGLES (FOR ATTITUDE INITIALIZATION)
            pTO%rPITCHO = 0.0
            pTO%rROLLO  = 0.0
            pTO%rYAWO   = 0.0

! INITIAL CONDITIONS ON DIRECTION COSINE STATE
           DO J = 1,3
              DO K = 1,3
                  pTO%rGBIO(J,K) = 0.0
              END DO
           END DO

!----------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 2
!----------------------------------------------
! ISP VALUE FOR DETERMINING MASS LOSS DUE TO THRUST
            pTO%rFCNISP = 0.0

! A CONSTANT COUPLE
            pTO%rGXCO = 0.0
            pTO%rGYCO = 0.0
            pTO%rGZCO = 0.0

! A SEQUENCE OF TIMED, CONSTANT FORCE PERIODS
            pTO%rCFT1   = 0.0
            pTO%rCFORX1 = 0.0
            pTO%rCFORY1 = 0.0
            pTO%rCFORZ1 = 0.0
            pTO%rCFT2   = 0.0
            pTO%rCFORX2 = 0.0  
            pTO%rCFORY2 = 0.0
            pTO%rCFORZ2 = 0.0
            pTO%rCFT3   = 0.0
            pTO%rCFORX3 = 0.0
            pTO%rCFORY3 = 0.0
            pTO%rCFORZ3 = 0.0
            pTO%rCFT4   = 0.0
            pTO%rCFORX4 = 0.0
            pTO%rCFORY4 = 0.0
            pTO%rCFORZ4 = 0.0
            pTO%rCFT5   = 0.0
            pTO%rCFORX5 = 0.0
            pTO%rCFORY5 = 0.0
            pTO%rCFORZ5 = 0.0
            pTO%rCFT6   = 0.0
            pTO%rCFORX6 = 0.0
            pTO%rCFORY6 = 0.0
            pTO%rCFORZ6 = 0.0
            pTO%rCFT7   = 0.0
            pTO%rCFORX7 = 0.0
            pTO%rCFORY7 = 0.0
            pTO%rCFORZ7 = 0.0
            pTO%rCFT8   = 0.0

! A SINUSOIDAL PSUEDO-CONTROL FORCE
! (SPECIFIED AS DISPLACEMENT HALF-AMPL AND FREQ-CPS)
            pTO%rCOSTRT = 0.0
            pTO%rCDELX  = 0.0
            pTO%rCFREQX = 0.0
            pTO%rCDELY  = 0.0
            pTO%rCFREQY = 0.0
            pTO%rCDELZ  = 0.0
            pTO%rCFREQZ = 0.0
            pTO%rCOSTOP = 0.0



! CONSTANT DRAG COEFFICIENT DEFAULT AERO OPTION DATA
!---------------------------------------------------
            pTO%rAAERO = 0.0
            pTO%rCDRGO = 0.0


! SECRET FORCE WHICH CAN BE GENERATED ONLY BETWEEN ATT POINTS 1 ON
! REF PT AND TOSS OBJECT 2 (ACTIVATED WITH NON-ZERO VALUE 'SECRET')
            pTO%rSECRET = 0.0



!------------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 3
! (PROVIDES BOOM TIP SKIP ROPE DAMPER CAPABILITY)
!------------------------------------------------
           DO J = 1,6
        
! DAMPER ARM ANCHOR POINT COORDS WR/T ATTACH PT (IN AP FRAME)
                pTO%rXASRA(J) = 0.0
                pTO%rYASRA(J) = 0.0
                pTO%rZASRA(J) = 0.0

! REFERENCE LENGTH OF DAMPER ARM (FOR SPRING CONSTANT CALCS)
                pTO%rREFSRL(J) = 0.0

! PRELOAD (ASSOCIATED WITH POSITIVE LINK EXTENSION RATE)
                pTO%rSRPL(J) = 0.0

! SPRING CONSTANT (ASSOCIATED WITH POSITIVE LINK EXTENSION RATE)
                pTO%rSRK(J) = 0.0

! DAMPING CONSTANT (ASSOCIATED WITH POSITIVE LINK EXTENSION RATE)
                pTO%rSRKD(J) = 0.0

! PRELOAD HYSTERESIS FACTOR (ASSOCIATED WITH NEG LINK EXTENSION RATE)
                pTO%rHYSRPL(J) = 0.0

! SPRING HYSTERESIS FACTOR (ASSOCIATED WITH NEG LINK EXTENSION RATE)
                pTO%rHYSRK(J) = 0.0

! DAMPING HYSTERESIS FACTOR (ASSOCIATED WITH LINK ARM EXTENSION RATE)
                pTO%rHYSRKD(J) = 0.0

           END DO
        
        
! COEFF OF FRICTION BETWEEN TETHER AND DAMPER CENTER-PIECE
            pTO%rCOEFMU = 0.0

! EXTENSION RATE AT WHICH FULL HYSTERESIS ONSET WILL HAVE OCCURED
            pTO%rHYSONS = 0.0

! CENTERPIECE/TETHER REL RATE AT WHICH FULL FRIC ONSET WILL HAVE OCCURED
            pTO%rFRCONS = 0.0


!-------------------------------------------------------------
! INITIAL VALUES FOR TRANSL IC OPT VIA LIBRATION SPECIFICATION
!-------------------------------------------------------------
! INITIAL LIBRATION IN-PLANE AND OUT-OF-PLANE ANGLES AND RATES
            pTO%rAIPLO  = 0.0
            pTO%rAOPLO  = 0.0
            pTO%rAIPLDO = 0.0
            pTO%rAOPLDO = 0.0

! INITIAL RANGE AND RANGE RATE
            pTO%rARNGO  = 0.0
            pTO%rARNGDO = 0.0


!----------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 4
!----------------------------------------------
! OPERATIONAL LIBRATION ANGLE AND DEADBAND SPECIFICATION
            pTO%rDBLIBD = 0.0
            pTO%rOPLIBD = 0.0

! DATA INPUT, INLINE THRUST LEVEL AND ON-OFF TIME SPECIFICATION
            pTO%rFINLIN = 0.0
            pTO%rFINON  = 0.0
            pTO%rFINOFF = 0.0

! DATA INPUT, LENGTH OF TETHER, INSIDE OF WHICH, IN-LINE THRUST WILL BE TURNED ON
            pTO%rFLONOF = 0.0

! INITIAL BODY TEMPERATURE <---- OUT OF NUMERICAL ORDER
! (NOTE, LINAKAGE BETWEEN OBJECT TEMP AND TETHER THERMAL IS NOT ACTIVE)
            pTO%rBTEMPO = 0.0

! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 0.04 (CONTINUED)
!----------------------------------------------------------
! DATA INPUT, START/STOP TIMES FOR LIBRATION CONTROL
            pTO%rTLIBON = 0.0
            pTO%rTLIBOF = 0.0


! DATA FOR TOSS OBJECT ATTITUDE CONTROL OPTION = 5
!-------------------------------------------------
! DATA INPUT, ATT ERROR-RATE GAINS
            pTO%rGOREX = 0.0
            pTO%rGOREY = 0.0
            pTO%rGOREZ = 0.0
! DATA INPUT, ATT ERROR GAINS
            pTO%rGOAEX = 0.0
            pTO%rGOAEY = 0.0
            pTO%rGOAEZ = 0.0


!-------------------------------------------------
! DATA FOR TOSS OBJECT (TOSCNO) CONTROL OPTION = 6
!-------------------------------------------------
! DATA INPUT, GENERAL DATA FOR ALL OPT 6 OPERATIONS
            pTO%rORBTISP = 0.0

! DATA INPUT, DATA RELATED TO ORBITAL VERTICAL-THRUST APPLICATION
            pTO%rOVT_ENABL = 0.0

            pTO%rOVT_T_ON  = 0.0
            pTO%rOVT_T_OFF = 0.0
            pTO%rOVT_H_OFF = 0.0

            pTO%rOVT_DB    = 0.0

            pTO%rOVT_CONTVA = 0.0

            pTO%rOVT_OBJ = 0.0
            pTO%rOVT_FAC = 0.0

            pTO%rOVT_TZERO = 0.0
            pTO%rOVT_HZERO = 0.0
            pTO%rOVT_THRUS = 0.0

! DATA INPUT, DATA RELATED TO ORBITAL HORIZONTAL-THRUST APPLICATION
            pTO%rOHT_ENABL = 0.0

            pTO%rOHT_T_ON  = 0.0
            pTO%rOHT_T_OFF = 0.0

            pTO%rOHT_CONTVA = 0.0
            pTO%rOHT_CONAZ  = 0.0
            pTO%rOHT_CONLAT = 0.0
            pTO%rOHT_CONLON = 0.0

            pTO%rOHT_LL_DB  = 0.0
            pTO%rOHT_VEL_DB = 0.0

            pTO%rOHT_THRUS  = 0.0

            pTO%rOHT_T_LLDZ = 0.0
            pTO%rOHT_H_LLDZ = 0.0

! DATA INPUT, DATA RELATED TO ORBITAL TENSION-ALIGNED THRUST APPLICATION
            pTO%rOTT_ENABL = 0.0

            pTO%rOTT_T_ON  = 0.0
            pTO%rOTT_T_OFF = 0.0

            pTO%rOTT_FAC = 0.0

! DATA INPUT, TABLE FOR VERTICAL PROFILE (EITHER VELOCITY OR THRUST)
           DO J = 1,27
                pTO%rOVTVATAB(J) = 0.0
           END DO
        
           DO J = 1,21
                pTO%rOHTVTAB(J) = 0.0
                pTO%rOHAZTAB(J) = 0.0
           END DO
        
        
! DATA INPUT, GENERAL ERROR AND RATE GAINS ASSOCIATED WITH VERT CONTROL
            pTO%rGAIN_VERR  = 0.0
            pTO%rGAIN_VERRD = 0.0

! DATA INPUT, GENERAL ERROR AND RATE GAINS ASSOCIATED WITH HORIZ CONTROL
            pTO%rGAIN_HERR  = 0.0
            pTO%rGAIN_HERRD = 0.0

! DATA INPUT, TIME INTERVAL TO ASSESS TENSION FOR VERTICAL CONTROL
            pTO%rALT_T_INTVL = 0.0
            pTO%rTENF_TTSPAN = 0.0
            pTO%rTENF_NSAMPL = 0.0
            pTO%rALT_ALT_FLG = 0.0
            pTO%rTEN_FILTER  = 0.0
            pTO%rALT_FILTER  = 0.0

!------------------------------------------------
! DATA INPUT, TO DEFINE BUOYANCY FOR TOSS OBEJCTS
!------------------------------------------------
! NOTE: THIS IS A WORK IN PROGRESS, SO CURRENTLY, A FIXED CONTAINER SIZE
!             IS ASSUMED, EG. ENVELOP EXPANSION EFFECTS ARE NOT INCLUDED
!             (IT IS ALSO ASSUMED THAT THE USER HAS INCLUDED IN THE TOSS OBJECT
!              MASS ACCOUNTING, THE MASS OF ANY CHOSEN GASEOUS "FILL")

            pTO%rOBVOL_MAX = 0.0

! DATA INPUT, CENTER OF BUOYANCY REFERENCE POINT
           DO J = 1,3
                pTO%rPBOYB(J) = 0.0
           END DO
        

!-------------------------------------------------------------
! DATA INPUT, DATA FOR TOSS OBJECT ATTITUDE CONTROL OPTION = 7
!-------------------------------------------------------------
! ATT CONTROL ERROR-RATE GAINS
            pTO%rGOREX7 = 0.0
            pTO%rGOREY7 = 0.0
            pTO%rGOREZ7 = 0.0
! ATT CONTROL ERROR GAINS
            pTO%rGOAEX7 = 0.0
            pTO%rGOAEY7 = 0.0
            pTO%rGOAEZ7 = 0.0

! DATA INPUT, AOA VALUE, .GT. WHICH, AOA CONTROL STARTS CONTROL OPTION 7
            pTO%rAOA_MX7 = 0.0

! DATA INPUT, TIME TO START CONTROL OPTION
            pTO%rTSTR_CO7 = 0.0

! DATA INPUT, AOA CONTROL ERROR AND RATE GAINS
            pTO%rAOAERR7 = 0.0
            pTO%rAOARAT7 = 0.0

! DATA INPUT, DELTA-TIME TO TRANSITION TO FINAL COMMANDED AOA OF 180 DEG
            pTO%rDELT7_AOA = 0.0

! DATA INPUT, BODY RATE .LT. WHICH AOA TRANSITION IS ALLOWED TO PROCEED
            pTO%rTST_OMD7 = 0.0

! DATA INPUT, DELTA-TIME TO DRIVE OMB TO ZERO
            pTO%rDELT7_OMB = 0.0




!-------------------------------------------------------
! DATA INPUT SPECIFICALLY FOR TOSCNTH CONTROL OPTION = 8
!-------------------------------------------------------
! DATA INPUT, HERE IS SOME SIMPLE THRUST DATA
            pTO%rCISP3    = 0.0
            pTO%rTTIMON3  = 0.0
            pTO%rTTIMOFF3 = 0.0
            pTO%rVTHRUS3  = 0.0
            pTO%rP_AREA3  = 0.0



!---------------------------------------------------------
! DATA FOR TOSS OBJECT ATTITUDE CONTROL OPTIONS = 9 AND 10
!---------------------------------------------------------
! DEFINE NUMBER OF TETHERS INVOKED TO CONTROL THIS OBJECT
            pTO%rC9_TTCNT = 0.0
      
! DEFINE WHETHER SHEAVE RATES ARE CONSIDERED IN CONTROL CALCS
            pTO%rC9_SHVONOF = 0.0
      
! DEFINE RATE-HOLD CONTROL FEEDBACK GAINS
            pTO%rC9_RTH_KERR = 0.0
            pTO%rC9_RTH_KRAT = 0.0
      
! DEFINE POSITION-HOLD CONTROL FEEDBACK GAINS
            pTO%rC9_PSH_KERR = 0.0
            pTO%rC9_PSH_KRAT = 0.0

! DEFINE LIBRATION CONTROL CONTROL FEEDBACK GAINS & UTIL FACTOR
            pTO%rC9_PSH_KLRAT = 0.0
            pTO%rC9_PSH_KLACC = 0.0
            pTO%rC9_PSH_KFACT = 0.0
            pTO%rC9_LHPOS_DB = 0.0
            pTO%rC9_LHVEL_DB = 0.0

! DEFINE USER-SPECIFIED TENSION ERROR GAINS FOR THE TENSION CONTROLLER
            pTO%rC9_TERR_GAIN = 0.0
            pTO%rC9_TERRR_GAIN = 0.0
            pTO%rC9_TERRI_GAIN = 0.0
            pTO%rC9_LBW_FLG = 0.0

! DEFINE USER-SPECIFIED DEBUG FLAG
            pTO%rC9_DEBUG_FLG = 0.0
            pTO%rC9_DEBUG_TTN1 = 0.0
            pTO%rC9_DEBUG_TTN2 = 0.0
            pTO%rC9_DEBUG_DT = 0.0
            
! DEFINE TIME TO START/STOP CONTROLLER
            pTO%rC9_TSTART = 0.0
            pTO%rC9_TSTOP = 0.0

            DO J = 1,10
! DEFINE TOSS TETHER NUMBERS DELEGATED TO CONTROLLING OBJECT
                pTO%rC9_TT(J) = 0.0
      
! DEFINE MAXIMUM ALLOWED TENSION FOR TETHER
                pTO%rC9_TMAX(J) = 0.0
      
! DEFINE MINIMUM ALLOWED TENSION FOR TETHER
                pTO%rC9_TMIN(J) = 0.0
            
! DEFINE MAXIMUM ALLOWED DEPLOY RATE FOR TETHER
                pTO%rC9_LDMAX(J) = 0.0
      
! DEFINE MAXIMUM ALLOWED DEPLOY ACCELERATION FOR TETHER
                pTO%rC9_LDDMAX(J) = 0.0

! DEPLOY RATE CONTROL BIAS
                pTO%rC9_DPR_BIAS(J) = 0.0
                
! TETHER STATE DATA RETENTION (PERSISTENCE) ALLOCATIONS
                pTO%rC9_DEP_LEN(J) = 0.0
                pTO%rC9_DEP_LEND(J) = 0.0
                pTO%rC9_DEP_LENDP(J) = 0.0
                pTO%rC9_DEP_LENDD(J) = 0.0
                pTO%rC9_TEN_PREV(J) = 0.0
                
        END DO

! DEFINE XD (NORTH), YD (EAST), AND ALT-RATE (UP) COMMAND TABLES VS TIME
        DO J = 1,30
                pTO%rC9_XD_TAB(J) = 0.0
                pTO%rC9_YD_TAB(J) = 0.0
                pTO%rC9_HD_TAB(J) = 0.0
        END DO

! DEFINE MINIMUM TENSION VS TETH LENGTH TATBLE
        DO J = 1,20
                pTO%rC9_TSAG_TAB(J) = 0.0
        END DO

! DEFINE USER-PREEMPT OF RATE COMMANDS VIA CONSTANT VALUES
            pTO%rC9_XDCON_CMD = 0.0
            pTO%rC9_YDCON_CMD = 0.0
            pTO%rC9_HDCON_CMD = 0.0

! DEFINE USER-SPECIFIED GO-TO COMMANDS VIA CONSTANT VALUES
            pTO%rC9_XCON_CMD = 0.0
            pTO%rC9_YCON_CMD = 0.0
            pTO%rC9_HCON_CMD = 0.0

! DEFINE USER-SPECIFIED MULTIPLIER-FACTORS ON TABLE LOOKUP COMMANDS
            pTO%rC9_XDFAC = 0.0
            pTO%rC9_YDFAC = 0.0
            pTO%rC9_HDFAC = 0.0
            pTO%rC9_TSAGFAC = 0.0

! DEFINE MASS-LOADING EVENT 1
            pTO%rC9_TIME_MSEV_1 = 0.0
            pTO%rC9_DELT_MSEV_1 = 0.0
            pTO%rC9_MASS_MSEV_1 = 0.0
      
! DEFINE MASS-LOADING EVENT 2
            pTO%rC9_TIME_MSEV_2 = 0.0
            pTO%rC9_DELT_MSEV_2 = 0.0
            pTO%rC9_MASS_MSEV_2 = 0.0
      
! DEFINE MASS-LOADING EVENT 3
            pTO%rC9_TIME_MSEV_3 = 0.0
            pTO%rC9_DELT_MSEV_3 = 0.0
            pTO%rC9_MASS_MSEV_3 = 0.0

! DEFINE OBJECT TO WHICH MASS-LOADING IS APPLIED
            pTO%rC9_MSEV_OBJ = 0.0

! DEFINE DATA RELATED TO TENSION AVERAGING FILTER
            pTO%rC9_TENF_TTSPAN = 0.0
            pTO%rC9_TENF_NSAMPL = 0.0
            pTO%rC9_TEN_TIM_CNTR = 0.0

            DO J = 1,10
               pTO%rC9_PREV_AVG_TEN(J) = 0.0
               pTO%rC9_FILTER_TENS(J) = 0.0
            END DO
            
            DO J = 1,50
               DO K = 1,10
                    pTO%rC9_TEN_AFIL(J,K) = 0.0
               END DO
            END DO

! DEFINE DATA RELATED TO SAG CONTROL LIMITS AND DEADBANDS
            pTO%rC9_SAG_LIMIT_FRAC = 0.0
            pTO%rC9_SAG_LIMIT_MIN = 0.0
            pTO%rC9_SAG_DB_FRAC = 0.0
            pTO%rC9_SAG_DB_VALUE = 0.0

! DEFINE VARIOUS CONTROL DISPLAY ENNUNCIATOR TOGGLES
            pTO%rALT_TOGGLE_50 = 0.0
            pTO%rALT_TOGGLE_0 = 0.0
            pTO%rALT_TOGGLE_HDCMD = 0.0
            pTO%rCMASS_EVNT_TOGGLE_1 = 0.0
            pTO%rCMASS_EVNT_TOGGLE_2 = 0.0
            pTO%rCMASS_EVNT_TOGGLE_3 = 0.0

         END SUBROUTINE ZERO_OBJ_IN_DATA


      END MODULE TOSS_OBJECT_SOLUTIONS

