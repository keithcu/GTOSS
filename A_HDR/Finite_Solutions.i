! COMDECK FINITE_SOLUTIONS
! %Z%GTOSS %M% H.12 code v02.00
!              H.12 code v02.00 Increased No. of Finite solns from 9 to 31
!                               
!-------------------------------------------------------------------------
!              H.11 code v01.00 baseline code for F90 conversion
!*********************************************************
!*********************************************************
!*********************************************************
!
! THIS MODULE, WHEN "USED" IN ANY GTOSS SUBPROGRAM THAT
! REFERENCES VARIABLES WITHIN A FINITE TETHER SOLUTION,
! WILL PROVIDE ACCESS TO THE ACTUAL INSTANTIATED FINITE
! TETHER SOLUTIONS
!
! ALSO INCLUDED ARE ROUTINES THAT WILL PROVIDE POINTERS
! WHEN CALLED WITH A FINITE SOLUTION NUMBER.
!
! NOTE THAT THE FINITE SOLN POINTER "pFS" DEFINED IN THIS
! FILE IS A GLOBAL ARGUMENT AVAILABLE TO ALL OTHER PROGRAMS
! THAT INCLUDE THIS FILE, AND IS SO USED BY ALL!
!
!*********************************************************
!*********************************************************
!*********************************************************
      MODULE FINITE_SOLUTIONS

! GAIN ACCESS TO THE FINITE SLUTION DATA STRUCTURE
         USE FINITE_DATA_STRUCTURE


! INSTANTIATE THE FINITE SOLUTIONS FOR TOSS
         TYPE (FINITE_SOLN_STATE), TARGET ::  &
     &      FSN_1,   FSN_2,   FSN_3,   FSN_4,   FSN_5,   FSN_6,  &
     &      FSN_7,   FSN_8,   FSN_9,   FSN_10,  FSN_11,  FSN_12, &
     &      FSN_13,  FSN_14,  FSN_15,  FSN_16,  FSN_17,  FSN_18, &
     &      FSN_19,  FSN_20,  FSN_21,  FSN_22,  FSN_23,  FSN_24, &
     &      FSN_25,  FSN_26,  FSN_27,  FSN_28,  FSN_29,  FSN_30, &
     &      FSN_31
          
          
! DEFINE GLOBAL POINTER USED TO ACCESS ELEMENTS OF A FINITE SOLUTION
         TYPE (FINITE_SOLN_STATE), POINTER ::  pFS

!---------------------------------------------------------------------
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! CONTAINED BELOW ARE SUBROUTINES CONVENIENTLY RELATED TO FINITE SOLN
! DATA STRUCTURE, AND THE TOSS POINTER ARRANGEMENT FOR REFERENCING IT.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!---------------------------------------------------------------------

      CONTAINS



!@NOS*DECK TISLDW
! %Z%GTOSS %M% H.12 code v03.00
!              H.12 code v03.00 Increased No. of Finite solns from 9 to 31
!                               Removed TOSS regime EFACT from data transfer 
!---------------------------------------------------------------------------
!                   code v02.00 Converted to use F90 finite soln data
!                               Objects and pointers
!-------------------------------------------------------------------
!              H.7 code v01.03 Added comments to more clearly describe
!                              the data connection between TOSS/FOSS
!-------------------------------------------------------------------
!              H.2 code v01.02 Added time-varying ether factor
!---------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG delivery)
!************************************
!************************************
!
      SUBROUTINE TISLDW(JFTETH)
!
!************************************
!************************************
! THIS ROUTINE ASSIGNS A GLOBAL POINTER TO THE DATA STRUCTURE FOR
! THE FINITE SOLN INSTANTIATION OF THE ASSOCIATED FINITE SOLN NUMBER
!
! IN ADDITION THIS ROUTINE MOVES SOME DATA ITEMS FROM GENERAL TOSS
! COMMON INTO THE FINITE SOLUTION DATA STRUCTURE...THIS DATA BEING
! THOSE VARIABLES THAT ARE DRIVERS TO THE FINITE SOLUTION MODEL. THIS
! IS AN ARTIFACT OF THE OLD SCHEME OF MANAGING FINITE SOLNS, AND DONE
! BEHALF OF EFFICIENCY AND CONVENIENCE, BUT IS STILL MAINTAINED FOR
! THE SAKE OF KEEPING HERITAGE CODE UNCHANGED.
!
! THIS ROUTINE RESIDES IN THE FINITE DATA STRUCTURE MODULE
! TO PROVIDE AN EXPLICIT INTERFACE DEFINITION TO THE CALLING
! REFERENCES.


! FIRST GAIN ACCESS TO THE FINITE DATA STRUCTURE
! AND INSTANTIATED FINITE SOLUTIONS
!--------------------------------------------
      USE FINITE_DATA_STRUCTURE


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"


!      TYPE (FINITE_SOLN_STATE), POINTER, INTENT(OUT) :: pFS


! DETERMINE WHICH TETHER SOLUTION IS TO BE LOADED
!------------------------------------------------
       IF( (JFTETH .LT. 1) .OR. (JFTETH .GT. NFSOLN) ) GO TO 300

       IF(JFTETH .EQ.  1) pFS => FSN_1
       IF(JFTETH .EQ.  2) pFS => FSN_2
       IF(JFTETH .EQ.  3) pFS => FSN_3
       IF(JFTETH .EQ.  4) pFS => FSN_4
       IF(JFTETH .EQ.  5) pFS => FSN_5
       IF(JFTETH .EQ.  6) pFS => FSN_6
       IF(JFTETH .EQ.  7) pFS => FSN_7
       IF(JFTETH .EQ.  8) pFS => FSN_8
       IF(JFTETH .EQ.  9) pFS => FSN_9
       IF(JFTETH .EQ. 10) pFS => FSN_10
       IF(JFTETH .EQ. 11) pFS => FSN_11
       IF(JFTETH .EQ. 12) pFS => FSN_12
       IF(JFTETH .EQ. 13) pFS => FSN_13
       IF(JFTETH .EQ. 14) pFS => FSN_14
       IF(JFTETH .EQ. 15) pFS => FSN_15
       IF(JFTETH .EQ. 16) pFS => FSN_16
       IF(JFTETH .EQ. 17) pFS => FSN_17
       IF(JFTETH .EQ. 18) pFS => FSN_18
       IF(JFTETH .EQ. 19) pFS => FSN_19
       IF(JFTETH .EQ. 20) pFS => FSN_20
       IF(JFTETH .EQ. 21) pFS => FSN_21
       IF(JFTETH .EQ. 22) pFS => FSN_22
       IF(JFTETH .EQ. 23) pFS => FSN_23
       IF(JFTETH .EQ. 24) pFS => FSN_24
       IF(JFTETH .EQ. 25) pFS => FSN_25
       IF(JFTETH .EQ. 26) pFS => FSN_26
       IF(JFTETH .EQ. 27) pFS => FSN_27
       IF(JFTETH .EQ. 28) pFS => FSN_28
       IF(JFTETH .EQ. 29) pFS => FSN_29
       IF(JFTETH .EQ. 30) pFS => FSN_30
       IF(JFTETH .EQ. 31) pFS => FSN_31


!*****************************************************************
!*****************************************************************
! THIS SECTION PROVIDES THE INTERFACE BETWEEN THE MORE FORMAL
! TOSS GENERATED DATA AND THE FINITE TETHER OBJECTS. THE TOSS
! DATA WILL TYPICALLY BE TOSS-TETHER-REFERENCED, WHILE FINITE
! DATA IS FINITE-SOLUTION-SPECIFIC.
!****************************************************************
!****************************************************************

! GET TOSS TETHER NUMBER TO WHICH THIS FINITE SOLN CORRESPONDS
       JTETH = JASIGN(JFTETH)


! MOVE UNDEFORMED DEPLOYED TETH LENGTH FROM TOSS CPMMON TO FINITE SOLN AREA
!-----------------------------------------------------------------------
! THIS IS THE CONNECTION BETWEEN THE INSTANTANEOUS UNDEFORMED TETHER
! LENGTH OF EACH "TOSS" TETHER (AS MAINTAINED IN "GENERAL" TOSS COMMON,
! WHETHER THE TETHER IS MASSLESS OF FINITE) AND THE UNDEFORMED LENGTH
! PARAMETER OF EACH FINITE SOLUTION OBJECT (A STILL OPERATIONAL ARTIFACT
! OF A SCHEME TO STREAMLINE THE ORIGINAL INEFFICIENT SCHEME FOR HANDLING
! FINITE SOLUTIONS...THIS IS MAINTAINED INTACT FOR HERITAGE CODE PURPOSES)
! ------------------------------------------------------------------------
! PROTECT AGAINST LACK OF KNOWLEDGE OF JTETH BEFORE TOSS INPUT-DATA HAS BEEN READ-IN
       IF(ICSTAG .NE. 0) THEN

          pFS%rEL   = TLENT(JTETH)
          pFS%rELD  = TLENTD(JTETH)
          pFS%rELDD = TLENDD(JTETH)

! PERFORM SIMILAR FUNCTION FOR UNIFORM-COMPONENT OF TETHER CURRENT
          pFS%rCURRT = CURRF(JTETH)

       END IF

       RETURN

!****************************************************
! EMIT ERROR MESSAGE ABOUT UNIDENTIFIED FINITE TETHER
!****************************************************
300    WRITE(LUE,301) JFTETH
301    FORMAT('FINITE TETHER # UNKNOWN TO TISLDW, #=', I3)
       STOP 'IN TISLDW'

       END SUBROUTINE TISLDW




!@NOS*DECK TISSTW
! %Z%GTOSS %M% H.11 code v02.00
!                   code v02.00 Converted to use F90 finite soln data
!                               Objects and pointers
!-------------------------------------------------------------------
! %Z%GTOSS %M% H.10 code v01.10
!              H.10 code v01.10 Added mass-flow force terms transfer
!---------------------------------------------------------------------
!              H.4 code v01.01 Modified calculation of End seg tension
!---------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vH1 inertial bead model)
!*************************************
!*************************************
!
      SUBROUTINE TISSTW(JFTETH)
!
!*************************************
!*************************************
! THIS ROUTINE RESIDES IN THE FINITE DATA STRUCTURE MODULE
! TO PROVIDE AN EXPLICIT INTERFACE DEFINITION TO THE CALLING
! REFERENCES.
!
! NOTE: AT THE POINT WHERE TISSTW IS CALLED, ALL THE INTRINSIC
!       DATA RELATED TO, AND MANIPULATED IN BEHALF-OF A FINITE 
!       SOLUTION WILL HAVE BEEN ALREADY STASHED IN THE FINITE
!       SOLN DATA AREA.


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"


!       TYPE (FINITE_SOLN_STATE), POINTER ::  pFS

! CHECK VALIDITY OF FINITE  SOLN NUMBER
       IF( (JFTETH .LT. 1) .OR. (JFTETH .GT. NFSOLN) ) GO TO 300

!------------------------------------------------------------------
! FOLLOWING CODE FORMS THE INTERFACE BETWEEN USERS (WHO MAY WISH TO
! BE NAIVE ABOUT, AND WHO DONT WANT TO PUT ANY CODE INTO, FINITE
! SOLN MODULES), AND VARIOUS PARAMETERS GENERATED BY FINITE SOLNS
!
! THIS IS AN ARTIFACT OF THE OLD SCHEME OF MANAGING FINITE SOLNS, AND
! DONE BEHALF OF EFFICIENCY AND CONVENIENCE, BUT IS STILL MAINTAINED
! FOR THE SAKE OF KEEPING HERITAGE CODE UNCHANGED.
!------------------------------------------------------------------

! DETERMINE WHICH TOSS TETHER THIS FINITE SOLN CORRESPONDS TO
      JTETH = JASIGN(JFTETH)

! PUT VARIOUS INTERFACE DATA INTO TOSS "GENERAL" DATA AREA

! SET IN TETHER LENGTH DATA TO GENERAL ACCESS AREA
      TDIST(JTETH) = pFS%rELMG
      STREC(JTETH) = pFS%rDELMEL
      STRECD(JTETH)= pFS%rELMGD - pFS%rELD

! SET IN TETHER MASS TO GENERAL ACCESS AREA
      TTMASS(JTETH) = pFS%rBMSTOT

! SET IN TETHERS EQUIVALENT AREA*YOUNGS MODULUS
      TTAEOL(JTETH) = pFS%rAEOL

! SET IN TETHERS EQUIVALENT END-END DAMPING (NOT AVAIL IN FINITE SOLN)
      TTCDMP(JTETH) = 0.0

! TELL GENERAL USER ABOUT VOLTAGE EXPERIENCED BY THIS FINITE SOLN
      VOLTF(JTETH) = pFS%rEMFTOT


! PRESENT ATTACH POINT TENSION LOAD TO GENERAL TOSS ARRAY
! (WHERE IT WILL BE INCORPORATED IN APS ARRAY BY TOSSET)

! SET IN COMPONENTS OF ATTACH PT TENSION VEC TO TOSS ACCUMULATOR LOC
      CALL VECMOV (pFS%rXEFTEN, TFXEND)
      CALL VECMOV (pFS%rYEFTEN, TFYEND)

! SET IN COMPONENTS OF MASS-FLOW FORCE INTO TOSS ACCUMULATOR LOC
      CALL VECMOV (pFS%rXEFMT,  FMTXEND)

! SET IN END POINT TENSIONS
      XELOAD(JTETH) = pFS%rTENSEG(1)
      YELOAD(JTETH) = pFS%rTENSEG(pFS%iNBEAD+1)
      IF(XELOAD(JTETH) .LT. 0.0) XELOAD(JTETH) = 0.0
      IF(YELOAD(JTETH) .LT. 0.0) YELOAD(JTETH) = 0.0

! STANDARD RETURN
1000  RETURN



!****************************************************
! EMIT ERROR MESSAGE ABOUT UNIDENTIFIED FINITE TETHER
!****************************************************
300    WRITE(LUE,301) JFTETH
301    FORMAT('FINITE TETHER # UNKNOWN TO "TISSTW", #=', I3)
       STOP 'IN TISSTW'

       END SUBROUTINE TISSTW









!@NOS*DECK ZERO_FIN_SOLN
!              H.11 code v02.00 (baseline for v11 delivery)
!************************************
!************************************
!
         SUBROUTINE ZERO_FIN_SOLN
!
!************************************
!************************************
! THIS ROUTINE ZERO'S OUT ALL DATA RELATED TO THE FINITE SOLUTION
! DATA STRUCTURE.
!
! THIS ROUTINE RESIDES IN THE FINITE DATA STRUCTURE MODULE
! TO PROVIDE AN EXPLICIT INTERFACE DEFINITION TO THE CALLING
! REFERENCES.

! FETCH ACCESS TO THE INSTANTIATED FINITE SOLUTIONS
         USE FINITE_DATA_STRUCTURE

!         TYPE(FINITE_SOLN_STATE), POINTER :: ptr_arg

         INTEGER I,J,K

!----------------------------------------------------------------
! INTEGER VARIABLES SPECIFIC TO EACH FINITE TETHER SOLUTION STATE
!----------------------------------------------------------------
         pFS%iNFTIME = 0
         pFS%iNXYHST = 0
         pFS%iNFTYP  = 0

         pFS%iNBEAD  = 0
         pFS%iN3BEAD = 0

         pFS%iJONCE  = 0

         pFS%iLITNIT = 0
         pFS%iLITERM = 0

         pFS%iNNUREF = 0
         pFS%iLFIC   = 0

         pFS%iLOPGRV = 0
         pFS%iLOPARO = 0
         pFS%iLOPELC = 0

         pFS%iJHSTER = 0

         pFS%iNBRKTP = 0

         pFS%iNSTBRK = 0
         pFS%iNS1BRK = 0
         pFS%iNS2BRK = 0
         pFS%iNF1BRK = 0
         pFS%iNF2BRK = 0

         pFS%iNNUPR  = 0

         pFS%iLSIC   = 0
         pFS%iLSMODE = 0

         pFS%iNNUITD = 0
         pFS%iNNUITE = 0
         pFS%iNNUITA = 0


!-------------------------------------
! MISCELLEANEOUS PROPERTIES AND STATES
!-------------------------------------
! INTEGRATION INTERVAL & LAPSED TIME OF FINITE TETHER SOLN
! (SEE: TIMSTG  BELOW FOR MORE TIME RELATED VARIABLES)
         pFS%rDELTIS = 0.0
         pFS%rTISTIM = 0.0

! NOMINAL INSTANTANEOUS DEPLOYED (UNDEFORMED) TETHER LENGTH
         pFS%rEL   = 0.0
         pFS%rELD  = 0.0
         pFS%rELDD = 0.0

! UNDEFORMED TETHER DENSITY, X-SECTIONAL AREA, YOUNGS MODULUS
! (THESE ARE BASIC CONSTANTS FOR A UNIFORM TETHER; HOWEVER,
!  FOR NON-UNIFORM TETHERS, THEY FORM BASE VALUES FOR REGION 1)
         pFS%rRHOT   = 0.0
         pFS%rAREAT  = 0.0
         pFS%rYOUNGT = 0.0
         pFS%rBETAT  = 0.0
         pFS%rDIAROT = 0.0

! SOME TIME VARIANT FINITE TETHER TERMS
         pFS%rAEOL   = 0.0
         pFS%rAYOUNG = 0.0

! TETHER NON-LINEAR STIFFNESS TERMS
         pFS%rDNLCS = 0.0
         pFS%rDNLEX = 0.0

! ATTRIBUTES FOR NON-UNIFORM-PROPERTIES BEAD MODEL
!-------------------------------------------------
! REGION 1: TETHER LENGTH, YOUNGS MODULUS, MATERIAL DAMPING FACTOR
         pFS%rTLETR1 = 0.0
         pFS%rYNGTR1 = 0.0
         pFS%rBETFR1 = 0.0
! BEGIN/END VALUES: LINEAL DENSITY, ELASTIC AREA, AERO DIA
         pFS%rRHOTB1 = 0.0
         pFS%rRHOTE1 = 0.0
         pFS%rARELB1 = 0.0
         pFS%rARELE1 = 0.0
         pFS%rDAROB1 = 0.0
         pFS%rDAROE1 = 0.0

! REGION 2: TETHER LENGTH, YOUNGS MODULUS, MATERIAL DAMPING FACTOR
         pFS%rTLETR2 = 0.0
         pFS%rYNGTR2 = 0.0
         pFS%rBETFR2 = 0.0
! BEGIN/END VALUES: LINEAL DENSITY, ELASTIC AREA, AERO DIA
         pFS%rRHOTB2 = 0.0
         pFS%rRHOTE2 = 0.0
         pFS%rARELB2 = 0.0
         pFS%rARELE2 = 0.0
         pFS%rDAROB2 = 0.0
         pFS%rDAROE2 = 0.0

! REGION 3: TETHER LENGTH, YOUNGS MODULUS, MATERIAL DAMPING FACTOR
         pFS%rTLETR3 = 0.0
         pFS%rYNGTR3 = 0.0
         pFS%rBETFR3 = 0.0
! BEGIN/END VALUES: LINEAL DENSITY, ELASTIC AREA, AERO DIA
         pFS%rRHOTB3 = 0.0
         pFS%rRHOTE3 = 0.0
         pFS%rARELB3 = 0.0
         pFS%rARELE3 = 0.0
         pFS%rDAROB3 = 0.0
         pFS%rDAROE3 = 0.0

! REGION 4: TETHER LENGTH, YOUNGS MODULUS, MATERIAL DAMPING FACTOR
         pFS%rTLETR4 = 0.0
         pFS%rYNGTR4 = 0.0
         pFS%rBETFR4 = 0.0
! BEGIN/END VALUES: LINEAL DENSITY, ELASTIC AREA, AERO DIA
         pFS%rRHOTB4 = 0.0
         pFS%rRHOTE4 = 0.0
         pFS%rARELB4 = 0.0
         pFS%rARELE4 = 0.0
         pFS%rDAROB4 = 0.0
         pFS%rDAROE4 = 0.0

! REGION 5: TETHER LENGTH, YOUNGS MODULUS, MATERIAL DAMPING FACTOR
         pFS%rTLETR5 = 0.0
         pFS%rYNGTR5 = 0.0
         pFS%rBETFR5 = 0.0
! BEGIN/END VALUES: LINEAL DENSITY, ELASTIC AREA, AERO DIA
         pFS%rRHOTB5 = 0.0
         pFS%rRHOTE5 = 0.0
         pFS%rARELB5 = 0.0
         pFS%rARELE5 = 0.0
         pFS%rDAROB5 = 0.0
         pFS%rDAROE5 = 0.0

! REGION 6: TETHER LENGTH, YOUNGS MODULUS, MATERIAL DAMPING FACTOR
         pFS%rTLETR6 = 0.0
         pFS%rYNGTR6 = 0.0
         pFS%rBETFR6 = 0.0
! BEGIN/END VALUES: LINEAL DENSITY, ELASTIC AREA, AERO DIA
         pFS%rRHOTB6 = 0.0
         pFS%rRHOTE6 = 0.0
         pFS%rARELB6 = 0.0
         pFS%rARELE6 = 0.0
         pFS%rDAROB6 = 0.0
         pFS%rDAROE6 = 0.0

! REGION 7: TETHER LENGTH, YOUNGS MODULUS, MATERIAL DAMPING FACTOR
         pFS%rTLETR7 = 0.0
         pFS%rYNGTR7 = 0.0
         pFS%rBETFR7 = 0.0
! BEGIN/END VALUES: LINEAL DENSITY, ELASTIC AREA, AERO DIA
         pFS%rRHOTB7 = 0.0
         pFS%rRHOTE7 = 0.0
         pFS%rARELB7 = 0.0
         pFS%rARELE7 = 0.0
         pFS%rDAROB7 = 0.0
         pFS%rDAROE7 = 0.0

! REGION 8: TETHER LENGTH, YOUNGS MODULUS, MATERIAL DAMPING FACTOR
         pFS%rTLETR8 = 0.0
         pFS%rYNGTR8 = 0.0
         pFS%rBETFR8 = 0.0
! BEGIN/END VALUES: LINEAL DENSITY, ELASTIC AREA, AERO DIA
         pFS%rRHOTB8 = 0.0
         pFS%rRHOTE8 = 0.0
         pFS%rARELB8 = 0.0
         pFS%rARELE8 = 0.0
         pFS%rDAROB8 = 0.0
         pFS%rDAROE8 = 0.0

! REGION 9: TETHER LENGTH, YOUNGS MODULUS, MATERIAL DAMPING FACTOR
         pFS%rTLETR9 = 0.0
         pFS%rYNGTR9 = 0.0
         pFS%rBETFR9 = 0.0
! BEGIN/END VALUES: LINEAL DENSITY, ELASTIC AREA, AERO DIA
         pFS%rRHOTB9 = 0.0
         pFS%rRHOTE9 = 0.0
         pFS%rARELB9 = 0.0
         pFS%rARELE9 = 0.0
         pFS%rDAROB9 = 0.0
         pFS%rDAROE9 = 0.0

! REGION 10: TETHER LENGTH, YOUNGS MODULUS, MATERIAL DAMPING FACTOR
         pFS%rTLETR10 = 0.0
         pFS%rYNGTR10 = 0.0
         pFS%rBETFR10 = 0.0
! BEGIN/END VALUES: LINEAL DENSITY, ELASTIC AREA, AERO DIA
         pFS%rRHOTB10 = 0.0
         pFS%rRHOTE10 = 0.0
         pFS%rARELB10 = 0.0
         pFS%rARELE10 = 0.0
         pFS%rDAROB10 = 0.0
         pFS%rDAROE10 = 0.0

! REGION 11: TETHER LENGTH, YOUNGS MODULUS, MATERIAL DAMPING FACTOR
         pFS%rTLETR11 = 0.0
         pFS%rYNGTR11 = 0.0
         pFS%rBETFR11 = 0.0
! BEGIN/END VALUES: LINEAL DENSITY, ELASTIC AREA, AERO DIA
         pFS%rRHOTB11 = 0.0
         pFS%rRHOTE11 = 0.0
         pFS%rARELB11 = 0.0
         pFS%rARELE11 = 0.0
         pFS%rDAROB11 = 0.0
         pFS%rDAROE11 = 0.0

! REGION 12: TETHER LENGTH, YOUNGS MODULUS, MATERIAL DAMPING FACTOR
         pFS%rTLETR12 = 0.0
         pFS%rYNGTR12 = 0.0
         pFS%rBETFR12 = 0.0
! BEGIN/END VALUES: LINEAL DENSITY, ELASTIC AREA, AERO DIA
         pFS%rRHOTB12 = 0.0
         pFS%rRHOTE12 = 0.0
         pFS%rARELB12 = 0.0
         pFS%rARELE12 = 0.0
         pFS%rDAROB12 = 0.0
         pFS%rDAROE12 = 0.0

! REGION 13: TETHER LENGTH, YOUNGS MODULUS, MATERIAL DAMPING FACTOR
         pFS%rTLETR13 = 0.0
         pFS%rYNGTR13 = 0.0
         pFS%rBETFR13 = 0.0
! BEGIN/END VALUES: LINEAL DENSITY, ELASTIC AREA, AERO DIA
         pFS%rRHOTB13 = 0.0
         pFS%rRHOTE13 = 0.0
         pFS%rARELB13 = 0.0
         pFS%rARELE13 = 0.0
         pFS%rDAROB13 = 0.0
         pFS%rDAROE13 = 0.0

! REGION 14: TETHER LENGTH, YOUNGS MODULUS, MATERIAL DAMPING FACTOR
         pFS%rTLETR14 = 0.0
         pFS%rYNGTR14 = 0.0
         pFS%rBETFR14 = 0.0
! BEGIN/END VALUES: LINEAL DENSITY, ELASTIC AREA, AERO DIA
         pFS%rRHOTB14 = 0.0
         pFS%rRHOTE14 = 0.0
         pFS%rARELB14 = 0.0
         pFS%rARELE14 = 0.0
         pFS%rDAROB14 = 0.0
         pFS%rDAROE14 = 0.0

! REGION 15: TETHER LENGTH, YOUNGS MODULUS, MATERIAL DAMPING FACTOR
         pFS%rTLETR15 = 0.0
         pFS%rYNGTR15 = 0.0
         pFS%rBETFR15 = 0.0
! BEGIN/END VALUES: LINEAL DENSITY, ELASTIC AREA, AERO DIA
         pFS%rRHOTB15 = 0.0
         pFS%rRHOTE15 = 0.0
         pFS%rARELB15 = 0.0
         pFS%rARELE15 = 0.0
         pFS%rDAROB15 = 0.0
         pFS%rDAROE15 = 0.0


! VARIABLES ASSOCIATED WITH HOST DRIVEN STEPSIZE CAPABILITY
         pFS%rTBIASF = 0.0
         pFS%rDELTPF = 0.0

! TETH DENSITY AT DEPLOY END (SEG 1) FOR USE IN MASS-FLOW FORCE CALCS
         pFS%rDENSEG1 = 0.0

! EMITTER-END CURRENT, INTEGRAL, AVER, BATT CURR INTEGRAL, MODE
         pFS%rEMICUR = 0.0
         pFS%rCURINT = 0.0
         pFS%rCURAVG = 0.0
         pFS%rBATINT = 0.0
         pFS%rCURMOD = 0.0

!--------------------------------------
! EXECUTE 3X LOOP TO INITIALIZE VECTORS
!--------------------------------------
         DO J = 1,3

! TETHER FRAME AND ATTACH POINT KINEMATICS
!-----------------------------------------
! POSITION VECTOR BETWEEN ATT PTS (INER FRAME)
             pFS%rELI(J) = 0.0

! DERIVATIVE OF POS VECTOR BETWEEN ATT PTS (INER FRAME)
             pFS%rELDI(J) = 0.0

! 2ND DERIV OF POS VECTOR BETWEEN ATT PTS (INER FRAME)
             pFS%rELDDI(J) = 0.0

! DERIVATIVE OF POS VECTOR BETWEEN ATT PTS (TETHER FRAME)
             pFS%rELDT(J) = 0.0

! ANGULAR MOMENTUM, ETC OF TOSS REF POINT (INER AND ORB FRAME)
             pFS%rRVDI(J) = 0.0
             pFS%rRVDO(J) = 0.0

! UNIT NORMAL TO TOSS REF PT ORB FRAME
             pFS%rUJOI(J) = 0.0

! DERIV OF ORB FRAME J UNIT VECTOR (ORB AND TETHER FRAME)
             pFS%rUJDO(J) = 0.0
             pFS%rUJDT(J) = 0.0

! TETHER FRAME ANGULAR VEL VECTOR (TETHER FRAME)
             pFS%rOMT(J) = 0.0

! POS,VEL,ACC OF REF END OF TETHER WR/T TOSS REF PT (INER FRAME)
             pFS%rAREFR(J) = 0.0
             pFS%rAREFV(J) = 0.0
             pFS%rAREFA(J) = 0.0

! REF ATTACH PT ("X" END) POS WR/T INER PT (INER FRAME)
             pFS%rARI(J) = 0.0

! REF ATTACH PT ("X" END) VEL WR/T INER PT (INER FRAME)
             pFS%rAVI(J) = 0.0

! REF ATTACH PT ("X" END) ACCEL WR/T INER PT (INER FRAME)
             pFS%rAAI(J) = 0.0

! TETHER FRAME UNIT VECTORS
             pFS%rUITI(J) = 0.0
             pFS%rUJTI(J) = 0.0
             pFS%rUKTI(J) = 0.0

! DERIV OF TETHER FRAME I UNIT VECTOR (INER FRAME)
             pFS%rUITDI(J) = 0.0
             pFS%rUITDDI(J) = 0.0

!--------------------
! FORCE DETERMINATION
!--------------------
! ACCEL OF GRAV AT POINT ON TETHER
             pFS%rAUGI(J) = 0.0

! AERO FORCE DENSITY AT PT ON TETHER
             pFS%rFUAI(J) = 0.0

! ELECTRO FORCE DENSITY AT PT ON TETHER
             pFS%rFUEI(J) = 0.0

! POS OF TETHER PT WR/T TETHER REF END (INER FRAME)
             pFS%rRUI(J) = 0.0

! VEL OF TETHER PT WR/T TETHER REF END (INER FRAME)
             pFS%rVUI(J) = 0.0

! POS OF TETHER PT WR/T INER PT
             pFS%rRURI(J) = 0.0

! VEL OF TETHER PT WRT/T INER PT
             pFS%rRDURI(J) = 0.0

! RELATIVE WIND ON TETHER POINT
             pFS%rVURLI(J) = 0.0

! MISC VARIABLES USED FOR GRAV GARDIENT IC CALCS
             pFS%rAUGT(J) = 0.0

! UNIT TANGENT VECTOR AT PT ON TETHER
             pFS%rTUI(J) = 0.0

! TOTAL ATMOSPHERIC WIND AT TETHER POINT
             pFS%rVWFI(J) = 0.0

! LIFT/DRAG UNIT VECTORS AND FORCES
             pFS%rUDRGI(J) = 0.0
             pFS%rULIFI(J) = 0.0
             pFS%rFDRGI(J) = 0.0
             pFS%rFLIFI(J) = 0.0

             pFS%rBVELI(J) = 0.0
             pFS%rBVRELI(J) = 0.0
             pFS%rBFLDI(J) = 0.0
             pFS%rBVELE(J) = 0.0
             pFS%rBFLDE(J) = 0.0
             pFS%rRURE(J) = 0.0

! "OFFICIAL" FINITE TETHER TENSION VECTOR (INER FRAME)
             pFS%rXEFTEN(J) = 0.0
             pFS%rYEFTEN(J) = 0.0

! TENSION VECS DERIVED VIA STRAIN CALCULATIONS (INERTIAL FRAME)
             pFS%rTNSOI(J) = 0.0
             pFS%rTNSLI(J) = 0.0

! DATA RELATED TO ASSESSING DYNAMIC EQUILIBRIUM OF BEAD SOLNS
!------------------------------------------------------------
             pFS%rDPBAI(J) = 0.0
             pFS%rDPAAI(J) = 0.0
             pFS%rDPBAIL(J) = 0.0
             pFS%rDPAAIL(J) = 0.0
             pFS%rDHBBIL(J) = 0.0

! SOME TERMS ASSOCIATED WITH BARE WIRE POWER SCENARIO OPERATIONS
! (SPECIFICALLY RELATED TO SCENARIO 5, BUT CAN BE USED FOR OTHERS)
!---------------------------------------------------------------

! POS VEC (AND DERIV) FROM X-END TO Y-END OF SEGMENT
             pFS%rSUI(J) = 0.0
             pFS%rSUDI(J) = 0.0

! POS,VEL WR/T INER PT OF Y-END OF A BEAD SEGMENT (INER FRAME)
             pFS%rRYENDI(J) = 0.0
             pFS%rVYENDI(J) = 0.0

! TETHERS X-END,Y-END-SEGMENT TARE FORCES (USED IN ATT PT TENS CALC)
             pFS%rXETARE(J) = 0.0
             pFS%rYETARE(J) = 0.0

! SUM OF AERO AND ELECTRO FORCES ON BEAD TETHER
             pFS%rFBMAI(J) = 0.0
             pFS%rFBMEI(J) = 0.0

! TOTAL LINEAR MOMENTUM (W/NUM DERIV), TOTAL EXT FORCE, FRACT ERROR
             pFS%rPBBI(J) = 0.0
             pFS%rPBBID(J) = 0.0
             pFS%rFEBBI(J) = 0.0
             pFS%rERRPBB(J) = 0.0

! TOTAL ANGULAR MOMENTUM (W/NUM DERIV), TOTAL EXT FORCE, FRACT ERROR
             pFS%rHBBI(J) = 0.0
             pFS%rHBBID(J) = 0.0
             pFS%rGEBBI(J) = 0.0
             pFS%rERRHBB(J) = 0.0

! BEAD MODEL CENTER OF MASS STATE WR/T X-END ATTACH PT
             pFS%rBBCGI(J) = 0.0
             pFS%rBBCGID(J) = 0.0

! MISCELLANEOUS DYN COUPLING VECTOR
             pFS%rSLROC(J) = 0.0

      END DO


!------------------------------------------
! EXECUTE 3x-3x LOOP TO INITIALIZE MATRICES
!------------------------------------------
       DO I = 1,3
         DO J = 1,3

             pFS%rGIT(I,J) = 0.0
             pFS%rGTO(I,J) = 0.0
             pFS%rOMIJT(I,J) = 0.0

          END DO
       END DO



! INSTANTANEOUS DISTANCE BETWEEN ATT PTS (AND 2 DERIVATIVES)

! PLASMA ELECTRON DENSITY ARRAY FOR UP TO 50 BANDS OF ALTITUDE
         DO J = 1,50
            pFS%rEDENS(J) = 0.0
         END DO


!------------------------------------
!  DYNAMICS VARIABLES AND BEAD STATES
!------------------------------------
          DO J = 1,MAX3BB

! BEAD DISPLACEMENT COORDINATES (AND PREVIOUS VALUES)
             pFS%rBUI(J) = 0.0
             pFS%rBUDI(J) = 0.0
             pFS%rBUDIP(J) = 0.0
             pFS%rBUDDI(J) = 0.0
             pFS%rBUDDIP(J) = 0.0

! SUM OF EXTERNAL FORCES ON BEAD COORDS (IE NO GRAV, NO SPRING)
             pFS%rBFEXI(J) = 0.0

! SPRING AND SPRING DAMPING FORCES ON BEAD COORDS
             pFS%rBFSI(J) = 0.0

! SUM OF ALL CONTRIBUTION TO ACCEL THAT HAS LOW COORD SENSITIVITY
             pFS%rBCINS(J) = 0.0

! PERTURBATION COORD COUPLING TERMS
             pFS%rSLROCJ(J) = 0.0

! ACCEL OF GRAV AT EACH BEAD POSITION (INER FRAME COMP)
             pFS%rBAGI(J) = 0.0

! BEAD STATES IN TETHER FRAME FOR ICS AND INTERPRETATION
             pFS%rBUT(J) = 0.0
             pFS%rBUDT(J) = 0.0
             pFS%rBFST(J) = 0.0

! BEAD POSITION AND VELOCITY WR/T TETHER REF END (INER FRAME)
             pFS%rRUBI(J) = 0.0
             pFS%rVUBI(J) = 0.0

          END DO


!--------------------------
! START MAXSEG EQUIVALENCES
!--------------------------
          DO J = 1,MAXSEG

! TENSION IN EACH SEGMENT BETWEEN 2 BEADS
             pFS%rTENSEG(J) = 0.0

! CURRENT EFFECTIVE SEGMENT SPRING RATE AND DAMPING COEFF
             pFS%rBEADKS(J) = 0.0
             pFS%rBEADKD(J) = 0.0

! CURRENT EQUIVALENT AERO DIAMETER OF A SEGMENT
             pFS%rDIARO(J) = 0.0

! DERIV OF SEG WR/T MU (AND ITS TIME DERIV) FOR FLOW CALCS
             pFS%rDSDU(J) = 0.0
             pFS%rDSDUD(J) = 0.0

! SEGMENT ATMOSPHERIC DENSITY
             pFS%rDENF(J) = 0.0

! SEGMENT LENGTH AND LENGTH RATE
             pFS%rELBSG(J) = 0.0
             pFS%rELBSGD(J) = 0.0

! TETHER THERMAL CALC ARRAYS
             pFS%rSTEMPK(J) = 0.0
             pFS%rQDTSW(J) = 0.0
             pFS%rTNEXTK(J) = 0.0
             pFS%rQDSARO(J) = 0.0

! ACTUAL SEGMENT DEPLOYED-UNDEFORMED LENGTH (REFLECTS THERMAL)
             pFS%rSEGLA(J) = 0.0

! INSTANTANEOUS SEGMENT-SPECIFIC ELECTRIC CURRENT IN EACH SEGMENT
             pFS%rCURRTS(J) = 0.0

! SEGMENT-SPECIFIC TEMPERATURE-COMPENSATED RESISTANCE/LENGTH
             pFS%rRHOELT(J) = 0.0

! SEGMENT-SPECIFIC TEMPERATURE-COMPENSATED PLASMA CIRCUMFERENCE
             pFS%rEDYNCT(J) = 0.0

! CURRENT EFFECTIVE SEGMENT ELASTIC AREA
             pFS%rBEADELA(J) = 0.0

! ELECTRON DENSITY AT A FINITE TETHER SEGEMNT
             pFS%rEDENSBB(J) = 0.0

          END DO


!--------------------------
! START MAXBBS EQUIVALENCES
!--------------------------
          DO J = 1,MAXBBS

! CURRENT EQUIVALENT MASS OF A BEAD
             pFS%rBMS(J) = 0.0

! HST FORCE COEFF MULTIPLIERS
             pFS%rCLAMBA(J) = 0.0

! HST CONSTRAINT EQUATION RHS VALUES
             pFS%rADA(J) = 0.0

          END DO

!----------------------------
! START 3*MAXSEG EQUIVALENCES
!----------------------------
          DO J = 1,MAXBBS
             DO K = 1,3

! UNIT TANGENT VECTORS AND THEIR DERIVATIVES FOR EACH SEGMENT
             pFS%rBBTUI(J,K) = 0.0
             pFS%rBBTUDI(J,K) = 0.0

! TOTAL ATMOSPHERIC WINDS (FOR HOST COMMANDED WIND EVAL)
             pFS%rWNDTOE(J,K) = 0.0

! MAGNETIC FIELD ATTRIBUTES (FOR HOST COMMANDED MAGNETIC ENVIRON EVAL)
             pFS%rBFLSEG(J,K) = 0.0
             pFS%rBVLSEG(J,K) = 0.0

! UNIT TANGENT VECTORS AND THEIR DERIVATIVES FOR EACH SEGMENT
             pFS%rBBTUT(J,K) = 0.0

! RELATIVE WIND FOR EACH SEGMENT
             pFS%rVRELSGI(J,K) = 0.0

! AERO LOAD FOR EACH SEGMENT
             pFS%rFAROSGI(J,K) = 0.0

! LOCAL WIND PERTURBATION FOR EACH SEGMENT
             pFS%rVWFSGI(J,K) = 0.0

! LOCAL ATMOSPHERIC WIND PERTURB (FOR HOST COMMANDED WIND EVAL)
             pFS%rWNDLPE(J,K) = 0.0

             END DO
         END DO

         RETURN

         END SUBROUTINE ZERO_FIN_SOLN

      END MODULE FINITE_SOLUTIONS

