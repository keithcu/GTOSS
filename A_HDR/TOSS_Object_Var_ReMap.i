! COMDECK FINITE_soln_var_remap
! %Z%GTOSS %M% H.11 code v06.00
!              H.11 code v06.00 baseline code for F90 conversion
!*************************************************************
!*************************************************************
!
! THIS FILE, WHEN INCLUDED IN ANY GTOSS SUBPROGRAM THAT MAKES
! REFERENCE TO VARIABLES WITHIN A TOSS FINITE TETHER SOLN,
! WILL TRANSFORM THE SUBPROGRAM CODE SUCH THAT REFERENCES TO
! FINITE SOLUTION DATA WILL TAKE PLACE BY MEANS OF POINTERS.
!
! THIS INCLUDE FILE MUST BE "USED" IN COOPERATION WITH THE F90
! MODULE THAT CREATES THE USER-DEFINED FINITE SOLUTION DATA
! STRUCTURE TYPE....THAT MODULE IS CALLED: FINITE_SOLN
!
!*************************************************************
!*************************************************************

!*************************************************************
! NOTE: BY CONVENTION WITHIN ALL THE FINITE SOLN ACCESSING
!       ROUTINES "pFS" IS THE POINTER VARIABLE TO AN INSTANCE
!       OF A FINITE SOLN DATA STRUCTURE
!
! THUS: ALL ACTUAL CODE VARIABLES REMAIN AS ORIGINALLY CODED
!       WITHIN GTOSS, AND THEY ARE AUTOMATICALLY REPLACED BY
!       THERE EQUIVALENT REFERENCE WITHIN THE FINITE DATA TYPE
!       PER COMPILER DIRECTIVE:
!
!                #define XXXXX pFS%rXXXX
!
!*************************************************************


!************************************************************************
!************************************************************************
!************************************************************************
!  BELOW IN OBJECT DATA STRUCTURE ARE DYNAMIC VARIABLES DEFINING OBJECT
!************************************************************************
!************************************************************************
!************************************************************************
!----------------------------------------
! INTEGER VARIABLES SPECIFIC TO EACH BODY
!----------------------------------------
#define NTIME pTO%iNTIME
#define JNOMOM pTO%iJNOMOM
#define JEULER pTO%iJEULER
#define JGRVON pTO%iJGRVON
#define JGGTON pTO%iJGGTON
#define JLIBAG pTO%iJLIBAG


!-----------------------------------------
! TIME DEPENDENT MASS & GEOMETRY VARIABLES
!-----------------------------------------
#define DMASS pTO%rDMASS
#define DMASSI pTO%rDMASSI

#define DIXX pTO%rDIXX
#define DIYY pTO%rDIYY
#define DIZZ pTO%rDIZZ
#define DIXY pTO%rDIXY
#define DIXZ pTO%rDIXZ
#define DIYZ pTO%rDIYZ

! COORDINATES OF CG WR/T BODY REF POINT
#define PCGB pTO%rPCGB

! MASS MATRIX AND ITS INVERSE
#define DIMS pTO%rDIMS
#define DIMSI pTO%rDIMSI

! AERO REFERENCE POINT POSITION
#define RDPB pTO%rRDPB

! OBJECT TOTAL MASS LOSS DUE TO THRUSTING MASS FLOW RATE
#define DMASINC pTO%rDMASINC
#define DMASDOT pTO%rDMASDOT

! TOTAL THRUSTING IMPULSE FROM ALL CONTRIBUTIONS
#define TOTIPULS pTO%rTOTIPULS

! CENTER OF BUOYANCY POINT POSITION
#define RBYPB pTO%rRBYPB


!------------------------
! ATTACH POINT VARIABLES
!------------------------
! UN-DEFORMED ATTACH POINT POS, VEL, ACC WR/T CG, BODY FRAME
#define XTUB pTO%rXTUB
#define YTUB pTO%rYTUB
#define ZTUB pTO%rZTUB
#define VXTUB pTO%rVXTUB
#define VYTUB pTO%rVYTUB
#define VZTUB pTO%rVZTUB
#define AXTUB pTO%rAXTUB
#define AYTUB pTO%rAYTUB
#define AZTUB pTO%rAZTUB

! ACTUAL ATTACH POINT POS, VEL, ACC  WR/T CG, BODY FRAME
#define XTB pTO%rXTB
#define YTB pTO%rYTB
#define ZTB pTO%rZTB
#define VXTB pTO%rVXTB
#define VYTB pTO%rVYTB
#define VZTB pTO%rVZTB
#define AXTB pTO%rAXTB
#define AYTB pTO%rAYTB
#define AZTB pTO%rAZTB

! ACTUAL ATTACH POINT POS, VEL, ACC  WR/T CG, INER FRAME
#define XTI pTO%rXTI
#define YTI pTO%rYTI
#define ZTI pTO%rZTI
#define VXTI pTO%rVXTI
#define VYTI pTO%rVYTI
#define VZTI pTO%rVZTI
#define AXTI pTO%rAXTI
#define AYTI pTO%rAYTI
#define AZTI pTO%rAZTI

! ELASTIC CONTRIBUTION TO ATTACH POINT POS, VEL, ACC WR/T CG
#define XTEB pTO%rXTEB
#define YTEB pTO%rYTEB
#define ZTEB pTO%rZTEB
#define VXTEB pTO%rVXTEB
#define VYTEB pTO%rVYTEB
#define VZTEB pTO%rVZTEB
#define AXTEB pTO%rAXTEB
#define AYTEB pTO%rAYTEB
#define AZTEB pTO%rAZTEB

! ATTACH POINT LOADS IN BODY FRAME
#define FXATB pTO%rFXATB
#define FYATB pTO%rFYATB
#define FZATB pTO%rFZATB


!----------------
! STATE VARIABLES
!----------------
! TIME VARIABLES FOR THIS OBJECT
#define TOSTIM pTO%rTOSTIM
#define DELTOS pTO%rDELTOS

! TOSS OBJECT POS, VEL, ACC  WR/T TOSS REF PT, IN INER FRAME
#define RBI pTO%rRBI
#define RBID pTO%rRBID
#define RBIDD pTO%rRBIDD

! TOSS OBJECT POS, VEL WR/T INERTIAL POINT, IN INER FRAME
#define RI pTO%rRI
#define RID pTO%rRID

! BODY AXIS ANGULAR VEL, ACC IN BODY FRAME
#define OMB pTO%rOMB
#define OMBD pTO%rOMBD

! DIRECTION COSINES AND DERIVS BETWEEN BODY AND INER FRAME
#define GBI pTO%rGBI
#define GBID pTO%rGBID

! VARIABLES ASSOCIATED WITH HOST DRIVEN STEPSIZE CAPABILITY
#define TBIASO pTO%rTBIASO
#define DELTPO pTO%rDELTPO

! TRANSF BETWEEN TOPOCENTRIC FRAME AT OBJECT AND INER FRAME
#define GIL pTO%rGIL

! ATMOSPHERIC THERMO PROPERTIES
#define DENS pTO%rDENS
#define SOUND pTO%rSOUND
#define AIRTMP pTO%rAIRTMP
#define VR pTO%rVR
#define VRSQ pTO%rVRSQ
#define QBAR pTO%rQBAR

! TOTAL RELATIVE WIND WR/T TOSS OBJECT
#define VRWI pTO%rVRWI
#define VRWB pTO%rVRWB

! ATMOS PARTICLE: TOTAL, EARTH ROTATING, AND PERTURB WIND (INER FRAME)
#define VWI pTO%rVWI
#define VWEI pTO%rVWEI
#define VWPI pTO%rVWPI

! ATMOS PARTICLE: TOTAL, EARTH ROTATING, AND PERTURB WIND (PLANET FRAME)
#define VWEP pTO%rVWEP
#define VWPP pTO%rVWPP

! OBJECT RADIUS VECTOR MAG AND SPHERICAL ALT
#define RMAG pTO%rRMAG
#define ALT pTO%rALT

! MACH NUMBER OF TOSS OBJECT
#define DMACH pTO%rDMACH

! OBJECT PLANET LONGITUDE, AND LATITUDE (RAD)
#define DMUE pTO%rDMUE
#define DLAE pTO%rDLAE

! OBJECT OBLATE ALT, LAT(DEG), AND RADIUS VECTOR MAG
#define ALTOBL pTO%rALTOBL
#define GEOLAT pTO%rGEOLAT
#define RGEOD pTO%rRGEOD

! PREVIOUS VALUES OF INTEGRATED STATE VARIABLE DERIVATIVES
#define OMBDP pTO%rOMBDP
#define RBIDP pTO%rRBIDP
#define RBIDDP pTO%rRBIDDP

#define GBIDP pTO%rGBIDP

! REFERENCE DIRCOS MATRIX FOR DATA RETENTION BY ROUTINE TOSCNA.F AND TOSCNAP.F
#define GBOHOL pTO%rGBOHOL


! GENERAL AERO TYPE DATA FOR GENERIC TOSS OBJECT AERODYNAMICS
! NOTE: WHETHER THESE ARE MEANINGFUL DEPENDS ON THE ACTUAL MODELS
#define CA_OBJ pTO%rCA_OBJ
#define CN_OBJ pTO%rCN_OBJ
#define CM_OBJ pTO%rCM_OBJ
#define FAMAG_OBJ pTO%rFAMAG_OBJ
#define FNMAG_OBJ pTO%rFNMAG_OBJ
#define ACMAG_OBJ pTO%rACMAG_OBJ
#define ALP_OBJ pTO%rALP_OBJ
#define CNQ_OBJ pTO%rCNQ_OBJ
#define CMQ_OBJ pTO%rCMQ_OBJ
#define ALP_DYN pTO%rALP_DYN
#define FNQMAG_OBJ pTO%rFNQMAG_OBJ
#define ACQMAG_OBJ pTO%rACQMAG_OBJ
#define AERO_RATE pTO%rAERO_RATE

! FOR ORB MANEUVER CONTROL PROGS: TOSCNO, TOSCNVT, TOSCNHT, TOSCNTT
!------------------------------------------------------------------
! FOR TOSCNO
#define OVT_PREV_H pTO%rOVT_PREV_H
#define OVT_FLG_ALT pTO%rOVT_FLG_ALT

! FOR TOSCNVT
#define ALCRVP pTO%rALCRVP
#define ACRVFLG pTO%rACRVFLG
#define ALT_TIM_CNTR pTO%rALT_TIM_CNTR
#define ALT_TAR pTO%rALT_TAR
#define ALT_EQUI pTO%rALT_EQUI
#define ALT_EQUI_P pTO%rALT_EQUI_P
#define ALT_SOLN_P pTO%rALT_SOLN_P
#define ALT_AT_TRAP pTO%rALT_AT_TRAP
#define ALT_LATCH pTO%rALT_LATCH
#define TEN_TIM_CNTR pTO%rTEN_TIM_CNTR
#define PREV_AVG_TEN pTO%rPREV_AVG_TEN
#define TENS_T_GRAD pTO%rTENS_T_GRAD

#define TEN_FIL6 pTO%rTEN_FIL6

! FOR TOSCNVZ
#define HDLATCH pTO%rHDLATCH

! FOR TOSCNHT
#define OPT10FAC pTO%rOPT10FAC


! BOUYANCY FORCE AND MOMENT EFFECTS ON OBJECT
!--------------------------------------------
! TOTAL BUOYANT FORCE AND MOMENT ON OBJECT
#define FBYB pTO%rFBYB
#define GBYB pTO%rGBYB


! STORAGE FOR TOSCNAP (OBJECT CNTRL OPT 7)
!-----------------------------------------
! AOA MODE SWITCH LATCHING
#define AOA_LATCH pTO%rAOA_LATCH
#define TO7_LATCH pTO%rTO7_LATCH
#define AOA_TRANS pTO%rAOA_TRANS

! OMB RATE DAMPING MODE SWITCH LATCHING
#define OMB_LATCH pTO%rOMB_LATCH
#define TOMB7_LATCH pTO%rTOMB7_LATCH
#define OMB1_TRANS pTO%rOMB1_TRANS
#define OMB2_TRANS pTO%rOMB2_TRANS
#define OMB3_TRANS pTO%rOMB3_TRANS




!-------------------
! FORCES AND MOMENTS
!-------------------
! ACCEL DUE TO GRAVITY AT OBJECT
#define AGI pTO%rAGI

! TOTAL OF ALL FORCES ON OBJECT, INER AND BODY FRAME
#define FI pTO%rFI
#define FB pTO%rFB

! TOTAL AERO FORCE ON OBJECT
#define FDB pTO%rFDB

! TOTAL CONTROL FORCE ON OBJECT
#define FCB pTO%rFCB

! TOTAL FORCE ON OBJECT DUE TO ALL ATTACH POINTS
#define FTB pTO%rFTB

! TOTAL MOMENT ON OBJECT WR/T CG
#define GB pTO%rGB

! TOTAL MOMENT WR/T CG DUE TO AERODYNAMICS
#define GDB pTO%rGDB

! TOTAL MOMENT WR/T CG DUE TO CONTROL SYSTEM
#define GCB pTO%rGCB

! TOTAL MOMENT WR/T CG DUE TO ALL ATTACH POINTS
#define GTB pTO%rGTB

! COUPLE DUE TO GRAVITY GRADIENT ON 6 DOF OBJECT
#define GGB pTO%rGGB

! SUM OF ATTACH POINT COUPLES IN BODY FRAME
#define SCTB pTO%rSCTB

! AERODYNAMIC COUPLE (ASSOCIATED W/ AERO FORCE @ AERO REF PT)
#define CDB pTO%rCDB

! COUPLE DUE TO EXTERNAL EFFECTS ON TOSS OBJECT
#define CEEB pTO%rCEEB

! FORCE DUE TO EXTERNAL EFFECTS ON TOSS OBJECT
#define FEEB pTO%rFEEB


!-----------------------------------
! VARIABLES OF INTERPRETATION + MISC
!-----------------------------------
! OBJECT POS, VEL, ACC WR/T RP: EXPRESSED IN RP ARBITRARY FRAME
#define RBAR pTO%rRBAR
#define VBAR pTO%rVBAR
#define ABAR pTO%rABAR

! OBJECT POS, VEL, ACC WR/T RP: EXPRESSED IN RP ORBITAL FRAME
#define RBOR pTO%rRBOR
#define RBORD pTO%rRBORD
#define RBORDD pTO%rRBORDD

! RANGE, AZIMUTH, ELEV (AND RATES), ETC
#define RANGE pTO%rRANGE
#define RANGED pTO%rRANGED

! TRANSFORMATION FROM OBJECT ORB FRAME TO INERTIAL
#define GOI pTO%rGOI

! OBJECT POS WR/T INER PT: EXPRESSED IN PLANET FIXED FRAME
#define RIP pTO%rRIP

! OBJECT RIGID BODY ANGULAR MOMENTUM IN INERTIAL FRAME
#define HMOMI pTO%rHMOMI


!--------------------------------------------------------------
! VARIABLES ASSOCIATED WITH THE SKIP ROPE DAMPER IMPLEMENTATION
!--------------------------------------------------------------
! DIRECTION COSINES BETWEEN ASSOCIATED ATT PT FRAME AND INER FRAME
#define GSDAI pTO%rGSDAI

! FRICTION GRADIENT AND NORMAL FORCE ON CENTER-PIECE
#define DLFRIC pTO%rDLFRIC
#define FRICNM pTO%rFRICNM

! TOTAL LOAD IN EACH PSD DAMPER LINK
#define ARMLOD pTO%rARMLOD

! DIST FROM DEPLOY ORIFICE TO CP DURING DAMPER QUIESCENT STATE OPS
#define ACPLEN pTO%rACPLEN

! BODY TEMPERATURE
!-----------------
! (NOTE, LINKAGE BETWEEN OBJECT TEMP AND TETHER THERMAL IS NOT ACTIVE)
#define BTEMPT pTO%rBTEMPT

!-----------------------------------------
! STORAGE FOR TOSCNSK1 (OBJECT CNTRL OPT 9)
!-----------------------------------------
#define C9_XY_TOGGLE pTO%rC9_XY_TOGGLE
#define C9_H_TOGGLE pTO%rC9_Z_TOGGLE
#define C9_X_HOLD_VAL pTO%rC9_X_HOLD_VAL
#define C9_Y_HOLD_VAL pTO%rC9_Y_HOLD_VAL
#define C9_H_HOLD_VAL pTO%rC9_Z_HOLD_VAL
#define C9_CMD_RATE_P pTO%rC9_CMD_RATE_P




!************************************************************************
!************************************************************************
!************************************************************************
! BELOW IN OBJECT DATA STRUCTURE IS USER-INPUT CONSTANTS DEFINING OBJECT
!************************************************************************
!************************************************************************
!************************************************************************
! IMPORTANT NOTE: THE DATA ITEMS BELOW MUST BE KEPT SYNCHRONIZED AND
!                 CONSISTENT WITH THE OBJI.i OBJECT INPUT ITEM-MAPPING
!                 EQUIVALENCE STATEMENTS. SO IF YOU ADD A NEW OBJECT
!                 INPUT DATA CONSTANT IT MUST BE RECOGNIZED IN TWO PLACES.

! INTEGER INPUT DATA DEFINING A TOSS OBJECT
#define NATPAT pTO%iNATPAT
#define LAERO pTO%iLAERO
#define LCONT pTO%iLCONT
#define LICTRN pTO%iLICTRN
#define LICROT pTO%iLICROT
#define LICSPL pTO%iLICSPL
#define LNOMOM pTO%iLNOMOM
#define LEULER pTO%iLEULER
#define LGRVON pTO%iLGRVON
#define LGGTON pTO%iLGGTON
#define LIBTYP pTO%iLIBTYP
#define LICOMT pTO%iLICOMT
#define LINERT pTO%iLINERT

! THESE ITEMS ARE ASSOCIATED WITH THE SKIP ROPE DAMPER FEATURE
!-------------------------------------------------------------
! ATTACH PT AND TOSS OBJECT NUMBER TO WHICH DAMPER BASE IS INSTALLED
#define LOBJSR pTO%iLOBJSR
#define LAPPSR pTO%iLAPPSR
! NUMBER OF ANCHOR POINTS FOR DAMPER LINK ARMS
#define LNCHOR pTO%iLNCHOR
! TOSS TETHER CONNECTING DAMPER CENTER-PIECE TO DAMPER BASE ATTACH PT
#define LTHBAS pTO%iLTHBAS
! TOSS TETHER WHOSE SKIP ROPE MOTION IS TO BE DAMPED
#define LTHDMP pTO%iLTHDMP
! TYPE OF INTEGRATOR TO BE USED FOR TOSS OBJECT CENTERPIECE (1=EULER)
#define LSRDIN pTO%iLSRDIN

! OBJ AND AP WR/T WHICH CONTROL OPT 4 PROVIDES INLINE-THRUST/LIB CONTROL
!-----------------------------------------------------------------------
#define LREFOB pTO%iLREFOB
#define LREFAT pTO%iLREFAT
#define LAPLIB pTO%iLAPLIB
! TOSS TETHER NUMBER WHICH CONTROL OPT 4 USES TO ACTIVATE INLINE-THRUST
#define LTTNNL pTO%iLTTNNL


! ATTITUDE CONTROL FRAME-REF SPECIFIER FOR CONTROL OPT 5
!-------------------------------------------------------
#define L05TYP pTO%iL05TYP

! FLAG TO ACTIVE THIS OBJECTS MASS AS A GRAVITY BODY TO OTHERS
!-------------------------------------------------------------
#define LGRAVBOD pTO%iLGRAVBOD

! ATTITUDE CONTROL FRAME-REF SPECIFIER FOR CONTROL OPT 7
!-------------------------------------------------------
#define L07TYP pTO%iL07TYP


!---------------------------------------
! MASS PROPERTIES, KINEMATICS, STATE ICS
!---------------------------------------
! MASS PROPERTIES AND GEOMETRY
#define FMASSO pTO%rFMASSO
#define FIXXO pTO%rFIXXO
#define FIYYO pTO%rFIYYO
#define FIZZO pTO%rFIZZO
#define FIXYO pTO%rFIXYO
#define FIXZO pTO%rFIXZO
#define FIYZO pTO%rFIYZO

! COORDINATES OF CG WR/T BODY REF POINT
#define PCGBO pTO%rPCGBO

! ATTACH POINT POSITIONS WR/T BODY GEOMETRY REF PT
#define PXTB pTO%rPXTB
#define PYTB pTO%rPYTB
#define PZTB pTO%rPZTB

! AERO REFERENCE POINT
#define PDPB pTO%rPDPB

! INITIAL CONDITIONS ON TRANSLATION POSITION STATE
#define POSO pTO%rPOSO

! INITIAL CONDITIONS ON TRANSLATION RATE STATE
#define RATEO pTO%rRATEO

! INITIAL CONDITONS ON BODY ANGULAR VELOCITY STATE
#define OMBO pTO%rOMBO

! INITIAL EULER ANGLES (FOR ATTITUDE INITIALIZATION)
#define PITCHO pTO%rPITCHO
#define ROLLO pTO%rROLLO
#define YAWO pTO%rYAWO

! INITIAL CONDITIONS ON DIRECTION COSINE STATE
#define GBIO pTO%rGBIO


!----------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 2
!----------------------------------------------
! ISP VALUE FOR DETERMINING MASS LOSS DUE TO THRUST
#define FCNISP pTO%rFCNISP

! A CONSTANT COUPLE
#define GXCO pTO%rGXCO
#define GYCO pTO%rGYCO
#define GZCO pTO%rGZCO

! A SEQUENCE OF TIMED, CONSTANT FORCE PERIODS
#define CFT1 pTO%rCFT1
#define CFORX1 pTO%rCFORX1
#define CFORY1 pTO%rCFORY1
#define CFORZ1 pTO%rCFORZ1
#define CFT2 pTO%rCFT2
#define CFORX2 pTO%rCFORX2
#define CFORY2 pTO%rCFORY2
#define CFORZ2 pTO%rCFORZ2
#define CFT3 pTO%rCFT3
#define CFORX3 pTO%rCFORX3
#define CFORY3 pTO%rCFORY3
#define CFORZ3 pTO%rCFORZ3
#define CFT4 pTO%rCFT4
#define CFORX4 pTO%rCFORX4
#define CFORY4 pTO%rCFORY4
#define CFORZ4 pTO%rCFORZ4
#define CFT5 pTO%rCFT5
#define CFORX5 pTO%rCFORX5
#define CFORY5 pTO%rCFORY5
#define CFORZ5 pTO%rCFORZ5
#define CFT6 pTO%rCFT6
#define CFORX6 pTO%rCFORX6
#define CFORY6 pTO%rCFORY6
#define CFORZ6 pTO%rCFORZ6
#define CFT7 pTO%rCFT7
#define CFORX7 pTO%rCFORX7
#define CFORY7 pTO%rCFORY7
#define CFORZ7 pTO%rCFORZ7
#define CFT8 pTO%rCFT8

! A SINUSOIDAL PSUEDO-CONTROL FORCE
! (SPECIFIED AS DISPLACEMENT HALF-AMPL AND FREQ-CPS)
#define COSTRT pTO%rCOSTRT
#define CDELX pTO%rCDELX
#define CFREQX pTO%rCFREQX
#define CDELY pTO%rCDELY
#define CFREQY pTO%rCFREQY
#define CDELZ pTO%rCDELZ
#define CFREQZ pTO%rCFREQZ
#define COSTOP pTO%rCOSTOP



! CONSTANT DRAG COEFFICIENT DEFAULT AERO OPTION DATA
!---------------------------------------------------
#define AAERO pTO%rAAERO
#define CDRGO pTO%rCDRGO


! SECRET FORCE WHICH CAN BE GENERATED ONLY BETWEEN ATT POINTS 1 ON
! REF PT AND TOSS OBJECT 2 (ACTIVATED WITH NON-ZERO VALUE 'SECRET')
#define SECRET pTO%rSECRET



!------------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 3
! (PROVIDES BOOM TIP SKIP ROPE DAMPER CAPABILITY)
!------------------------------------------------
! DAMPER ARM ANCHOR POINT COORDS WR/T ATTACH PT (IN AP FRAME)
#define XASRA pTO%rXASRA
#define YASRA pTO%rYASRA
#define ZASRA pTO%rZASRA

! REFERENCE LENGTH OF DAMPER ARM (FOR SPRING CONSTANT CALCS)
#define REFSRL pTO%rREFSRL

! PRELOAD (ASSOCIATED WITH POSITIVE LINK EXTENSION RATE)
#define SRPL pTO%rSRPL

! SPRING CONSTANT (ASSOCIATED WITH POSITIVE LINK EXTENSION RATE)
#define SRK pTO%rSRK

! DAMPING CONSTANT (ASSOCIATED WITH POSITIVE LINK EXTENSION RATE)
#define SRKD pTO%rSRKD

! PRELOAD HYSTERESIS FACTOR (ASSOCIATED WITH NEG LINK EXTENSION RATE)
#define HYSRPL pTO%rHYSRPL

! SPRING HYSTERESIS FACTOR (ASSOCIATED WITH NEG LINK EXTENSION RATE)
#define HYSRK pTO%rHYSRK

! DAMPING HYSTERESIS FACTOR (ASSOCIATED WITH LINK ARM EXTENSION RATE)
#define HYSRKD pTO%rHYSRKD

! COEFF OF FRICTION BETWEEN TETHER AND DAMPER CENTER-PIECE
#define COEFMU pTO%rCOEFMU

! EXTENSION RATE AT WHICH FULL HYSTERESIS ONSET WILL HAVE OCCURED
#define HYSONS pTO%rHYSONS

! CENTERPIECE/TETHER REL RATE AT WHICH FULL FRIC ONSET WILL HAVE OCCURED
#define FRCONS pTO%rFRCONS


!-------------------------------------------------------------
! INITIAL VALUES FOR TRANSL IC OPT VIA LIBRATION SPECIFICATION
!-------------------------------------------------------------
! INITIAL LIBRATION IN-PLANE AND OUT-OF-PLANE ANGLES AND RATES
#define AIPLO pTO%rAIPLO
#define AOPLO pTO%rAOPLO
#define AIPLDO pTO%rAIPLDO
#define AOPLDO pTO%rAOPLDO

! INITIAL RANGE AND RANGE RATE
#define ARNGO pTO%rARNGO
#define ARNGDO pTO%rARNGDO


!----------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 4
!----------------------------------------------
! OPERATIONAL LIBRATION ANGLE AND DEADBAND SPECIFICATION
#define DBLIBD pTO%rDBLIBD
#define OPLIBD pTO%rOPLIBD

! DATA INPUT, INLINE THRUST LEVEL AND ON-OFF TIME SPECIFICATION
#define FINLIN pTO%rFINLIN
#define FINON pTO%rFINON
#define FINOFF pTO%rFINOFF

! DATA INPUT, LENGTH OF TETHER, INSIDE OF WHICH, IN-LINE THRUST WILL BE TURNED ON
#define FLONOF pTO%rFLONOF


! ITEM OUT OF ORDER, THERMAL CONDUCTION AT OBJECT
!------------------------------------------------
! INITIAL BODY TEMPERATURE <---- OUT OF NUMERICAL ORDER
! (NOTE, LINAKAGE BETWEEN OBJECT TEMP AND TETHER THERMAL IS NOT ACTIVE)
#define BTEMPO pTO%rBTEMPO


! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 4 (CONTINUED)
!----------------------------------------------------------
! DATA INPUT, START/STOP TIMES FOR LIBRATION CONTROL
#define TLIBON pTO%rTLIBON
#define TLIBOF pTO%rTLIBOF


! DATA FOR TOSS OBJECT ATTITUDE CONTROL OPTION = 5
!-------------------------------------------------
! DATA INPUT, ATT ERROR-RATE GAINS
#define GOREX pTO%rGOREX
#define GOREY pTO%rGOREY
#define GOREZ pTO%rGOREZ
! DATA INPUT, ATT ERROR GAINS
#define GOAEX pTO%rGOAEX
#define GOAEY pTO%rGOAEY
#define GOAEZ pTO%rGOAEZ


!-------------------------------------------------
! DATA FOR TOSS OBJECT (TOSCNO) CONTROL OPTION = 6
!-------------------------------------------------
! DATA INPUT, GENERAL DATA FOR ALL OPT 6 OPERATIONS
#define ORBTISP pTO%rORBTISP

! DATA INPUT, DATA RELATED TO ORBITAL VERTICAL-THRUST APPLICATION
#define OVT_ENABL pTO%rOVT_ENABL

#define OVT_T_ON pTO%rOVT_T_ON
#define OVT_T_OFF pTO%rOVT_T_OFF
#define OVT_H_OFF pTO%rOVT_H_OFF

#define OVT_DB pTO%rOVT_DB

#define OVT_CONTVA pTO%rOVT_CONTVA

#define OVT_OBJ pTO%rOVT_OBJ
#define OVT_FAC pTO%rOVT_FAC

#define OVT_TZERO pTO%rOVT_TZERO
#define OVT_HZERO pTO%rOVT_HZERO
#define OVT_THRUS pTO%rOVT_THRUS

! DATA INPUT, DATA RELATED TO ORBITAL HORIZONTAL-THRUST APPLICATION
#define OHT_ENABL pTO%rOHT_ENABL

#define OHT_T_ON pTO%rOHT_T_ON
#define OHT_T_OFF pTO%rOHT_T_OFF

#define OHT_CONTVA pTO%rOHT_CONTVA
#define OHT_CONAZ pTO%rOHT_CONAZ
#define OHT_CONLAT pTO%rOHT_CONLAT
#define OHT_CONLON pTO%rOHT_CONLON

#define OHT_LL_DB pTO%rOHT_LL_DB
#define OHT_VEL_DB pTO%rOHT_VEL_DB

#define OHT_THRUS pTO%rOHT_THRUS

#define OHT_T_LLDZ pTO%rOHT_T_LLDZ
#define OHT_H_LLDZ pTO%rOHT_H_LLDZ

! DATA INPUT, DATA RELATED TO ORBITAL TENSION-ALIGNED THRUST APPLICATION
#define OTT_ENABL pTO%rOTT_ENABL

#define OTT_T_ON pTO%rOTT_T_ON
#define OTT_T_OFF pTO%rOTT_T_OFF

#define OTT_FAC pTO%rOTT_FAC

! DATA INPUT, TABLE FOR VERTICAL PROFILE (EITHER VELOCITY OR THRUST)
#define OVTVATA pTO%rOVTVATAB

#define OHTVTAB pTO%rOHTVTAB
#define OHAZTAB pTO%rOHAZTAB

! DATA INPUT, GENERAL ERROR AND RATE GAINS ASSOCIATED WITH VERT CONTROL
#define GAIN_VERR pTO%rGAIN_VERR
#define GAIN_VERRD pTO%rGAIN_VERRD

! DATA INPUT, GENERAL ERROR AND RATE GAINS ASSOCIATED WITH HORIZ CONTROL
#define GAIN_HERR pTO%rGAIN_HERR
#define GAIN_HERRD pTO%rGAIN_HERRD

! DATA INPUT, TIME INTERVAL TO ASSESS TENSION FOR VERTICAL CONTROL
#define ALT_T_INTVL pTO%rALT_T_INTVL
#define TENF_TTSPAN pTO%rTENF_TTSPAN
#define TENF_NSAMPL pTO%rTENF_NSAMPL
#define ALT_ALT_FLG pTO%rALT_ALT_FLG
#define TEN_FILTER pTO%rTEN_FILTER
#define ALT_FILTER pTO%rALT_FILTER

!------------------------------------------------
! DATA INPUT, TO DEFINE BUOYANCY FOR TOSS OBEJCTS
!------------------------------------------------
! NOTE: THIS IS A WORK IN PROGRESS, SO CURRENTLY, A FIXED CONTAINER SIZE
!             IS ASSUMED, EG. ENVELOP EXPANSION EFFECTS ARE NOT INCLUDED
!             (IT IS ALSO ASSUMED THAT THE USER HAS INCLUDED IN THE TOSS OBJECT
!              MASS ACCOUNTING, THE MASS OF ANY CHOSEN GASEOUS "FILL")

#define OBVOL_MAX pTO%rOBVOL_MAX

! DATA INPUT, CENTER OF BUOYANCY REFERENCE POINT
#define PBOYB pTO%rPBOYB


!-------------------------------------------------------------
! DATA INPUT, DATA FOR TOSS OBJECT ATTITUDE CONTROL OPTION = 7
!-------------------------------------------------------------
! ATT CONTROL ERROR-RATE GAINS
#define GOREX7 pTO%rGOREX7
#define GOREY7 pTO%rGOREY7
#define GOREZ7 pTO%rGOREZ7
! ATT CONTROL ERROR GAINS
#define GOAEX7 pTO%rGOAEX7
#define GOAEY7 pTO%rGOAEY7
#define GOAEZ7 pTO%rGOAEZ7

! DATA INPUT, AOA VALUE, GREATER THAN WHICH, AOA CONTROL STARTS
#define AOA_MX7 pTO%rAOA_MX7

! DATA INPUT, TIME TO START CONTROL OPTION
#define TSTR_CO7 pTO%rTSTR_CO7

! DATA INPUT, AOA CONTROL ERROR AND RATE GAINS
#define AOAERR7 pTO%rAOAERR7
#define AOARAT7 pTO%rAOARAT7

! DATA INPUT, DELTA-TIME TO TRANSITION TO FINAL COMMANDED AOA OF 180 DEG
#define DELT7_AOA pTO%rDELT7_AOA

! DATA INPUT, BODY RATE .LT. WHICH AOA TRANSITION IS ALLOWED TO PROCEED
#define TST_OMD7 pTO%rTST_OMD7

! DATA INPUT, DELTA-TIME TO DRIVE OMB TO ZERO
#define DELT7_OMB pTO%rDELT7_OMB



!------------------------------------------
! DATA INPUT FOR TOSCNTH CONTROL OPTION = 8
!------------------------------------------
! DATA INPUT, HERE IS SOME SIMPLE THRUST DATA
#define CISP3 pTO%rCISP3
#define TTIMON3 pTO%rTTIMON3
#define TTIMOFF3 pTO%rTTIMOFF3
#define VTHRUS3 pTO%rVTHRUS3
#define P_AREA3 pTO%rP_AREA3


!-------------------------------------------------
! DATA FOR TOSS OBJECT ATTITUDE CONTROL OPTION = 9
!-------------------------------------------------
! DEFINE NUMBER OF TETHERS INVOKED TO CONTROL THIS OBJECT
#define C9_TTCNT pTO%rC9_TTCNT
      
! DEFINE WHETHER SHEAVE RATES ARE CONSIDERED IN CONTROL CALCS
#define C9_SHVONOF pTO%rC9_SHVONOF
      
! DEFINE RATE-HOLD CONTROL FEEDBACK GAINS
#define C9_RTH_KERR pTO%rC9_RTH_KERR
#define C9_RTH_KRAT pTO%rC9_RTH_KRAT
      
! DEFINE POSITION-HOLD CONTROL FEEDBACK GAINS
#define C9_PSH_KERR pTO%rC9_PSH_KERR
#define C9_PSH_KRAT pTO%rC9_PSH_KRAT

! DEFINE LIBRATION CONTROL CONTROL FEEDBACK GAINS & UTIL FACTOR
#define C9_PSH_KLRAT pTO%rC9_PSH_KLRAT
#define C9_PSH_KLACC pTO%rC9_PSH_KLACC
#define C9_PSH_KFACT pTO%rC9_PSH_KFACT
#define C9_LHPOS_DB pTO%rC9_LHPOS_DB
#define C9_LHVEL_DB pTO%rC9_LHVEL_DB

! DEFINE USER-SPECIFIED TENSION ERROR GAINS FOR THE TENSION CONTROLLER
#define C9_TERR_GAIN pTO%rC9_TERR_GAIN
#define C9_TERRR_GAIN pTO%rC9_TERRR_GAIN
#define C9_TERRI_GAIN pTO%rC9_TERRI_GAIN
#define C9_LBW_FLG pTO%rC9_LBW_FLG

! DEFINE USER-SPECIFIED DEBUG FLAG
#define C9_DEBUG_FLG pTO%rC9_DEBUG_FLG
#define C9_DEBUG_TTN1 pTO%rC9_DEBUG_TTN1
#define C9_DEBUG_TTN2 pTO%rC9_DEBUG_TTN2
#define C9_DEBUG_DT pTO%rC9_DEBUG_DT

! DEFINE TIME TO START/STOP CONTROLLER
#define C9_TSTART pTO%rC9_TSTART
#define C9_TSTOP pTO%rC9_TSTOP
            
! DEFINE TOSS TETHER NUMBERS DELEGATED TO CONTROLLING OBJECT
#define C9_TT pTO%rC9_TT
      
! DEFINE MAXIMUM ALLOWED TENSION FOR TETHER
#define C9_TMAX pTO%rC9_TMAX
      
! DEFINE MINIMUM ALLOWED TENSION FOR TETHER
#define C9_TMIN pTO%rC9_TMIN
            
! DEFINE MAXIMUM ALLOWED DEPLOY RATE FOR TETHER
#define C9_LDMAX pTO%rC9_LDMAX
      
! DEFINE MAXIMUM ALLOWED DEPLOY ACCELERATION FOR TETHER
#define C9_LDDMAX pTO%rC9_LDDMAX

! DEFINE XD (NORTH), YD (EAST), AND ALT-RATE (UP) COMMAND TABLES VS TIME
#define C9_XD_TAB pTO%rC9_XD_TAB
#define C9_YD_TAB pTO%rC9_YD_TAB
#define C9_HD_TAB pTO%rC9_HD_TAB

! DEFINE MINIMUM TENSION VS TETHER LENGTH TABLE
#define C9_TSAG_TAB pTO%rC9_TSAG_TAB

! DEFINE USER-PREEMPT OF RATE COMMANDS VIA CONSTANT VALUES
#define C9_XDCON_CMD pTO%rC9_XDCON_CMD
#define C9_YDCON_CMD pTO%rC9_YDCON_CMD
#define C9_HDCON_CMD pTO%rC9_HDCON_CMD

! DEFINE USER-SPECIFIED GO-TO COMMANDS VIA CONSTANT VALUES
#define C9_XCON_CMD pTO%rC9_XCON_CMD
#define C9_YCON_CMD pTO%rC9_YCON_CMD
#define C9_HCON_CMD pTO%rC9_HCON_CMD

! DEFINE USER-SPECIFIED MULTIPLIER-FACTORS ON TABLE LOOKUP COMMANDS
#define C9_XDFAC pTO%rC9_XDFAC
#define C9_YDFAC pTO%rC9_YDFAC
#define C9_HDFAC pTO%rC9_HDFAC
#define C9_TSAGFAC pTO%rC9_TSAGFAC

! DEPLOY RATE CONTROL BIAS
#define C9_DPR_BIAS pTO%rC9_DPR_BIAS

! TETHER STATE DATA RETENTION (PERSISTENCE) ALLOCATIONS
#define C9_DEP_LEN pTO%rC9_DEP_LEN
#define C9_DEP_LEND pTO%rC9_DEP_LEND
#define C9_DEP_LENDP pTO%rC9_DEP_LENDP
#define C9_DEP_LENDD pTO%rC9_DEP_LENDD
#define C9_TEN_PREV pTO%rC9_TEN_PREV

! DEFINE MASS-LOADING EVENT 1
#define C9_TIME_MSEV_1 pTO%rC9_TIME_MSEV_1
#define C9_DELT_MSEV_1 pTO%rC9_DELT_MSEV_1
#define C9_MASS_MSEV_1 pTO%rC9_MASS_MSEV_1
      
! DEFINE MASS-LOADING EVENT 2
#define C9_TIME_MSEV_2 pTO%rC9_TIME_MSEV_2
#define C9_DELT_MSEV_2 pTO%rC9_DELT_MSEV_2
#define C9_MASS_MSEV_2 pTO%rC9_MASS_MSEV_2
      
! DEFINE MASS-LOADING EVENT 3
#define C9_TIME_MSEV_3 pTO%rC9_TIME_MSEV_3
#define C9_DELT_MSEV_3 pTO%rC9_DELT_MSEV_3
#define C9_MASS_MSEV_3 pTO%rC9_MASS_MSEV_3

! DEFINE OBJECT TO WHICH MASS-LOADING IS APPLIED
#define C9_MSEV_OBJ pTO%rC9_MSEV_OBJ

! DEFINE DATA RELATED TO TENSION AVERAGING FILTER
#define C9_TENF_TTSPAN pTO%rC9_TENF_TTSPAN
#define C9_TENF_NSAMPL pTO%rC9_TENF_NSAMPL
#define C9_TEN_TIM_CNTR pTO%rC9_TEN_TIM_CNTR
#define C9_PREV_AVG_TEN pTO%rC9_PREV_AVG_TEN
#define C9_TEN_AFIL pTO%rC9_TEN_AFIL
#define C9_FILTER_TENS pTO%rC9_FILTER_TENS 

! DEFINE DATA RELATED TO SAG CONTROL LIMITS AND DEADBANDS
#define C9_SAG_LIMIT_FRAC pTO%rC9_SAG_LIMIT_FRAC
#define C9_SAG_LIMIT_MIN pTO%rC9_SAG_LIMIT_MIN
#define C9_SAG_DB_FRAC pTO%rC9_SAG_DB_FRAC
#define C9_SAG_DB_VALUE pTO%rC9_SAG_DB_VALUE

! DEFINE VARIOUS CONTROL DISPLAY ENNUNCIATOR TOGGLES
#define ALT_TOGGLE_50 pTO%rALT_TOGGLE_50 
#define ALT_TOGGLE_0 pTO%rALT_TOGGLE_0 
#define ALT_TOGGLE_HDCMD pTO%rALT_TOGGLE_HDCMD 
#define CMASS_EVNT_TOGGLE_1 pTO%rCMASS_EVNT_TOGGLE_1 
#define CMASS_EVNT_TOGGLE_2 pTO%rCMASS_EVNT_TOGGLE_2 
#define CMASS_EVNT_TOGGLE_3 pTO%rCMASS_EVNT_TOGGLE_3 
