! COMDECK TOSS_OJECT_DATA_STRUCTURE
! %Z%GTOSS %M% H.12 code v02.00
!              H.12 code v02.00 Increased Att Pts to 16
!---------------------------------------------------------------
!              H.11 code v01.00 baseline code for F90 conversion
!*********************************************************
!*********************************************************
!
! THIS MODULE, WHEN "USED" IN ANY GTOSS SUBPROGRAM THAT
! REFERENCES VARIABLES WITHIN A TOSS OBJECT SOLUTION,
! WILL PROVIDE THE OFFICIAL USER-DEFINED DATA STRUCTURE
! FOR A TOSS OBJECT SOLUTION
!
!*********************************************************
!*********************************************************
      MODULE TOSS_OBJECT_DATA_STRUCTURE

! FOR NOW, NO NOT ENFORCE STRONG-TYPING IN ORDER TO ALLOW
! COMPATIBLE INCLUSION OF EXISTNG TOSS ARRAY DEFNS
!         IMPLICIT NONE



!***********************************************************
!***********************************************************
! NOW DEFINE THE DATA THAT SPECIFIES A TOSS OBJECT STRUCTURE
!***********************************************************
!***********************************************************
         TYPE OBJECT_SOLN_STATE

!************************************************************************
!************************************************************************
!************************************************************************
!  BELOW IN OBJECT DATA STRUCTURE ARE DYNAMIC VARIABLES DEFINING OBJECT
!************************************************************************
!************************************************************************
!************************************************************************
!----------------------------------------
! INTEGER VARIABLES SPECIFIC TO EACH BODY
!----------------------------------------
            INTEGER :: iNTIME
            INTEGER :: iJNOMOM
            INTEGER :: iJEULER
            INTEGER :: iJGRVON
            INTEGER :: iJGGTON
            INTEGER :: iJLIBAG


!-----------------------------------------
! TIME DEPENDENT MASS & GEOMETRY VARIABLES
!-----------------------------------------
            REAL*8 :: rDMASS
            REAL*8 :: rDMASSI

            REAL*8 :: rDIXX
            REAL*8 :: rDIYY
            REAL*8 :: rDIZZ
            REAL*8 :: rDIXY
            REAL*8 :: rDIXZ
            REAL*8 :: rDIYZ

! COORDINATES OF CG WR/T BODY REF POINT
            REAL*8, DIMENSION(3) :: rPCGB

! MASS MATRIX AND ITS INVERSE
            REAL*8, DIMENSION(3,3) :: rDIMS
            REAL*8, DIMENSION(3,3) :: rDIMSI

! AERO REFERENCE POINT POSITION
            REAL*8, DIMENSION(3) :: rRDPB

! OBJECT TOTAL MASS LOSS DUE TO THRUSTING MASS FLOW RATE
            REAL*8 :: rDMASINC
            REAL*8 :: rDMASDOT

! TOTAL THRUSTING IMPULSE FROM ALL CONTRIBUTIONS
            REAL*8 :: rTOTIPULS

! CENTER OF BUOYANCY POINT POSITION
            REAL*8, DIMENSION(3) :: rRBYPB


!------------------------
! ATTACH POINT VARIABLES
!------------------------
! UN-DEFORMED ATTACH POINT POS, VEL, ACC WR/T CG, BODY FRAME
            REAL*8, DIMENSION(16) :: rXTUB
            REAL*8, DIMENSION(16) :: rYTUB
            REAL*8, DIMENSION(16) :: rZTUB
            REAL*8, DIMENSION(16) :: rVXTUB
            REAL*8, DIMENSION(16) :: rVYTUB
            REAL*8, DIMENSION(16) :: rVZTUB
            REAL*8, DIMENSION(16) :: rAXTUB
            REAL*8, DIMENSION(16) :: rAYTUB
            REAL*8, DIMENSION(16) :: rAZTUB

! ACTUAL ATTACH POINT POS, VEL, ACC  WR/T CG, BODY FRAME
            REAL*8, DIMENSION(16) :: rXTB
            REAL*8, DIMENSION(16) :: rYTB
            REAL*8, DIMENSION(16) :: rZTB
            REAL*8, DIMENSION(16) :: rVXTB
            REAL*8, DIMENSION(16) :: rVYTB
            REAL*8, DIMENSION(16) :: rVZTB
            REAL*8, DIMENSION(16) :: rAXTB
            REAL*8, DIMENSION(16) :: rAYTB
            REAL*8, DIMENSION(16) :: rAZTB

! ACTUAL ATTACH POINT POS, VEL, ACC  WR/T CG, INER FRAME
            REAL*8, DIMENSION(16) :: rXTI
            REAL*8, DIMENSION(16) :: rYTI
            REAL*8, DIMENSION(16) :: rZTI
            REAL*8, DIMENSION(16) :: rVXTI
            REAL*8, DIMENSION(16) :: rVYTI
            REAL*8, DIMENSION(16) :: rVZTI
            REAL*8, DIMENSION(16) :: rAXTI
            REAL*8, DIMENSION(16) :: rAYTI
            REAL*8, DIMENSION(16) :: rAZTI

! ELASTIC CONTRIBUTION TO ATTACH POINT POS, VEL, ACC WR/T CG
            REAL*8, DIMENSION(16) :: rXTEB
            REAL*8, DIMENSION(16) :: rYTEB
            REAL*8, DIMENSION(16) :: rZTEB
            REAL*8, DIMENSION(16) :: rVXTEB
            REAL*8, DIMENSION(16) :: rVYTEB
            REAL*8, DIMENSION(16) :: rVZTEB
            REAL*8, DIMENSION(16) :: rAXTEB
            REAL*8, DIMENSION(16) :: rAYTEB
            REAL*8, DIMENSION(16) :: rAZTEB

! ATTACH POINT LOADS IN BODY FRAME
            REAL*8, DIMENSION(16) :: rFXATB
            REAL*8, DIMENSION(16) :: rFYATB
            REAL*8, DIMENSION(16) :: rFZATB


!----------------
! STATE VARIABLES
!----------------
! TIME VARIABLES FOR THIS OBJECT
            REAL*8 :: rTOSTIM
            REAL*8 :: rDELTOS

! TOSS OBJECT POS, VEL, ACC  WR/T TOSS REF PT, IN INER FRAME
            REAL*8, DIMENSION(3) :: rRBI
            REAL*8, DIMENSION(3) :: rRBID
            REAL*8, DIMENSION(3) :: rRBIDD

! TOSS OBJECT POS, VEL WR/T INERTIAL POINT, IN INER FRAME
            REAL*8, DIMENSION(3) :: rRI
            REAL*8, DIMENSION(3) :: rRID

! BODY AXIS ANGULAR VEL, ACC IN BODY FRAME
            REAL*8, DIMENSION(3) :: rOMB
            REAL*8, DIMENSION(3) :: rOMBD

! DIRECTION COSINES AND DERIVS BETWEEN BODY AND INER FRAME
            REAL*8, DIMENSION(3,3) :: rGBI
            REAL*8, DIMENSION(3,3) :: rGBID

! VARIABLES ASSOCIATED WITH HOST DRIVEN STEPSIZE CAPABILITY
            REAL*8 :: rTBIASO
            REAL*8 :: rDELTPO

! TRANSF BETWEEN TOPOCENTRIC FRAME AT OBJECT AND INER FRAME
            REAL*8, DIMENSION(3,3) :: rGIL

! ATMOSPHERIC THERMO PROPERTIES
            REAL*8 :: rDENS
            REAL*8 :: rSOUND
            REAL*8 :: rAIRTMP
            REAL*8 :: rVR
            REAL*8 :: rVRSQ
            REAL*8 :: rQBAR

! TOTAL RELATIVE WIND WR/T TOSS OBJECT
            REAL*8, DIMENSION(3) :: rVRWI
            REAL*8, DIMENSION(3) :: rVRWB

! ATMOS PARTICLE: TOTAL, EARTH ROTATING, AND PERTURB WIND (INER FRAME)
            REAL*8, DIMENSION(3) :: rVWI
            REAL*8, DIMENSION(3) :: rVWEI
            REAL*8, DIMENSION(3) :: rVWPI

! ATMOS PARTICLE: TOTAL, EARTH ROTATING, AND PERTURB WIND (PLANET FRAME)
            REAL*8, DIMENSION(3) :: rVWEP
            REAL*8, DIMENSION(3) :: rVWPP

! OBJECT RADIUS VECTOR MAG AND SPHERICAL ALT
            REAL*8 :: rRMAG
            REAL*8 :: rALT

! MACH NUMBER OF TOSS OBJECT
            REAL*8 :: rDMACH

! OBJECT PLANET LONGITUDE, AND LATITUDE (RAD)
            REAL*8 :: rDMUE
            REAL*8 :: rDLAE

! OBJECT OBLATE ALT, LAT(DEG), AND RADIUS VECTOR MAG
            REAL*8 :: rALTOBL
            REAL*8 :: rGEOLAT
            REAL*8 :: rRGEOD

! PREVIOUS VALUES OF INTEGRATED STATE VARIABLE DERIVATIVES
            REAL*8, DIMENSION(3) :: rOMBDP
            REAL*8, DIMENSION(3) :: rRBIDP
            REAL*8, DIMENSION(3) :: rRBIDDP

            REAL*8, DIMENSION(3,3) :: rGBIDP

! REFERENCE DIRCOS MATRIX FOR DATA RETENTION BY ROUTINE TOSCNA.F AND TOSCNAP.F
             REAL*8, DIMENSION(3,3) :: rGBOHOL


! GENERAL AERO TYPE DATA FOR GENERIC TOSS OBJECT AERODYNAMICS
! NOTE: WHETHER THESE ARE MEANINGFUL DEPENDS ON THE ACTUAL MODELS
            REAL*8 :: rCA_OBJ
            REAL*8 :: rCN_OBJ
            REAL*8 :: rCM_OBJ
            REAL*8 :: rFAMAG_OBJ
            REAL*8 :: rFNMAG_OBJ
            REAL*8 :: rACMAG_OBJ
            REAL*8 :: rALP_OBJ
            REAL*8 :: rCNQ_OBJ
            REAL*8 :: rCMQ_OBJ
            REAL*8 :: rALP_DYN
            REAL*8 :: rFNQMAG_OBJ
            REAL*8 :: rACQMAG_OBJ
            REAL*8 :: rAERO_RATE

!------------------------------------------------------------------
! FOR ORB MANEUVER CONTROL PROGS: TOSCNO, TOSCNVT, TOSCNHT, TOSCNTT
!------------------------------------------------------------------
! FOR TOSCNO
            REAL*8 :: rOVT_PREV_H
            REAL*8 :: rOVT_FLG_ALT

! FOR TOSCNVT
            REAL*8 :: rALCRVP
            REAL*8 :: rACRVFLG
            REAL*8 :: rALT_TIM_CNTR
            REAL*8 :: rALT_TAR
            REAL*8 :: rALT_EQUI
            REAL*8 :: rALT_EQUI_P
            REAL*8 :: rALT_SOLN_P
            REAL*8 :: rALT_AT_TRAP
            REAL*8 :: rALT_LATCH
            REAL*8 :: rTEN_TIM_CNTR
            REAL*8 :: rPREV_AVG_TEN
            REAL*8 :: rTENS_T_GRAD

            REAL*8, DIMENSION(50) :: rTEN_FIL6

! FOR TOSCNVZ
            REAL*8 :: rHDLATCH

! FOR TOSCNHT
            REAL*8 :: rOPT10FAC



! BOUYANCY FORCE AND MOMENT EFFECTS ON OBJECT
!--------------------------------------------
! TOTAL BUOYANT FORCE AND MOMENT ON OBJECT
            REAL*8, DIMENSION(3) :: rFBYB
            REAL*8, DIMENSION(3) :: rGBYB


!-----------------------------------------
! STORAGE FOR TOSCNAP (OBJECT CNTRL OPT 7)
!-----------------------------------------
! AOA MODE SWITCH LATCHING
            REAL*8 :: rAOA_LATCH
            REAL*8 :: rTO7_LATCH
            REAL*8 :: rAOA_TRANS

! OMB RATE DAMPING MODE SWITCH LATCHING
            REAL*8 :: rOMB_LATCH
            REAL*8 :: rTOMB7_LATCH
            REAL*8 :: rOMB1_TRANS
            REAL*8 :: rOMB2_TRANS
            REAL*8 :: rOMB3_TRANS


!-----------------------------------------
! STORAGE FOR TOSCNSK1 (OBJECT CNTRL OPT 9)
!-----------------------------------------
            REAL*8 :: rC9_XY_TOGGLE
            REAL*8 :: rC9_Z_TOGGLE
            REAL*8 :: rC9_X_HOLD_VAL
            REAL*8 :: rC9_Y_HOLD_VAL
            REAL*8 :: rC9_Z_HOLD_VAL
            REAL*8 :: rC9_CMD_RATE_P
            REAL*8, DIMENSION(10) :: rC9_DPR_BIAS
            REAL*8, DIMENSION(10) :: rC9_FILTER_TENS

!-------------------
! FORCES AND MOMENTS
!-------------------
! ACCEL DUE TO GRAVITY AT OBJECT
            REAL*8, DIMENSION(3) :: rAGI

! TOTAL OF ALL FORCES ON OBJECT, INER AND BODY FRAME
            REAL*8, DIMENSION(3) :: rFI
            REAL*8, DIMENSION(3) :: rFB

! TOTAL AERO FORCE ON OBJECT
            REAL*8, DIMENSION(3) :: rFDB

! TOTAL CONTROL FORCE ON OBJECT
            REAL*8, DIMENSION(3) :: rFCB

! TOTAL FORCE ON OBJECT DUE TO ALL ATTACH POINTS
            REAL*8, DIMENSION(3) :: rFTB

! TOTAL MOMENT ON OBJECT WR/T CG
            REAL*8, DIMENSION(3) :: rGB

! TOTAL MOMENT WR/T CG DUE TO AERODYNAMICS
            REAL*8, DIMENSION(3) :: rGDB

! TOTAL MOMENT WR/T CG DUE TO CONTROL SYSTEM
            REAL*8, DIMENSION(3) :: rGCB

! TOTAL MOMENT WR/T CG DUE TO ALL ATTACH POINTS
            REAL*8, DIMENSION(3) :: rGTB

! COUPLE DUE TO GRAVITY GRADIENT ON 6 DOF OBJECT
            REAL*8, DIMENSION(3) :: rGGB

! SUM OF ATTACH POINT COUPLES IN BODY FRAME
            REAL*8, DIMENSION(3) :: rSCTB

! AERODYNAMIC COUPLE (ASSOCIATED W/ AERO FORCE @ AERO REF PT)
            REAL*8, DIMENSION(3) :: rCDB

! COUPLE DUE TO EXTERNAL EFFECTS ON TOSS OBJECT
            REAL*8, DIMENSION(3) :: rCEEB

! FORCE DUE TO EXTERNAL EFFECTS ON TOSS OBJECT
            REAL*8, DIMENSION(3) :: rFEEB


!-----------------------------------
! VARIABLES OF INTERPRETATION + MISC
!-----------------------------------
! OBJECT POS, VEL, ACC WR/T RP: EXPRESSED IN RP ARBITRARY FRAME
            REAL*8, DIMENSION(3) :: rRBAR
            REAL*8, DIMENSION(3) :: rVBAR
            REAL*8, DIMENSION(3) :: rABAR

! OBJECT POS, VEL, ACC WR/T RP: EXPRESSED IN RP ORBITAL FRAME
            REAL*8, DIMENSION(3) :: rRBOR
            REAL*8, DIMENSION(3) :: rRBORD
            REAL*8, DIMENSION(3) :: rRBORDD

! RANGE, AZIMUTH, ELEV (AND RATES), ETC
            REAL*8 :: rRANGE
            REAL*8 :: rRANGED

! TRANSFORMATION FROM OBJECT ORB FRAME TO INERTIAL
            REAL*8, DIMENSION(3,3) :: rGOI

! OBJECT POS WR/T INER PT: EXPRESSED IN PLANET FIXED FRAME
            REAL*8, DIMENSION(3) :: rRIP

! OBJECT RIGID BODY ANGULAR MOMENTUM IN INERTIAL FRAME
            REAL*8, DIMENSION(3) :: rHMOMI


!--------------------------------------------------------------
! VARIABLES ASSOCIATED WITH THE SKIP ROPE DAMPER IMPLEMENTATION
!--------------------------------------------------------------
! DIRECTION COSINES BETWEEN ASSOCIATED ATT PT FRAME AND INER FRAME
            REAL*8, DIMENSION(3,3) :: rGSDAI

! FRICTION GRADIENT AND NORMAL FORCE ON CENTER-PIECE
            REAL*8 :: rDLFRIC
            REAL*8 :: rFRICNM

! TOTAL LOAD IN EACH PSD DAMPER LINK
            REAL*8, DIMENSION(6) :: rARMLOD

! DIST FROM DEPLOY ORIFICE TO CP DURING DAMPER QUIESCENT STATE OPS
            REAL*8 :: rACPLEN

! BODY TEMPERATURE
!-----------------
! (NOTE, LINKAGE BETWEEN OBJECT TEMP AND TETHER THERMAL IS NOT ACTIVE)
            REAL*8 :: rBTEMPT








!************************************************************************
!************************************************************************
!************************************************************************
!************************************************************************
! BELOW IN OBJECT DATA STRUCTURE IS USER-INPUT CONSTANTS DEFINING OBJECT
!************************************************************************
!************************************************************************
!************************************************************************
!************************************************************************
! IMPORTANT NOTE: THE DATA ITEMS BELOW MUST BE KEPT SYNCHRONIZED AND
!                 CONSISTENT WITH THE OBJI.i OBJECT INPUT ITEM-MAPPING
!                 EQUIVALENCE STATEMENTS. SO IF YOU ADD A NEW OBJECT
!                 INPUT DATA CONSTANT IT MUST BE RECOGNIZED IN TWO PLACES.

! INTEGER INPUT DATA DEFINING A TOSS OBJECT
            INTEGER :: iNATPAT
            INTEGER :: iLAERO 
            INTEGER :: iLCONT 
            INTEGER :: iLICTRN
            INTEGER :: iLICROT
            INTEGER :: iLICSPL
            INTEGER :: iLNOMOM
            INTEGER :: iLEULER
            INTEGER :: iLGRVON
            INTEGER :: iLGGTON
            INTEGER :: iLIBTYP
            INTEGER :: iLICOMT
            INTEGER :: iLINERT

! THESE ITEMS ARE ASSOCIATED WITH THE SKIP ROPE DAMPER FEATURE
!-------------------------------------------------------------
! ATTACH PT AND TOSS OBJECT NUMBER TO WHICH DAMPER BASE IS INSTALLED
            INTEGER :: iLOBJSR
            INTEGER :: iLAPPSR
! NUMBER OF ANCHOR POINTS FOR DAMPER LINK ARMS
            INTEGER :: iLNCHOR
! TOSS TETHER CONNECTING DAMPER CENTER-PIECE TO DAMPER BASE ATTACH PT
            INTEGER :: iLTHBAS
! TOSS TETHER WHOSE SKIP ROPE MOTION IS TO BE DAMPED
            INTEGER :: iLTHDMP
! TYPE OF INTEGRATOR TO BE USED FOR TOSS OBJECT CENTERPIECE (1=EULER)
            INTEGER :: iLSRDIN

! OBJ AND AP WR/T WHICH CONTROL OPT 4 PROVIDES INLINE-THRUST/LIB CONTROL
!-----------------------------------------------------------------------
            INTEGER :: iLREFOB
            INTEGER :: iLREFAT
            INTEGER :: iLAPLIB
! TOSS TETHER NUMBER WHICH CONTROL OPT 4 USES TO ACTIVATE INLINE-THRUST
            INTEGER :: iLTTNNL


! ATTITUDE CONTROL FRAME-REF SPECIFIER FOR CONTROL OPT 5
!-------------------------------------------------------
            INTEGER :: iL05TYP

! FLAG TO ACTIVE THIS OBJECTS MASS AS A GRAVITY BODY TO OTHERS
!-------------------------------------------------------------
            INTEGER :: iLGRAVBOD

! ATTITUDE CONTROL FRAME-REF SPECIFIER FOR CONTROL OPT 7
!-------------------------------------------------------
            INTEGER :: iL07TYP


!---------------------------------------
! MASS PROPERTIES, KINEMATICS, STATE ICS
!---------------------------------------
! MASS PROPERTIES AND GEOMETRY
            REAL*8 :: rFMASSO
            REAL*8 :: rFIXXO 
            REAL*8 :: rFIYYO 
            REAL*8 :: rFIZZO 
            REAL*8 :: rFIXYO 
            REAL*8 :: rFIXZO 
            REAL*8 :: rFIYZO 

! COORDINATES OF CG WR/T BODY REF POINT
            REAL*8, DIMENSION(3) :: rPCGBO

! ATTACH POINT POSITIONS WR/T BODY GEOMETRY REF PT
            REAL*8, DIMENSION(16) :: rPXTB
            REAL*8, DIMENSION(16) :: rPYTB
            REAL*8, DIMENSION(16) :: rPZTB

! AERO REFERENCE POINT
            REAL*8, DIMENSION(3) :: rPDPB

! INITIAL CONDITIONS ON TRANSLATION POSITION STATE
            REAL*8, DIMENSION(3) :: rPOSO

! INITIAL CONDITIONS ON TRANSLATION RATE STATE
            REAL*8, DIMENSION(3) :: rRATEO

! INITIAL CONDITONS ON BODY ANGULAR VELOCITY STATE
            REAL*8, DIMENSION(3) :: rOMBO

! INITIAL EULER ANGLES (FOR ATTITUDE INITIALIZATION)
            REAL*8 :: rPITCHO
            REAL*8 :: rROLLO 
            REAL*8 :: rYAWO  

! INITIAL CONDITIONS ON DIRECTION COSINE STATE
            REAL*8, DIMENSION(3,3) :: rGBIO


!----------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 2
!----------------------------------------------
! ISP VALUE FOR DETERMINING MASS LOSS DUE TO THRUST
            REAL*8 :: rFCNISP

! A CONSTANT COUPLE
            REAL*8 :: rGXCO 
            REAL*8 :: rGYCO 
            REAL*8 :: rGZCO 

! A SEQUENCE OF TIMED, CONSTANT FORCE PERIODS
            REAL*8 :: rCFT1          
            REAL*8 :: rCFORX1  
            REAL*8 :: rCFORY1  
            REAL*8 :: rCFORZ1  
            REAL*8 :: rCFT2          
            REAL*8 :: rCFORX2  
            REAL*8 :: rCFORY2  
            REAL*8 :: rCFORZ2  
            REAL*8 :: rCFT3          
            REAL*8 :: rCFORX3  
            REAL*8 :: rCFORY3  
            REAL*8 :: rCFORZ3  
            REAL*8 :: rCFT4          
            REAL*8 :: rCFORX4  
            REAL*8 :: rCFORY4  
            REAL*8 :: rCFORZ4  
            REAL*8 :: rCFT5          
            REAL*8 :: rCFORX5  
            REAL*8 :: rCFORY5  
            REAL*8 :: rCFORZ5  
            REAL*8 :: rCFT6          
            REAL*8 :: rCFORX6  
            REAL*8 :: rCFORY6  
            REAL*8 :: rCFORZ6  
            REAL*8 :: rCFT7          
            REAL*8 :: rCFORX7  
            REAL*8 :: rCFORY7  
            REAL*8 :: rCFORZ7  
            REAL*8 :: rCFT8          

! A SINUSOIDAL PSUEDO-CONTROL FORCE
! (SPECIFIED AS DISPLACEMENT HALF-AMPL AND FREQ-CPS)
            REAL*8 :: rCOSTRT  
            REAL*8 :: rCDELX   
            REAL*8 :: rCFREQX  
            REAL*8 :: rCDELY   
            REAL*8 :: rCFREQY  
            REAL*8 :: rCDELZ   
            REAL*8 :: rCFREQZ  
            REAL*8 :: rCOSTOP  



! CONSTANT DRAG COEFFICIENT DEFAULT AERO OPTION DATA
!---------------------------------------------------
            REAL*8 :: rAAERO   
            REAL*8 :: rCDRGO   


! SECRET FORCE WHICH CAN BE GENERATED ONLY BETWEEN ATT POINTS 1 ON
! REF PT AND TOSS OBJECT 2 (ACTIVATED WITH NON-ZERO VALUE 'SECRET')
            REAL*8 :: rSECRET   



!------------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 3
! (PROVIDES BOOM TIP SKIP ROPE DAMPER CAPABILITY)
!------------------------------------------------
! DAMPER ARM ANCHOR POINT COORDS WR/T ATTACH PT (IN AP FRAME)
            REAL*8, DIMENSION(6) :: rXASRA
            REAL*8, DIMENSION(6) :: rYASRA
            REAL*8, DIMENSION(6) :: rZASRA

! REFERENCE LENGTH OF DAMPER ARM (FOR SPRING CONSTANT CALCS)
            REAL*8, DIMENSION(6) :: rREFSRL

! PRELOAD (ASSOCIATED WITH POSITIVE LINK EXTENSION RATE)
            REAL*8, DIMENSION(6) :: rSRPL

! SPRING CONSTANT (ASSOCIATED WITH POSITIVE LINK EXTENSION RATE)
            REAL*8, DIMENSION(6) :: rSRK

! DAMPING CONSTANT (ASSOCIATED WITH POSITIVE LINK EXTENSION RATE)
            REAL*8, DIMENSION(6) :: rSRKD

! PRELOAD HYSTERESIS FACTOR (ASSOCIATED WITH NEG LINK EXTENSION RATE)
            REAL*8, DIMENSION(6) :: rHYSRPL

! SPRING HYSTERESIS FACTOR (ASSOCIATED WITH NEG LINK EXTENSION RATE)
            REAL*8, DIMENSION(6) :: rHYSRK

! DAMPING HYSTERESIS FACTOR (ASSOCIATED WITH LINK ARM EXTENSION RATE)
            REAL*8, DIMENSION(6) :: rHYSRKD

! COEFF OF FRICTION BETWEEN TETHER AND DAMPER CENTER-PIECE
            REAL*8 :: rCOEFMU  

! EXTENSION RATE AT WHICH FULL HYSTERESIS ONSET WILL HAVE OCCURED
            REAL*8 :: rHYSONS  

! CENTERPIECE/TETHER REL RATE AT WHICH FULL FRIC ONSET WILL HAVE OCCURED
            REAL*8 :: rFRCONS  


!-------------------------------------------------------------
! INITIAL VALUES FOR TRANSL IC OPT VIA LIBRATION SPECIFICATION
!-------------------------------------------------------------
! INITIAL LIBRATION IN-PLANE AND OUT-OF-PLANE ANGLES AND RATES
            REAL*8 :: rAIPLO
            REAL*8 :: rAOPLO
            REAL*8 :: rAIPLDO
            REAL*8 :: rAOPLDO

! INITIAL RANGE AND RANGE RATE
            REAL*8 :: rARNGO
            REAL*8 :: rARNGDO


!----------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 4
!----------------------------------------------
! OPERATIONAL LIBRATION ANGLE AND DEADBAND SPECIFICATION
            REAL*8 :: rDBLIBD 
            REAL*8 :: rOPLIBD 

! DATA INPUT, INLINE THRUST LEVEL AND ON-OFF TIME SPECIFICATION
            REAL*8 :: rFINLIN 
            REAL*8 :: rFINON  
            REAL*8 :: rFINOFF 

! DATA INPUT, LENGTH OF TETHER, INSIDE OF WHICH, IN-LINE THRUST WILL BE TURNED ON
            REAL*8 :: rFLONOF 


! INPUT ITEM RELATED TO THERMAL TRANSFER AT OBJECTS
!--------------------------------------------------
! INITIAL BODY TEMPERATURE <---- OUT OF NUMERICAL ORDER
! (NOTE, LINAKAGE BETWEEN OBJECT TEMP AND TETHER THERMAL IS NOT ACTIVE)
            REAL*8 :: rBTEMPO 


!----------------------------------------------------------
! DATA FOR TOSS OBJECT FORCE CONTROL OPTION = 4 (CONTINUED)
!----------------------------------------------------------
! DATA INPUT, START/STOP TIMES FOR LIBRATION CONTROL
            REAL*8 :: rTLIBON 
            REAL*8 :: rTLIBOF 


!-------------------------------------------------
! DATA FOR TOSS OBJECT ATTITUDE CONTROL OPTION = 5
!-------------------------------------------------
! DATA INPUT, ATT ERROR-RATE GAINS
            REAL*8 :: rGOREX 
            REAL*8 :: rGOREY 
            REAL*8 :: rGOREZ 
! DATA INPUT, ATT ERROR GAINS
            REAL*8 :: rGOAEX 
            REAL*8 :: rGOAEY 
            REAL*8 :: rGOAEZ 


!-------------------------------------------------
! DATA FOR TOSS OBJECT CONTROL (TOSCNO) OPTION = 6
!-------------------------------------------------
! DATA INPUT, GENERAL DATA FOR ALL OPT 6 OPERATIONS
            REAL*8 :: rORBTISP  

! DATA INPUT, DATA RELATED TO ORBITAL VERTICAL-THRUST APPLICATION
            REAL*8 :: rOVT_ENABL

            REAL*8 :: rOVT_T_ON 
            REAL*8 :: rOVT_T_OFF
            REAL*8 :: rOVT_H_OFF

            REAL*8 :: rOVT_DB   

            REAL*8 :: rOVT_CONTVA

            REAL*8 :: rOVT_OBJ  
            REAL*8 :: rOVT_FAC  

            REAL*8 :: rOVT_TZERO
            REAL*8 :: rOVT_HZERO
            REAL*8 :: rOVT_THRUS

! DATA INPUT, DATA RELATED TO ORBITAL HORIZONTAL-THRUST APPLICATION
            REAL*8 :: rOHT_ENABL  

            REAL*8 :: rOHT_T_ON   
            REAL*8 :: rOHT_T_OFF  

            REAL*8 :: rOHT_CONTVA 
            REAL*8 :: rOHT_CONAZ  
            REAL*8 :: rOHT_CONLAT 
            REAL*8 :: rOHT_CONLON 

            REAL*8 :: rOHT_LL_DB  
            REAL*8 :: rOHT_VEL_DB 

            REAL*8 :: rOHT_THRUS  

            REAL*8 :: rOHT_T_LLDZ 
            REAL*8 :: rOHT_H_LLDZ 

! DATA INPUT, DATA RELATED TO ORBITAL TENSION-ALIGNED THRUST APPLICATION
            REAL*8 :: rOTT_ENABL

            REAL*8 :: rOTT_T_ON
            REAL*8 :: rOTT_T_OFF

            REAL*8 :: rOTT_FAC 

! DATA INPUT, TABLE FOR VERTICAL PROFILE (EITHER VELOCITY OR THRUST)
            REAL*8, DIMENSION(27) :: rOVTVATAB

            REAL*8, DIMENSION(21) :: rOHTVTAB
            REAL*8, DIMENSION(21) :: rOHAZTAB

! DATA INPUT, GENERAL ERROR AND RATE GAINS ASSOCIATED WITH VERT CONTROL
            REAL*8 :: rGAIN_VERR  
            REAL*8 :: rGAIN_VERRD 

! DATA INPUT, GENERAL ERROR AND RATE GAINS ASSOCIATED WITH HORIZ CONTROL
            REAL*8 :: rGAIN_HERR  
            REAL*8 :: rGAIN_HERRD 

! DATA INPUT, TIME INTERVAL TO ASSESS TENSION FOR VERTICAL CONTROL
            REAL*8 :: rALT_T_INTVL
            REAL*8 :: rTENF_TTSPAN
            REAL*8 :: rTENF_NSAMPL
            REAL*8 :: rALT_ALT_FLG
            REAL*8 :: rTEN_FILTER
            REAL*8 :: rALT_FILTER

!------------------------------------------------
! DATA INPUT, TO DEFINE BUOYANCY FOR TOSS OBEJCTS
!------------------------------------------------
! NOTE: THIS IS A WORK IN PROGRESS, SO CURRENTLY, A FIXED CONTAINER SIZE
!             IS ASSUMED, EG. ENVELOP EXPANSION EFFECTS ARE NOT INCLUDED
!             (IT IS ALSO ASSUMED THAT THE USER HAS INCLUDED IN THE TOSS OBJECT
!              MASS ACCOUNTING, THE MASS OF ANY CHOSEN GASEOUS "FILL")

            REAL*8 :: rOBVOL_MAX

! DATA INPUT, CENTER OF BUOYANCY REFERENCE POINT
            REAL*8, DIMENSION(3) :: rPBOYB


!-------------------------------------------------------------
! DATA INPUT, DATA FOR TOSS OBJECT ATTITUDE CONTROL OPTION = 7
!-------------------------------------------------------------
! ATT CONTROL ERROR-RATE GAINS
            REAL*8 :: rGOREX7
            REAL*8 :: rGOREY7
            REAL*8 :: rGOREZ7
! ATT CONTROL ERROR GAINS
            REAL*8 :: rGOAEX7
            REAL*8 :: rGOAEY7
            REAL*8 :: rGOAEZ7

! DATA INPUT, AOA VALUE, GREATER THAN WHICH, AOA CONTROL STARTS
            REAL*8 :: rAOA_MX7

! DATA INPUT, TIME TO START CONTROL OPTION
            REAL*8 :: rTSTR_CO7

! DATA INPUT, AOA CONTROL ERROR AND RATE GAINS
            REAL*8 :: rAOAERR7
            REAL*8 :: rAOARAT7

! DATA INPUT, DELTA-TIME TO TRANSITION TO FINAL COMMANDED AOA OF 180 DEG
            REAL*8 :: rDELT7_AOA

! DATA INPUT, BODY RATE .LT. WHICH AOA TRANSITION IS ALLOWED TO PROCEED
            REAL*8 :: rTST_OMD7

! DATA INPUT, DELTA-TIME TO DRIVE OMB TO ZERO
            REAL*8 :: rDELT7_OMB



!------------------------------------------------
! DATA INPUT, DATA FOR TOSCNTH CONTROL OPTION = 8
!------------------------------------------------
! DATA INPUT, HERE IS SOME SIMPLE THRUST DATA
            REAL*8 :: rCISP3  
            REAL*8 :: rTTIMON3
            REAL*8 :: rTTIMOFF3
            REAL*8 :: rVTHRUS3
            REAL*8 :: rP_AREA3



!-------------------------------------------------
! DATA FOR TOSS OBJECT ATTITUDE CONTROL OPTION = 9
!-------------------------------------------------
! DEFINE NUMBER OF TETHERS INVOKED TO CONTROL THIS OBJECT
            REAL*8 :: rC9_TTCNT
      
! DEFINE WHETHER SHEAVE RATES ARE CONSIDERED IN CONTROL CALCS
            REAL*8 :: rC9_SHVONOF
      
! DEFINE RATE-HOLD CONTROL FEEDBACK GAINS
            REAL*8 :: rC9_RTH_KERR
            REAL*8 :: rC9_RTH_KRAT
      
! DEFINE POSITION-HOLD CONTROL FEEDBACK GAINS
            REAL*8 :: rC9_PSH_KERR
            REAL*8 :: rC9_PSH_KRAT

! DEFINE LIBRATION CONTROL CONTROL FEEDBACK GAINS & UTIL FACTOR
            REAL*8 :: rC9_PSH_KLRAT
            REAL*8 :: rC9_PSH_KLACC
            REAL*8 :: rC9_PSH_KFACT
            REAL*8 :: rC9_LHPOS_DB
            REAL*8 :: rC9_LHVEL_DB

! DEFINE USER-SPECIFIED TENSION ERROR GAINS FOR THE TENSION CONTROLLER
            REAL*8 :: rC9_TERR_GAIN
            REAL*8 :: rC9_TERRR_GAIN
            REAL*8 :: rC9_TERRI_GAIN
            REAL*8 :: rC9_LBW_FLG

! DEFINE USER-SPECIFIED DEBUG FLAG
            REAL*8 :: rC9_DEBUG_FLG
            REAL*8 :: rC9_DEBUG_TTN1
            REAL*8 :: rC9_DEBUG_TTN2
            REAL*8 :: rC9_DEBUG_DT

! DEFINE TIME TO START/STOP CONTROLLER
            REAL*8 :: rC9_TSTART
            REAL*8 :: rC9_TSTOP

! DEFINE TOSS TETHER NUMBERS DELEGATED TO CONTROLLING OBJECT
            REAL*8, DIMENSION(10) :: rC9_TT
      
! DEFINE MAXIMUM ALLOWED TENSION FOR TETHER
            REAL*8, DIMENSION(10) :: rC9_TMAX
      
! DEFINE MINIMUM ALLOWED TENSION FOR TETHER
            REAL*8, DIMENSION(10) :: rC9_TMIN
            
! DEFINE MAXIMUM ALLOWED DEPLOY RATE FOR TETHER
            REAL*8, DIMENSION(10) :: rC9_LDMAX
      
! DEFINE MAXIMUM ALLOWED DEPLOY ACCELERATION FOR TETHER
            REAL*8, DIMENSION(10) :: rC9_LDDMAX

! TETHER STATE DATA RETENTION (PERSISTENCE) ALLOCATIONS
            REAL*8, DIMENSION(10) :: rC9_DEP_LEN
            REAL*8, DIMENSION(10) :: rC9_DEP_LEND
            REAL*8, DIMENSION(10) :: rC9_DEP_LENDP
            REAL*8, DIMENSION(10) :: rC9_DEP_LENDD
            REAL*8, DIMENSION(10) :: rC9_TEN_PREV
            
! DEFINE XD (NORTH), YD (EAST), AND ALT-RATE (UP) COMMAND TABLES VS TIME
            REAL*8, DIMENSION(30) :: rC9_XD_TAB
            REAL*8, DIMENSION(30) :: rC9_YD_TAB
            REAL*8, DIMENSION(30) :: rC9_HD_TAB

! DEFINE MINIMUM TETHER TENSION VS TETHER LENGTH
            REAL*8, DIMENSION(20) :: rC9_TSAG_TAB

! DEFINE USER-PREEMPT OF RATE COMMANDS VIA CONSTANT VALUES
            REAL*8 :: rC9_XDCON_CMD
            REAL*8 :: rC9_YDCON_CMD
            REAL*8 :: rC9_HDCON_CMD

! DEFINE USER-SPECIFIED GO-TO COMMANDS VIA CONSTANT VALUES
            REAL*8 :: rC9_XCON_CMD
            REAL*8 :: rC9_YCON_CMD
            REAL*8 :: rC9_HCON_CMD

! DEFINE USER-SPECIFIED MULTIPLIER-FACTORS ON TABLE LOOKUP COMMANDS
            REAL*8 :: rC9_XDFAC
            REAL*8 :: rC9_YDFAC
            REAL*8 :: rC9_HDFAC
            REAL*8 :: rC9_TSAGFAC

! DEFINE MASS-LOADING EVENT 1
            REAL*8 :: rC9_TIME_MSEV_1
            REAL*8 :: rC9_DELT_MSEV_1
            REAL*8 :: rC9_MASS_MSEV_1
      
! DEFINE MASS-LOADING EVENT 2
            REAL*8 :: rC9_TIME_MSEV_2
            REAL*8 :: rC9_DELT_MSEV_2
            REAL*8 :: rC9_MASS_MSEV_2
      
! DEFINE MASS-LOADING EVENT 3
            REAL*8 :: rC9_TIME_MSEV_3
            REAL*8 :: rC9_DELT_MSEV_3
            REAL*8 :: rC9_MASS_MSEV_3

! DEFINE OBJECT TO WHICH MASS-LOADING IS APPLIED
            REAL*8 :: rC9_MSEV_OBJ

! DEFINE DATA RELATED TO TENSION AVERAGING FILTER
            REAL*8 :: rC9_TENF_TTSPAN
            REAL*8 :: rC9_TENF_NSAMPL
            REAL*8 :: rC9_TEN_TIM_CNTR
            REAL*8, DIMENSION(10) :: rC9_PREV_AVG_TEN
            REAL*8, DIMENSION(50,10) :: rC9_TEN_AFIL

! DEFINE DATA RELATED TO SAG CONTROL LIMITS AND DEADBANDS
            REAL*8 :: rC9_SAG_LIMIT_FRAC
            REAL*8 :: rC9_SAG_LIMIT_MIN
            REAL*8 :: rC9_SAG_DB_FRAC
            REAL*8 :: rC9_SAG_DB_VALUE

! DEFINE VARIOUS CONTROL DISPLAY ENNUNCIATOR TOGGLES
            REAL*8 :: rALT_TOGGLE_50
            REAL*8 :: rALT_TOGGLE_0
            REAL*8 :: rALT_TOGGLE_HDCMD
            REAL*8 :: rCMASS_EVNT_TOGGLE_1
            REAL*8 :: rCMASS_EVNT_TOGGLE_2
            REAL*8 :: rCMASS_EVNT_TOGGLE_3


         END TYPE OBJECT_SOLN_STATE

      END MODULE TOSS_OBJECT_DATA_STRUCTURE

