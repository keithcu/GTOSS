! %Z%GTOSS %M% H.10 code v01.01
!              H.10 code v01.01  Changed RE to REMDUM
!-----------------------------------------------------------
!              H.7 code v01.00  (baseline for vH.7 delivery)
!-----------------------------------------------------------
! THIS COMMON BLOCK IS A PART OF THE JACCHIA ATMOSPHERE
! IMPLEMENTATION AND IS NOT A STANDARD PART OF TOSS
      COMMON / FATMO3 / AA(6)
      COMMON / FATMO3 / ALPHAA
      COMMON / FATMO3 / ALPHV
      COMMON / FATMO3 / BETAA
      COMMON / FATMO3 / BK
      COMMON / FATMO3 / BM(6)
      COMMON / FATMO3 / CFRHO
      COMMON / FATMO3 / CON1(5)
      COMMON / FATMO3 / CON2(5)
      COMMON / FATMO3 / CON3(5)
      COMMON / FATMO3 / CON4(5)
      COMMON / FATMO3 / CPCV
      COMMON / FATMO3 / DATES(40)
      COMMON / FATMO3 / DAZE
      COMMON / FATMO3 / DELTAA
      COMMON / FATMO3 / DELV
      COMMON / FATMO3 / DJ1970
      COMMON / FATMO3 / F107
      COMMON / FATMO3 / F107B
      COMMON / FATMO3 / F107BAR(40)
      COMMON / FATMO3 / FM
      COMMON / FATMO3 / FN
      COMMON / FATMO3 / GAM
      COMMON / FATMO3 / GB
      COMMON / FATMO3 / GKP
      COMMON / FATMO3 / GKPB
      COMMON / FATMO3 / GKPBAR(40)
      COMMON / FATMO3 / HO
      COMMON / FATMO3 / P
      COMMON / FATMO3 / REMDUM
      COMMON / FATMO3 / TZO
      COMMON / FATMO3 / UNGC
      COMMON / FATMO3 / WTM
      COMMON / FATMO3 / ZO

      COMMON / XATMO3 / ATMO3X(1024)
