! COMDECK COM_ALL
! %Z%GTOSS %M% H.9 code v01.04
!              H.9 code v01.04 Increased precision on PIIALL
!------------------------------------------------------------
!              H.7 code v01.03 Added reference date and time
!-----------------------------------------------------------------
!              H.5 code v01.02 Added logical sim run flag PROCED
!                              Changed labeled common names
!                              Added universal real data constants
!-----------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!********************************************************
!********************************************************
!
!       DECLARE COMMON VISIBLE TO ALL TOSS CODE
!
!********************************************************
!********************************************************

!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@@=@=@=@=@=@=@=@
! DECLARE DEFAULT REAL VARIABLE PRECISION TO BE USED THROUGHOUT (G)TOSS
!@NOS      IMPLICIT REAL (A-H, O-Z)
      IMPLICIT REAL*8 (A-H, O-Z)
!@VAX      IMPLICIT REAL*8 (A-H, O-Z)
!@DOS      IMPLICIT REAL*8 (A-H, O-Z)

!@MAC      IMPLICIT REAL*8 (A-H, O-Z)
!=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@=@@=@=@=@=@=@=@=@

! DEFINE UNIVERSAL REAL DATA CONSTANTS
!-------------------------------------
      PARAMETER ( PIIALL = 3.14159265358979323846 )
      PARAMETER ( RTDALL = 180./PIIALL       )
      PARAMETER ( DTRALL = 1./RTDALL         )

! STATE SIZE OF UNIVERSAL ARRAYS
      PARAMETER ( NJTOSS = 25 )
      PARAMETER ( NLLOSS =  5 )

! STORAGE FOR UNITY AND NULL VECTORS AND MATRICES
      COMMON /ALLCMR/ TVXNUL(3), TVXUNT(3), TMXNUL(3,3), TMXUNT(3,3)
      COMMON /ALLCMR/ TVXUP1(3), TVXUP2(3), TVXUP3(3)

! STORAGE FOR UNIVERSAL CONTROL FLAGS
      COMMON /ALLCMI/ JTOSS(NJTOSS)

! STORAGE FOR UNIVERSAL LOGICAL FLAGS
      COMMON /ALLCML/ ALLOG(NLLOSS)
      LOGICAL ALLOG

!=======================================================================
! NOTE:
!
!       HOST MUST PROVIDE VALUES FOR ITEMS SHOWN AS.......(REF PT->TOSS)
!
!       TOSS MUST PROVIDE VALUES FOR ITEMS SHOWN AS.......(TOSS->REF PT)
!
!       TOSS-OR-HOST MAY PROVIDE VALUES FOR ITEMS SHOWN AS...(ALL->HOST)
!=======================================================================

!***********************************************************************
!***********************************************************************
! GLOBAL INITIALIZATION PHASE FLAG                        (REF PT->TOSS)
! ( 1,2,3,... = INITIALIZATION PASS THAT IS IN PROGRESS)
! (        -1 = INITIALIZATION STAGE FINISHED   )
!***********************************************************************
!***********************************************************************
      EQUIVALENCE (JTOSS( 1),  ICSTAG )

!***********************************************************************
!***********************************************************************
! GLOBAL SOLUTION ADVANCEMENT FLAG                        (REF PT->TOSS)
! ( 1 =      INTEGRATE TO ADVANCE SOLN )
! (-1 = DONT INTEGRATE                 )
!***********************************************************************
!***********************************************************************
      EQUIVALENCE (JTOSS( 2),  JINTEG )


!***********************************************************************
!***********************************************************************
! REFERENCE DATE (CORRESPONDING TO ZERO SIM TIME)         (REF PT->TOSS)
!***********************************************************************
!***********************************************************************
      EQUIVALENCE (JTOSS( 3),  LRYEAR )
      EQUIVALENCE (JTOSS( 4),  LRMON  )
      EQUIVALENCE (JTOSS( 5),  LRDAY  )
      EQUIVALENCE (JTOSS( 6),  LRHOUR )
      EQUIVALENCE (JTOSS( 7),  LRMIN  )
      EQUIVALENCE (JTOSS( 8),  LRSEC  )


!***********************************************************************
!***********************************************************************
! GLOBAL SIMULATION TERMINATION LOGICAL FLAG                 (ALL->HOST)
! ( .TRUE.  = CONTINUE EXECUTION OF RUN)
! ( .FALSE. = TERMINATE THIS RUN)
!***********************************************************************
!***********************************************************************
      LOGICAL PROCED
      EQUIVALENCE (ALLOG(1),  PROCED   )


