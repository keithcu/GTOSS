! ROUTINE: YFM045
! %Z%GTOSS %M% H.10 code v01.10
!              H.10 code v01.10 Added total heat rate variable output
!--------------------------------------------------------------------
!              H.1  code v01.01 (baseline for vG.1 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM045 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! <BEAD MODEL TETHER>  THERMAL RESPONSE


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND 
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_COSS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.i"





!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! DEFINE COLUMN HEADINGS
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE COLUMN DATA
      IF(JFUNC .EQ. 2) GO TO 2000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM045-0'


!******************************************
!******************************************
!     WRITE COLUMN HEADER TO PLOT FILE
!******************************************
!******************************************
1000  CONTINUE

! CHOOSE STANDARD OR TAG FORMAT
!------------------------------
      IF(TAGFLG .EQ. 1.0) THEN

! DEFINE SPECIAL TIME HEADER
      WRITE(COLIF,1001) TIMID6, FSNID4, TAG4
1001  FORMAT(A6, A4, '_', A4)

! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = COLIF

      ECOLHD( 2) = 'SEGLAT_F_'//TAG4//'  '

      ECOLHD( 3) = 'LIT_NIT_'//TAG4//'   '
      ECOLHD( 4) = 'LIT_TERM_'//TAG4//'  '

      ECOLHD( 5) = 'QDEBBT_W_'//TAG4//'  '
      ECOLHD( 6) = 'QDEMTT_W_'//TAG4//'  '
      ECOLHD( 7) = 'QDSOLT_W_'//TAG4//'  '
      ECOLHD( 8) = 'QDAROT_W_'//TAG4//'  '
      ECOLHD( 9) = 'QDALBT_W_'//TAG4//'  '
      ECOLHD(10) = 'QDELCT_W_'//TAG4//'  '

      ECOLHD(11) = 'STEMPK_1_K_'//TAG4
      ECOLHD(12) = 'STEMPK_N_K_'//TAG4

      ECOLHD(13) = 'QDTSW_1_W_'//TAG4//' '
      ECOLHD(14) = 'QDTSW_S_W_'//TAG4//' '

      ECOLHD(15) = 'SEGLA_1_F_'//TAG4//' '
      ECOLHD(16) = 'SEGLA_S_F_'//TAG4//' '
      ECOLHD(17) = ' '//'TOTAL_QD_W'//TAG4

! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = COLIF

      MCOLHD( 2) = 'SEGLAT_M_'//TAG4//'  '
      MCOLHD( 3) = 'LITNIT_'//TAG4//'    '
      MCOLHD( 4) = 'LITERM_'//TAG4//'    '

      MCOLHD( 5) = 'QDEBBT_W_'//TAG4//'  '
      MCOLHD( 6) = 'QDEMTT_W_'//TAG4//'  '
      MCOLHD( 7) = 'QDSOLT_W_'//TAG4//'  '
      MCOLHD( 8) = 'QDAROT_W_'//TAG4//'  '
      MCOLHD( 9) = 'QDALBT_W_'//TAG4//'  '
      MCOLHD(10) = 'QDELCT_W_'//TAG4//'  '

      MCOLHD(11) = 'STEMPK_1_K_'//TAG4
      MCOLHD(12) = 'STEMPK_N_K_'//TAG4

      MCOLHD(13) = 'QDTSW_1_W_'//TAG4//' '
      MCOLHD(14) = 'QDTSW_S_W_'//TAG4//' '

      MCOLHD(15) = 'SEGLA_1_F_'//TAG4//' '
      MCOLHD(16) = 'SEGLA_S_F_'//TAG4//' '
      MCOLHD(17) = ' '//'TOTAL_QD_W'//TAG4

      ELSE

! DEFINE SPECIAL TIME HEADER
!---------------------------
      WRITE(COLIF,1002) TIMID6, FSNID4
1002  FORMAT(A6, A4, '     ')

! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = COLIF

      ECOLHD( 2) = 'SEGLAT_F'//FSNID4//'   '

      ECOLHD( 3) = 'LIT_NIT'//FSNID4//'    '
      ECOLHD( 4) = 'LIT_TERM'//FSNID4//'   '

      ECOLHD( 5) = 'QDEBBT_W'//FSNID4//'   '
      ECOLHD( 6) = 'QDEMTT_W'//FSNID4//'   '
      ECOLHD( 7) = 'QDSOLT_W'//FSNID4//'   '
      ECOLHD( 8) = 'QDAROT_W'//FSNID4//'   '
      ECOLHD( 9) = 'QDALBT_W'//FSNID4//'   '
      ECOLHD(10) = 'QDELCT_W'//FSNID4//'   '

      ECOLHD(11) = 'STEMPK_1_K'//FSNID4//' '
      ECOLHD(12) = 'STEMPK_S_K'//FSNID4//' '

      ECOLHD(13) = 'QDTSW_1_W'//FSNID4//'  '
      ECOLHD(14) = 'QDTSW_S_W'//FSNID4//'  '

      ECOLHD(15) = 'SEGLA_1_F'//FSNID4//'  '
      ECOLHD(16) = 'SEGLA_S_F'//FSNID4//'  '
      ECOLHD(17) = ' '//'TOTAL_QD_W'//FSNID4

! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = COLIF

      MCOLHD( 2) = 'SEGLAT_M'//FSNID4//'   '

      MCOLHD( 3) = 'LITNIT'//FSNID4//'     '
      MCOLHD( 4) = 'LITERM'//FSNID4//'     '

      MCOLHD( 5) = 'QDEBBT_W'//FSNID4//'   '
      MCOLHD( 6) = 'QDEMTT_W'//FSNID4//'   '
      MCOLHD( 7) = 'QDSOLT_W'//FSNID4//'   '
      MCOLHD( 8) = 'QDAROT_W'//FSNID4//'   '
      MCOLHD( 9) = 'QDALBT_W'//FSNID4//'   '
      MCOLHD(10) = 'QDELCT_W'//FSNID4//'   '

      MCOLHD(11) = 'STEMPK_1_K'//FSNID4//' '
      MCOLHD(12) = 'STEMPK_S_K'//FSNID4//' '

      MCOLHD(13) = 'QDTSW_1_W'//FSNID4//'  '
      MCOLHD(14) = 'QDTSW_S_W'//FSNID4//'  '

      MCOLHD(15) = 'SEGLA_1_M'//FSNID4//'  '
      MCOLHD(16) = 'SEGLA_S_M'//FSNID4//'  '
      MCOLHD(17) = ' '//'TOTAL_QD_W'//FSNID4

      END IF


! DEFINE NUMBER OF COLUMNS BEING DEFINED
      NUMCOL = 17
      RETURN



!******************************************
!******************************************
!     CALCULATE DATA FOR PLOT FILE
!******************************************
!******************************************
2000  CONTINUE

! IF NON-VALID FINIT SOLN # IS SPEC FOR THIS PAGE, GET OUT
      IF( (NFTSHO.LT.1) .OR. (NFTSHO.GT.NFINIT) ) GO TO 100

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
      CALL TISLDW(NFTSHO)

! SET IN SIMULATION TIME FOR THIS TETHER
      OUTP( 1) = TISTIM

! SET IN THERMAL PARAMETERS
      OUTP( 2) = XXLNS * SEGLAT

      OUTP( 3) = REAL(LITNIT)
      OUTP( 4) = REAL(LITERM)

      OUTP( 5) = QDEBBT
      OUTP( 6) = QDEMTT
      OUTP( 7) = QDSOLT
      OUTP( 8) = QDAROT
      OUTP( 9) = QDALBT
      OUTP(10) = QDELCT

      OUTP(11) = STEMPK(1)
      OUTP(12) = STEMPK(NBEAD+1)

      OUTP(13) = QDTSW(1)
      OUTP(14) = QDTSW(NBEAD+1)

      OUTP(15) = XXLNS * SEGLA(1)
      OUTP(16) = XXLNS * SEGLA(NBEAD+1)

      OUTP(17) = QDEBBT +QDEMTT +QDSOLT +QDAROT +QDALBT +QDELCT

100   CONTINUE

      RETURN
      END
