! ROUTINE: YFM056
! %Z%GTOSS %M% H.10 code v01.01 (baseline for vH10 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM056 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! DISPLAYS VARIABLES ASSOCIATED WITH THE RP PLANET-FIXED
! BASE MOTION PERTURBATION STATE


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_COSS.i"
      include "../../A_HDR/COM_HOST.i"
      include "../../A_HDR/EQU_HOST.i"
      include "../../A_HDR/COM_RPS.i"




!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! DEFINE COLUMN HEADINGS
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE COLUMN DATA
      IF(JFUNC .EQ. 2) GO TO 2000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM056-0'


!******************************************
!******************************************
!     WRITE COLUMN HEADER TO PLOT FILE
!******************************************
!******************************************
1000  CONTINUE

! CHOOSE STANDARD OR TAG FORMAT
!------------------------------
      IF(TAGFLG .EQ. 1.0) THEN

! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = 'T_E056_RP_'//TAG4//' '

      ECOLHD( 2) = 'XBAS_F_'//TAG4//'    '
      ECOLHD( 3) = 'YBAS_F_'//TAG4//'    '
      ECOLHD( 4) = 'HBAS_F_'//TAG4//'    '

      ECOLHD( 5) = 'XBAS_D_F_'//TAG4//'  '
      ECOLHD( 6) = 'YBAS_D_F_'//TAG4//'  '
      ECOLHD( 7) = 'HBAS_D_F_'//TAG4//'  '

      ECOLHD( 8) = 'XBAS_DD_F_'//TAG4//' '
      ECOLHD( 9) = 'YBAS_DD_F_'//TAG4//' '
      ECOLHD(10) = 'HBAS_DD_F_'//TAG4//' '

! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = 'T_M056_RP_'//TAG4//' '

      MCOLHD( 2) = 'XBAS_M_'//TAG4//'    '
      MCOLHD( 3) = 'YBAS_M_'//TAG4//'    '
      MCOLHD( 4) = 'HBAS_M_'//TAG4//'    '

      MCOLHD( 5) = 'XBAS_D_M_'//TAG4//'  '
      MCOLHD( 6) = 'YBAS_D_M_'//TAG4//'  '
      MCOLHD( 7) = 'HBAS_D_M_'//TAG4//'  '

      MCOLHD( 8) = 'XBAS_DD_M_'//TAG4//' '
      MCOLHD( 9) = 'YBAS_DD_M_'//TAG4//' '
      MCOLHD(10) = 'HBAS_DD_M_'//TAG4//' '

      ELSE

! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = 'T_E056_J01     '

      ECOLHD( 2) = 'X_BASE_F       '
      ECOLHD( 3) = 'Y_BASE_F       '
      ECOLHD( 4) = 'H_BASE_F       '

      ECOLHD( 5) = 'X_BASE_D_FS    '
      ECOLHD( 6) = 'Y_BASE_D_FS    '
      ECOLHD( 7) = 'H_BASE_D_FS    '

      ECOLHD( 8) = 'X_BASE_DD_FSS  '
      ECOLHD( 9) = 'Y_BASE_DD_FSS  '
      ECOLHD(10) = 'H_BASE_DD_FSS  '

! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = 'T_M056_J01     '

      MCOLHD( 2) = 'X_BASE_M       '
      MCOLHD( 3) = 'Y_BASE_M       '
      MCOLHD( 4) = 'H_BASE_M       '

      MCOLHD( 5) = 'X_BASE_D_MS    '
      MCOLHD( 6) = 'Y_BASE_D_MS    '
      MCOLHD( 7) = 'H_BASE_D_MS    '

      MCOLHD( 8) = 'X_BASE_DD_MSS  '
      MCOLHD( 9) = 'Y_BASE_DD_MSS  '
      MCOLHD(10) = 'H_BASE_DD_MSS  '

      END IF

! DEFINE NUMBER OF COLUMNS BEING DEFINED
      NUMCOL = 10

      RETURN


!******************************************
!******************************************
!     CALCULATE DATA FOR PLOT FILE
!******************************************
!******************************************
2000  CONTINUE

! DISPLAY FOR BASIC REFERENCE POINT STATE AND FORCE SUMMARY
      OUTP( 1) = T

      OUTP( 2) =  XXLNS * BSPER(1)
      OUTP( 3) =  XXLNS * BSPER(2)
      OUTP( 4) = -XXLNS * BSPER(3)

      OUTP( 5) =  XXLNS * BSPERD(1)
      OUTP( 6) =  XXLNS * BSPERD(2)
      OUTP( 7) = -XXLNS * BSPERD(3)

      OUTP( 8) =  XXLNS * BSPERDD(1)
      OUTP( 9) =  XXLNS * BSPERDD(2)
      OUTP(10) = -XXLNS * BSPERDD(3)

      RETURN
      END
