! ROUTINE: YFM008
! %Z%GTOSS %M% H.10 code v01.02
!              H.10 code v01.02 Added object altitude and alt rate.
!                               Added mass, deploy mass, fuel usage.
!                               Added total thrusting impulses.
!-------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM008 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! TOSS OBJECT POSITION WR/T REF PT ORBITAL AND
!                           ARBITRARY FRAME, OBJECT #


! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND 
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_COSS.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"

!      include "../../A_HDR/EQU_OBJ.i"  (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.i"




      DIMENSION UDUMR(3)

!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! DEFINE COLUMN HEADINGS
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE COLUMN DATA
      IF(JFUNC .EQ. 2) GO TO 2000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM008-0'


!******************************************
!******************************************
!     WRITE COLUMN HEADER TO PLOT FILE
!******************************************
!******************************************
1000  CONTINUE


! CHOOSE STANDARD OR TAG FORMAT
!------------------------------
      IF(TAGFLG .EQ. 1.0) THEN

! DEFINE SPECIAL TIME HEADER
      WRITE(COLIF,1001) TIMID6, OBJID4, TAG4
1001  FORMAT(A6, A4, '_', A4)

      WRITE(IFCOL8,1002) NOBJZ, TAG4
1002  FORMAT('_', I2.2, '_',  A4)

! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = COLIF
      ECOLHD( 2) = 'XBOR_F'//IFCOL8//' '
      ECOLHD( 3) = 'YBOR_F'//IFCOL8//' '
      ECOLHD( 4) = 'ZBOR_F'//IFCOL8//' '

      ECOLHD( 5) = 'XBORD_F'//IFCOL8
      ECOLHD( 6) = 'YBORD_F'//IFCOL8
      ECOLHD( 7) = 'ZBORD_F'//IFCOL8

      ECOLHD( 8) = 'XBAR_F'//IFCOL8//' '
      ECOLHD( 9) = 'YBAR_F'//IFCOL8//' '
      ECOLHD(10) = 'ZBAR_F'//IFCOL8//' '

      ECOLHD(11) = 'VXBAR_F'//IFCOL8
      ECOLHD(12) = 'VYBAR_F'//IFCOL8
      ECOLHD(13) = 'VZBAR_F'//IFCOL8

      ECOLHD(14) = 'AXGI_F'//IFCOL8//' '
      ECOLHD(15) = 'AYGI_F'//IFCOL8//' '
      ECOLHD(16) = 'AZGI_F'//IFCOL8//' '

      ECOLHD(17) = 'MASS_S'//IFCOL8//' '
      ECOLHD(18) = 'DEPM_S'//IFCOL8//' '
      ECOLHD(19) = 'FUEL_S'//IFCOL8//' '
      ECOLHD(20) = 'IMPL_P'//IFCOL8//' '

      ECOLHD(21) = 'HOBJ_F'//IFCOL8//' '
      ECOLHD(22) = 'HOBJD_F'//IFCOL8

! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = COLIF
      MCOLHD( 2) = 'XBOR__M'//IFCOL8//' '
      MCOLHD( 3) = 'YBOR__M'//IFCOL8//' '
      MCOLHD( 4) = 'ZBOR__M'//IFCOL8//' '

      MCOLHD( 5) = 'XBORD_M'//IFCOL8
      MCOLHD( 6) = 'YBORD_M'//IFCOL8
      MCOLHD( 7) = 'ZBORD_M'//IFCOL8

      MCOLHD( 8) = 'XBAR_M'//IFCOL8//' '
      MCOLHD( 9) = 'YBAR_M'//IFCOL8//' '
      MCOLHD(10) = 'ZBAR_M'//IFCOL8//' '

      MCOLHD(11) = 'VXBAR_M'//IFCOL8
      MCOLHD(12) = 'VYBAR_M'//IFCOL8
      MCOLHD(13) = 'VZBAR_M'//IFCOL8

      MCOLHD(14) = 'AXGI_M'//IFCOL8//' '
      MCOLHD(15) = 'AYGI_M'//IFCOL8//' '
      MCOLHD(16) = 'AZGI_M'//IFCOL8//' '

      MCOLHD(17) = 'MASS_K'//IFCOL8//' '
      MCOLHD(18) = 'DEPM_K'//IFCOL8//' '
      MCOLHD(19) = 'FUEL_K'//IFCOL8//' '
      MCOLHD(20) = 'IMPL_N'//IFCOL8//' '

      MCOLHD(21) = 'HOBJ_M'//IFCOL8//' '
      MCOLHD(22) = 'HOBJD_M'//IFCOL8

      ELSE

! DEFINE SPECIAL TIME HEADER
      WRITE(COLIF,1003) TIMID6, OBJID4
1003  FORMAT(A6, A4, '     ')

! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = COLIF
      ECOLHD( 2) = 'XBOR_F'//OBJID4//'     '
      ECOLHD( 3) = 'YBOR_F'//OBJID4//'     '
      ECOLHD( 4) = 'ZBOR_F'//OBJID4//'     '

      ECOLHD( 5) = 'XBORD_FS'//OBJID4//'   '
      ECOLHD( 6) = 'YBORD_FS'//OBJID4//'   '
      ECOLHD( 7) = 'ZBORD_FS'//OBJID4//'   '

      ECOLHD( 8) = 'XBAR_F'//OBJID4//'     '
      ECOLHD( 9) = 'YBAR_F'//OBJID4//'     '
      ECOLHD(10) = 'ZBAR_F'//OBJID4//'     '

      ECOLHD(11) = 'VXBAR_FS'//OBJID4//'   '
      ECOLHD(12) = 'VYBAR_FS'//OBJID4//'   '
      ECOLHD(13) = 'VZBAR_FS'//OBJID4//'   '

      ECOLHD(14) = 'AXGI_FSS'//OBJID4//'   '
      ECOLHD(15) = 'AYGI_FSS'//OBJID4//'   '
      ECOLHD(16) = 'AZGI_FSS'//OBJID4//'   '

      ECOLHD(17) = 'MASS__SL'//OBJID4//'   '
      ECOLHD(18) = 'DEPLM_SL'//OBJID4//'   '
      ECOLHD(19) = 'FUELM_SL'//OBJID4//'   '
      ECOLHD(20) = 'IMPLS_PS'//OBJID4//'   '

      ECOLHD(21) = 'HOBJ_FSS'//OBJID4//'   '
      ECOLHD(22) = '   '//'HOBJD_FS'//OBJID4

! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = COLIF
      MCOLHD( 2) = 'XBOR_M'//OBJID4//'     '
      MCOLHD( 3) = 'YBOR_M'//OBJID4//'     '
      MCOLHD( 4) = 'ZBOR_M'//OBJID4//'     '

      MCOLHD( 5) = 'XBORD_MS'//OBJID4//'   '
      MCOLHD( 6) = 'YBORD_MS'//OBJID4//'   '
      MCOLHD( 7) = 'ZBORD_MS'//OBJID4//'   '

      MCOLHD( 8) = 'XBAR_M'//OBJID4//'     '
      MCOLHD( 9) = 'YBAR_M'//OBJID4//'     '
      MCOLHD(10) = 'ZBAR_M'//OBJID4//'     '

      MCOLHD(11) = 'VXBAR_MS'//OBJID4//'   '
      MCOLHD(12) = 'VYBAR_MS'//OBJID4//'   '
      MCOLHD(13) = 'VZBAR_MS'//OBJID4//'   '

      MCOLHD(14) = 'AXGI_MSS'//OBJID4//'   '
      MCOLHD(15) = 'AYGI_MSS'//OBJID4//'   '
      MCOLHD(16) = 'AZGI_FSS'//OBJID4//'   '

      MCOLHD(17) = 'MASS__KG'//OBJID4//'   '
      MCOLHD(18) = 'DEPLM_KG'//OBJID4//'   '
      MCOLHD(19) = 'FUELM_KG'//OBJID4//'   '
      MCOLHD(20) = 'IMPLS_NS'//OBJID4//'   '

      MCOLHD(21) = 'HOBJ_MSS'//OBJID4//'   '
      MCOLHD(22) = '   '//'HOBJD_MS'//OBJID4

      END IF


! DEFINE NUMBER OF COLUMNS BEING DEFINED
      NUMCOL = 22

      RETURN


!******************************************
!******************************************
!     CALCULATE DATA FOR PLOT FILE
!******************************************
!******************************************
2000  CONTINUE

! DISPLAY OF TOSS DYNAMICS FOR ARBITRARY TOSS OBJECT

! IF TOSS OBJECT NUMBER IS NOT PROPERLY DEFINED, GET OUT
      IF((NOBJZ.LT.2) .OR. (NOBJZ .GT. LASOBJ)) GO TO 100

! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
      CALL TOSLDW (NOBJZ)

      OUTP( 1) = TOSTIM

      OUTP( 2) = XXLNS * RBOR(1)
      OUTP( 3) = XXLNS * RBOR(2)
      OUTP( 4) = XXLNS * RBOR(3)

      OUTP( 5) = XXLNS * RBORD(1)
      OUTP( 6) = XXLNS * RBORD(2)
      OUTP( 7) = XXLNS * RBORD(3)

      OUTP( 8) = XXLNS * RBAR(1)
      OUTP( 9) = XXLNS * RBAR(2)
      OUTP(10) = XXLNS * RBAR(3)

      OUTP(11) = XXLNS * VBAR(1)
      OUTP(12) = XXLNS * VBAR(2)
      OUTP(13) = XXLNS * VBAR(3)

      OUTP(14) = XXLNS * AGI(1)
      OUTP(15) = XXLNS * AGI(1)
      OUTP(16) = XXLNS * AGI(1)

! OBJECT MASS REMAINING
      OUTP(17) = XXMS1*DMASS

! MASS DEPLOYED VIA TETHERS
      OUTP(18) = 0.0
      DO 2001 JAP = 1,NATPAT
            OUTP(18) = OUTP(18) + XXMS1*APS(32,JAP,NOBJZ)
2001  CONTINUE

! MASS LOST IN THRUSTING (FUEL USAGE)
      OUTP(19) = XXMS1*DMASINC

! TOTAL THRUSTING IMPULSE
      OUTP(20) = XXFORS*TOTIPULS

! DISPLAY ALTITUDE ABOVE OBLATE SPHEROID
      OUTP(21) = XXLNS * ALTOBL

! CALC OBJECT RATE COMPONENT ALONG RADIUS VECTOR
      CALL VECNRM(RI,UDUMR)
      HDDUM = DOT(UDUMR,RID)

      OUTP(22) = XXLNS * HDDUM

100   CONTINUE

      RETURN
      END
