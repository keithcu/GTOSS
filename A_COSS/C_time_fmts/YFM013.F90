! ROUTINE: YFM013
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM013 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! 6 DOF ANGULAR MOMENTUM (ALL OBJECTS)


! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND 
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_COSS.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"

!      include "../../A_HDR/EQU_OBJ.i"  (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.i"




      DIMENSION JODUM(10)

!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! DEFINE COLUMN HEADINGS
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE COLUMN DATA
      IF(JFUNC .EQ. 2) GO TO 2000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM013-0'


!******************************************
!******************************************
!     WRITE COLUMN HEADER TO PLOT FILE
!******************************************
!******************************************
1000  CONTINUE

! PROCESS NON-DENSELY PACKED, RANDOM LIST OF 10 OBJECT NUMBERS
!-------------------------------------------------------------
      JCNDUM = 0
      DO 10 J=1,10
           J10DUM = NINT(PTOBJ(J))
! USE ONLY MEANINGFUL OBJECTS
           IF((J10DUM.LT.1) .OR. (J10DUM.GT.LASOBJ)) GO TO 10
! COUNT NON-TRIVIAL SPECIFICATIONS
           IF(J10DUM .NE. 0) JCNDUM = JCNDUM + 1
! SAVE THEIR ASSOCIATED TETHER NUMBERS
           IF(J10DUM .NE. 0) JODUM(JCNDUM) = J10DUM
10    CONTINUE


! CHOOSE STANDARD OR TAG FORMAT
!------------------------------
      IF(TAGFLG .EQ. 1.0) THEN

! DEFINE SPECIAL TIME HEADER
      WRITE(COLIF,1001) TIMID6, JODUM(1), TAG4
1001  FORMAT(A6,'_J',I2.2,'_', A4)

      ECOLHD(1) = COLIF
      MCOLHD(1) = COLIF


! SET IN LABELS FOR SPECIFIED OBJECTS
      JDUM = 0
      DO 1022 J=1,JCNDUM

! CHARACTERIZE OBJECT NUMBER
         WRITE(IFCOL7,1002) JODUM(J), TAG4
1002     FORMAT('_', I1, '_', A4)

! SET IN ENGLISH UNITS COLUMN HEADERS
         ECOLHD(JDUM+2) = '6_AMXI_E'//IFCOL7
         ECOLHD(JDUM+3) = '6_AMYI_E'//IFCOL7
         ECOLHD(JDUM+4) = '6_AMZI_E'//IFCOL7
         ECOLHD(JDUM+5) = '6_AMAG_E'//IFCOL7

! SET IN METRIC UNITS COLUMN HEADERS
         MCOLHD(JDUM+2) = '6_AMXI_M'//IFCOL7
         MCOLHD(JDUM+3) = '6_AMYI_M'//IFCOL7
         MCOLHD(JDUM+4) = '6_AMZI_M'//IFCOL7
         MCOLHD(JDUM+5) = '6_AMAG_M'//IFCOL7

         JDUM = JDUM + 4
1022  CONTINUE

      ELSE

! DEFINE SPECIAL TIME HEADER
!---------------------------
      WRITE(COLIF,1003) TIMID6, JODUM(1)
1003  FORMAT(A6,'_JX',I2.2, '    ')

      ECOLHD(1) = COLIF
      MCOLHD(1) = COLIF


! SET IN LABELS FOR SPECIFIED OBJECTS
!------------------------------------
      JDUM = 0
      DO 1033 J=1,JCNDUM

! CHARACTERIZE OBJECT NUMBER
         WRITE(OBJID4,1004) JODUM(J)
1004     FORMAT('_J',I2.2)

! SET IN ENGLISH UNITS COLUMN HEADERS
         ECOLHD(JDUM+2) = '6DOF_AMXI_E'//OBJID4
         ECOLHD(JDUM+3) = '6DOF_AMYI_E'//OBJID4
         ECOLHD(JDUM+4) = '6DOF_AMZI_E'//OBJID4
         ECOLHD(JDUM+5) = '6DOF_AMAG_E'//OBJID4

! SET IN METRIC UNITS COLUMN HEADERS
         MCOLHD(JDUM+2) = '6DOF_AMXI_M'//OBJID4
         MCOLHD(JDUM+3) = '6DOF_AMYI_M'//OBJID4
         MCOLHD(JDUM+4) = '6DOF_AMZI_M'//OBJID4
         MCOLHD(JDUM+5) = '6DOF_AMAG_M'//OBJID4

         JDUM = JDUM + 4
1033  CONTINUE

      END IF



! DEFINE NUMBER OF COLUMNS BEING DEFINED
      NUMCOL = JDUM + 1

      RETURN


!******************************************
!******************************************
!     CALCULATE DATA FOR PLOT FILE
!******************************************
!******************************************
2000  CONTINUE

      OUTP( 1) = RPTIME

! DISPLAY TOSS OBJECT 6DOF ANGULAR MOMENTUM DATA FROM A 10 ELEMENT,
! NON-DENSELY SPECIFIED LIST OF RANDOMLY ORDERED TOSS OBJECTS
      JJ = 0

      DO 20 J=1,10

         JPOBJ = NINT(PTOBJ(J))

! DISPLAY ONLY MEANINGFUL OBJECTS
         IF((JPOBJ.LT.1) .OR. (JPOBJ.GT.LASOBJ)) GO TO 20

              IF(JPOBJ .EQ. 1) THEN
                 OUTP(JJ+2) = (XXMS1*XXLNS**2) * RPDOFI(1)
                 OUTP(JJ+3) = (XXMS1*XXLNS**2) * RPDOFI(2)
                 OUTP(JJ+4) = (XXMS1*XXLNS**2) * RPDOFI(3)
                 OUTP(JJ+5) = (XXMS1*XXLNS**2) * VECMAG(RPDOFI)

              ELSE
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
                 CALL TOSLDW (JPOBJ)

                 OUTP(JJ+2) = (XXMS1*XXLNS**2) * HMOMI(1)
                 OUTP(JJ+3) = (XXMS1*XXLNS**2) * HMOMI(2)
                 OUTP(JJ+4) = (XXMS1*XXLNS**2) * HMOMI(3)
                 OUTP(JJ+5) = (XXMS1*XXLNS**2) * VECMAG(HMOMI)

              END IF

              JJ = JJ + 4

20    CONTINUE

      RETURN
      END
