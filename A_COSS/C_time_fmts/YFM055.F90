! ROUTINE: YFM055
! %Z%GTOSS %M% H.10 code v01.01 (baseline for vH10 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM055 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
!
!
!    A TETHER-CHAIN TIME-HISTORY DISPLAY FORMAT
!    ------------------------------------------
!
! THIS ROUTINE DISPLAYS A TIME HISTORY OF TETHER PARAMETERS
! CORRESPONDING TO A USER-SPECIFIED POINT ON THE CHAIN. AT THE
! SPECIFIED POINT, THE FOLLOWING PARAMETERS ARE SAMPLED AND
! CONVERTED TO A TIME HISTORY FOR PLOTTING:
!
!      BEAD POSITIONS (+EAST,  TOPO FRAME) [CALC]
!      BEAD POSITIONS (+NORTH, TOPO FRAME) [CALC]
!
! THIS ROUTINE WILL RETURN INFORMATION ONLY ABOUT THE TETHER-CHAIN THAT
! CONTAINS TOSS OBJECT 1. IF NO CHAIN CONTAINS TOSS OBJECT 1, THEN
! THE ELEMENT-COUNT-VALUE OF THE RETURNED RESULTS ARRAYS IS NEGATIVE.
!
! IN ADDITION, THE USER SPECIFIES THE POINT ALONG THE CHAIN OF TETHERS
! VIA THE OUTPUT-FILE SPECIFIC DEFINITOR (FOR EXAMPLE, INPUT PARAMETER
! NUMBER 180 FOR THE FIRST OUTPUT FILE SPECIFICATION). THIS FORMAT
! CAN BE USED REPEATEDLY IN A CTOSS RUN TO SAMPLE MULTIPLE POINTS
! ALONG A CHAIN. THE USER DOES NOT NEED TO IDENTIFY EITHER THE TETHERS
! OR FINITE SOLUTIONS, NOR BE CONVERSANT IN THE CHAIN'S TOPOLOGY.
!
! THIS INPUT SPECIFIER FOR LENGTH IS INTERPRETED AS FOLLOWS:
!
!  IF >  1.0  THEN IT IS INTERPRETED AS AN ALTITUDE ALONG THE CHAIN,
!             STARTING FROM THE LOWEST POINT ON THE CHAIN. THE UNITS OF
!             THE ENTERED LENGTH IS THE UNIT THAT IS EXTANT FOR THAT
!             PARTICULAR OUTPUT FILE SPECIFICATION (ENGLISH OR METRIC).
!
!  IF <,= 1.0 THEN IT IS INTERPRETED AS "FRACTIONAL ALTITUDE" ALONG THE
!             CHAIN, STARTING WITH ZERO AT THE LOWEST POINT ON THE CHAIN, A
!             VALUE  OF 1.0 CORRESPONDING TO THE TOP END OF THE CHAIN.


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND 
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_COSS.i"
      include "../../A_HDR/COM_ROSS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.i"





! STORAGE FOR ARG DATA (SELF MAINTAINING WR/T MAXBBS, VIA FOSS PARAM)
      DIMENSION DLENDUM(NCOSSEG), DATADUM(NCOSSEG,2)
      CHARACTER *5 CDUM5

!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! DEFINE COLUMN HEADINGS
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE COLUMN DATA
      IF(JFUNC .EQ. 2) GO TO 2000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM055-0'


!******************************************
!******************************************
!     WRITE COLUMN HEADER TO PLOT FILE
!******************************************
!******************************************
1000  CONTINUE


! CREATE A CHARACTER STRING TO PORTRAY USER LENGTH SPEC
!------------------------------------------------------
! FETCH A DATA IMAGE OF THIS CHAIN
      CALL CHAINALD (JNPDUM, DLENDUM, DATADUM)


! SEE IF USER IS SPECIFYING ACTUAL LENGTH, OR FRACTION OF LENGTH
!---------------------------------------------------------------
      IF(PLENFRC .LE. 1.0) THEN

! FRACTION-OF-LENGTH IS SPECIFIED, SO FIND A SEARCH LENGTH
           SERCDUM = PLENFRC*DLENDUM(JNPDUM)
      ELSE

! LENGTH SPEC, CONVERT USER SPEC UNITS (STAT MI OR KM) TO ENG FOR SEARCH
           SERCDUM = PLENFRC/XXLNB3
      END IF

! IN EVERY CASE, FORM FRACTIONAL EQUIVALENT OF SEARCH LENGTH
!-----------------------------------------------------------
      FRCDUM = SERCDUM/DLENDUM(JNPDUM)

! WRITE A CHAR STRING TO ANNOTATE DISPLAY HEADERS
      WRITE( CDUM5, 551) FRCDUM
551   FORMAT(F5.4)


! CHOOSE STANDARD OR TAG FORMAT
!------------------------------
      IF(TAGFLG .EQ. 1.0) THEN

! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = 'T_E055_CH_'//TAG4//' '

      ECOLHD( 2) = 'EAST_F'//CDUM5//TAG4
      ECOLHD( 3) = 'NORT_F'//CDUM5//TAG4

! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = 'T_M055_CH_'//TAG4//' '

      MCOLHD( 2) = 'EAST_M'//CDUM5//TAG4
      MCOLHD( 3) = 'NORT_M'//CDUM5//TAG4

      ELSE

! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = 'T_E055_CHN'//OBJID4//' '

      ECOLHD( 2) = 'EAST_DIS_F'//CDUM5
      ECOLHD( 3) = 'NORT_DIS_F'//CDUM5

! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = 'T_M055_CHN'//OBJID4//' '

      MCOLHD( 2) = 'EAST_DIS_M'//CDUM5
      MCOLHD( 3) = 'NORT_DIS_M'//CDUM5

      END IF

! DEFINE NUMBER OF COLUMNS BEING DEFINED
      NUMCOL = 3
      RETURN



!******************************************
!******************************************
!     CALCULATE DATA FOR PLOT FILE
!******************************************
!******************************************
2000  CONTINUE

! FETCH A DATA IMAGE OF THIS CHAIN
      CALL CHAINALD(JNPDUM, DLENDUM, DATADUM)

! SEE IF USER IS SPECIFYING ACTUAL LENGTH, OR FRACTION OF LENGTH
!---------------------------------------------------------------
      IF(PLENFRC .LE. 1.0) THEN

! FRACTION-OF-LENGTH IS SPECIFIED, SO FIND A SEARCH LENGTH
           SERCDUM = PLENFRC*DLENDUM(JNPDUM)
      ELSE

! LENGTH SPECIFIED, CONVERT FROM USER SPEC UNITS TO ENGLISH FOR SEARCH
           SERCDUM = PLENFRC/XXLNB3
      END IF

! SET IN END VALUES FOR CRAZY VALUES OF SERCDUM
!----------------------------------------------
      IF(SERCDUM .LE. 0.0) THEN
          EASTDUM  = DATADUM(1,1)
          CNORTDUM = DATADUM(1,2)
          GO TO 230
      END IF

      IF(SERCDUM .GE. DLENDUM(JNPDUM)) THEN
          EASTDUM  = DATADUM(JNPDUM,1)
          CNORTDUM = DATADUM(JNPDUM,2)
          GO TO 230
      END IF


! EARCH FOR INDICES BOUNDING INTERVAL CONTAINING SPECIFIED LENGTH
!----------------------------------------------------------------
      DO 222 J = 1,JNPDUM

          IF(DLENDUM(J) .GT. SERCDUM) THEN
             JH = J
             JL = J-1
             GO TO 223
          END IF

222   CONTINUE

! CALC LINEAR INTERP COEFF BETWEEN BOUNDING INTERVAL VALUES
!----------------------------------------------------------
223   CONTINUE

      DUMHIVAL = DLENDUM(JH)
      DUMLOVAL = DLENDUM(JL)
      COEFDUM = (SERCDUM - DUMLOVAL)/(DUMHIVAL - DUMLOVAL)


! NOW CALCULATE INTERPOLATED PARAMETERS
!--------------------------------------
      EASTDUM  = DATADUM(JL,1) +COEFDUM*(DATADUM(JH,1) -DATADUM(JL,1))
      CNORTDUM = DATADUM(JL,2) +COEFDUM*(DATADUM(JH,2) -DATADUM(JL,2))


!-----------------------------------------------------
! SET IN SIMULATION TIME VARIABLE AND OTHER PARAMETERS
!-----------------------------------------------------
230   CONTINUE

      OUTP(1) = RPTIME

! THEN BARE WIRE OPERATIONS DATA
      OUTP(2) = XXLNS *  EASTDUM
      OUTP(3) = XXLNS * CNORTDUM

100   CONTINUE

      RETURN

      END
