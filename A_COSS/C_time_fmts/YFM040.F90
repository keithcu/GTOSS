! ROUTINE: YFM040
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM040 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! RESUME OF REF PT FLEX BOOM DATA


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_COSS.i"
      include "../../A_HDR/COM_BOOM.i"




!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! DEFINE COLUMN HEADINGS
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE COLUMN DATA
      IF(JFUNC .EQ. 2) GO TO 2000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM040-0'


!******************************************
!******************************************
!     WRITE COLUMN HEADER TO PLOT FILE
!******************************************
!******************************************
1000  CONTINUE
! CHOOSE STANDARD OR TAG FORMAT
!------------------------------
      IF(TAGFLG .EQ. 1.0) THEN

! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = 'T_E040_'//TAG4//'    '

      ECOLHD( 2) = 'Y_TIP_IN_'//TAG4//'  '
      ECOLHD( 3) = 'Z_TIP_IN_'//TAG4//'  '

      ECOLHD( 4) = 'Y_TIPD_IN_'//TAG4//' '
      ECOLHD( 5) = 'Z_TIPD_IN_'//TAG4//' '

      ECOLHD( 6) = 'Y_TIPDD_IN_'//TAG4
      ECOLHD( 7) = 'Z_TIPDD_IN_'//TAG4

      ECOLHD( 8) = 'TEN_XBFR_P_'//TAG4
      ECOLHD( 9) = 'TEN_YBFR_P_'//TAG4
      ECOLHD(10) = 'TEN_ZBFR_P_'//TAG4

      ECOLHD(11) = 'BASE_FXB_P_'//TAG4
      ECOLHD(12) = 'BASE_FYB_P_'//TAG4
      ECOLHD(13) = 'BASE_FZB_P_'//TAG4

      ECOLHD(14) = 'BAS_GXB_FP_'//TAG4
      ECOLHD(15) = 'BAS_GYB_FP_'//TAG4
      ECOLHD(16) = 'BAS_GZB_FP_'//TAG4

! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = 'T_M040_'//TAG4//'    '

      MCOLHD( 2) = 'Y_TIP_CM_'//TAG4//'  '
      MCOLHD( 3) = 'Z_TIP_CM_'//TAG4//'  '

      MCOLHD( 4) = 'Y_TIPD_CM_'//TAG4//' '
      MCOLHD( 5) = 'Z_TIPD_CM_'//TAG4//' '

      MCOLHD( 6) = 'Y_TIPDD_CM_'//TAG4
      MCOLHD( 7) = 'Z_TIPDD_CM_'//TAG4

      MCOLHD( 8) = 'TEN_XBFR_N_'//TAG4
      MCOLHD( 9) = 'TEN_YBFR_N_'//TAG4
      MCOLHD(10) = 'TEN_ZBFR_N_'//TAG4

      MCOLHD(11) = 'BASE_FXB_N_'//TAG4
      MCOLHD(12) = 'BASE_FYB_N_'//TAG4
      MCOLHD(13) = 'BASE_FZB_N_'//TAG4

      MCOLHD(14) = 'BAS_GXB_NM_'//TAG4
      MCOLHD(15) = 'BAS_GXB_NM_'//TAG4
      MCOLHD(16) = 'BAS_GXB_NM_'//TAG4

      ELSE


! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = 'T_E040         '

      ECOLHD( 2) = 'Y_TIP_IN       '
      ECOLHD( 3) = 'Z_TIP_IN       '

      ECOLHD( 4) = 'Y_TIPD_INS     '
      ECOLHD( 5) = 'Z_TIPD_INS     '

      ECOLHD( 6) = 'Y_TIPDD_INSS   '
      ECOLHD( 7) = 'Z_TIPDD_INSS   '

      ECOLHD( 8) = 'TENS_XB_FRM_P  '
      ECOLHD( 9) = 'TENS_YB_FRM_P  '
      ECOLHD(10) = 'TENS_ZB_FRM_P  '

      ECOLHD(11) = 'BASE_XB_FORC_P '
      ECOLHD(12) = 'BASE_YB_FORC_P '
      ECOLHD(13) = 'BASE_ZB_FORC_P '

      ECOLHD(14) = 'BASE_XBMOM_F_P '
      ECOLHD(15) = 'BASE_YBMOM_F_P '
      ECOLHD(16) = ' '//'BASE_ZBMOM_F_P'

! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = 'T_M040         '

      MCOLHD( 2) = 'Y_TIP_CM       '
      MCOLHD( 3) = 'Z_TIP_CM       '

      MCOLHD( 4) = 'Y_TIPD_CMS     '
      MCOLHD( 5) = 'Z_TIPD_CMS     '

      MCOLHD( 6) = 'Y_TIPDD_CMSS   '
      MCOLHD( 7) = 'Z_TIPDD_CMSS   '

      MCOLHD( 8) = 'TENS_XB_FRM_N  '
      MCOLHD( 9) = 'TENS_YB_FRM_N  '
      MCOLHD(10) = 'TENS_ZB_FRM_N  '

      MCOLHD(11) = 'BASE_XB_FORC_N '
      MCOLHD(12) = 'BASE_YB_FORC_N '
      MCOLHD(13) = 'BASE_ZB_FORC_N '

      MCOLHD(14) = 'BASE_XBMOM_N_M '
      MCOLHD(15) = 'BASE_XBMOM_N_M '
      MCOLHD(16) = ' '//'BASE_XBMOM_N_M'

      END IF


! DEFINE NUMBER OF COLUMNS BEING DEFINED
      NUMCOL = 16

      RETURN


!******************************************
!******************************************
!     CALCULATE DATA FOR PLOT FILE
!******************************************
!******************************************
2000  CONTINUE

      OUTP( 1) = TIMBOM

      OUTP( 2) = XXLNB2 * 12.0 * TIPF(2)
      OUTP( 3) = XXLNB2 * 12.0 * TIPF(3)

      OUTP( 4) = XXLNB2 * 12.0 * TIPDF(2)
      OUTP( 5) = XXLNB2 * 12.0 * TIPDF(3)

      OUTP( 6) = XXLNB2 * 12.0 * TIPDDF(2)
      OUTP( 7) = XXLNB2 * 12.0 * TIPDDF(3)

      OUTP( 8) = XXFORS * TENSF(1)
      OUTP( 9) = XXFORS * TENSF(2)
      OUTP(10) = XXFORS * TENSF(3)

      OUTP(11) = XXFORS * FORBF(1)
      OUTP(12) = XXFORS * FORBF(2)
      OUTP(13) = XXFORS * FORBF(3)

      OUTP(14) = XXMOMS * CUPBF(1)
      OUTP(15) = XXMOMS * CUPBF(2)
      OUTP(16) = XXMOMS * CUPBF(3)

      RETURN
      END
