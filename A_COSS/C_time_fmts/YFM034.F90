! ROUTINE: YFM034
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM034 (JFUNC)
!**************************************************
!**************************************************
!**************************************************
! TOSS GENERAL FORMAT TO DISPLAY TOTAL SYSTEM DYNAMICS
! VERIFICATION PARAMETERS (PARTICLE ANGULAR MOMENTUM ETC)


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/COM_COSS.i"




! USE PARALLEL DATA FOR RE-ENTRANT TYPE ACTIVITY WR/T UNITS
      DIMENSION DUMHLE(3), DUMHLM(3)
      SAVE DUMTLE, DUMTLM, DUMHLE, DUMHLM

!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! DEFINE COLUMN HEADINGS
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE COLUMN DATA
      IF(JFUNC .EQ. 2) GO TO 2000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM034-0'


!******************************************
!******************************************
!     WRITE COLUMN HEADER TO PLOT FILE
!******************************************
!******************************************
1000  CONTINUE

! CHOOSE STANDARD OR TAG FORMAT
!------------------------------
      IF(TAGFLG .EQ. 1.0) THEN

! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = 'T_E034_'//TAG4//'    '

      ECOLHD( 2) = 'HXI_PAM_E_'//TAG4//' '
      ECOLHD( 3) = 'HYI_PAM_E_'//TAG4//' '
      ECOLHD( 4) = 'HZI_PAM_E_'//TAG4//' '

      ECOLHD( 5) = 'HXIODELT_E_'//TAG4
      ECOLHD( 6) = 'HYIODELT_E_'//TAG4
      ECOLHD( 7) = 'HZIODELT_E_'//TAG4

      ECOLHD( 8) = 'MG_CGPAM_E_'//TAG4
      ECOLHD( 9) = 'MG_HODEL_E_'//TAG4
      ECOLHD(10) = 'MG_GVMOM_E_'//TAG4

! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = 'T_M034_'//TAG4//'    '

      MCOLHD( 2) = 'HXI_PAM_M_'//TAG4//' '
      MCOLHD( 3) = 'HYI_PAM_M_'//TAG4//' '
      MCOLHD( 4) = 'HZI_PAM_M_'//TAG4//' '

      MCOLHD( 5) = 'HXIODELT_M_'//TAG4
      MCOLHD( 6) = 'HYIODELT_M_'//TAG4
      MCOLHD( 7) = 'HZIODELT_M_'//TAG4

      MCOLHD( 8) = 'MG_CGPAM_M_'//TAG4
      MCOLHD( 9) = 'MG_HODEL_M_'//TAG4
      MCOLHD(10) = 'MG_GVMOM_M_'//TAG4

      ELSE

! SET IN ENGLISH UNITS COLUMN HEADERS
      ECOLHD( 1) = 'T_E034         '

      ECOLHD( 2) = 'HCGXI_PAM_E    '
      ECOLHD( 3) = 'HCGYI_PAM_E    '
      ECOLHD( 4) = 'HCGZI_PAM_E    '

      ECOLHD( 5) = 'HCGXIODELT_E   '
      ECOLHD( 6) = 'HCGYIODELT_E   '
      ECOLHD( 7) = 'HCGZIODELT_E   '

      ECOLHD( 8) = 'MAG_CG_PAM_E   '
      ECOLHD( 9) = 'MAG_HCGIODELT_E'
      ECOLHD(10) = 'MAG_CG_GRVMOM_E'

! SET IN METRIC UNITS COLUMN HEADERS
      MCOLHD( 1) = 'T_M034         '

      MCOLHD( 2) = 'HCGXI_PAM_M    '
      MCOLHD( 3) = 'HCGYI_PAM_M    '
      MCOLHD( 4) = 'HCGZI_PAM_M    '

      MCOLHD( 5) = 'HCGXIODELT_M   '
      MCOLHD( 6) = 'HCGYIODELT_M   '
      MCOLHD( 7) = 'HCGZIODELT_M   '

      MCOLHD( 8) = 'MAG_CG_PAM_M   '
      MCOLHD( 9) = 'MAG_HCGIODELT_M'
      MCOLHD(10) = 'MAG_CG_GRVMOM_M'

      END IF


! DEFINE NUMBER OF COLUMNS BEING DEFINED
      NUMCOL = 10

      RETURN


!******************************************
!******************************************
!     CALCULATE DATA FOR PLOT FILE
!******************************************
!******************************************
2000  CONTINUE

          OUTP( 1) = RPTIME

! UNIT CONVERT PARTICLE ANGULAR MOMENTUM
      TOSVX1(1) = XXMS1 * (XXLNS**2) * PAMOMI(1)
      TOSVX1(2) = XXMS1 * (XXLNS**2) * PAMOMI(2)
      TOSVX1(3) = XXMS1 * (XXLNS**2) * PAMOMI(3)

          OUTP( 2) = TOSVX1(1)
          OUTP( 3) = TOSVX1(2)
          OUTP( 4) = TOSVX1(3)

! UNIT CONVERT CG POSITION
      TOSVX2(1) = XXLNS * POSCGI(1)
      TOSVX2(2) = XXLNS * POSCGI(2)
      TOSVX2(3) = XXLNS * POSCGI(3)

! UNIT CONVERT GRAVITY TORQUE
      TOSVX3(1) = XXMOMS * SYSMOG(1)
      TOSVX3(2) = XXMOMS * SYSMOG(2)
      TOSVX3(3) = XXMOMS * SYSMOG(3)

! CALCULATE NUMERICAL DERIVATIVES OF ANG MOMENTUM
!------------------------------------------------
! (ALLOW FOR RE-ENTRANT TYPE ACTIVITY WR/T UNITS)
      IF(STAR) DUMTLE = 1.0
      IF(STAR) DUMTLM = 1.0
      IF(STAR) CALL VECMOV(TOSVX1, DUMHLE)
      IF(STAR) CALL VECMOV(TOSVX1, DUMHLM)

! CALCULATE DERIVS
          IF(NUNIT.EQ.1) CALL VECDIF(TOSVX1,DUMHLE, TOSVX4)
          IF(NUNIT.EQ.1) CALL VECSCL(1./(RPTIME-DUMTLE),TOSVX4,TOSVX4)

          IF(NUNIT.EQ.2) CALL VECDIF(TOSVX1,DUMHLM, TOSVX4)
          IF(NUNIT.EQ.2) CALL VECSCL(1./(RPTIME-DUMTLM),TOSVX4,TOSVX4)

          OUTP( 5) = TOSVX4(1)
          OUTP( 6) = TOSVX4(2)
          OUTP( 7) = TOSVX4(3)

! SAVE PREVIOUS VALUES FOR NEXT PASS THRU
          IF(NUNIT.EQ.1) DUMTLE = RPTIME
          IF(NUNIT.EQ.1) CALL VECMOV(TOSVX1,DUMHLE)

          IF(NUNIT.EQ.2) DUMTLM = RPTIME
          IF(NUNIT.EQ.2) CALL VECMOV(TOSVX1,DUMHLM)


! OUTPUT MAGNITUDE OF VARIOUS VECTORS
!------------------------------------
! ANGULAR MOMENTUM
          OUTP( 8) = VECMAG(TOSVX1)

! NUM. DERIV OF ANG MOM
          OUTP( 9) = VECMAG(TOSVX4)

! GRAVITY TORQUE
          OUTP(10) = VECMAG(TOSVX3)

      RETURN
      END
