! ROUTINE: YPSHBO
! %Z%GTOSS %M% H.4 code v01.02
!              H.4 code v01.02 Changed DIMENSION to REAL *4 OPDUM
!----------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!*********************************
!*********************************
      SUBROUTINE YPSHBO(JYORZ)
!*********************************
!*********************************
! THIS ROUTINE WRITES THE PAIR OF X-Y OR X-Z ORB FRAME-SHAPES
! OF ALL BEADS IN A BEAD MODEL TETHER, TO A SHAPE SCRATCH FILE,
! IF SO REQUESTED BY THE SHAPE EVENT FLAG NSHPEV

! FURTHERMORE, THE LAST BEAD IN A SNAPSHOT IS FOLLOWED BY THE
! PAIR OF ORB FRAME POS COORDS OF THE END OBJECT

! IF JYORZ = 2,  THE X-Y ORB FRAME SHAPE SNAP-SHOT IS TAKEN

! IF JYORZ = 3,  THE X-Z ORB FRAME SHAPE SNAP-SHOT IS TAKEN


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND 
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS

! GAIN ACCESS TO THE TOSS OBJECT DATA STRUCTURE AND 
! INSTANTIATED TOSS OBJECT SOLUTIONS
      USE TOSS_OBJECT_DATA_STRUCTURE
      USE TOSS_OBJECT_SOLUTIONS


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_COSS.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"

!      include "../../A_HDR/EQU_OBJ.i"  (replaced w/USE & DEFINE statements)
!      include "../../A_HDR/EQU_OBJI.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/TOSS_Object_Var_ReMap.i"

!      include "../../A_HDR/COM_FOSS.i"  (replaced w/USE statement)
!      include "../../A_HDR/EQU_FOSS.i"  (replaced w/USE & DEFINE statements)
#include "../../A_HDR/Finite_Solution_Var_ReMap.i"





! SCRATCH FOR Y OR Z SET OF COORDS THAT MUST BE SAVED
! (X COORDS ARE STASHED IN OUTP ARRAY)
      REAL *4 OPDUM(53)

! IF SHAPE RECORDING IS NOT REQUESTED, GET OUT
      IF(NSHPEV .EQ. 0) RETURN

! IF NON-VALID FINIT SOLN # IS SPEC FOR THIS PAGE, GET OUT
      IF( (NFTSHO.LT.1) .OR. (NFTSHO.GT.NFINIT) ) GO TO 100

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
      CALL TISLDW(NFTSHO)


! FIND THE TOSS OBJECTS ATTACHED TO X AND Y ENDS OF TETHER
!---------------------------------------------------------
      JTETH = JASIGN(NFTSHO)
      NXFEND = NOBJX(JTETH)
      NYFEND = NOBJY(JTETH)

! DEPENDING UPON WHETHER X-END IS RP OR TOSS OBJ, GET TRNS STATE
!--------------------------------------------------------------

! ASSUME IT IS THE REF PT (LET TOSMX1 CONTAIN GOI MATRIX)
      CALL MATMOV (0,RPGOI,  TOSMX1)

! IF ITS TOSS OBJECT, CONTRUCT GOI MATRIX FROM THERE
! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
      IF(NXFEND .NE. 1) CALL TOSLDW (NXFEND)
      IF(NXFEND .NE. 1) CALL ORBFRM (RI,RID, TOSMX1)


! FORM TETHER FRAME TO ORB FRAME (GOT) TRANSF (STASH AS SCRATCH)
      CALL MATMUL (0,TOSMX1,GIT, TOSMX2)


! ASSOCIATE TOSS OBJ GLOBAL-POINTER BEFORE ACCESSING OBJ DATA
! WITH OBJECT AT Y-END OF TETHER
!--------------------------------------------------------
      CALL TOSLDW (NYFEND)


! SAVE SIMULATION TIME FOR THIS SHAPE EVENT
      OUTP(1)  = TISTIM
      OPDUM(1) = TISTIM

! ASSUME FIRST COORD IS THE ORB FRAME ORIGIN
      OUTP(2)  = 0.0
      OPDUM(2) = 0.0


!---------------------------------------------------
! START A LOOP TO FIND EACH SPECIFIED BEADS POSITION
!---------------------------------------------------
      DO 20 J = 1,NBEAD

! FORM INDEX INTO COMPONENTS OF THIS BEADS SYSTEM STATE VECTOR
            JB1 = 3*(J - 1) + 1
            JB2 = JB1 + 1
            JB3 = JB1 + 2

! FORM TETHER FRAME COMPS OF POS VECTOR WR/T REF END
            TOSVX1(1) = BUT(JB1) + REAL(J)*ELSGX
            TOSVX1(2) = BUT(JB2)
            TOSVX1(3) = BUT(JB3)

! TRANSFORM THIS POINT TO OBJECT ORB FRAME (AT "X" END)
            CALL MATVEC (0,TOSMX2,TOSVX1, TOSVX2)

! BEADS X COORD WR/T REF-END ORB FRAME, WILL ALWAYS BE IN WRITE ARRAY
            OUTP(J+2) = XXLNS * TOSVX2(1)

! THEN, BEADS Y 0R Z COORD (FOR WRITE THAT FOLLOWS THIS LOOP)
            IF(JYORZ .EQ. 2) OPDUM(J+2) = XXLNS * TOSVX2(2)
            IF(JYORZ .EQ. 3) OPDUM(J+2) = XXLNS * TOSVX2(3)

!-------------
20    CONTINUE
!-------------


! FINALLY, SET IN X-ORB POS OF THE THE END OBJECT
!------------------------------------------------
      OUTP(NBEAD+3) = XXLNS * RBOR(1)

! THEN, CONDITIOANLLY SET IN Y OR Z-ORB POS OF END OBJ
!-----------------------------------------------------
      IF(JYORZ .EQ. 2) OPDUM(NBEAD+3) = XXLNS * RBOR(2)
      IF(JYORZ .EQ. 3) OPDUM(NBEAD+3) = XXLNS * RBOR(3)



! WRITE THE X-ORB COORDINATE TO THE ASSOCIATED SHAPE SCRATCH FILE
!----------------------------------------------------------------
! BUMP ASSOCIATED RECORD COUNTER
      NEXSHP = NEXSHP + 1

      WRITE(IUSHAP,REC=NEXSHP) (OUTP(J),J=1,NBEAD+3)

! WRITE THE Y OR Z-ORB COORDINATE TO ASSOCIATED SHAPE SCRATCH FILE
!-----------------------------------------------------------------
! BUMP ASSOCIATED RECORD COUNTER
      NEXSHP = NEXSHP + 1

      WRITE(IUSHAP,REC=NEXSHP) (OPDUM(J),J=1,NBEAD+3)

100   RETURN
      END
