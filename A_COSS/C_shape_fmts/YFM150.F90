! ROUTINE: YFM150
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!**************************************************
!**************************************************
!**************************************************
              SUBROUTINE YFM150 (JFUNC)
!**************************************************
!**************************************************
!**************************************************

!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
! WRITE HEADING TO PRINTOUT PAGE
      IF(JFUNC .EQ. 1) GO TO 1000

! DEFINE DATA FOR PRINTOUT PAGE
      IF(JFUNC .EQ. 2) GO TO 2000

! PUT OUT A LINE OF DATA TO PRINTOUT PAGE
      IF(JFUNC .EQ. 3) GO TO 3000

! GIVE MINIMAL WARNING OF A PROBLEM
      STOP ' IN YFM150-0'


!******************************************
!******************************************
!     WRITE COLUMN HEADER TO PLOT FILE
!******************************************
!******************************************
1000  CONTINUE

! SET UP ALL-BEAD ORB FRAME-SHAPE SCRATCH FILE
      CALL YPHDBO

      RETURN


!******************************************
!******************************************
!     CALCULATE DATA FOR PLOT FILE
!******************************************
!******************************************
2000  CONTINUE

! THIS ROUTINE CAUSES THE X-Y ORB-FRAME SHAPE OF ALL
! BEADS IN A TETHER, TO BE WRITTEN TO A SCRATCH FILE

      CALL YPSHBO(2)

      RETURN


!******************************************
!******************************************
!     WRITE LINE OF DATA TO PLOT FILE
!******************************************
!******************************************
3000  CONTINUE

! THIS ROUTINE CONVERTS ABOVE ORB FRAME TETHER SHAPE
! SCRATCH FILE TO A COLUMN DELIMITED OUTPUT FILE

      CALL YPLNBO(2)

      RETURN
      END
