! ROUTINE: SHPSAV
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!***************************
!***************************
!
      SUBROUTINE SHPSAV
!
!***************************
!***************************
! THIS ROUTINE ORCHESTRATES THE CONVERSION
! SHAPE SCRATCH FILES INTO COLUMN DELIMITED OUTPUT FILES

! A CALL IS MADE TO THIS ROUTINE IF IT HAS BEEN DECIDED
! THAT A CTOSS RUN IS FINISHED


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_COSS.i"
      include "../../A_HDR/COM_ROSS.i"






!****************************************************************
! EXECUTE A LOOP FOR A MAXIMUM OF 7 COLUMN DELIMITED OUTPUT FILES
!****************************************************************
      DO 50 JWIN = 1,LIMPDS

! VISIT PROGRAM TO SET UP OUTPUT-SPECIFIC CONTROL, ID STRINGS, ETC
!-----------------------------------------------------------------
            CALL WINSET(JWIN)

! SKIP OVER PAGES WITH NO FORMAT INVOKED
            IF( NCHUZ .GT. 0 ) THEN

! ONLY CALL FORMATS KNOWN TO BE IN THE TETHER SHAPE FACILITY
!-----------------------------------------------------------
! ESTABLISH CRITERIA FOR TIME HISTORY FORMAT
                IF( NCHUZ .LT. LSC1)                        GO TO 50
                IF((NCHUZ .GT. LSC2).AND.(NCHUZ .LT. LSU1)) GO TO 50


! CALL ROUTINE TO CONVERT THIS SHAPE SCRATCH FILE TO OUTPUT FILE
                CALL XFMT(3)

            END IF

50    CONTINUE


! RESET SNAP-SHOT-TAKEN FLAGS TO NONE-TAKEN, FOR NEXT RUN
!--------------------------------------------------------
      DO 80 J = 1,39
         NSNAP(J) = 0
80    CONTINUE

      RETURN
      END
