! ROUTINE: LOOKHD
! %Z%GTOSS %M% H.10 code v01.1
!              H.10 code v01.1 Added special new QUICKLOOK fmt 6 and 7
!---------------------------------------------------------------------
!              H.1 code v01.01 (baseline for vG.1 delivery)
!******************************
!******************************
!
      SUBROUTINE LOOKHD
!
!*****************************
!*****************************

!==============================================
!==============================================
! THIS PROGRAM IS NOT AN EXPLICIT PART OF TOSS
! (BUT RATHER PART OF THE "STANDALONE" FUNCTION
!  OF REF POINT GENERATION AND RESULT DISPLAY)
!==============================================
!==============================================

! THIS SUBROUTINE OUTPUTS A HEADING ON A NEW PAGE
! FOR THE GTOSS QUICK LOOK SINGLE PAGE OUTPUT FEATURE
! (USED AS A VERIFCATION OF RUN INTEGRITY FOR RESULTS
! DATA BASE SUPPORT)


! GAIN ACCESS TO THE FINITE DATA STRUCTURE AND 
! INSTANTIATED FINITE SOLUTIONS
      USE FINITE_DATA_STRUCTURE
      USE FINITE_SOLUTIONS



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_HOST.i"
      include "../../A_HDR/EQU_HOST.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"




! NOTE: THE PARAMETERS CONTROLLING THIS QUICK LOOK CAPABILITY
!       USE INPUT CONTROL VARIABLES FROM OLD VERS B GTOSS AS
!       APPROPRIATE.  THE OLD INPUT ALLOCATIONS HAVE NOT BEEN
!       REMOVED FROM VERS C GTOSS SO AS TO ALLOW DIRECT USE OF
!       EXISTING INPUT FILES FOR GTOSS/TOSS.
!       [ HOWEVER, SOME LICENSE HAS BEEN TAKEN IN INTERPRETING
!       INPUT VALUES AS APPROPRIATE TO THE QUICK LOOK FEATURE ]

! FIX INPUT SPECIFIED OBJECT NUMBERS WHICH MAY APPLY TO THIS PAGE
      DO 1 J=1,6
         NOBJQ(J) = NINT( POBJQ(J) )
1     CONTINUE
! SET IN DEFAULT IF NEEDED
      IF(NOBJQ(1) .EQ. 0) NOBJQ(1) = 2


! FIX INPUT SPECIFIED TOSS TETHER NUMBERS WHICH MAY APPLY TO THIS PAGE
      DO 2 J=1,12
         NTSHQ(J) = NINT( PTSHQ(J) )
2     CONTINUE
! SET IN DEFAULT IF NEEDED
      IF(NTSHQ(1) .EQ. 0) NTSHQ(1) = 1


! FIX INPUT SPECIFIED FINITE TETHER SOLN # WHICH MAY APPLY TO THIS PAGE
      NFTSHQ = NINT( PFTSHQ )
      IF(NFTSHQ .EQ. 0) NFTSHQ = 1

! FIX INPUT SPECIFIED BEAD NUMBERS WHICH MAY APPLY TO THIS PAGE
      NBSHQ(1) = NINT(PBSHQ(1))
      NBSHQ(2) = NINT(PBSHQ(2))
      NBSHQ(3) = NINT(PBSHQ(3))

! SEE IF BEAD NUMBER DEFAULTS ARE NEEDED
      IF( (NBSHQ(1)+NBSHQ(2)+NBSHQ(3)) .NE. 0) GO TO 4

! SET IN DEFAULTS (USE QUARTER,HALF,THREE QUARTER BEAD POINTS)
         DUM = REAL(NBEADS(NFTSHQ))
         NBSHQ(1) = NINT(.25*DUM)
         NBSHQ(2) = NINT(.50*DUM)
         NBSHQ(3) = NINT(.75*DUM)


! FIX INPUT SPECIFIED QUICK LOOK FORMAT SELECTOR
4     NQSEL = NINT( PQSEL )


!*********************************
! SELECT DESIRED QUICK LOOK FORMAT
!*********************************
      IF( NQSEL .LE. 1 ) GO TO 10
      IF( NQSEL .EQ. 2 ) GO TO 20
      IF( NQSEL .EQ. 3 ) GO TO 30
      IF( NQSEL .EQ. 4 ) GO TO 40
      IF( NQSEL .EQ. 5 ) GO TO 50
      IF( NQSEL .EQ. 6 ) GO TO 60
      IF( NQSEL .EQ. 7 ) GO TO 70

      WRITE(IOUQLK,99)
99    FORMAT(///3X,'=====>>>>>  UN-DEFINED QUICKLOOK FORMAT <=====')
      GO TO 100



!=============================================================
! HEAD PAGE FOR QUICK LOOK FORMAT NUMBER 1 (OR DEFAULT FOR =0)
!=============================================================
10    CONTINUE
      WRITE(IOUQLK,11) NOBJQ(1), NTSHQ(1)
   11 FORMAT(3X,   &
     &'QUICK LOOK FMT 1:   SUMMARY OF:  REF PT STATE;  A SPECIFIED',   &
     &   ' TOSS OBJECT #',I2,';  AND A SPECIFIED TOSS TETHER #',I2/)

        WRITE(IOUQLK,12)
   12   FORMAT(3X,'RP-TIME   RP-ALT  RP-VELI  RP-HD RP-MUI RP-LAI',   &
     &         '  R-THTO  R-PHIO  R-PSIO   TLEN#  TLEND#  I-LIB',   &
     &         '  O-LIB   TETH#  OTHTO OPHIO OPSIO')

        WRITE(IOUQLK,13)
   13   FORMAT(3X,'  (SEC)    (FT)    (FPS)   (FPS)  (DEG)  (DEG)',   &
     &         '   (DEG)   (DEG)   (DEG)    (FT)   (FPS)  (DEG)',   &
     &         '  (DEG)   (LBS)  (DEG) (DEG) (DEG)' /)

        GO TO 100


!=========================================
! HEAD PAGE FOR QUICK LOOK FORMAT NUMBER 2
!=========================================
20    CONTINUE

      WRITE(IOUQLK,21)
21    FORMAT(3X,   &
     &'QUICK LOOK FMT 2:   TETHER TENSION (FOR TETHER NUMBERS SHOWN)'/)

      WRITE(IOUQLK,22) ( NTSHQ(J),J=1,12 )
22    FORMAT(3X,'RP-TIME', 12( 3X, 'TETH#',I2) )

      WRITE(IOUQLK,23)
23    FORMAT(3X,'  (SEC)', 12( 4X, '  (LB)')  /)

      GO TO 100



!=========================================
! HEAD PAGE FOR QUICK LOOK FORMAT NUMBER 3
!=========================================
30    CONTINUE

      WRITE(IOUQLK,31)( NOBJQ(J),J=1,6 ),( NOBJQ(J),J=1,6 )
31    FORMAT(3X,   &
     &  'QUICK LOOK FMT 3:   RANGE, LIB ANGLES WRT/T REF PT ORB FRAME',   &
     &              ' FOR OBJECTS NUMBER ', 6I6 //   &
     &   10X, 6('   OBJECT  NUMBER',I3) )

      WRITE(IOUQLK,32)
32    FORMAT(3X,'RP-TIME', 6('   RANGE  ILIB  OLIB')  )

      WRITE(IOUQLK,33)
33    FORMAT(3X,'  (SEC)', 6('    (FT)   DEG   DEG') /)

      GO TO 100



!=========================================
! HEAD PAGE FOR QUICK LOOK FORMAT NUMBER 4
!=========================================
40    CONTINUE

      WRITE(IOUQLK,41) ( NBSHQ(J),J=1,3 ),( NBSHQ(J),J=1,3 )
41    FORMAT(3X,   &
     &  'QUICK LOOK FMT 4:   ATTACH PT TENSIONS, AND BEAD DISPLACE',   &
     &  'MENTS/SEGMENT TENSION,   FOR BEADS NUMBERED ', 3I6 //   &
     &   27X, 3(' <------- BEAD  NUMBER',I3,' ------>') )

      WRITE(IOUQLK,42)
42    FORMAT(3X,'RP-TIME   XELOAD  YELOAD',   &
     &       3('  X-DIST  Y-DEFL  Z-DEFL  SEGTENS')  )

      WRITE(IOUQLK,43)
43    FORMAT(3X,'  (SEC)    (LB)     (LB) ',   &
     &       3('   (FT)     (FT)    (FT)    (LB) ') /)

      GO TO 100



!=========================================
! HEAD PAGE FOR QUICK LOOK FORMAT NUMBER 5
!=========================================
50    CONTINUE

! BYPASS IF THIS TETHER DISPLAY IS IN-APPROPRIATE
!------------------------------------------------
      IF(NFTSHQ .EQ. 0) GO TO 100
      IF(NTETH  .EQ. 0) GO TO 100
      IF(NFINIT .EQ. 0) GO TO 100

      IF(NFTSHQ .GT. NFINIT) GO TO 100

! ASSOCIATE GLOBAL FINITE SOLUTION POINTER W/SPECIFIED FINITE SOLN
      CALL TISLDW(NFTSHQ)


      WRITE(IOUQLK,51) NFTSHQ
51    FORMAT(3X,   &
     &  'QUICK LOOK FMT 5:  MISCELLANEOUS +BOOM +FINITE SOLN #',I3,//)

      WRITE(IOUQLK,52)
52    FORMAT(3X,'RP-TIME    XELOAD    YELOAD    TLENT   TLENTD    ',   &
     &'SEGL(1)  SEGLD1    SEGL(N)  SEGLDN   TIPY  TIPDY   TIPZ  TIPDZ')

!---------------------------------------------------------------------
!                   0.00    0.XXXX    0.XXXX XXXXXX.XX -XX.XXX  ',
!    'XXXXX.XXX -XX.XXX  XXXXX.XXX -XX.XXX  -X.XX -XX.XX  -X.XX -XX.XX
!                 F8.2,     F10.4,     F10.4,   F10.2,   F8.3,  ',
!    '  F11.3,   F8.3,    F11.3,      F8.3; F7.2,  F7.2,   F7.2,  F7.2
!---------------------------------------------------------------------

      WRITE(IOUQLK,53)
53    FORMAT(3X,'  (SEC)     (LB)      (LB)      (FT)   (FT/S)    ',   &
     &'   (FT)   (FPS)       (FT)   (FPS)   (FT)  (FPS)   (FT)  (FPS)'/)

      GO TO 100




!=========================================
! HEAD PAGE FOR QUICK LOOK FORMAT NUMBER 6
!=========================================
60    CONTINUE

! BYPASS IF THIS TETHER DISPLAY IS IN-APPROPRIATE
!------------------------------------------------
! (FORMAT WORKS OK EVEN FOR NO TETHERS)
!      IF(NTETH  .EQ. 0) GO TO 100

      WRITE(IOUQLK,61) NOBJQ(1), NOBJQ(2), NTSHQ(1)
61    FORMAT(3X,   &
     &'QUICK LOOK FMT 6:   TRAJ. SUMMARY OF TOSS OBJ #',I2,' AND',I2,   &
     &                          ' ;  AND TOSS TETH #',I2/)

      WRITE(IOUQLK,62)
62    FORMAT(3X,'RP-TIME   CAP H   CAP DRG    BOOS H   BOOS HD  BOOS',   &
     &          ' MS   BOOS DRG   THRUST  CAP TEN  MACH DYNPRS  C_AL',   &
     &       'P_T  C-PITCH  C-YAW  C-ROLL    OMB_Y    OMB_Z    OMB_X')

      WRITE(IOUQLK,63)
63    FORMAT(3X,'  (SEC)    (FT)     (LB)      (FT)     (FT/S)   (LBM',   &
     &          ')      (LB)      (LB)     (LB)    NO.  (PSI)   (DEG)',   &
     &         '    (DEG)   (DEG)   (DEG)    (D/S)    (D/S)    (D/S)'/)

      GO TO 100
!----------------------------------------------------------------------------------------------------------------------------------------------------
!   1        2        3         4        5         6         7         8        9      10    11      12       13      14      15       16       17       18
!RP-TIME   CAP_H   CAP_DRG    BOOS_H   BOOS HD  BOOS MS   BOOS DRG   THRUST  CAP TEN  MACH DYNPRS  C_ALP_T  C-PITCH  C-YAW  C-ROLL    OMB_Y    OMB_Z    OMB_X
!  (SEC)    (FT)     (LB)      (FT)     (FT/S)   (LBM)      (LB)      (LB)     (LB)    NO.  (PSI)   (DEG)    (DEG)   (DEG)   (DEG)    (D/S)    (D/S)    (D/S)
!  XX.XX  XXXXXX.   XXXXXX.  XXXXXX.X  XXXX.XX   XXXXXX.   XXXXXX.  XXXXXXX. XXXXXX.X  X.XX  XXX.X  -XXX.X -XXX.XX -XXX.XX -XXX.XX    XX.XX    XX.XX    XX.XX
!  F10.2     F9.0    F10.0     F9.0     F9.2     F10.0      F11.0    F10.0      F9.1   F6.2   F7.1   F8.1     F8.2    F8.2    F8.2     F9.2     F9.2     F9.2                                                                  
!-----------------------------------------------------------------------------------------------------------------------------------------------------



!=========================================
! HEAD PAGE FOR QUICK LOOK FORMAT NUMBER 7
!=========================================
70    CONTINUE

      WRITE(IOUQLK,71) NOBJQ(1)
71    FORMAT(3X,   &
     &'QUICK LOOK FMT 7:   SPECIAL TOSS OBJ AERO FORMAT FOR OBJ',I2/)

      WRITE(IOUQLK,72)
72    FORMAT(3X,'RP-TIME    ALT     DENSITY   DYNPRS  VXRWB   VYRWB',   &
     &'   VZRWB  MACH   ALP_T    CA    CN    CM   ALP_Q   CNQ   CMQ',   &
     &'    FXDB     FYDB    FZDB   CXDB     CYDB    CZDB    GXDB   ',   &
     &'  GYDB    GZDB')

      WRITE(IOUQLK,73)
73    FORMAT(3X,' (SEC)    (FT)    (SL/FT3)   (PSF)   <-------FT/SEC',   &
     &' ----->   NO.   (DEG)                     (DEG)              ',   &
     &'  <-------- LB ------->   <------ FT-LB ------>    <------ FT',   &
     &'-LB ------>'/)


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
!     1        2         3        4       5       6       7      8      9      10    11    12     13     14    15     16       17      18     19       20      21      22       23      24
!  RP-TIME    ALT     DENSITY   DYNPRS  VXRWB   VYRWB   VZRWB  MACH   ALP_T    CA    CN    CM   ALP_Q   CNQ   CMQ    FXDB     FYDB    FZDB   CXDB     CYDB    CZDB    GXDB     GYDB    GZDB
!   (SEC)    (FT)    (SL/FT3)   (PSF)   <-------FT/SEC ----->   NO.   (DEG)                     (DEG)                <-------- LB ------->   <------ FT-LB ------>    <------ FT-LB ------>
!
!    66.00   40566.  x.xxxE-9  399.70  -1187.      0.      0.  1.23 -180.01  1.07   .00   .00     .01   .00   .00   -56221.     28.      0.      0.      0.   -334.      0.      0.   -307.
!   F10.2,   F9.0,    1PE10.3, 0PF8.2,   F8.0,   F8.0,  F8.0,  F6.2,  F8.2,  F6.2, F6.2, F6.2,  F8.2,  F6.2,  F6.2,  F10.0,   F8.0,   F8.0,    F8.0,   F8.0,   F8.0,   F8.0,   F8.0,  F8.0
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


      GO TO 100


!----------------
! STANDARD RETURN
!----------------
100   CONTINUE
      RETURN
      END
