! ROUTINE: RPAFOR
! %Z%GTOSS %M% H.10 code v01.0X
!              H.10 code v01.0X Replaced RPGIM->RPGBI (cosmetic consistency).
!----------------------------------------------------------------------------
!              H.6 code v01.00 (baseline for vH.6 delivery)
!*******************************
!*******************************
!
      SUBROUTINE RPAFOR
!
!*******************************
!*******************************
! THIS SUBROUTINE PROVIDES A TABLE LOOKUP FOR ARBITRARY BODY
! AXIS FORCES ON THE HOST OBJECT, AND PROVIDE FOR ALGORITHMIC
! DETERMINATION OF THOSE FORCES


! NOTE  THAT THE HOST EULER ANGLES AND BODY AXIS COMPONENTS OF
!       ANGULAR VEL WR/T ORB FRAME ARE AVAILABLE HERE AS....
!
!               RPPHIO, RPTHTO, RPPSIO
!
!               RPOMO(1), RPOMO(2), RPOMO(3)



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_HOST.i"
      include "../../A_HDR/EQU_HOST.i"
      include "../../A_HDR/COM_RPS.i"
      include "../../A_HDR/COM_TOSS.i"
      include "../../A_HDR/EQU_TOSS.i"





!---------------------------------------------------------------------
! SEE IF TABLE LOOKUP WANTED (THIS ALSO PROVIDES A CONSTANT FORCE OPT)
!---------------------------------------------------------------------
      IF(ARBFON .EQ. 1.) THEN

! DO SOME TABLE PARAMETER SETUP
          TABFX(1) = 0.0
          TABFY(1) = 0.0
          TABFZ(1) = 0.0

! LOOKUP VALUES FOR BODY AXIS COUPLES
          VECB(1) = SLINT(T, TABFX)
          VECB(2) = SLINT(T, TABFY)
          VECB(3) = SLINT(T, TABFZ)

      END IF



!-----------------------------------
! SEE IF FORCE ALGORITHM-A IS WANTED
!-----------------------------------
! NOTE THAT THIS CODE BLOCK CAN BE CLONED REPEATEDLY IN THIS
!      SUBROUTINE TO ADD MORE ALGORITHM TYPES

       IF(ARBFON .EQ. 2.) THEN

! DETERMINE THE LIBRATION ANGLE OF THE USER SPECIFIED TETHER
!-----------------------------------------------------------
! FIND SPECIFIED TETHERS ATTACH POINT TOPOLOGY
          JTLIB = NINT(PJTLIB)

          JOB1 = NOBJX(JTLIB)
          JOB2 = NOBJY(JTLIB)

          JATT1 = LATTX(JTLIB)
          JATT2 = LATTY(JTLIB)

! GET ATTACH POINT COORDINATES OF ONE END OF TETHER (IN INER FRAME)
          XAPS1 = APS( 7, JATT1, JOB1)
          YAPS1 = APS( 8, JATT1, JOB1)
          ZAPS1 = APS( 9, JATT1, JOB1)

! GET ATTACH POINT COORDINATES OF OTHER END OF TETHER (IN INER FRAME)
          XAPS2 = APS( 7, JATT2, JOB2)
          YAPS2 = APS( 8, JATT2, JOB2)
          ZAPS2 = APS( 9, JATT2, JOB2)

! GET POS VECTOR FROM HOST TO TOSS OBJECT (ASSUMING END 1 IS AT HOST)
          VECI(1) = XAPS2 - XAPS1
          VECI(2) = YAPS2 - YAPS1
          VECI(3) = ZAPS2 - ZAPS1

! CHECK THIS ASSUMPTION AND ADJUST IF REQUIRED
          DUM = -1.0
          IF(JOB1 .NE. 1) CALL VECSCL(DUM,VECI,   VECI)

! TRANSFORM THE RELATIVE POS VECTOR INTO HOSTS ORB FRAME
          CALL MATVEC(0, RPGOI, VECI,    VECB)

! FIND LIBRATION ANGLE ASSOCIAED WITH THIS REL POS VECTOR
! (EMPLOYEE USER SPECIFIED LIB ANGLE TYPE)
          NTPDUM = NINT(PJLTYP)
          CALL XYZLIB (NTPDUM, VECB(1),VECB(2),VECB(3),   ANGIP,ANGOP)

! NOTE  YOU CAN INTERPRET THE COMPONENTS OF THE VECTOR QUANTITY VECB
!       (AVAILABLE ABOVE) IN ANY WAY YOU WANT TO REPRESENT A VALUE FOR
!       SENSED LIBRATION. IN THIS EXAMPLE I AM MERELY SHOWING HERE HOW
!       TO USE THE CONVENTIONAL CALCULATION OF LIBRATION ANGLE AVAILABLE
!       IN TOSS.


! FINALLY, EVALUATE LIB CONTROL FORCES HERE (PUT FORCE RESULTS IN VECB)
          VECB(1) = 0.0
          VECB(2) = 0.0
          VECB(3) = 0.0

      END IF



!-----------------------------------
! SEE IF FORCE ALGORITHM-B IS WANTED
!-----------------------------------
! CLONE ALOGORITHM-A ABOVE AS STARTER FOR NEXT ALGORITHM
      IF(ARBFON .EQ. 3.) THEN

! FOR NOW, JUST STORE ZERO FORCE RESULTS
          VECB(1) = 0.0
          VECB(2) = 0.0
          VECB(3) = 0.0

      END IF



!****************************************************************
! TRANSFORM THE ABOVE DETERMINED BODY AXIS FORCES INTO INER FRAME
!****************************************************************
      CALL MATVEC(1, RPGBI, VECB,   VECI)

! INCREMENT THE INERTIAL COMPONENTS OF BODY CONTROL FORCE RPFCI(3)
      RPFCI(1) = RPFCI(1) + VECI(1)
      RPFCI(2) = RPFCI(2) + VECI(2)
      RPFCI(3) = RPFCI(3) + VECI(3)

      RETURN
      END
