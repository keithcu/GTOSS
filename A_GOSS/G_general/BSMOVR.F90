! ROUTINE: BSMOVR
! %Z%GTOSS %M% H.10 code v01.10
!              H.10 code v01.10 Added vertical motion & Scenario 2
!-----------------------------------------------------------------
!              H.10 code v01.00 (baseline for vH.10 delivery)
!*************************************
!*************************************
!
      SUBROUTINE BSMOVR
!
!*************************************
!*************************************
! THIS SUBROUTINE PROVIDES AN ASSORTMENT OF "CANNED" STATE
! PERTURBATIONS TO THE NOMINALLY FIXED LOCATION OF THE RP
! STATE WHEN IT IS DEFINED BY THE RP PLANET-FIXED OPTION.
!
! GTOSS INPUT-STREAM DRIVEN FLAGS AND DATA ALLOW THE USER TO
! SELECT FROM AN ASSORTMENT OF CANNED SCENARIOS EACH OF WHICH
! PROVIDES CERTAIN TYPES OF STATE PERTURBATIONS. SUPPLEMENTARY
! INPUT DATA ITEMS (PECULIAR TO EACH SCENARIO) ARE EMPLOYED BY
! EACH SCENARIO TO DESCRIBE THE EXTENT AND NATURE OF THE
! SUBSEQUENTLY GENERATED PERTURBATION.
!
! THE PERTURBATION STATES ARE PROVIDED RELATIVE TO A NOMINAL
! USER-DEFINED REFERENCE PLANET FIXED POINT (LAT/LONG/ALT), AND ARE
! AXIS-REFERENCED TO THE LOCAL TOPOCENTRIC FRAME DEFINED BY THIS
! ORIGINAL USER SPECIFICATION OF RP FIXED LATITIDE AND LONGITUDE.
!
! FURTHERMORE, THIS PROGRAM PROVIDES HOOKS FOR USER DEFINED
! STATE PERTURBATION ROUTINES, THE INVOCATION OF WHICH PRECLUDES
! BSMOVR FROM USING ITS OWN PROVIDED CANNED PERTURBATION SCENARIOS.
! A USER-DEFINED PROGRAM MUST PROVIDE THE POSITION STATE POSITION
! PERTURBATIONS AND A CONSISTENT SET OF 1ST AND 2ND DERVIATIVES
! (THESE ARE NEEDED BY PLNFIX TO CREATE A NEWTONIANLY-CONSISTENT
! RP STATE FOR THE REST OF TOSS DYNAMICS CALCULATIONS)
!
! BSMOVR "BUILT-IN" SCENARIOS ARE INVOKED VIA SELECTION NO'S.  1 THRU 20.
! "USER PROVIDED"   SCENARIOS ARE INVOKED VIA SELECTION NO'S. 20 THRU 40.
!
! THE BUILT IN SCENARIOS ARE:
!
! BASE-MOTION
!  SCENARIO#    DESCRIPTION OF RESULTING MOTION SCENARIO
!  ---------    ----------------------------------------
!     1         DATA-DRIVEN, 1-COS SURFACE-DISPLACEMENT +VERTICAL DISPL
!
!     2         SIMULATES SEA-PLATFORM PERFORMANCE
!
!     3         ......UNASSIGNED........
!     4         ......UNASSIGNED........
!     5         ......UNASSIGNED........
!
!
!====================================================================
!====================================================================
!
! NOTE: ALL SCENARIOS MUST START WITH ZERO PERTURBATIONS
!                       ----------      ------------------
! NOTE: DUE TO THE LOGICAL STRUCTURE OF THE REF PT STATE ADVANCEMENT
!       SCHEME, THIS CODE WILL BE VISITED TWICE PER INTEGRATION CYCLE.
!       THUS ANY SUMMATION OR SUB-INTEGRATION PROCESSES CONTAINED
!       EITHER BUILT-IN OR USER SUPPLIED SCENARIOS MUST BE AWARE OF
!       THIS AND COMPENSATE ACCORDINGLY. THIS IS USUALLY DONE BY USING
!       TIME EXPLICITLY TO PERFORM INTEGRATIONS, IE. MULTIPLING
!       DERIVATIVES BY TIME DIFFERENCES, RATHER THAN AN INTEGRATION
!       INTERVAL TO PERFORM A SUMMATION PROCESS.
!
! NOTE: ALL SCENARIOS MUST RETURN THE FOLLOWING VALUES IN HOST COMMON
!
!       POSITION PERTURBATION (TOPO FRAME):   BSPER(1) = TOPO X COMP
!                                             BSPER(2) = TOPO Y COMP
!                                             BSPER(3) = TOPO Z COMP
!
!       POSITION COMP 1ST DERIV (TOPO FRAME): BSPERD(1) = TOPO X COMP
!                                             BSPERD(2) = TOPO Y COMP
!                                             BSPERD(3) = TOPO Z COMP
!
!       POSITION COMP 2ND DERIV (TOPO FRAME): BSPERDD(1) = TOPO X COMP
!                                             BSPERDD(2) = TOPO Y COMP
!                                             BSPERDD(3) = TOPO Z COMP
!
!====================================================================
!====================================================================



      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_HOST.i"
      include "../../A_HDR/EQU_HOST.i"
      include "../../A_HDR/COM_RPS.i"





! ZERO ALL DISPLACEMENTS (THIS WILL KEEP SCENARIOS THAT WOULD
! BASE MOTION STATE VARIABLES AS INTEGRATION ACCUMULATORS) TO
! TAKE DUE-NOTICE....NOTE ADMONITION IN PLNFIX AND BSMOVR.
!---------------------------------------------------------
!      DO 1 J = 1,3
!         BSPER(J) = 0.0
!         BSPERD(J) = 0.0
!         BSPERDD(J) = 0.0
!1     CONTINUE

!-----------------------------------------
! VISIT USER SELECTED BASE MOTION SCENARIO
!-----------------------------------------
      IF(NINT(PBSPER) .EQ. 1) GO TO 100
      IF(NINT(PBSPER) .EQ. 2) GO TO 200
      IF(NINT(PBSPER) .EQ. 3) GO TO 300
      IF(NINT(PBSPER) .EQ. 4) GO TO 400
      IF(NINT(PBSPER) .EQ. 5) GO TO 500

!      IF(NINT(PBSPER) .EQ. 20) CALL USCN20
!      IF(NINT(PBSPER) .EQ. 21) CALL USCN21
!      IF(NINT(PBSPER) .EQ. 22) CALL USCN22
!      IF(NINT(PBSPER) .EQ. 23) CALL USCN23

! TO TO STANDARD RETURN
      GO TO 1000


!***********************************************************
!***********************************************************
! GTOSS SCENARIO 1: MULTIPLE A*(1-COS) WAVES & DWELL PERIODS
!***********************************************************
!***********************************************************
! A FULLY DATA-DRIVEN 1-COSINE SURFACE-DISPLACEMENT SCENARIO
! ALLOWING PARTIAL OR MULTIPLE CYCLES WITH SPECIFIABLE DWELL
! TIMES, AND PEAK DISPLACEMENTS IN MOTION OCCURING IN POSSIBLY
! OPPOSITE DIRECTIONS ALONG A SPECIFIED AZIMUTH. A VERTICAL
! DISPLACEMENT IS ALSO BE DEFINED AS SIMPLE SINE WAVE-ACTION
!
! DEFINITION OF VARIBLES FOR SCENARIO 1
!
! INPUT ITEM    DESCRIPTION
! ----------    -----------
!----------------------------------------------------------------------
!  S1START   START TIME FOR SCENARIO (SEC)
!
!  S1STOP    STOP TIME (NEG MEANS DO 1 FULL CYCLE ONLY, 0 MEANS REPEAT)
!
!  S1AZSCN   AZI WR/T TRUE N ALONG-WHICH + BASE MOTION PROCEEDS (DEG)
!----------------------------------------------------------------------
!  S1PER1    1ST WAVE RISE-TIME (SEC)
!
!  S1RAT1    1ST WAVE (ABS VALUE OF) MAX TRANSL RATE (FT/SEC)
!
!  S1ACC1    1ST WAVE (ABS VALUE OF) MAX TRANSL ACCELERATION (FT/SEC2)
!
!  S1TDL1    1ST WAVE DWELL-DURATION AT MAX AMPLITUDE (SECS)
!
!  S1AMP1    1ST WAVE MAX AMPLITUDE (FT)
!----------------------------------------------------------------------
!  S1DEAD    TIME TO DWELL BETWEEN COSINE WAVES (INCREMENTAL SECS)
!----------------------------------------------------------------------
!  S1PER2    2ND WAVE RISE-TIME (SEC)
!
!  S1RAT2    2ND WAVE (ABS VALUE OF) MAX TRANSL RATE (FT/SEC)
!
!  S1ACC2    2ND WAVE (ABS VALUE OF) MAX TRANSL ACCELERATION (FT/SEC2)
!
!  S1TDL2    2ND WAVE DWELL-DURATION AT MAX AMPLITUDE (SECS)
!
!  S1AMP2    2ND WAVE MAX AMPLITUDE (FT)
!----------------------------------------------------------------------
!  S1VPER    SIGN WAVE PERIOD (SEC)  VERTICAL-DISPLACEMENT
!
!  S1VAMP    SIGN WAVE PEAK-TO-PEAK AMPLITUDE (FT)
!----------------------------------------------------------------------
100   CONTINUE

!-----------------------------------------------------------------
! FIRST, MAKE EVALUATION OF SIMPLE SIN-WAVE VERTICAL DISPLACEMENTS
!-----------------------------------------------------------------
! FIRST, TEST VERTICAL DISPLACEMENT USER INPUT VALIDITY
      DUMTST = ABS(S1VAMP) + ABS(S1VPER)

      IF(DUMTST .NE. 0.0) THEN
          IF((S1VAMP .EQ. 0.0) .OR. (S1VPER .EQ. 0.0)) THEN
               WRITE(IOUERR,23)
 23       FORMAT(' FATAL: PATHOLOGICAL USER SPECIFICATION OF RP BASE',   &
     &           '  VERTICAL MOTION. MUST HAVE NON-ZERO ATTRIBUTES',   &
     &           '  BOTH COMPOENTS OF SIN-WAVE')
               STOP 'IN SUBR: BSMOVR-1'
          END IF


! EVALUATE VERTICAL DISPLACEMENT INGREDIENTS
!-------------------------------------------
          DUMCIRC = 2.0*PIIALL/S1VPER
          DUMVARG = DUMCIRC*T
          DUMSINV = SIN(DUMVARG)
          DUMCOSV = COS(DUMVARG)

! SET IN THE RESULTANT FINAL VERTICAL BASE MOTION PERTURBATION VALUES
          BSPER(3)   =  S1VAMP*DUMSINV
          BSPERD(3)  =  S1VAMP*DUMCOSV* DUMCIRC
          BSPERDD(3) = -S1VAMP*DUMSINV*(DUMCIRC**2)

      END IF



!---------------------------
! DO START/STOP DELIBERTIONS
!---------------------------
! DON'T EXECUTE UNTIL ABSOLUTE START TIME
      IF(T .LE. S1START) GO TO 1000

! SET DERIVS = 0 IN CASE SCENARIO STOPS OR VALUES FREEZE
      BSPERD(1) = 0.0
      BSPERD(2) = 0.0

      BSPERDD(1) = 0.0
      BSPERDD(2) = 0.0

! CHECK FOR A STOP ON ABSOLUTE TIME
      IF((S1STOP .GT. 0.0) .AND. (T .GE. S1STOP)) GO TO 1000

!-------------------------------------------------------------------
! CALC TRIG ARG COEF USING "RISE-TIME", "PEAK RATE" OR, "PEAK ACCEL"
!-------------------------------------------------------------------
! CHECK FOR A STOP AFTER A SINGLE CYCLE BEFORE PROCEEDING
      IF((S1STOP .LT. 0.0) .AND. (S1CYFL .GT. 0.0)) GO TO 1000


! CHECK FOR GENERAL PATHOLOGICAL USER SPECIFICATIONS BEFORE PROCEEDING
!---------------------------------------------------------------------
! TEST 1-COS VALIDITY
      DUMTST = ABS(S1PER1) + ABS(S1RAT1) + ABS(S1ACC1) + ABS(S1AMP1)

      IF(DUMTST .EQ. 0.0) THEN
          WRITE(IOUERR,22)
 22       FORMAT(' FATAL: PATHOLOGICAL USER SPECIFICATION OF RP BASE',   &
     &           '  MOTION. MUST HAVE NON-ZERO ATTRIBUTES FOR FIRST',   &
     &           '  COSINE-WAVE')
          STOP 'IN SUBR: BSMOVR-2'
      END IF


! 1ST WAVE DETERMINATION OF COEF AND RISE TIME
!*********************************************
! FIRST CHECK SPECIFICALLY FOR 1ST WAVE FOR PATHOLOGICAL RISE TIME DEFN
      DUMTST = ABS(S1PER1) + ABS(S1RAT1) + ABS(S1ACC1)

      IF(DUMTST .EQ. 0.0) THEN
          WRITE(IOUERR,11)
 11       FORMAT(' FATAL: FOR RP BASE MOTION PERTURBATIONS;',   &
     &    ' USER MUST SPECIFY EITHER PERIOD, PEAK RATE, OR, ',   &
     &    ' PEAK ACCEL AS NON-ZERO, FOR 1ST WAVE')
          STOP 'IN SUBR: BSMOVR-3'
      END IF

! PROCEED ON AS THERE IS NON-NULL SPECIFICATION
      IF(S1PER1 .EQ. 0.0) THEN

! TRY RATE SPECIFICATION
           IF(S1RAT1 .NE. 0.0) THEN

! USE MAX RATE TO CALC COS ARG COEF & RISE-TIME DURATION
                DUMCO1  = ABS(2.0 * S1RAT1/S1AMP1)
                DUMTRZ1 = PIIALL/DUMCO1
           ELSE

! USE MAX ACCEL TO CALC COS ARG COEF & RISE-TIME DURATION
                DUMCO1  = SQRT(ABS(2.0 * S1ACC1/S1AMP1))
                DUMTRZ1 = PIIALL/DUMCO1
           END IF

      ELSE

! USE COS HALF-PERIOD TO CALC COS ARG COEFFS AND RISE-TIME DURATION
            DUMCO1  = PIIALL/S1PER1
            DUMTRZ1 = S1PER1
      END IF


! 2ND WAVE DETERMINATION OF COEF AND RISE TIME
!*********************************************
! IF 2ND WAVE RISE TIME DATA IS NULL, DON'T ATTEMPT CALCULATIONS
      DUMTST2 = ABS(S1PER2) + ABS(S1RAT2) + ABS(S1ACC2)

      IF(DUMTST2 .EQ. 0.0) GO TO 111

! PROCEED ON AS THERE IS NON-NULL SPECIFICATION FOR 2ND WAVE
      IF(S1PER2 .EQ. 0.0) THEN

! TRY RATE SPECIFICATION
           IF(S1RAT2 .NE. 0.0) THEN

! USE MAX RATE TO CALC COS ARG COEF & RISE-TIME DURATION
                DUMCO2  = ABS(2.0 * S1RAT2/S1AMP2)
                DUMTRZ2 = PIIALL/DUMCO2
           ELSE

! USE MAX ACCEL TO CALC COS ARG COEF & RISE-TIME DURATION
                DUMCO2  = SQRT(ABS(2.0 * S1ACC2/S1AMP2))
                DUMTRZ2 = PIIALL/DUMCO2
           END IF

      ELSE

! USE COS HALF-PERIOD TO CALC COS ARG COEFFS AND RISE-TIME DURATION
            DUMCO2  = PIIALL/S1PER2
            DUMTRZ2 = S1PER2
      END IF


!------------------------------------------------------------
! DETERMINE VARIOUS CHARACTERISTIC TIMES RELATED TO EXECUTION
!------------------------------------------------------------
111   CONTINUE

! TIMES ASSOCIATED WITH 1ST PEAK'S CYCLE
      S1T1 = DUMTRZ1
      S1T2 = DUMTRZ1 + S1TDL1
      S1T3 = DUMTRZ1 + S1TDL1 + DUMTRZ1
      S1T4 = DUMTRZ1 + S1TDL1 + DUMTRZ1 + S1DEAD

! TIMES ASSOCIATED WITH 2ND PEAK'S CYCLE
      S1T5 = S1T4 + DUMTRZ2
      S1T6 = S1T4 + DUMTRZ2 + S1TDL2
      S1T7 = S1T4 + DUMTRZ2 + S1TDL2 + DUMTRZ2
      S1T8 = S1T4 + DUMTRZ2 + S1TDL2 + DUMTRZ2 + S1DEAD

! IF 2ND PEAK DWELL AMPLITUDE =0, SET UP POSSIBLE REPEAT OF 1ST CYCLE ONLY
      IF(S1AMP2 .EQ. 0.0) THEN
           S1T5 = S1T4
           S1T6 = S1T4
           S1T7 = S1T4
           S1T8 = S1T4
      END IF


! CALC REDUCED-TIME INTO THE SCENARIO EXECUTION
!----------------------------------------------
      TRDUM = T - S1START - S1CYFL*S1T8



!---------------------------------------------------------------
! REDUCE & TRIAGE TIME TO FIND WHERE IT RESIDES IN OVERALL CYCLE
!---------------------------------------------------------------

! LET PRESENT VALUES PERSIST IF TIME LIES IN ANY CONSTANT-VALUE REGION
!---------------------------------------------------------------------
      IF((TRDUM .GE. S1T1) .AND. (TRDUM .LE. S1T2)) GO TO 1000
      IF((TRDUM .GE. S1T3) .AND. (TRDUM .LE. S1T4)) GO TO 1000
      IF((TRDUM .GE. S1T5) .AND. (TRDUM .LE. S1T6)) GO TO 1000
      IF((TRDUM .GE. S1T7) .AND. (TRDUM .LT. S1T8)) GO TO 1000

! LAST DEFINED REGION IS EXCEEDED, SO DO RE-CYCLE DELIBERATIONS
      IF(TRDUM .GE. S1T8) THEN
            S1CYFL = S1CYFL + 1.0
            GO TO 1000
       END IF


!---------------------------------------------------------------
! OK, REDUCED TIME MUST LIE IN A COSINE REGION, FIND THAT REGION
!---------------------------------------------------------------

! CHECK 1ST & 2ND HALF OF -> FIRST COSINE REGION
!                            -------------------
!-----------------------------------------------
      IF((TRDUM .GE. 0.0) .AND. (TRDUM .LE. S1T1)) THEN

! FIND ARGUMENT-TIME, TRIG COEFF, & AMPLITUDE FOR THIS REGION COSINE
          TCDUM = TRDUM
          DUMAMP = S1AMP1
          DUMCO = DUMCO1
      END IF


      IF((TRDUM .GE. S1T2) .AND. (TRDUM .LE. S1T3)) THEN

! FIND ARGUMENT-TIME, TRIG COEFF, & AMPLITUDE FOR THIS REGION COSINE
          TCDUM = TRDUM - S1TDL1
          DUMAMP = S1AMP1
          DUMCO = DUMCO1
      END IF


!--------------------------------------------------
! DON'T DO 2ND COSINE WAVE IF CRITICAL DATA IS NULL
!--------------------------------------------------
      IF(DUMTST2 .NE. 0.0) THEN

! CHECK 1ST & 2ND HALF OF -> SECOND COSINE REGION
!                            --------------------
!------------------------------------------------
          IF((TRDUM .GE. S1T4) .AND. (TRDUM .LE. S1T5)) THEN

! FIND ARGUMENT-TIME, TRIG COEFF, & AMPLITUDE FOR THIS REGION COSINE
             TCDUM = TRDUM - S1T4
             DUMAMP = S1AMP2
             DUMCO = DUMCO2
          END IF


         IF((TRDUM .GE. S1T6) .AND. (TRDUM .LE. S1T7)) THEN

! FIND ARGUMENT-TIME, TRIG COEFF, & AMPLITUDE FOR THIS REGION COSINE
             TCDUM = TRDUM - S1T4 - S1TDL2
             DUMAMP = S1AMP2
             DUMCO = DUMCO2
          END IF

      END IF


! CALCULATE TRIG TERMS COMMON TO 1ST OR 2ND WAVE
!-----------------------------------------------
      ARGDUM = DUMCO * TCDUM
      DUMA   = 0.5 * DUMAMP

      COSDUM = COS(ARGDUM)
      SINDUM = SIN(ARGDUM)

! FIND DISPLACEMENT ALONG THE SPECIFIED AZIMUTH
      DUMDIS = (1.0 - COSDUM) * DUMA

! FIND DISPLACEMENT-RATE ALONG THE SPECIFIED AZIMUTH
      DUMRAT =        SINDUM  * DUMA * DUMCO

! FIND DISPLACEMENT-ACCELERATION ALONG THE SPECIFIED AZIMUTH
      DUMACC =        COSDUM  * DUMA * DUMCO**2

! RESOLVE AZIMTUH STATES INTO TOPO-FRAME STATES
!----------------------------------------------
      CAZDUM = COS(S1AZSCN*DTRALL)
      SAZDUM = SIN(S1AZSCN*DTRALL)


! SET IN THE RESULTANT FINAL BASE MOTION PERTURBATION VALUES
!-----------------------------------------------------------
      BSPER(1) = DUMDIS*CAZDUM
      BSPER(2) = DUMDIS*SAZDUM

      BSPERD(1) = DUMRAT*CAZDUM
      BSPERD(2) = DUMRAT*SAZDUM

      BSPERDD(1) = DUMACC*CAZDUM
      BSPERDD(2) = DUMACC*SAZDUM

! GO TO STANDARD RETURN
      GO TO 1000





!***********************************************************
!***********************************************************
! GTOSS SCENARIO 2: SEA PLATFORM PERFORMANCE MODEL
!***********************************************************
!***********************************************************
! THIS SCENARIO IS A SIMPLE MODEL OF SEA PLATFORM PEFORMANCE
! BASED ON USER INPUT DEFINITION OF THREE INDEPENDENT PLATFORM
! PERFORMANCE RELATED PARAMETERS:
!
!        1. MAXIMUM ATTAINABLE SPEED
!        2. TIME TO REACH THIS MAXIMUM SPEED
!        3. PLATFORM MASS
!
! WITH THESE THREE PARAMETERS, AND ASSUMING THAT:
!        A. MOTIVE THRUST IS CONSTANT IN MAGNITUDE (+/- OR ZERO)
!        B. THRUST EQUALS HYDRODYNAMIC DRAG AT MAX SPEED
!        C. HYDRODYNAMIC DRAG VARIES AS THE SQUARE OF THE SPEED
!
! IT IS POSSIBLE TO UNIQUELY DETERMINE THE MAXIMUM ACCELERATION
! CAPABILITY OF THE PLATFORM AS A FUNCTION OF SPEED, AND THE DRAG.
! COEFFICIENT. THIS THEN ALLOWS THE USER TO SPECIFY A SEQUENCE OF "TIMED"
! MAXIMUM PERFORMANCE THRUSTING MANEUVERS INTERPSERSED WITH COASTING
! MANEUVERS (IE. THRUST  = 0, BUT HYDRODYNAMICS DRAG IN EFFECT) TO
! ACHIEVE DESIRED BASE MOTIONS THAT COULD BE SIMPLISTICALLY \
! REPRESENTATIVE OF A SEA PLATFORM.
!
! DEFINITION OF VARIBLES FOR SCENARIO 2
!
! INPUT ITEM    DESCRIPTION
! ----------    -----------
!----------------------------------------------------------------------
!  S2START   START TIME FOR SCENARIO (SEC)
!
!  S2STOP    STOP TIME AT WHICH A MAX-THRUST ARREST MANEUVER INITIATES
!
!  S2AZSCN   AZI WR/T TRUE N ALONG-WHICH +BASE MOTION PROCEEDS (DEG)
!----------------------------------------------------------------------
!  S2VPER    SIGN WAVE PERIOD (SEC)  VERTICAL-DISPLACEMENT
!
!  S2VAMP    SIGN WAVE PEAK-TO-PEAK AMPLITUDE (FT)
!----------------------------------------------------------------------
!  S2BASMS   MASS OF PLATFORM (SLUGS)
!  S2MAXSP   MAXIMUM SPEED FOR PLATFORM (FPS)
!  S2MAXTM   TIME TO REACH MAXIMUM SPEED FOR PLATFORM (SEC)
!  S2CDOVR   GROSS DRAG COEFF (IGNORED IF = 0.0, OTHERWISE CALC HERE)
!----------------------------------------------------------------------
!  S2MXPDT1  1ST MAX PERF PERIOD (+/- SEC TO MOVE IN +/- AZIMUTH DIR)
!  S2COSDT1  1ST COAST PERIOD (SEC)
!
!  S2MXPDT2  2ND MAX PERF PERIOD (+/- SEC TO MOVE IN +/- AZIMUTH DIR)
!  S2COSDT2  2ND COAST PERIOD (SEC)
!
!  S2MXPDT3  3RD MAX PERF PERIOD (+/- SEC TO MOVE IN +/- AZIMUTH DIR)
!  S2COSDT3  3RD COAST PERIOD (SEC)
!
!  S2MXPDT4  4TH MAX PERF PERIOD (+/- SEC TO MOVE IN +/- AZIMUTH DIR)
!  S2COSDT4  4TH COAST PERIOD (SEC)
!
!  S2MXPDT5  5TH MAX PERF PERIOD (+/- SEC TO MOVE IN +/- AZIMUTH DIR)
!  S2COSDT5  5TH COAST PERIOD (SEC)
!
!  S2MXPDT6  6TH MAX PERF PERIOD (+/- SEC TO MOVE IN +/- AZIMUTH DIR)
!  S2COSDT6  6TH COAST PERIOD (SEC)
!
!  S2MXPDT7  7TH MAX PERF PERIOD (+/- SEC TO MOVE IN +/- AZIMUTH DIR)
!  S2COSDT7  7TH COAST PERIOD (SEC)
!
!  S2MXPDT8  8TH MAX PERF PERIOD (+/- SEC TO MOVE IN +/- AZIMUTH DIR)
!  S2COSDT8  8TH COAST PERIOD (SEC)
!----------------------------------------------------------------------

! START SCENARIO 2 OPERATIONS
!----------------------------
200   CONTINUE


!-----------------------------------------------------------------
! FIRST, MAKE EVALUATION OF SIMPLE SIN-WAVE VERTICAL DISPLACEMENTS
!-----------------------------------------------------------------
! FIRST, TEST VERTICAL DISPLACEMENT USER INPUT VALIDITY
      DUMTST = ABS(S2VAMP) + ABS(S2VPER)

      IF(DUMTST .NE. 0.0) THEN
          IF((S2VAMP .EQ. 0.0) .OR. (S2VPER .EQ. 0.0)) THEN
               WRITE(IOUERR,23)
               STOP 'IN SUBR: BSMOVR-4'
          END IF

! EVALUATE VERTICAL DISPLACEMENT INGREDIENTS
!-------------------------------------------
          DUMCIRC = 2.0*PIIALL/S2VPER
          DUMVARG = DUMCIRC*T
          DUMSINV = SIN(DUMVARG)
          DUMCOSV = COS(DUMVARG)

! SET IN THE RESULTANT FINAL VERTICAL BASE MOTION PERTURBATION VALUES
          BSPER(3)   =  S2VAMP*DUMSINV
          BSPERD(3)  =  S2VAMP*DUMCOSV* DUMCIRC
          BSPERDD(3) = -S2VAMP*DUMSINV*(DUMCIRC**2)

      END IF


!---------------------------
! DO START/STOP DELIBERTIONS
!---------------------------
! DON'T EXECUTE UNTIL ABSOLUTE START TIME
      IF(T .LE. S2START) GO TO 1000

! SET DERIVS = 0 IN CASE SCENARIO STOPS OR VALUES FREEZE
!      BSPERD(1) = 0.0
!      BSPERD(2) = 0.0

!      BSPERDD(1) = 0.0
!      BSPERDD(2) = 0.0


!------------------------------------------------------
! START EVALUATION OF HORIZONTAL MANEUVER DISPLACEMENTS
!------------------------------------------------------
! FIRST, TEST USER PLATFORM INPUT VALIDITY
          IF((S2BASMS .EQ. 0.0) .OR. (S2MAXSP .EQ. 0.0)   &
     &              .OR. (S2MAXTM .EQ. 0.0)) THEN
               WRITE(IOUERR,26)
 26       FORMAT(' FATAL: PATHOLOGICAL USER SPECIFICATION OF BASE',   &
     &           ' PLATFORM PERFORMANCE. MUST HAVE NON-ZERO',   &
     &           ' ATTRIBUTES FOR ALL THREE PLATFORM PARAMETERS',   &
     &           ' FOR MOTION SCENARIO 2')
               STOP 'IN SUBR: BSMOVR-5'
          END IF


! THEN CALCULATE PLATFORM PERFORMANCE FROM USER INPUTS
!-----------------------------------------------------
! NOTE: THESE PERFORMANCE CALCULATIONS ARE MORE OR LESS
!       BASED ON INTUITIVE/ANALTYICAL OBSERVATIONS. THIS
!       RESULTS IN VALUES FOR THRUST AND INITIAL ACCEL
!       THAT MAY OR MAY-NOT ALWAYS REFLECT THE TRUE
!       PERFORMANCE OF THE SEA PLATFORM. THE USER CAN
!       PREEMPT THE DRAG COEFFICIENT, WHICH IN TURN
!       PREEMPTS THE THRUST VALUE. THIS ALLOWS THE USER
!       TO TUNE PERFORMANCE TO BE CONSISTENT WITH THE
!-----------------------------------------------------
! INITIAL ACCELERATION (AT SPEED =0)
      DUMXDDO = 1.5*S2MAXSP/S2MAXTM

! HYDRODYNAMIC DRAG COEFFICIENT (ALLOWING USER TO PREEMEPT)
      DUMDCOEF = S2BASMS*DUMXDDO/S2MAXSP**2

      IF(S2CDOVR .LT. 0.0) DUMDCOEF = ABS(S2CDOVR)*DUMDCOEF
      IF(S2CDOVR .GT. 0.0) DUMDCOEF = S2CDOVR

! MAXIMUM THRUST FROM PROPELLERS
      DUMTHRMX = DUMDCOEF*S2MAXSP**2

!----------------------------------
! CHECK FOR A STOP ON ABSOLUTE TIME
!----------------------------------
      IF((S2STOP .GT. 0.0) .AND. (T .GE. S2STOP)) THEN

! THE STOP EVENT IS A THRUSTING MANEUVER TO BRING PLATFORM TO STOP
!-----------------------------------------------------------------
          IF(ABS(BS2XD) .GT. 0.01) THEN

! COMMENCE A STOPPING THRUST CONTROL LOGIC
! (DEFINE A PROPTIONAL SPEED CONTROL THRESHOLD)
                DUMPROSP = 0.1*S2MAXSP

! CHECK FOR PROPORTIONAL SPEED CONTROL DEADBAND
                IF(ABS(BS2XD) .GT. DUMPROSP) THEN
                   DUMTMOD = -BS2XD/ABS(BS2XD)
                ELSE
                   DUMTMOD = -BS2XD/DUMPROSP
                END IF
          END IF

          GO TO 210

      END IF


!----------------------------------------------
! SORT OUT THE USER DEFINED SCENARIO SEQUENCING
!----------------------------------------------
! FIRST, SET THRUST MODULATOR TO ASSUME COASTING PERIOD
      DUMTMOD = 0.0


! TEST 1ST PERIOD ACTIVITY
      DUMT1X = ABS(S2MXPDT1)
      DUMT1C = DUMT1X + ABS(S2COSDT1)

      IF(T .LT. DUMT1X) THEN
          DUMTMOD = 1.0
          IF(S2MXPDT1 .LT. 0.0) DUMTMOD = -1.0
          GO TO 210
      ELSE
         IF(T .LT. DUMT1C) GO TO 210
      END IF

! TEST 2ND PERIOD ACTIVITY
      DUMT2X = DUMT1C + ABS(S2MXPDT2)
      DUMT2C = DUMT2X + ABS(S2COSDT2)

      IF(T .LT. DUMT2X) THEN
          DUMTMOD = 1.0
          IF(S2MXPDT2 .LT. 0.0) DUMTMOD = -1.0
          GO TO 210
      ELSE
         IF(T .LT. DUMT2C) GO TO 210
      END IF

! TEST 3ND PERIOD ACTIVITY
      DUMT3X = DUMT2C + ABS(S2MXPDT3)
      DUMT3C = DUMT3X + ABS(S2COSDT3)

      IF(T .LT. DUMT3X) THEN
          DUMTMOD = 1.0
          IF(S2MXPDT3 .LT. 0.0) DUMTMOD = -1.0
          GO TO 210
      ELSE
         IF(T .LT. DUMT3C) GO TO 210
      END IF

! TEST 4TH PERIOD ACTIVITY
      DUMT4X = DUMT3C + ABS(S2MXPDT4)
      DUMT4C = DUMT4X + ABS(S2COSDT4)

      IF(T .LT. DUMT4X) THEN
          DUMTMOD = 1.0
          IF(S2MXPDT4 .LT. 0.0) DUMTMOD = -1.0
          GO TO 210
      ELSE
         IF(T .LT. DUMT4C) GO TO 210
      END IF

! TEST 5TH PERIOD ACTIVITY
      DUMT5X = DUMT4C + ABS(S2MXPDT5)
      DUMT5C = DUMT5X + ABS(S2COSDT5)

      IF(T .LT. DUMT5X) THEN
          DUMTMOD = 1.0
          IF(S2MXPDT5 .LT. 0.0) DUMTMOD = -1.0
          GO TO 210
      ELSE
         IF(T .LT. DUMT5C) GO TO 210
      END IF

! TEST 6TH PERIOD ACTIVITY
      DUMT6X = DUMT5C + ABS(S2MXPDT6)
      DUMT6C = DUMT6X + ABS(S2COSDT6)

      IF(T .LT. DUMT6X) THEN
          DUMTMOD = 1.0
          IF(S2MXPDT6 .LT. 0.0) DUMTMOD = -1.0
          GO TO 210
      ELSE
         IF(T .LT. DUMT6C) GO TO 210
      END IF

! TEST 7TH PERIOD ACTIVITY
      DUMT7X = DUMT6C + ABS(S2MXPDT7)
      DUMT7C = DUMT7X + ABS(S2COSDT7)

      IF(T .LT. DUMT7X) THEN
          DUMTMOD = 1.0
          IF(S2MXPDT7 .LT. 0.0) DUMTMOD = -1.0
          GO TO 210
      ELSE
         IF(T .LT. DUMT7C) GO TO 210
      END IF

! TEST 8TH PERIOD ACTIVITY
      DUMT8X = DUMT7C + ABS(S2MXPDT8)
      DUMT8C = DUMT8X + ABS(S2COSDT8)

      IF(T .LT. DUMT8X) THEN
          DUMTMOD = 1.0
          IF(S2MXPDT8 .LT. 0.0) DUMTMOD = -1.0
          GO TO 210
      ELSE
         IF(T .LT. DUMT8C) GO TO 210
      END IF


!-----------------------------------------------------
! EVALUATE ACCELERATION OF THE PLATFORM, AND INTEGRATE
!-----------------------------------------------------
210    CONTINUE

! FIRST DETERMINE THRUST LEVEL AND DIRECTION
       DUMTHR = DUMTMOD*DUMTHRMX

! DETERMINE DRAG LEVEL AND DIRECTION
       DUMDRG = 0.0
       IF(BS2XD .NE. 0.0) THEN
           DUMDRG = (BS2XD/ABS(BS2XD))*DUMDCOEF*BS2XD**2
       END IF

! NOW EVALUATE ACCELERATION
       BS2XDD = ( DUMTHR - DUMDRG)/S2BASMS

! ADVANCE ACCELERATION TO SPEED
       BS2XD = BS2XD + DELTAT*BS2XDD

! ADVANCE SPEED TO DISPLACEMENT
       BS2X = BS2X   + DELTAT*BS2XD


! RESOLVE PLATFORM DISPLACEMENT AZIMUTH STATES INTO TOPO-FRAME
!-------------------------------------------------------------
      CAZDUM = COS(S2AZSCN*DTRALL)
      SAZDUM = SIN(S2AZSCN*DTRALL)

! SET IN THE RESULTANT FINAL BASE MOTION PERTURBATION VALUES
      BSPER(1)   = BS2X*CAZDUM
      BSPER(2)   = BS2X*SAZDUM

      BSPERD(1)  = BS2XD*CAZDUM
      BSPERD(2)  = BS2XD*SAZDUM

      BSPERDD(1) = BS2XDD*CAZDUM
      BSPERDD(2) = BS2XDD*SAZDUM

! GO TO STANDARD RETURN
      GO TO 1000




!*********************************
!*********************************
! GTOSS-SUPPLIED SCENARIO NUMBER 3
!*********************************
!*********************************
300   CONTINUE

! GO TO STANDARD RETURN
      GO TO 1000


!*********************************
!*********************************
! GTOSS-SUPPLIED SCENARIO NUMBER 4
!*********************************
!*********************************
400   CONTINUE

! GO TO STANDARD RETURN
      GO TO 1000


!*********************************
!*********************************
! GTOSS-SUPPLIED SCENARIO NUMBER 5
!*********************************
!*********************************
500   CONTINUE

! GO TO STANDARD RETURN
      GO TO 1000



! STANDARD RETURN
1000  CONTINUE

      RETURN
      END
