! ROUTINE: YFMT54
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!******************************************************************
!******************************************************************
!******************************************************************
              SUBROUTINE YFMT54 (JFUNC)
!******************************************************************
!******************************************************************
!******************************************************************
! THIS IS AN AVAILABLE UTOSS EXPANSION FORMAT STUBB
! SEE FORMAT 50 FOR STRUCTURAL DETAILS


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_UOSS.i"




      WRITE(IUERR,1011)
1011  FORMAT(' YFMT54: EXPORT FORMAT 54 NOT IMPLEMENTED BY USER')
      IF(JFUNC .GE. 0 ) STOP ' IN YFMT54-1'

      RETURN

      END
