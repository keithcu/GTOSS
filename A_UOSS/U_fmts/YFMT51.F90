! ROUTINE: YFMT51
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!******************************************************************
!******************************************************************
!******************************************************************
              SUBROUTINE YFMT51 (JFUNC)
!******************************************************************
!******************************************************************
!******************************************************************
! THIS IS AN AVAILABLE UTOSS EXPANSION FORMAT (IN TEMPLATE FORM)
! USER SUPPLIED EXPORT/IMPORT FORMATS WOULD START HERE


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_UOSS.i"




!---------------------------------
!---------------------------------
! DETERMINE FUNCTION OF THIS VISIT
!---------------------------------
!---------------------------------
! EXPORT MODE: INITIALIZATION AND HEADER WRITE
      IF(JFUNC .EQ. 1) GO TO 1000

! EXPORT MODE: DATA DEFINITION AND WRITING
      IF(JFUNC .EQ. 2) GO TO 2000

! IMPORT MODE: INITIALIZATION AND HEADER EXTRACT
      IF(JFUNC .EQ. 3) GO TO 3000

! IMPORT MODE: DATA EXTRACTION AND RDB WRITING
      IF(JFUNC .EQ. 4) GO TO 4000

! WARN USER OF ILLEGAL ARGUMENT
!------------------------------
      WRITE(IUERR,11)
11    FORMAT(' YFMT50: ILLEGAL ARGUMENT')
      STOP ' IN YFMT50-0'


!******************************************
!******************************************
!   START EXPORT INITIALIZATION SECTION
!******************************************
!******************************************
1000  CONTINUE

! THIS SECTION PERFORMS INITIALIZATION AND WRITES
! HEADERS ASSOCIATED WITH DATA EXPORT FROM TOSS


! DUMMY (SEE CORRESPONDING PART OF YFMT 03 AS A TEMPLATE)


      WRITE(IUERR,1011)
1011  FORMAT(' YFMT50: EXPORT FORMAT 50 NOT IMPLEMENTED BY USER')
      STOP ' IN YFMT50-1'

!******************************************
! RETURN FROM EXPORT INITIALIZATION SECTION
!******************************************
!CC      RETURN




!**********************************************
!**********************************************
!   START EXPORT DEFINITION/WRITING SECTION
!**********************************************
!**********************************************
2000  CONTINUE

! THIS SECTION DEFINES DATA AND PERFORMS FILE WRITES
! ASSOCIATED WITH DATA EXPORT FROM TOSS

! DUMMY (SEE CORRESPONDING PART OF YFMT 03 AS A TEMPLATE)

!*****************************************************
! RETURN FROM EXPORT DATA DEFINITION AND WRITE SECTION
!*****************************************************
      RETURN




!*************************************************
!*************************************************
!  START IMPORT INITIALIZATION AND HEADER READS
!*************************************************
!*************************************************
3000  CONTINUE

! THIS SECTION READS IMPORT FILE HEADERS AND DOES
! INITIALIZATION OF TOSS COMMON AS A PART OF DATA
! IMPORT AND BUILDING OF A TOSS RDB

! DUMMY (SEE CORRESPONDING PART OF YFMT 03 AS A TEMPLATE)

      WRITE(IUERR,3011)
3011  FORMAT(' YFMT50: IMPORT FORMAT 50 NOT IMPLEMENTED BY USER')
      STOP ' IN YFMT50-2'

!******************************************
! RETURN FROM IMPORT INITIALIZATION SECTION
!******************************************
!CC      RETURN



!*************************************************
!*************************************************
!  START IMPORT DATA READS AND TOSS COMMON BUILD
!*************************************************
!*************************************************
4000  CONTINUE

! THIS SECTION READS THE IMPORT FILE AND POPULATES
! TOSS COMMON AS A PART OF DATA IMPORT AND BUILDING
! OF AN ASSOCIATED TOSS RDB

! DUMMY (SEE CORRESPONDING PART OF YFMT 03 AS A TEMPLATE)

!***********************************************
! RETURN FROM IMPORT READS AND TOSS COMMON BUILD
!***********************************************
      RETURN

      END
