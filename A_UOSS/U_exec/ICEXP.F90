! ROUTINE: ICEXP
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!***************************
!***************************
!
      SUBROUTINE ICEXP
!
!***************************
!***************************
! THIS ROUTINE PERFORMS INITIALIZATION ASSOCIATED
! WITH DATA EXPORT AND FILE CREATION


      include "../../A_HDR/COM_ALL.i"
      include "../../A_HDR/COM_UOSS.i"
      include "../../A_HDR/COM_ROSS.i"





! NOTIFY INTERACTIVE USER OF EXECUTION PROGRESS
      IF(LBATCC .EQ. 0) WRITE(IUCRT,11)
11    FORMAT(/, ' INITIALIZING DATA EXPORT OPERATION')


! RESET SNAP-SHOT-TAKEN FLAGS TO NONE-TAKEN, FOR NEXT RUN
      DO 20 J = 1,39
         NSNAP(J) = 0
20    CONTINUE



! NOTIFY INTERACTIVE USER OF EXECUTION PROGRESS
      IF(LBATCC .EQ. 0) WRITE(IUCRT,22)
22    FORMAT(' OPENING SOURCE RDB FILES')


!---------------------------------------------------------
! BEFORE INVOKING RDB NAMING, PASS RTOSS ANY NEW RDB NAMES
!---------------------------------------------------------
! RE-SET RDB FAMILY NAME IF NEW DATA IS ENTERED VIA UTOSS INPUT
      IF(NPMFLG .NE. 0) THEN

! INFORM RTOSS OF NEW NAME TO BE CONSIDERED
           CALL NEWNAM (PDBNAM,PDBNUM)

! RESET UTOSS NAME-INPUT-FLAG FOR NEXT UTOSS INPUT OCCURANCE
           NPMFLG = 0

      END IF


!---------------------------------------------
! INVOKE RTOSS DATA RETRIEVAL RDB OPEN MODULES
!---------------------------------------------
! VISIT PROGRAM TO BUILD RDB NAME OF EXPORT DATA SOURCE
      CALL PULNAM

! VISIT PROGRAM TO OPEN RESULTS DATA BASE FILES
      CALL PULOPN




! NOTIFY INTERACTIVE USER OF EXECUTION PROGRESS
      IF(LBATCC .EQ. 0) WRITE(IUCRT,33)
33    FORMAT(' STARTING EXPORT FILE CREATIONS')

!------------------------
! OPEN ASCII EXPORT FILES
!------------------------
      CALL EXPOPN


! NOTIFY INTERACTIVE USER OF EXECUTION PROGRESS
      IF(LBATCC .EQ. 0) WRITE(IUCRT,66)
66    FORMAT(' EXPORT INITIALIZATION COMPLETE',/)

      RETURN
      END
