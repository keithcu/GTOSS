! ROUTINE: MATMUL
! %Z%GTOSS %M% H.1 code v01.01 (baseline for vG.1 delivery)
!*************************************
!     
      SUBROUTINE MATMUL (IOPT,A,B, C)
!     
!*************************************
! (CALCULATION IS PROTECTED ALLOWING INPUT TO BE OVERWRITTEN)
!    IF IOPT = 0  C =  A  *  B
!            = 1  C =  AT *  B
!            = 2  C =  A  *  BT
!            = 3  C =  AT *  BT

      include "../A_HDR/COM_ALL.i"

      DIMENSION A(3,3), B(3,3), C(3,3)
      DIMENSION CS(3,3)


! CHECK VALIDITY OF ARG AND SELECT EXECUTION OPTION
!--------------------------------------------------
      GO TO (100, 101, 102, 103) IOPT+1

! FAILED VALIDITY CHECK, STOP AND NOTE...WRITE TO SHOERR
      STOP 'MATMUL: IMPROPER OPT ARG'



! IOPT = 0;   C = A * B
!----------------------
100   DO 200 IS = 1,3
      DO 200 JS = 1,3
         CS(IS,JS) = A(IS,1)*B(1,JS)+ A(IS,2)*B(2,JS)+ A(IS,3)*B(3,JS)
200   CONTINUE

      GO TO 500

! IOPT = 1;   C = AT * B
!----------------------
101   DO 201 IS = 1,3
      DO 201 JS = 1,3
         CS(IS,JS) = A(1,IS)*B(1,JS)+ A(2,IS)*B(2,JS)+ A(3,IS)*B(3,JS)
201   CONTINUE

      GO TO 500


! IOPT = 2;   C = A * BT
!----------------------
102   DO 202 IS = 1,3
      DO 202 JS = 1,3
         CS(IS,JS) = A(IS,1)*B(JS,1)+ A(IS,2)*B(JS,2)+ A(IS,3)*B(JS,3)
202   CONTINUE

      GO TO 500


! IOPT = 3;   C = AT * BT
!------------------------
103   DO 203 IS = 1,3
      DO 203 JS = 1,3
         CS(IS,JS) = A(1,IS)*B(JS,1)+ A(2,IS)*B(JS,2)+ A(3,IS)*B(JS,3)
203   CONTINUE


! TRANSFER RESULT TO THE OUTPUT MATRIX
!-------------------------------------
500   DO 210 IS = 1,3
      DO 210 JS = 1,3
         C(IS,JS) = CS(IS,JS)
210   CONTINUE

      RETURN
      END
